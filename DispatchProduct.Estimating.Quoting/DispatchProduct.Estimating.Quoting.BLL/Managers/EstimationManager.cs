﻿using DispatchProduct.Estimating.Quoting.BLL.IManagers;
using DispatchProduct.Estimating.Quoting.Context;
using DispatchProduct.Estimating.Quoting.Entities;
using DispatchProduct.Estimating.Quoting.Settings;
using DispatchProduct.Repoistry;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Linq;
using Utilites;

namespace DispatchProduct.Estimating.Quoting.BLL.Managers
{
    public class EstimationManager : Repositry<Estimation>, IEstimationManager
    {
        IEstimationItemManager estimationItemManager;
        EstimatingQuotingAppSettings estQuot;
        public EstimationManager(EstimationQuotationDbContext context, IEstimationItemManager _estimationItemManager, IOptions<EstimatingQuotingAppSettings> _estQuot)
            : base(context)
        {
            estimationItemManager = _estimationItemManager;
            estQuot = _estQuot.Value;
        }
        public override Estimation Add(Estimation entity)
        {
            entity.RefNumber = CreateRefNumber();
            entity = base.Add(entity);
            entity = AddRelatedItemstoEstimation(entity);
            return entity;
        }
        public override bool Update(Estimation entity)
        {
            bool result = false;
            result = base.Update(entity);
            DeleteRelatedItemstoEstimation(entity.Id);
            AddRelatedItemstoEstimation(entity);
            return result;
        }
        public bool UpdateHasQuotation(Estimation entity)
        {
            bool result = false;
            result = base.Update(entity);
            return result;
        }
        public List<Estimation> GetByCall(int callId)
        {
            List<Estimation> result = null;
            result = GetAll().Where(cal => cal.FK_Call_Id == callId).ToList();
            for (int i = 0; i < result.Count; i++)
            {
                result[i].lstEstimationItems = GetEstimationItemsByEstimationId(result[i].Id);
            }
            return result;
        }
        public bool Delete(int id)
        {
            DeleteRelatedItemstoEstimation(id);
            return DeleteById(id);
        }
        public Estimation Get(int id)
        {
            Estimation result = null;
            result = base.Get(id);
            result.lstEstimationItems = GetEstimationItemsByEstimationId(id);
            return result;
        }
        private Estimation AddRefrenceNumber(Estimation entity)
        {
            if (entity != null && entity.Id > 0)
            {
                entity.RefNumber = "Dasco" + entity.Id;
                base.Update(entity);
            }
            return entity;
        }


        public List<Estimation> FilterEstimation(FilterEstimation filter)
        {
            var estimations = GetAll()
                .Where(est =>
                (
                (filter.RefNumbers != null && filter.RefNumbers.Count > 0 ? filter.RefNumbers.Contains(est.RefNumber) : true)
                ||
                (filter.RefNumbers != null && filter.RefNumbers.Count > 0 ? filter.RefNumbers.Contains(est.Area) : true))
                &&
                (
                (filter.QuotationRefNumbers != null && filter.QuotationRefNumbers.Count > 0 ? filter.QuotationRefNumbers.Contains(est.QuotationRefNumber) : true)
                ||
                (filter.QuotationRefNumbers != null && filter.QuotationRefNumbers.Count > 0 ? filter.QuotationRefNumbers.Contains(est.Area) : true)

                )
                &&
                (filter.HasQuotation != null ? est.HasQuotation == filter.HasQuotation : true)
                 &&
                (filter.Area != null ? est.Area.ToLower().Contains(filter.Area) : true)
                &&
                (filter.PriceFrom != null ? est.Price >= filter.PriceFrom : true)
                &&
                (filter.PriceTo != null ? est.Price <= filter.PriceTo : true)
                &&
                (filter.FromDate != null ? est.CreatedDate >= filter.FromDate : true)
                &&
                (filter.ToDate != null ? est.CreatedDate <= filter.ToDate : true)
                //&&
                //(filter.SearchText != null ? est.Area.ToLower().Contains(filter.SearchText) : true)
                )
                .ToList();
            return estimations;
        }
        public List<Estimation> Search(string key)
        {
            return GetAll().Where(r => r.RefNumber == key || r.Area == key).ToList();
        }
        public Estimation GetByRefNumber(string refNo)
        {
            Estimation result = null;
            result = GetAll().Where(est => est.RefNumber == refNo).FirstOrDefault();
            if (result != null)
            {
                result.lstEstimationItems = GetEstimationItemsByEstimationId(result.Id);
            }
            return result;
        }
        private void DeleteRelatedItemstoEstimation(int id)
        {
            var lstEstitms = estimationItemManager.GetByEstimationId(id);
            if (lstEstitms.Count > 0)
            {
                estimationItemManager.Delete(lstEstitms);
            }
        }
        private Estimation AddRelatedItemstoEstimation(Estimation entity)
        {
            if (entity != null)
            {
                foreach (var item in entity.lstEstimationItems)
                {
                    item.FK_Estimation_Id = entity.Id;
                    estimationItemManager.Add(item);
                }
            }
            return entity;
        }
        private int? GetMaxOrderToday(string date)
        {
            int? result = null;
            var lstEst = GetAll().Where(refNo => refNo.RefNumber.StartsWith(estQuot.EstimationPrefix + date)).Select(e => e.RefNumber.Replace(estQuot.EstimationPrefix + date, "")).ToList();
            if (lstEst != null && lstEst.Count > 0)
            {
                result = lstEst.Select(str => int.Parse(str)).Max();
            }
            return result;
        }
        private string CreateRefNumber()
        {
            string result = null;
            string orderNo = estQuot.EstimationOrderDayStartNo;
            string orderdate = Helper.CreateInfixDateCode();
            int? max = GetMaxOrderToday(orderdate);
            if (max != null)
            {
                orderNo = (max.Value + 1).ToString();
            }
            result = estQuot.EstimationPrefix + orderdate + orderNo;
            return result;
        }
        private List<EstimationItem> GetEstimationItemsByEstimationId(int estimationId)
        {
            return estimationItemManager.GetAll().Where(itm => itm.FK_Estimation_Id == estimationId).ToList();
        }


    }
}
