﻿using DispatchProduct.Repoistry;
using System.Collections.Generic;
using DispatchProduct.Estimating.Quoting.Entities;
using DispatchProduct.Estimating.Quoting.BLL.IManagers;
using DispatchProduct.Estimating.Quoting.Context;
using Microsoft.Extensions.Options;
using DispatchProduct.Estimating.Quoting.Settings;
using System.Linq;
using Utilites;

namespace DispatchProduct.Estimating.Quoting.BLL.Managers
{
    public class QuotationManager : Repositry<Quotation>, IQuotationManager
    {
        EstimatingQuotingAppSettings estQuot;
        IEstimationManager estimationManager;
        public QuotationManager(EstimationQuotationDbContext context, IOptions<EstimatingQuotingAppSettings> _estQuot, IEstimationManager _estimationManager)
            : base(context)
        {
            estQuot = _estQuot.Value;
            estimationManager = _estimationManager;
        }

        public override Quotation Add(Quotation entity)
        {
            entity.RefNumber = CreateRefNumber();
            Estimation estimation = null;
            if (entity.EstimationRefNumber != null)
            {
                estimation = estimationManager.GetByRefNumber(entity.EstimationRefNumber);
                entity.Area = estimation.Area;
            }
            entity = base.Add(entity);
            if (estimation != null)
            {
                estimation = estimationManager.GetByRefNumber(entity.EstimationRefNumber);
                estimation.HasQuotation = true;
                estimation.QuotationRefNumber = entity.RefNumber;
                estimationManager.UpdateHasQuotation(estimation);
                entity.Estimation = estimation;
            }
            return entity;
        }
        //public override bool Update(Quotation entity)
        //{
        //    bool result = false;
        //    Estimation estimation = null;
        //    var quotation=Get(entity.Id);
        //     estimation = estimationManager.GetByRefNumber(quotation.EstimationRefNumber);
        //    if (estimation != null && estimation.RefNumber != entity.EstimationRefNumber)
        //    {
        //        estimation.HasQuotation = false;
        //        estimationManager.UpdateHasQuotation(estimation);
        //        var newEstimation = estimationManager.GetByRefNumber(quotation.EstimationRefNumber);
        //        newEstimation.HasQuotation = true;
        //        estimationManager.UpdateHasQuotation(newEstimation);
        //    }
        //    result=base.Update(entity);
        //    return result;
        //}
        public List<Quotation> Search(string key)
        {
            return this.GetAll().Where(r => r.RefNumber == key || r.Area == key).ToList();
        }
        public override bool Delete(Quotation entity)
        {
            bool result = false;
            if (entity.EstimationRefNumber != null)
            {
                var estimation = estimationManager.GetByRefNumber(entity.EstimationRefNumber);
                if (estimation != null)
                {
                    estimation.HasQuotation = false;
                    estimation.QuotationRefNumber = null;
                    estimationManager.UpdateHasQuotation(estimation);
                }
            }
            result = base.Delete(entity);
            return result;
        }
        private string CreateRefNumber()
        {
            string result = null;
            string orderNo = estQuot.EstimationOrderDayStartNo;
            string orderdate = Helper.CreateInfixDateCode();
            int? max = GetMaxOrderToday(orderdate);
            if (max != null)
            {
                orderNo = (max.Value + 1).ToString();
            }
            result = estQuot.EstimationPrefix + orderdate + orderNo;
            return result;
        }
        private int? GetMaxOrderToday(string date)
        {
            int? result = null;
            var lstEst = GetAll().Where(refNo => refNo.RefNumber.StartsWith(estQuot.EstimationPrefix + date)).Select(e => e.RefNumber.Replace(estQuot.EstimationPrefix + date, "")).ToList();
            if (lstEst != null && lstEst.Count > 0)
            {
                result = lstEst.Select(str => int.Parse(str)).Max();
            }
            return result;
        }
        public Quotation Get(int id)
        {
            Quotation quotation = base.Get(id);
            if (quotation != null && quotation.EstimationRefNumber != null)
                quotation.Estimation = estimationManager.GetByRefNumber(quotation.EstimationRefNumber);
            return quotation;
        }
        public Quotation GetByRefNumber(string refNo)
        {
            Quotation result = null;
            result = GetAll().Where(quot => quot.RefNumber == refNo).FirstOrDefault();
            return result;
        }
        public Quotation GetByEstmRefNumber(string refNo)
        {
            Quotation result = null;
            result = GetAll().Where(quot => quot.EstimationRefNumber == refNo).FirstOrDefault();
            return result;
        }
        public List<Quotation> FilterQuotation(FilterQuotation filter)
        {
            return GetAll().Where(est =>
            (
            (filter.RefNumbers != null && filter.RefNumbers.Count > 0 ? filter.RefNumbers.Contains(est.RefNumber) : true)
            ||
            (filter.RefNumbers != null && filter.RefNumbers.Count > 0 ? filter.RefNumbers.Contains(est.Area) : true)
            )
            &&
            (
            (filter.EstimationRefNumbers != null && filter.EstimationRefNumbers.Count > 0 ? filter.EstimationRefNumbers.Contains(est.EstimationRefNumber) : true)
            ||
            (filter.EstimationRefNumbers != null && filter.EstimationRefNumbers.Count > 0 ? filter.EstimationRefNumbers.Contains(est.Area) : true)
            )
            &&
            (filter.PriceFrom != null ? est.Price >= filter.PriceFrom : true)
            &&
            (filter.Area != null ? est.Area.ToLower().Contains(filter.Area) : true)
            &&
            (filter.PriceTo != null ? est.Price <= filter.PriceTo : true)
            &&
            (filter.FromDate != null ? est.CreatedDate >= filter.FromDate : true)
           // &&
           //(filter.SearchText != null ? est.Area.ToLower().Contains(filter.SearchText) : true)
            &&
            (filter.ToDate != null ? est.CreatedDate <= filter.ToDate : true)
            ).ToList();
        }
        public List<Quotation> GetByEstmRefNumber(List<string> estimationRefNos)
        {
            List<Quotation> result = null;
            if (estimationRefNos != null && estimationRefNos.Count > 0)
                result = GetAll().Where(quot => estimationRefNos.Contains(quot.EstimationRefNumber)).ToList();
            return result;
        }
    }
}
