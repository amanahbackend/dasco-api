﻿using DispatchProduct.Repoistry;
using System.Collections.Generic;
using DispatchProduct.Estimating.Quoting.Context;
using DispatchProduct.Estimating.Quoting.BLL.IManagers;
using DispatchProduct.Estimating.Quoting.Entities;
using System.Linq;

namespace DispatchProduct.Estimating.Quoting.BLL.Managers
{
    public class EstimationItemManager : Repositry<EstimationItem>, IEstimationItemManager
    {
        public EstimationItemManager(EstimationQuotationDbContext context )
            : base(context)
        {

        }
        public List<EstimationItem> GetByEstimationId(int estimationId)
        {
            return   GetAll().Where(est => est.FK_Estimation_Id == estimationId).ToList();
        }
    }
}
