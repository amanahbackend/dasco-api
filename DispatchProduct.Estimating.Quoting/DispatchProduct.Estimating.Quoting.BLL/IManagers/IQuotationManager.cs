﻿using DispatchProduct.Estimating.Quoting.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;

namespace DispatchProduct.Estimating.Quoting.BLL.IManagers
{
    public interface IQuotationManager : IRepositry<Quotation>
    {
        Quotation GetByRefNumber(string refNo);

        Quotation GetByEstmRefNumber(string refNo);

        Quotation Get(int id);

        List<Quotation> Search(string key);

        List<Quotation> GetByEstmRefNumber(List<string> estimationRefNos);

        List<Quotation> FilterQuotation(FilterQuotation filter);
    }
}
