﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Swashbuckle.AspNetCore.Swagger;
using DispatchProduct.Repoistry;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using DispatchProduct.Estimating.Quoting.Context;
using System.IdentityModel.Tokens.Jwt;
using IdentityServer4.AccessTokenValidation;
using DispatchProduct.Estimating.Quoting.IEntities;
using DispatchProduct.Estimating.Quoting.Entities;
using DispatchProduct.Estimating.Quoting.BLL.IManagers;
using DispatchProduct.Estimating.Quoting.BLL.Managers;
using DispatchProduct.Estimating.Quoting.API.Settings;
using DispatchProduct.Estimating.Quoting.API.ServicesCommunication.EmployeeSalary;
using DispatchProduct.Estimating.Quoting.API.ServicesCommunication.Inventory;
using DispatchProduct.Estimating.Quoting.Settings;
using DispatchProduct.Estimating.Quoting.API.ServicesCommunication.Call;
using DispatchProduct.Estimating.Quoting.API.Controllers;
using DispatchProduct.Quoting.Hubs;
using DispatchProduct.Quoting.Settings;
using Microsoft.Extensions.FileProviders;
using System.IO;

namespace DispatchProduct.Estimating.Quoting.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });
            services.AddDbContext<EstimationQuotationDbContext>(options =>
            options.UseSqlServer(Configuration["ConnectionString"],
            sqlOptions => sqlOptions.MigrationsAssembly("DispatchProduct.Estimating.Quoting.EFCore.MSSQL")));

            services.AddOptions();
            services.Configure<EstimatingQuotingAppSettings>(Configuration);
            services.Configure<InventoryServiceSettings>(Configuration.GetSection("InventoryServiceSettings"));
            services.Configure<EmployeeSalaryServiceSetting>(Configuration.GetSection("EmployeeSalaryServiceSetting"));
            services.Configure<CallServiceSetting>(Configuration.GetSection("CallServiceSetting"));
            services.Configure<QuotationHubSettings>(Configuration.GetSection("QuotationHubSettings"));
            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1",
                    new Info()
                    {
                        Title = "Calling API",
                        Description = "Calling  API"
                    });
                c.AddSecurityDefinition("oauth2", new OAuth2Scheme
                {
                    Type = "oauth2",
                    Flow = "implicit",
                    AuthorizationUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/authorize",
                    TokenUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/token",
                    Scopes = new Dictionary<string, string>()
                    {
                        { "calling", "calling API" }
                    }
                });
            });

            ConfigureAuthService(services);

            services.AddMvc();
            services.AddSignalR();

            services.AddAutoMapper(typeof(Startup));
            Mapper.AssertConfigurationIsValid();

            services.AddScoped(typeof(IRepositry<>), typeof(Repositry<>));
            services.AddScoped(typeof(IEstimation), typeof(Estimation));
            services.AddScoped(typeof(IQuotation), typeof(Quotation));
            services.AddScoped(typeof(IEstimationItem), typeof(EstimationItem));
            services.AddScoped(typeof(IEstimationManager), typeof(EstimationManager));
            services.AddScoped(typeof(IQuotationManager), typeof(QuotationManager));
            services.AddScoped(typeof(IEstimationItemManager), typeof(EstimationItemManager));
            services.AddScoped(typeof(IEmployeeSalaryService), typeof(EmployeeSalaryService));
            services.AddScoped(typeof(IInventoryService), typeof(InventoryService));
            services.AddScoped(typeof(ICallService), typeof(CallService));
            services.AddScoped(typeof(ICallEstimationService), typeof(CallEstimationService));
            services.AddScoped(typeof(EstimationController), typeof(EstimationController));
            services.AddScoped(typeof(IQuotationHub), typeof(QuotationHub));
            services.AddDirectoryBrowser();
            var container = new ContainerBuilder();
            container.Populate(services);

            return new AutofacServiceProvider(container.Build());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCors("AllowAll");

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            var pathBase = Configuration["PATH_BASE"];
            if (!string.IsNullOrEmpty(pathBase))
            {
                loggerFactory.CreateLogger("init").LogDebug($"Using PATH BASE '{pathBase}'");
                app.UsePathBase(pathBase);
            }
            ConfigureAuth(app);
            app.UseStaticFiles();
            app.UseSignalR(routes =>  // <-- SignalR
            {
                routes.MapHub<QuotationHub>("quotationHub");
            });
            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory(), "WordDocOutput")),
                RequestPath = "/WordDocOutput",
                EnableDirectoryBrowsing = true
            });
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Calling API");
            });


            // Make work identity server redirections in Edge and lastest versions of browers. WARN: Not valid in a production environment.
            //app.Use(async (context, next) =>
            //{
            //    context.Response.Headers.Add("Content-Security-Policy", "script-src 'unsafe-inline'");
            //    await next();
            //});

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private void ConfigureAuthService(IServiceCollection services)
        {
            // prevent from mapping "sub" claim to nameidentifier.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            var identityUrl = Configuration.GetValue<string>("IdentityUrl");
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
            .AddIdentityServerAuthentication(options =>
            {
                // base-address of your identityserver
                options.Authority = identityUrl;

                // name of the API resource
                options.ApiName = Configuration["ClientId"];
                options.ApiSecret = Configuration["Secret"];
                options.RequireHttpsMetadata = false;
                options.EnableCaching = true;
                options.CacheDuration = TimeSpan.FromMinutes(10);
                options.SaveToken = true;
            });

        }

        protected virtual void ConfigureAuth(IApplicationBuilder app)
        {
            app.UseAuthentication();
        }
    }
}
