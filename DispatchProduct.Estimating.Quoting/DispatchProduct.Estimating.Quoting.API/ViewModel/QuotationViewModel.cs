﻿namespace DispatchProduct.Estimating.Quoting.API.ViewModel
{
    public class QuotationViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string RefNumber { get; set; }

        public string EstimationRefNumber { get; set; }

        public EstimationViewModel Estimation { get; set; }

        public double Price { get; set; }

        public string CustomerName { get; set; }

        public string CustomerMobile { get; set; }

        public string Area { get; set; }

        public CallViewModel Call { get; set; }
    }
}
