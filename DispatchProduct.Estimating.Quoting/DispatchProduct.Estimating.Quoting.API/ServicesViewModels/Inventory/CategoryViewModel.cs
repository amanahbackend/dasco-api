﻿using DispatchProduct.Estimating.Quoting.API.ViewModel;

namespace DispatchProduct.Estimating.Quoting.API.ServicesViewModels
{
    public class CategoryViewModel : BaseEntityViewModel
    {
        public string Name { get; set; }

        public int Id { get; set; }
    }
}
