﻿using DispatchProduct.Estimating.Quoting.API.ViewModel;

namespace DispatchProduct.Estimating.Quoting.API.ServicesViewModels
{
    public class EmployeeSalaryViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string EmployeeId { get; set; }

        public string EmployeeName { get; set; }

        public double Salary { get; set; }

        public int FK_Currency_Id { get; set; }

        public CurrencyViewModel Currency { get; set; }

        public int FK_SalaryPeriod_Id { get; set; }

        public PeriodViewModel Period { get; set; }
    }
}
