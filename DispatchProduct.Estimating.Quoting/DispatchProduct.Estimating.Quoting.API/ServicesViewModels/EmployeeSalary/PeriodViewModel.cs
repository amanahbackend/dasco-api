﻿using DispatchProduct.Estimating.Quoting.API.ViewModel;

namespace DispatchProduct.Estimating.Quoting.API.ServicesViewModels
{
    public class PeriodViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
