﻿using DispatchProduct.Estimating.Quoting.API.ViewModel;

namespace DispatchProduct.Estimating.Quoting.API.ServicesViewModels
{
    public class CurrencyViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
