﻿namespace DispatchProduct.Estimating.Quoting.API.ViewModel
{
    public class CallViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string CallerNumber { get; set; }

        public string CallerName { get; set; }

        public string CustomerServiceName { get; set; }

        public string CustomerServiceUserName { get; set; }

        public bool NeedAction { get; set; }

        public int FK_Customer_Id { get; set; }

        public string CustomerDescription { get; set; }

        public string CallStatus { get; set; }

        public int FK_CustomerType_Id { get; set; }

        public int FK_PhoneType_Id { get; set; }

        public int FK_CallType_Id { get; set; }

        public string PACINumber { get; set; }

        public string Governorate { get; set; }

        public string Area { get; set; }

        public string Block { get; set; }

        public string Street { get; set; }

        public string AddressNote { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string Title { get; set; }

        public bool HasEstimation { get; set; }

        public string EstimationRefNo { get; set; }
    }
}
