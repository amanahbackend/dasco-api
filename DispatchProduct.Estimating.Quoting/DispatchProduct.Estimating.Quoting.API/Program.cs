﻿using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using DispatchProduct.Estimating.Quoting.Context;
using Microsoft.Extensions.Options;
using DispatchProduct.Estimating.Quoting.API.Seed;
using DispatchProduct.Estimating.Quoting.Settings;

namespace DispatchProduct.Estimating.Quoting.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).MigrateDbContext<EstimationQuotationDbContext>((context, services) =>
            {
                var env = services.GetService<IHostingEnvironment>();
                var logger = services.GetService<ILogger<EstimationQuotationDbContextSeed>>();
                var settings = services.GetService<IOptions<EstimatingQuotingAppSettings>>();
                new EstimationQuotationDbContextSeed()
                    .SeedAsync(context, env, logger, settings)
                    .Wait();
            }).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
          WebHost.CreateDefaultBuilder(args)
              .UseKestrel()
              .UseContentRoot(Directory.GetCurrentDirectory())
              .UseIISIntegration()
              .UseStartup<Startup>()
              .ConfigureLogging((hostingContext, builder) =>
              {
                  builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                  builder.AddConsole();
                  builder.AddDebug();
              })
              .Build();
    }
}
