﻿using AutoMapper;
using DispatchProduct.Estimating.Quoting.API.ViewModel;
using DispatchProduct.Estimating.Quoting.BLL.IManagers;
using DispatchProduct.Estimating.Quoting.Entities;
using DispatchProduct.Estimating.Quoting.Settings;
using DispatchProduct.Quoting.Hubs;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Utilites;
using Utilities.Utilites;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Estimating.Quoting.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]

    public class QuotationController : Controller
    {
        private IQuotationManager manger;
        private readonly IMapper mapper;
        private EstimationController estimationController;
        private IQuotationHub hub;
        private IHubContext<QuotationHub> quotationHub;
        private EstimatingQuotingAppSettings settings;
        private IHostingEnvironment env;

        public QuotationController(IQuotationManager _manger, IMapper _mapper, EstimationController _estimationController, IQuotationHub _hub, IHubContext<QuotationHub> _quotationHub, IOptions<EstimatingQuotingAppSettings> _settings, IHostingEnvironment _env)
        {
            manger = _manger;
            mapper = _mapper;
            estimationController = _estimationController;
            settings = _settings.Value;
            quotationHub = _quotationHub;
            hub = _hub;
            env = _env;
        }
        // GET: api/values
        #region DefaultCrudOperation

        #region GetApi
        [HttpGet("{id}")]
        [Route("Get/{id}")]

        public async Task<QuotationViewModel> Get([FromRoute]int id, string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            Quotation entityResult = manger.Get(id);
            QuotationViewModel result = mapper.Map<Quotation, QuotationViewModel>(entityResult);
            if (result.EstimationRefNumber != null)
            {
                var estimation = await estimationController.GetByRef(result.EstimationRefNumber, authHeader);
                if (estimation != null && estimation.Call != null)
                {
                    result.Estimation = estimation;
                    mapper.Map(estimation.Call, result);
                }
            }
            return result;
        }
        private async Task<EstimationViewModel> GetEstimationByRef(string estimationRefNumber, string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            var estimation = await estimationController.GetByRef(estimationRefNumber, authHeader);
            return estimation;
        }

        [Route("GetAll")]
        [HttpGet]
        public async Task<IActionResult> Get(string authHeader = null)
        {
            var entityResult = manger.GetAll().ToList();
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            var result = mapper.Map<List<Quotation>, List<QuotationViewModel>>(entityResult);
            List<QuotationViewModel> quotation = await BindEstimationToQuotation(result);
            return Ok(result);
        }
        [HttpGet("{id}")]
        [Route("GetByRef/{refNo}")]
        public async Task<IActionResult> GetByRef([FromRoute]string refNo, string authHeader = null)
        {
            Quotation entityResult = manger.GetByRefNumber(refNo);
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            QuotationViewModel result = mapper.Map<Quotation, QuotationViewModel>(entityResult);
            if (result.EstimationRefNumber != null)
            {
                var estimation = await estimationController.GetByRef(result.EstimationRefNumber, authHeader);
                result.Estimation = estimation;
                if (estimation.Call != null)
                {
                    mapper.Map(estimation.Call, result);
                }
            }
            return Ok(result);
        }
        #endregion

        #region Search
        [HttpPost]
        [Route("Filter")]
        public async Task<IActionResult> Filter([FromBody] FilterQuotationViewModel model, string auth = null)
        {
            List<QuotationViewModel> quotationViewModelList = null;
            FilterQuotation filter = mapper.Map<FilterQuotationViewModel, FilterQuotation>(model);
            if (filter != null)
            {
                List<Quotation> source = manger.FilterQuotation(filter);
                List<QuotationViewModel> result = mapper.Map<List<Quotation>, List<QuotationViewModel>>(source);
                quotationViewModelList = await BindEstimationToQuotation(result);
            }
            return Ok(quotationViewModelList);
        }
        [HttpGet("key")]
        [Route("Search/{key}")]
        public async Task<List<QuotationViewModel>> Search([FromRoute]string key, string authHeader = null)
        {
            List<Quotation> entityResult = null;
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            List<QuotationViewModel> result = new List<QuotationViewModel>();
            entityResult = manger.Search(key);
            if (entityResult == null || entityResult.Count == 0)
            {
                var estLst = await estimationController.SearchInCall(key, authHeader);
                if (estLst != null)
                {
                    List<string> estListNo = estLst.Select(r => r.RefNumber).ToList();
                    entityResult = manger.GetByEstmRefNumber(estListNo);
                }
            }
            result = mapper.Map<List<Quotation>, List<QuotationViewModel>>(entityResult);
            return await BindEstimationToQuotation(result);
        }
        #endregion
        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post([FromBody]QuotationViewModel model)
        {
            // await fillEntityIdentity(entityResult);
            var entityResult = mapper.Map<QuotationViewModel, Quotation>(model);
            entityResult = manger.Add(entityResult);
            model = mapper.Map<Quotation, QuotationViewModel>(entityResult);
            if (model != null)
                hub.AddQuotation(model, quotationHub);
            return Ok(model);
        }
        #endregion
        [Route("GenerateWord/{quotationId}")]
        [HttpGet]
        public async Task<ActionResult> GenerateWord([FromRoute] int quotationId)
        {
            QuotationViewModel quot = await Get(quotationId);
            if (quot == null)
                return BadRequest("This Quotation Not Exist");
            if (quot.Call == null || quot.Estimation == null)
                return BadRequest("This Quotation Not Have Estimation Items");
            string bindedDoc = GetBindedDoc(quot);
            return Ok(bindedDoc);
        }
        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Put([FromBody]QuotationViewModel model)
        {
            bool result = false;
            Quotation entityResult = new Quotation();
            entityResult = mapper.Map<QuotationViewModel, Quotation>(model);
            // await fillEntityIdentity(entityResult);
            result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [HttpDelete]
        [Route("Delete/{id}")]

        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            Quotation entity = manger.Get(id);
            // await fillEntityIdentity(entity);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        public async Task<List<QuotationViewModel>> BindEstimationToQuotation(List<QuotationViewModel> result, string authHeader = null)
        {
            if (Request != null && authHeader == null)
                authHeader = Helper.GetValueFromRequestHeader(Request, "Authorization");
            for (int i = 0; i < result.Count; ++i)
            {
                if (result[i].EstimationRefNumber != null)
                {
                    EstimationViewModel byRef = await estimationController.GetByRef(result[i].EstimationRefNumber, authHeader);
                    if (byRef.Call != null)
                        result[i] = mapper.Map(byRef.Call, result[i]);
                }
            }
            return result;
        }

        public Table AddTable(List<EstimationItemViewModel> data)
        {
            Table table = new Table();
            OpenXmlElement[] openXmlElementArray1 = new OpenXmlElement[1];
            int index1 = 0;
            OpenXmlElement[] openXmlElementArray2 = new OpenXmlElement[6];
            int index2 = 0;
            TopBorder topBorder = new TopBorder();
            topBorder.Val = new EnumValue<BorderValues>(BorderValues.Single);
            topBorder.Size = 12U;
            openXmlElementArray2[index2] = topBorder;
            int index3 = 1;
            BottomBorder bottomBorder = new BottomBorder();
            bottomBorder.Val = new EnumValue<BorderValues>(BorderValues.Single);
            bottomBorder.Size = 12U;
            openXmlElementArray2[index3] = bottomBorder;
            int index4 = 2;
            LeftBorder leftBorder = new LeftBorder();
            leftBorder.Val = new EnumValue<BorderValues>(BorderValues.Single);
            leftBorder.Size = 12U;
            openXmlElementArray2[index4] = leftBorder;
            int index5 = 3;
            RightBorder rightBorder = new RightBorder();
            rightBorder.Val = new EnumValue<BorderValues>(BorderValues.Single);
            rightBorder.Size = 12U;
            openXmlElementArray2[index5] = rightBorder;
            int index6 = 4;
            InsideHorizontalBorder horizontalBorder = new InsideHorizontalBorder();
            horizontalBorder.Val = new EnumValue<BorderValues>(BorderValues.Single);
            horizontalBorder.Size = 12U;
            openXmlElementArray2[index6] = horizontalBorder;
            int index7 = 5;
            InsideVerticalBorder insideVerticalBorder = new InsideVerticalBorder();
            insideVerticalBorder.Val = new EnumValue<BorderValues>(BorderValues.Single);
            insideVerticalBorder.Size = 12U;
            openXmlElementArray2[index7] = insideVerticalBorder;
            TableBorders tableBorders = new TableBorders(openXmlElementArray2);
            openXmlElementArray1[index1] = tableBorders;
            TableProperties newChild = new TableProperties(openXmlElementArray1);
            table.AppendChild<TableProperties>(newChild);
            TableRow row1 = CreateRow(new List<TableCell>()
      {
        CreateCell("Item Number"),
        CreateCell("Name"),
        CreateCell("Quantity")
      });
            table.Append(new OpenXmlElement[1]
            {
                row1
            });
            foreach (EstimationItemViewModel estimationItemViewModel in data)
            {
                TableRow row2 = CreateRow(new List<TableCell>()
        {
          CreateCell(estimationItemViewModel.No.ToString()),
          CreateCell(estimationItemViewModel.Name.ToString()),
          CreateCell(estimationItemViewModel.Quantity.ToString())
        });
                table.Append(new OpenXmlElement[1]
                {
           row2
                });
            }
            return table;
        }

        public TableCell CreateCell(string text)
        {
            TableCell tableCell1 = new TableCell();
            tableCell1.Append(new OpenXmlElement[1]
            {
         new Paragraph(new OpenXmlElement[1]
        {
           new Run(new OpenXmlElement[1]
          {
             new Text(text)
          })
        })
            });
            TableCell tableCell2 = tableCell1;
            OpenXmlElement[] openXmlElementArray1 = new OpenXmlElement[1];
            int index1 = 0;
            OpenXmlElement[] openXmlElementArray2 = new OpenXmlElement[1];
            int index2 = 0;
            TableCellWidth tableCellWidth = new TableCellWidth();
            tableCellWidth.Type = (EnumValue<TableWidthUnitValues>)TableWidthUnitValues.Auto;
            openXmlElementArray2[index2] = tableCellWidth;
            TableCellProperties tableCellProperties = new TableCellProperties(openXmlElementArray2);
            openXmlElementArray1[index1] = tableCellProperties;
            tableCell2.Append(openXmlElementArray1);
            return tableCell1;
        }

        public TableRow CreateRow(List<TableCell> cells)
        {
            TableRow tableRow = new TableRow();
            foreach (TableCell cell in cells)
                tableRow.Append(new OpenXmlElement[1]
                {
          cell.CloneNode(true)
                });
            return tableRow;
        }

        public string GetSrcQuotationDocPath()
        {
            string str = (string)null;
            if (string.IsNullOrWhiteSpace(env.WebRootPath))
                env.WebRootPath = Directory.GetCurrentDirectory();
            string webRootPath = env.WebRootPath;
            if (!string.IsNullOrEmpty(settings.QuotationDocSrcPath))
                str = Path.Combine(webRootPath, settings.QuotationDocSrcPath);
            return str;
        }

        public string GetDstQuotationDocPath()
        {
            string str = null;
            if (string.IsNullOrWhiteSpace(env.WebRootPath))
                env.WebRootPath = Directory.GetCurrentDirectory();
            string webRootPath = env.WebRootPath;
            if (!string.IsNullOrEmpty(settings.QuotationDocDstPath))
                str = Path.Combine(webRootPath, settings.QuotationDocDstPath);
            return str;
        }

        public string GetBindedDoc(QuotationViewModel quot)
        {
            string str1 = null;
            Dictionary<string, string> dataFromQuotation = ExtractWordDataFromQuotation(quot);
            if (dataFromQuotation != null && dataFromQuotation.Count > 0)
            {
                string quotationDocPath1 = GetSrcQuotationDocPath();
                string quotationDocPath2 = GetDstQuotationDocPath();
                if (!string.IsNullOrEmpty(quotationDocPath1) && !string.IsNullOrEmpty(settings.QuotationDocSrcPath))
                {
                    string str2 = quot.Call.CallerName + settings.QuotationDocDstSuffix;
                    DocHelper.CopyFile(quotationDocPath1, settings.QuotationDocSrcName, quotationDocPath2, str2);
                    DocHelper.SearchAndReplace(Path.Combine(quotationDocPath2, str2), dataFromQuotation);
                    var path = $"{settings.QuotationDocDstPath}/{str2}";
                    str1 = GetWordDoc(path);
                }
            }
            return str1;
        }

        private Dictionary<string, string> ExtractWordDataFromQuotation(QuotationViewModel quot)
        {
            Dictionary<string, string> dictionary = (Dictionary<string, string>)null;
            if (quot != null && quot.Call != null)
            {
                dictionary = new Dictionary<string, string>();
                dictionary.Add("QuotationNoValue", quot.Call.CallerName != null ? quot.RefNumber : "");
                dictionary.Add("CustomerNameValue", quot.Call.CallerName != null ? quot.Call.CallerName : "");
                dictionary.Add("PACIValue", quot.Call.PACINumber != null ? quot.Call.PACINumber : "");
                dictionary.Add("GovernmentValue", quot.Call.Governorate != null ? quot.Call.Governorate : "");
                dictionary.Add("CityValue", quot.Call.Area != null ? quot.Call.Area : "");
                dictionary.Add("BlockValue", quot.Call.Block != null ? quot.Call.Block : "");
                dictionary.Add("StreetValue", quot.Call.Street != null ? quot.Call.Street : "");
                dictionary.Add("CallDescriptionValue", quot.Call.CustomerDescription != null ? quot.Call.CustomerDescription : "");
                dictionary.Add("PriceValue", quot.Price.ToString());
            }
            return dictionary;
        }

        public void AddEstimationItemsToWordDoc(QuotationViewModel quotation, string dstFilePath)
        {
            if (quotation == null || quotation.Estimation == null || (quotation.Estimation.lstEstimationItems == null || quotation.Estimation.lstEstimationItems.Count <= 0))
                return;
            using (WordprocessingDocument wordprocessingDocument = WordprocessingDocument.Open(dstFilePath, true))
            {
                wordprocessingDocument.MainDocumentPart.Document.Body.Append(new OpenXmlElement[1]
                {
           AddTable(quotation.Estimation.lstEstimationItems)
                });
                wordprocessingDocument.MainDocumentPart.Document.Save();
            }
        }

        private string GetWordDoc(string file)
        {
            string result = null;
            string str = $"{Request.Scheme}://{Request.Host}/{Request.PathBase}";
            file = file.Replace('\\', '/');
            result=  $"{str}/{file}";
            return result;
        }
        #endregion
    }
}
