﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.Estimating.Quoting.API.ViewModel;
using DispatchProduct.Estimating.Quoting.Entities;
using DispatchProduct.Estimating.Quoting.BLL.IManagers;
using DispatchProduct.Estimating.Quoting.API.ServicesCommunication.Call;
using Utilites;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Estimating.Quoting.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]

    public class EstimationController : Controller
    {
        public IEstimationManager manger;
        public readonly IMapper mapper;
        ICallService callService;
        ICallEstimationService callEstimationService;
        public EstimationController(IEstimationManager _manger, IMapper _mapper, ICallService _callService, ICallEstimationService _callEstimationService)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            callService = _callService;
            callEstimationService = _callEstimationService;
        }
        // GET: api/values
        #region DefaultCrudOperation

        #region GetApi
        [HttpGet("{id}")]
        [Route("Get/{id}")]
        public async Task<IActionResult> Get(int id, string authHeader = null)
        {
            EstimationViewModel result = new EstimationViewModel();
            Estimation entityResult = new Estimation();
            entityResult = manger.Get(id);
            if (entityResult != null)
            {

                string FK_Call_Id = entityResult.FK_Call_Id.ToString();
                result = mapper.Map<Estimation, EstimationViewModel>(entityResult);
                await AddCallInfo(result, authHeader);
            }

            return Ok(result);
        }

        [HttpGet("{refNo}")]
        [Route("GetByRef/{refNo}")]
        public async Task<EstimationViewModel> GetByRef(string refNo, string authHeader = null)
        {
            EstimationViewModel result = new EstimationViewModel();
            Estimation entityResult = new Estimation();
            entityResult = manger.GetByRefNumber(refNo);
            if (entityResult != null)
            {
                string FK_Call_Id = entityResult.FK_Call_Id.ToString();
                result = mapper.Map<Estimation, EstimationViewModel>(entityResult);
                await AddCallInfo(result, authHeader);
            }
            return result;
        }
        [HttpGet]
        public async Task<IActionResult> Get(string authHeader = null)
        {
            List<EstimationViewModel> result = new List<EstimationViewModel>();
            List<Estimation> entityResult = new List<Estimation>();
            entityResult = manger.GetAll().ToList();

            if (entityResult != null)
            {
                result = mapper.Map<List<Estimation>, List<EstimationViewModel>>(entityResult);
                foreach (var item in result)
                {
                    await AddCallInfo(item, authHeader);
                }
            }
            return Ok(result);
        }
        [HttpGet("{key}")]
        [Route("Search/{key}")]
        public async Task<List<EstimationViewModel>> Search(string key, string auth = null)
        {
            List<EstimationViewModel> result = new List<EstimationViewModel>();
            var entityResult = manger.Search(key);
            if (entityResult == null || entityResult.Count == 0)
                result = await SearchInCall(key);
            else
                result = mapper.Map<List<Estimation>, List<EstimationViewModel>>(entityResult);
            foreach (EstimationViewModel estimation in result)
            {
                EstimationViewModel estimationViewModel = await AddCallInfo(estimation, auth);
            }
            result = result.DistinctBy(r => r.Id).ToList();
            return result;
        }
        [HttpGet("{key}")]
        [Route("SearchInCall/{key}")]
        public async Task<List<EstimationViewModel>> SearchInCall(string key, string auth = null)
        {
            List<EstimationViewModel> result = new List<EstimationViewModel>();
            List<CallViewModel> callViewModelList = await SearchCall(key, auth);
            if (callViewModelList != null)
            {
                foreach (CallViewModel itm in callViewModelList)
                {
                    List<Estimation> byCall = manger.GetByCall(itm.Id);
                    if (byCall != null && byCall.Count > 0)
                    {
                        List<EstimationViewModel> estimationViewModelList = mapper.Map<List<Estimation>, List<EstimationViewModel>>(byCall);
                        for (int index = 0; index < estimationViewModelList.Count; ++index)
                        {
                            estimationViewModelList[index] = mapper.Map<CallViewModel, EstimationViewModel>(itm, estimationViewModelList[index]);
                            estimationViewModelList[index].Call = itm;
                            result.Add(estimationViewModelList[index]);
                        }
                    }
                }
            }
            result = result.DistinctBy(r => r.Id).ToList();
            return result;
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post([FromBody]EstimationViewModel model)
        {
            if (model != null)
            {
                var call = await GetCall(model.FK_Call_Id);
                model.Area = call.Area;
                //model = mapper.Map<CallViewModel, EstimationViewModel>(call);
                var entityResult = mapper.Map<EstimationViewModel, Estimation>(model);
                // await fillEntityIdentity(entityResult);
                entityResult = manger.Add(entityResult);
                model = mapper.Map<Estimation, EstimationViewModel>(entityResult);
                await UpdateCallEstimation(model);
            }

            return Ok(model);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Put([FromBody]EstimationViewModel model)
        {
            bool result = false;
            Estimation entityResult = new Estimation();
            entityResult = mapper.Map<EstimationViewModel, Estimation>(model);
            // await fillEntityIdentity(entityResult);
            if (entityResult != null)
            {
                result = manger.Update(entityResult);
            }
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [HttpDelete]
        [Route("Delete/{id}")]

        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            result = manger.Delete(id);
            return Ok(result);
        }
        #endregion
        #endregion
        [HttpPost]
        [Route("Filter")]
        public async Task<IActionResult> Filter([FromBody] FilterEstimationViewModel model, string auth = null)
        {
            List<EstimationViewModel> result = null;
            FilterEstimation filter = mapper.Map<FilterEstimationViewModel, FilterEstimation>(model);
            if (filter != null)
            {
                List<Estimation> source = manger.FilterEstimation(filter);
                result = mapper.Map<List<Estimation>, List<EstimationViewModel>>(source);
                foreach (EstimationViewModel estimation in result)
                {
                    EstimationViewModel estimationViewModel = await AddCallInfo(estimation, auth);
                }
            }
            return Ok(result);
        }
        #region Services
        private async Task<List<CallViewModel>> SearchCall(string key, string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            List<CallViewModel> callresult = null;
            if (authHeader != null)
            {
                callresult = await callService.SearchCall(key, authHeader);
            }
            return callresult;
        }
        private async Task<EstimationViewModel> AddCallInfo(EstimationViewModel estimation, string authHeader = null)
        {

            if (estimation.FK_Call_Id > 0)
            {
                if (authHeader == null && Request != null)
                {
                    authHeader = Helper.GetValueFromRequestHeader(Request);
                }
                CallViewModel callresult = null;
                if (authHeader != null)
                {
                    callresult = await callService.GetItem(estimation.FK_Call_Id.ToString(), authHeader);
                }
                if (callresult != null)
                {
                    estimation = mapper.Map(callresult, estimation);

                }
            }
            return estimation;
        }
        private async Task<CallViewModel> GetCall(int fK_Call_Id, string authHeader = null)
        {
            CallViewModel callresult = null;
            if (authHeader == null && Request != null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            if (authHeader != null)
            {
                callresult = await callService.GetItem(fK_Call_Id.ToString(), authHeader);
            }
            return callresult;
        }
        private async Task<bool> UpdateCallEstimation(EstimationViewModel model, string authHeader = null)
        {
            var result = false;
            if (authHeader == null && Request != null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            var callEstmodel = mapper.Map<EstimationViewModel, CallEstimationViewModel>(model);
            result = await callEstimationService.UpdateCallEstimation(callEstmodel, authHeader);
            return result;

        }
        #endregion
    }
}
