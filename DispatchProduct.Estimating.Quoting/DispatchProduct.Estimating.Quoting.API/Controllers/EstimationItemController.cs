﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.Estimating.Quoting.BLL.IManagers;
using DispatchProduct.Estimating.Quoting.API.ViewModel;
using DispatchProduct.Estimating.Quoting.Entities;
using DispatchProduct.Estimating.Quoting.API.ServicesCommunication.Inventory;
using DispatchProduct.Estimating.Quoting.API.ServicesCommunication.EmployeeSalary;
using DispatchProduct.Estimating.Quoting.API.ServicesViewModels;
using Utilites;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Estimating.Quoting.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]

    public class EstimationItemController : Controller
    {
        public IEstimationItemManager manger;
        public readonly IMapper mapper;
        IInventoryService inventoryService;
        IEmployeeSalaryService employeeService;
        public EstimationItemController(IEstimationItemManager _manger, IMapper _mapper, IInventoryService _inventoryService, IEmployeeSalaryService _employeeService)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            inventoryService = _inventoryService;
            employeeService = _employeeService;
        }
        // GET: api/values
        #region DefaultCrudOperation

        #region GetApi
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
           EstimationItemViewModel result = new EstimationItemViewModel();
            EstimationItem entityResult = new EstimationItem();
            entityResult = manger.Get(id);
            result = mapper.Map<EstimationItem, EstimationItemViewModel>(entityResult);
            return Ok(result);
        }
        [HttpGet]
        public IActionResult Get()
        {
            List<EstimationItemViewModel> result = new List<EstimationItemViewModel>();
            List<EstimationItem> entityResult = new List<EstimationItem>();
            entityResult = manger.GetAll().ToList();
            result = mapper.Map<List<EstimationItem>, List<EstimationItemViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post([FromBody]EstimationItemViewModel model)
        {
            EstimationItemViewModel result = new EstimationItemViewModel();
            EstimationItem entityResult = new EstimationItem();
            entityResult = mapper.Map<EstimationItemViewModel, EstimationItem>(model);
           // await fillEntityIdentity(entityResult);
            entityResult = manger.Add(entityResult);
            result = mapper.Map<EstimationItem, EstimationItemViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Put([FromBody]EstimationItemViewModel model)
        {
            bool result = false;
            EstimationItem entityResult = new EstimationItem();
            entityResult = mapper.Map<EstimationItemViewModel, EstimationItem>(model);
           // await fillEntityIdentity(entityResult);
            result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [HttpDelete]
        [Route("Delete/{id}")]

        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            EstimationItem entity = manger.Get(id);
           // await fillEntityIdentity(entity);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion

        #region Buisness
        [Route("SearchItems/{key}")]
        [HttpGet("{key}")]
        public async Task<EstimationItemViewModel> SearchItems([FromRoute]string key)
        {
            EstimationItemViewModel result = null;

            string authHeader = Helper.GetValueFromRequestHeader(Request);
            if (authHeader != null)
            {
                var item =await SearchInInventory(key, authHeader);
                if (item != null)
                {
                    result = mapper.Map<ItemViewModel, EstimationItemViewModel>(item);
                }
                else
                {
                    var employee = await SearchInEmployees(key,authHeader);
                    if (employee != null)
                    {
                        result = mapper.Map<EmployeeSalaryViewModel, EstimationItemViewModel>(employee);
                    }
                }
            }
            return result;
        }
        private Task<ItemViewModel> SearchInInventory(string code,string authHeader)
        {
           return inventoryService.GetItemByCode(code, authHeader);
        }
        private Task<EmployeeSalaryViewModel> SearchInEmployees(string employeeId, string authHeader)
        {
            return employeeService.GetEmployeeByEmployeeId(employeeId, authHeader);
        }
        #endregion
    }
}
