﻿using DispatchProduct.Estimating.Quoting.API.ServicesViewModels;
using DispatchProduct.Estimating.Quoting.API.Settings;
using DispatchProduct.HttpClient;
using System.Threading.Tasks;

namespace DispatchProduct.Estimating.Quoting.API.ServicesCommunication.EmployeeSalary
{
    public interface IEmployeeSalaryService : IDefaultHttpClientCrud<EmployeeSalaryServiceSetting, EmployeeSalaryViewModel, EmployeeSalaryViewModel>
    {
        Task<EmployeeSalaryViewModel> GetEmployeeByEmployeeId(string empId,string authHeader);
    }
}
