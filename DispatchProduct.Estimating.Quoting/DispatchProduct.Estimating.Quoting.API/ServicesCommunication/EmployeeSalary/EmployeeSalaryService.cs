﻿using DispatchProduct.Estimating.Quoting.API.ServicesViewModels;
using DispatchProduct.Estimating.Quoting.API.Settings;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;

namespace DispatchProduct.Estimating.Quoting.API.ServicesCommunication.EmployeeSalary
{
    public class EmployeeSalaryService : DefaultHttpClientCrud<EmployeeSalaryServiceSetting, EmployeeSalaryViewModel, EmployeeSalaryViewModel>, IEmployeeSalaryService
    {
        EmployeeSalaryServiceSetting settings;
        public EmployeeSalaryService(IOptions<EmployeeSalaryServiceSetting> _settings) :base(_settings.Value)
        {
            settings = _settings.Value;
        }
        public async Task<EmployeeSalaryViewModel> GetEmployeeByEmployeeId(string empId,string authHeader)
        {
            var requesturi = $"{settings.Uri}/{settings.GetByEmpIdVerb}/{empId}";
            return await GetByUri(requesturi,authHeader);
        }
    }
}
