﻿using DispatchProduct.Estimating.Quoting.API.Settings;
using DispatchProduct.Estimating.Quoting.API.ViewModel;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchProduct.Estimating.Quoting.API.ServicesCommunication.Call
{
    public class CallService : DefaultHttpClientCrud<CallServiceSetting, CallViewModel, CallViewModel>, ICallService
    {
        CallServiceSetting settings;
        public CallService(IOptions<CallServiceSetting> _settings) :base(_settings.Value)
        {
            settings = _settings.Value;
        }
        public async Task<List<CallViewModel>> SearchCall(string key, string authHeader="")
        {
            var requesturi = $"{settings.Uri}/{settings.SearchCallVerb}/{key}";
            return await GetListByUri(requesturi, authHeader);
        }

    }
}
