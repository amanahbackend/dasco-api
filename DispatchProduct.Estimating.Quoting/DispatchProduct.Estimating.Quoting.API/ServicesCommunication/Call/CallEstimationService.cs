﻿using DispatchProduct.Api.HttpClient;
using DispatchProduct.Estimating.Quoting.API.Settings;
using DispatchProduct.Estimating.Quoting.API.ViewModel;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;

namespace DispatchProduct.Estimating.Quoting.API.ServicesCommunication.Call
{
    public class CallEstimationService : DefaultHttpClientCrud<CallServiceSetting, CallEstimationViewModel, CallEstimationViewModel>, ICallEstimationService
    {
        CallServiceSetting settings;
        public CallEstimationService(IOptions<CallServiceSetting> _settings) :base(_settings.Value)
        {
            settings = _settings.Value;
        }

        public async Task<bool> UpdateCallEstimation(CallEstimationViewModel model, string auth = "")
        {
            bool result = false;
            var requesturi = $"{settings.Uri}/{settings.UpdateCallEstimationVerb}";
            var response = await HttpRequestFactory.Post(requesturi, model, auth);
            if (response.IsSuccessStatusCode)
            {
                result = true;
            }
            return result;
        }
    }
}
