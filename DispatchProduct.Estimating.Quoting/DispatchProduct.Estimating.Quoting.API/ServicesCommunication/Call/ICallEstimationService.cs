﻿using DispatchProduct.Estimating.Quoting.API.Settings;
using DispatchProduct.Estimating.Quoting.API.ViewModel;
using DispatchProduct.HttpClient;
using System.Threading.Tasks;

namespace DispatchProduct.Estimating.Quoting.API.ServicesCommunication.Call
{
    public interface ICallEstimationService : IDefaultHttpClientCrud<CallServiceSetting, CallEstimationViewModel, CallEstimationViewModel>
    {
        Task<bool> UpdateCallEstimation(CallEstimationViewModel model, string auth = "");
    }
}
