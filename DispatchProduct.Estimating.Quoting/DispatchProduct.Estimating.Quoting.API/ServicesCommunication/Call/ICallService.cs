﻿using DispatchProduct.Estimating.Quoting.API.Settings;
using DispatchProduct.Estimating.Quoting.API.ViewModel;
using DispatchProduct.HttpClient;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchProduct.Estimating.Quoting.API.ServicesCommunication.Call
{
    public interface ICallService : IDefaultHttpClientCrud<CallServiceSetting, CallViewModel, CallViewModel>
    {
        Task<List<CallViewModel>> SearchCall(string key, string authHeader = "");

    }
}
