﻿using DispatchProduct.Estimating.Quoting.API.ServicesViewModels;
using DispatchProduct.Estimating.Quoting.API.Settings;
using DispatchProduct.HttpClient;
using System.Threading.Tasks;

namespace DispatchProduct.Estimating.Quoting.API.ServicesCommunication.Inventory
{
    public interface IInventoryService : IDefaultHttpClientCrud<InventoryServiceSettings, ItemViewModel, ItemViewModel>
    {
        Task<ItemViewModel> GetItemByCode(string code, string bearer);
    }
}
