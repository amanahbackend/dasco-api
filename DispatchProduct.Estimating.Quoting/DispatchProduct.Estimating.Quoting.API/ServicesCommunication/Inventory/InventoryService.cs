﻿using DispatchProduct.Estimating.Quoting.API.ServicesViewModels;
using DispatchProduct.Estimating.Quoting.API.Settings;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;

namespace DispatchProduct.Estimating.Quoting.API.ServicesCommunication.Inventory
{
    public class InventoryService : DefaultHttpClientCrud<InventoryServiceSettings, ItemViewModel, ItemViewModel>, IInventoryService
    {
       InventoryServiceSettings settings;
        public InventoryService(IOptions<InventoryServiceSettings> _settings) :base(_settings.Value)
        {
            settings = _settings.Value;
        }
        public async Task<ItemViewModel> GetItemByCode(string code,string bearer)
        {
            var requesturi = $"{settings.Uri}/{settings.GetByCodeVerb}/{code}";
            return await GetByUri(requesturi, bearer);
        }
    }
}
