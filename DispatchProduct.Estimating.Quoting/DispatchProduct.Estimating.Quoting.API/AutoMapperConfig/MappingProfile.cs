﻿using AutoMapper;
using DispatchProduct.Estimating.Quoting.API.ServicesViewModels;
using DispatchProduct.Estimating.Quoting.API.ViewModel;
using DispatchProduct.Estimating.Quoting.Entities;

namespace DispatchProduct.Estimating.Quoting.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<EstimationItem, EstimationItemViewModel>();
            CreateMap<EstimationItemViewModel, EstimationItem>();

            CreateMap<ItemViewModel, EstimationItemViewModel>()
            .ForMember(dest => dest.No, opt => opt.MapFrom(src => src.Code))
            .ForMember(dest => dest.UnitPrice, opt => opt.MapFrom(src => src.Price))
            .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => src.Quantity))
            .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
            .ForAllOtherMembers(dest => dest.Ignore());


            CreateMap<EstimationItemViewModel,ItemViewModel>()
            .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.No))
            .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.UnitPrice))
            .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => src.Quantity))
            .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
            .ForAllOtherMembers(dest => dest.Ignore());

            CreateMap<FilterQuotationViewModel, FilterQuotation>();
            CreateMap<FilterQuotation, FilterQuotationViewModel>();
            CreateMap<FilterEstimationViewModel, FilterEstimation>();
            CreateMap<FilterEstimation, FilterEstimationViewModel>();

            CreateMap<EmployeeSalaryViewModel, EstimationItemViewModel>()
            .ForMember(dest => dest.No, opt => opt.MapFrom(src => src.EmployeeId))
            .ForMember(dest => dest.UnitPrice, opt => opt.MapFrom(src => src.Salary))
            .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.EmployeeName))
            .ForAllOtherMembers(dest => dest.Ignore());

            CreateMap<EstimationItemViewModel,EmployeeSalaryViewModel>()
            .ForMember(dest => dest.EmployeeId, opt => opt.MapFrom(src => src.No))
            .ForMember(dest => dest.Salary, opt => opt.MapFrom(src => src.UnitPrice))
            .ForMember(dest => dest.EmployeeName, opt => opt.MapFrom(src => src.Name))
            .ForAllOtherMembers(dest => dest.Ignore());


            CreateMap<Estimation, EstimationViewModel>()
            .ForMember(dest => dest.lstEstimationItems, opt => opt.MapFrom(src => src.lstEstimationItems))
            .ForMember(dest => dest.CustomerMobile, opt => opt.Ignore())
            .ForMember(dest => dest.Call, opt => opt.Ignore())
            .ForMember(dest => dest.CustomerName, opt => opt.Ignore());

            CreateMap<EstimationViewModel, Estimation>()
            .ForMember(dest => dest.lstEstimationItems, opt => opt.MapFrom(src => src.lstEstimationItems));

            CreateMap<CallViewModel, EstimationViewModel>()
            .ForMember(dest => dest.Call, opt => opt.MapFrom(src => src))
            .ForMember(dest => dest.Area, opt => opt.MapFrom(src => src.Area))
            .ForMember(dest => dest.CustomerName, opt => opt.MapFrom(src => src.CallerName))
            .ForMember(dest => dest.CustomerMobile, opt => opt.MapFrom(src => src.CallerNumber))
            .ForAllOtherMembers(dest => dest.Ignore());

            CreateMap<EstimationViewModel, CallEstimationViewModel>()
            .ForMember(dest => dest.CallId, opt => opt.MapFrom(src => src.FK_Call_Id))
            .ForMember(dest => dest.EstimationRefNo, opt => opt.MapFrom(src => src.RefNumber))
            .ForAllOtherMembers(dest => dest.Ignore());

            CreateMap<CallEstimationViewModel,EstimationViewModel>()
            .ForMember(dest => dest.FK_Call_Id, opt => opt.MapFrom(src => src.CallId))
            .ForMember(dest => dest.RefNumber, opt => opt.MapFrom(src => src.EstimationRefNo))
            .ForAllOtherMembers(dest => dest.Ignore());

            //CreateMap<EstimationViewModel, CallViewModel>()
            //.ForMember(dest => dest, opt => opt.MapFrom(src => src.Call));
            CreateMap<CallViewModel, QuotationViewModel>()
           .ForMember(dest => dest.Call, opt => opt.MapFrom(src => src))
           .ForMember(dest => dest.Area, opt => opt.MapFrom(src => src.Area))
           .ForMember(dest => dest.CustomerName, opt => opt.MapFrom(src => src.CallerName))
           .ForMember(dest => dest.CustomerMobile, opt => opt.MapFrom(src => src.CallerNumber))
           .ForAllOtherMembers(dest => dest.Ignore());

            //CreateMap<QuotationViewModel, CallViewModel>()
            //.ForMember(dest => dest, opt => opt.MapFrom(src => src.Call));

            CreateMap<Quotation, QuotationViewModel>()
            .ForMember(dest => dest.Estimation, opt => opt.MapFrom(src => src.Estimation))
            .ForMember(dest => dest.CustomerMobile, opt => opt.Ignore())
            .ForMember(dest => dest.Call, opt => opt.Ignore())
            .ForMember(dest => dest.CustomerName, opt => opt.Ignore());
            CreateMap<QuotationViewModel, Quotation>()
            .ForMember(dest => dest.Estimation, opt => opt.MapFrom(src => src.Estimation));
        }
    }
}
