﻿using DispatchProduct.Estimating.Quoting.Context;
using DispatchProduct.Estimating.Quoting.Entities;
using DispatchProduct.Estimating.Quoting.Settings;
using DispatchProduct.Repoistry;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchProduct.Estimating.Quoting.API.Seed
{
    public class EstimationQuotationDbContextSeed : ContextSeed
    {
        public async Task SeedAsync(EstimationQuotationDbContext context, IHostingEnvironment env,
            ILogger<EstimationQuotationDbContextSeed> logger, IOptions<EstimatingQuotingAppSettings> settings, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;

            try
            {
                var useCustomizationData = settings.Value.UseCustomizationData;
                var contentRootPath = env.ContentRootPath;
                var webroot = env.WebRootPath;
                List<EstimationItem> lstEstimationItem = new List<EstimationItem>();
                List<Estimation> lstEstimation = new List<Estimation>();
                List<Quotation> lstQuotation = new List<Quotation>();

                if (useCustomizationData)
                {
                    //from file e.g (look at ApplicationDbContextSeed)
                }
                else
                {
                    //default from here
                    lstEstimationItem = GetDefaultEstimationItems();
                    lstEstimation = GetDefaultEstimation();
                    lstQuotation = GetDefaultQuotation();
                }
                await SeedEntityAsync(context, lstEstimationItem);
            
                await SeedEntityAsync(context, lstEstimation);
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 3)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for ApplicationDbContext");

                    await SeedAsync(context, env, logger, settings, retryForAvaiability);
                }
            }
        }
        private List<EstimationItem> GetDefaultEstimationItems()
        {
            List<EstimationItem> result = new List<EstimationItem>
            {
                //new EstimationItem() { Name = "test 1" },
                //new EstimationItem() { Name = "test 2" }
            };
            return result;
        }

        private List<Estimation> GetDefaultEstimation()
        {
            List<Estimation> result = new List<Estimation>
            {
              
            };
            return result;
        }

        private List<Quotation> GetDefaultQuotation()
        {
            List<Quotation> result = new List<Quotation>
            {

            };
            return result;
        }
    }
}
