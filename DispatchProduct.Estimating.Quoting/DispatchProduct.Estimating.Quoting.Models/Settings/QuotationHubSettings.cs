﻿using System.Collections.Generic;

namespace DispatchProduct.Quoting.Settings
{
    public class QuotationHubSettings
    {
        public string token { get; set; }

        public string userId { get; set; }

        public string roles { get; set; }

        public string AddQuotation { get; set; }

        public string AddQuotationGroupName { get; set; }

        public List<string> AddQuotationRoles { get; set; }
    }
}
