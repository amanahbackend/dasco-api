﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using DispatchProduct.Estimating.Quoting.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DispatchProduct.Estimating.Quoting.EntityConfigurations
{
    public class EstimationItemEntityTypeConfiguration : BaseEntityTypeConfiguration<EstimationItem>
    {
        public void Configure(EntityTypeBuilder<EstimationItem> EstimationItemConfiguration)
        {
            base.Configure(EstimationItemConfiguration);

            EstimationItemConfiguration.ToTable("EstimationItem");

            EstimationItemConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("EstimationItemseq");

            EstimationItemConfiguration.HasKey(o => o.Id);
            EstimationItemConfiguration.Property(o => o.UnitPrice).IsRequired();
            EstimationItemConfiguration.Property(o => o.Quantity).IsRequired();
            EstimationItemConfiguration.Property(o => o.Name).IsRequired();
            EstimationItemConfiguration.Property(o => o.Margin).IsRequired();
            EstimationItemConfiguration.Property(o => o.TotalPrice).IsRequired();
            EstimationItemConfiguration.Property(o => o.No).IsRequired();
            EstimationItemConfiguration.Property(o => o.TotalMarginPrice).IsRequired();
        }
    }
}