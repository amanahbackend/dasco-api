﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using DispatchProduct.Estimating.Quoting.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DispatchProduct.Estimating.Quoting.EntityConfigurations
{
    public class EstimationEntityTypeConfiguration : BaseEntityTypeConfiguration<Estimation>, IEntityTypeConfiguration<Estimation> 
    {
        public void Configure(EntityTypeBuilder<Estimation> EstimationConfiguration)
        {
            EstimationConfiguration.ToTable("Estimation");

            EstimationConfiguration.HasKey(o => o.Id);

            EstimationConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("Estimationseq");
            EstimationConfiguration.Property(o => o.Price).IsRequired();
            EstimationConfiguration.Property(o => o.FK_Call_Id).IsRequired();
            EstimationConfiguration.Property(o => o.RefNumber).IsRequired();
            EstimationConfiguration.Ignore(o => o.lstEstimationItems);
               
        }
    }
}
