﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using DispatchProduct.Estimating.Quoting.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DispatchProduct.Estimating.Quoting.EntityConfigurations
{
    public class QuotationEntityTypeConfiguration : BaseEntityTypeConfiguration<Quotation>, IEntityTypeConfiguration<Quotation> 
    {
        public void Configure(EntityTypeBuilder<Quotation> quotationConfiguration)
        {
            quotationConfiguration.ToTable("Quotation");

            quotationConfiguration.HasKey(o => o.Id);

            quotationConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("Quotationseq");
            quotationConfiguration.Property(o => o.Price).IsRequired();
            quotationConfiguration.Property(o => o.RefNumber).IsRequired();
            quotationConfiguration.Property(o => o.EstimationRefNumber).IsRequired();
        }
    }
}
