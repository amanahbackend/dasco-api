﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.Estimating.Quoting.IEntities;
using System.Collections.Generic;

namespace DispatchProduct.Estimating.Quoting.Entities
{
    public class Estimation : BaseEntity, IEstimation
    {
        public int Id { get; set; }

        public string RefNumber { get; set; }

        public string QuotationRefNumber { get; set; }

        public int FK_Call_Id { get; set; }

        public bool HasQuotation { get; set; }

        public List<EstimationItem> lstEstimationItems { get; set; }

        public double Price { get; set; }

        public string Area { get; set; }

    }
}
