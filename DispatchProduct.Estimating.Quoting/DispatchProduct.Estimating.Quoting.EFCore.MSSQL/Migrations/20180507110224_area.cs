﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DispatchProduct.Estimating.Quoting.EFCore.MSSQL.Migrations
{
    public partial class area : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Area",
                table: "Quotation",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Area",
                table: "Estimation",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Area",
                table: "Quotation");

            migrationBuilder.DropColumn(
                name: "Area",
                table: "Estimation");
        }
    }
}
