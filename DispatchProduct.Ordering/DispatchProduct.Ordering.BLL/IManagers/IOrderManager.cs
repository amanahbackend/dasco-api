﻿

using DispatchProduct.Ordering.BLL.Filters;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public interface IOrderManager : IRepositry<Order>
    {
        List<Order> Search(string orderCode);

        List<Order> SearchByCustomerId(int customerId);

        List<Order> GetAllOrderFilledProps();

        OrderProgress AddOrderProgress(OrderProgress orderProgress);

        List<Order> GetOrdersAssignedToTechnicians(List<string> technicianIds);

        List<Order> GetOrdersAssignedToTechnician(string technicianId);

        List<Order> GetDispatcherOrders(string dispatcherId);

        List<Order> GetOrdersAssignedToDispatchers(List<string> dispatcherIds);

        List<Order> GetFilteredOrderByDispatcherId(FilterOrderByDispatcher filter);

        List<Order> GetOrdersAssignedToDispatcher(string dispatcherId);

        Order Get(int id);

        bool AssignOrderToTechnician(int orderId, string technicianId);

        bool AssignOrderToDispatcher(int orderId, string dispatcherId);

        bool TransferOrderToDispatcher(int orderId, string dispatcherId);

        bool SetPreferedVisitTime(int orderId, DateTime PreferedVisitTime);

        bool UnAssignOrderToTechnician(int orderId);

        List<Order> GetAllDispatcherOrders(string dispatcherId);

        bool AssignOrderToDispatcher_Technician(int orderId, string dispatcherId, string technicianId);

        bool AssignOrdersToDispatcher(List<int> orderIds, string dispatcherId);

        bool AssignOrdersToTechnician(List<int> orderIds, string technicianId);

        bool AssignOrdersToDispatcher_Technician(List<int> orderIds, string dispatcherId, string technicianId);
    }
}
