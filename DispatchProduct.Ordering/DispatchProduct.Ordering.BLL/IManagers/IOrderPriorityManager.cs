﻿using DispatchProduct.Ordering.Entities;
using DispatchProduct.Repoistry;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public interface IOrderPriorityManager : IRepositry<OrderPriority>
    {
    }
}
