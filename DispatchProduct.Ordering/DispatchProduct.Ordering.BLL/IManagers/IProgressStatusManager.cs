﻿using DispatchProduct.Ordering.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public interface IProgressStatusManager : IRepositry<ProgressStatus>
    {
        ProgressStatus GetAssignedValue();

        ProgressStatus GetTransferValue();

        List<ProgressStatus> GetAll();

        ProgressStatus GetInitialValue();
    }
}
