﻿using DispatchProduct.Ordering.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public interface IOrderFilesManager : IRepositry<OrderFiles>
    {
        List<OrderFiles> GetByOrderId(int orderId);
    }
}
