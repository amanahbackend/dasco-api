﻿using DispatchProduct.Ordering.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public interface IAssignedTechniciansManager : IRepositry<AssignedTechnicians>
    {
        List<Technician> GetByDispatcherId(string fk_Dispatcher_Id);

        bool UpdateAssignedTechnician(AssignedTechnicians assignedTechnician);

        bool DeleteAssignedTechnician(string technicianId);

        AssignedTechnicians GetByTechnicianId(string technicianId);
    }
}
