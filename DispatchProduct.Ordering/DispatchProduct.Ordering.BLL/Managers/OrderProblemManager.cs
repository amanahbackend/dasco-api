﻿using DispatchProduct.Inventory.Context;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Repoistry;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public class OrderProblemManager : Repositry<OrderProblem>, IOrderProblemManager
    {
        public OrderProblemManager(OrderDbContext context)
            : base(context)
        {

        }
    }
}
