﻿using DispatchProduct.Inventory.Context;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Repoistry;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public class OrderPriorityManager: Repositry<OrderPriority>, IOrderPriorityManager
    {
        public OrderPriorityManager(OrderDbContext context)
            : base(context)
        {

        }
    }
}
