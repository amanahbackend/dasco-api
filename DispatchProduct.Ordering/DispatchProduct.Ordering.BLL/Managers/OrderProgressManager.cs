﻿using DispatchProduct.Inventory.Context;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Ordering.IEntities;
using DispatchProduct.Repoistry;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public class OrderProgressManager : Repositry<OrderProgress>, IOrderProgressManager, IRepositry<OrderProgress>
    {
        private IServiceProvider serviceProvider;
        private IProgressStatusManager orderProgressStatusManager;

        public OrderProgressManager(OrderDbContext context, IProgressStatusManager _orderProgressStatusManager, IServiceProvider _serviceProvider)
          : base(context)
        {
            orderProgressStatusManager = _orderProgressStatusManager;
            serviceProvider = _serviceProvider;
        }

        public List<OrderProgress> GetOrderProgressByOrderId(int orderId)
        {
            List<OrderProgress> list = GetAll().Where(ord => ord.FK_Order_Id == orderId).ToList();
            foreach (OrderProgress orderProgress in list)
                orderProgress.ProgressStatus = orderProgressStatusManager.Get(orderProgress.FK_ProgressStatus_Id);
            return list;
        }

        public OrderProgress AddAssignedProgress(IOrder order)
        {
            OrderProgress entity = (OrderProgress)serviceProvider.GetService<IOrderProgress>();
            if (entity != null)
            {
                entity.FK_Technician_Id = order.FK_Technician_Id;
                entity.FK_Order_Id = order.Id;
                ProgressStatus assignedValue = orderProgressStatusManager.GetAssignedValue();
                if (assignedValue != null)
                    entity.FK_ProgressStatus_Id = assignedValue.Id;
                entity.FK_CreatedBy_Id = order.FK_Dispatcher_Id;
                entity = Add(entity);
            }
            return entity;
        }

        public OrderProgress AddTransferProgress(IOrder order)
        {
            OrderProgress entity = (OrderProgress)serviceProvider.GetService<IOrderProgress>();
            if (entity != null)
            {
                entity.FK_Technician_Id = order.FK_Technician_Id;
                entity.FK_Order_Id = order.Id;
                ProgressStatus transferValue = orderProgressStatusManager.GetTransferValue();
                if (transferValue != null)
                    entity.FK_ProgressStatus_Id = transferValue.Id;
                entity.FK_CreatedBy_Id = order.FK_Dispatcher_Id;
                entity = Add(entity);
            }
            return entity;
        }

        public OrderProgress AddOpenProgress(IOrder order)
        {
            OrderProgress entity = (OrderProgress)serviceProvider.GetService<IOrderProgress>();
            if (entity != null)
            {
                entity.FK_Technician_Id = order.FK_Technician_Id;
                entity.FK_Order_Id = order.Id;
                ProgressStatus initialValue = orderProgressStatusManager.GetInitialValue();
                if (initialValue != null)
                    entity.FK_ProgressStatus_Id = initialValue.Id;
                entity.FK_CreatedBy_Id = order.FK_Dispatcher_Id;
                entity = Add(entity);
            }
            return entity;
        }

        public override OrderProgress Add(OrderProgress entity)
        {
            return base.Add(entity);
        }
    }
}
