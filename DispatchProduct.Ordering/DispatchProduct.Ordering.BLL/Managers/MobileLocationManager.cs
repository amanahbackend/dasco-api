﻿using DispatchProduct.Inventory.Context;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Repoistry;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public class MobileLocationManager : Repositry<MobileLocation>, IMobileLocationManager
    {
        public MobileLocationManager(OrderDbContext context)
            : base(context)
        {

        }
    }
}
