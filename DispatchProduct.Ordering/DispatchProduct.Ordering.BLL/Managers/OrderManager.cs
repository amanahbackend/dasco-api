﻿using DispatchProduct.Inventory.Context;
using DispatchProduct.Ordering.BLL.Filters;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Ordering.Settings;
using DispatchProduct.Repoistry;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilites;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public class OrderManager : Repositry<Order>, IOrderManager, IRepositry<Order>
    {
        private OrderAppSettings ordersettings;
        private IOrderTypeManager orderTypeMnger;
        private IOrderStatusManager orderStatusManager;
        private IOrderPriorityManager orderPriorityManager;
        private IOrderProblemManager orderProblemManager;
        private IOrderProgressManager orderProgressManager;
        private IOrderFilesManager orderFilesManager;
        private IProgressStatusManager progressStatusManager;
        private OrderDbContext _context;

        public OrderManager(OrderDbContext context, IOptions<OrderAppSettings> _ordersettings, IOrderTypeManager _orderTypeMnger, IOrderStatusManager _orderStatusManager, IOrderPriorityManager _orderPriorityManager, IOrderProgressManager _orderProgressManager, IProgressStatusManager _progressStatusManager, IOrderProblemManager _orderProblemManager, IOrderFilesManager _orderFilesManager)
          : base((DbContext)context)
        {
            ordersettings = _ordersettings.Value;
            orderStatusManager = _orderStatusManager;
            orderTypeMnger = _orderTypeMnger;
            orderPriorityManager = _orderPriorityManager;
            orderProgressManager = _orderProgressManager;
            progressStatusManager = _progressStatusManager;
            orderFilesManager = _orderFilesManager;
            orderProblemManager = _orderProblemManager;
            _context = context;
        }

        public override Order Add(Order order)
        {
            order.Code = CreateRefNumber(order);
            OrderStatus intialOrderStatus = orderStatusManager.GetIntialOrderStatus();
            if (intialOrderStatus != null)
                order.FK_OrderStatus_Id = intialOrderStatus.Id;
            order = base.Add(order);
            order = fillOrderNavProps(order);
            return order;
        }

        public override IQueryable<Order> GetAll()
        {
            List<int> statusIds = orderStatusManager.GetAll().
                Where(
                st => st.Name == ordersettings.OrderCancelStatus
                ||
                st.Name == ordersettings.OrderCompleteStatus
                ).Select(st => st.Id).ToList();
            return base.GetAll().Where(ord => !statusIds.Contains(ord.FK_OrderStatus_Id));
        }

        public Order Get(int id)
        {
            Order order = base.Get(id);
            if (order != null)
                order = FillOrderProgress(order);
            return order;
        }

        public List<Order> GetAllOrderFilledProps()
        {
            List<int> statusIds = orderStatusManager.GetAll()
                .Where(st => st.Name == ordersettings.OrderCancelStatus || st.Name == ordersettings.OrderCompleteStatus)
                .Select(st => st.Id).ToList();
            List<Order> list = base.GetAll().Where(ord => !statusIds.Contains(ord.FK_OrderStatus_Id)).ToList();
            FillOrdersNavProps(list);
            return list;
        }

        public List<Order> GetFilteredOrderByDispatcherId(FilterOrderByDispatcher filter)
        {
            List<Order> orders = base.GetAll().Where(ord => (filter.FK_Dispatcher_Ids != null && filter.FK_Dispatcher_Ids.Count > 0 ? filter.FK_Dispatcher_Ids.Contains(ord.FK_Dispatcher_Id) : true)
            &&
            (filter.FK_OrderProblem_Ids != null && filter.FK_OrderProblem_Ids.Count > 0 ? filter.FK_OrderProblem_Ids.Contains(ord.FK_OrderProblem_Id) : true)
            &&
            (filter.FK_OrderStatus_Ids != null && filter.FK_OrderStatus_Ids.Count > 0 ? filter.FK_OrderStatus_Ids.Contains(ord.FK_OrderStatus_Id) : true)
            &&
            (filter.FK_Technician_Ids != null && filter.FK_Technician_Ids.Count > 0 ? filter.FK_Technician_Ids.Contains(ord.FK_Technician_Id) : true)
            &&
            (filter.Areas != null && filter.Areas.Count > 0 ? filter.Areas.Contains(ord.Area) : true)
            &&
            (filter.CreatedDateFrom != new DateTime?() ? (DateTime?)ord.CreatedDate >= filter.CreatedDateFrom : true)
            &&
            (filter.CreatedDateTo != new DateTime?() ? (DateTime?)ord.CreatedDate <= filter.CreatedDateTo : true)).ToList();
            FillOrdersNavProps(orders);
            if (filter.HasOrderProgress)
                orders = FillOrderProgress(orders);
            return orders;
        }

        public List<Order> GetAllOrdersNavigated()
        {
            return FillOrdersNavProps(base.GetAll().ToList());
        }

        public List<Order> Search(string key)
        {
            return FillOrdersNavProps(GetAll().Where(ord => ord.Code.Contains(key) ||ord.Area== key).ToList());
        }

        public OrderProgress AddOrderProgress(OrderProgress orderProgress)
        {
            Order entity = Get(orderProgress.FK_Order_Id);
            OrderProgress result = orderProgressManager.Add(orderProgress);
            ProgressStatus progressStatus = progressStatusManager.Get(result.FK_ProgressStatus_Id);
            entity.FK_OrderStatus_Id = progressStatus.FK_OrderStatus_Id;
            Update(entity);
            return result;
        }

        public List<Order> GetOrdersAssignedToTechnicians(List<string> technicianIds)
        {
            return FillOrdersNavProps(GetAll().Where(ord => technicianIds.Contains(ord.FK_Technician_Id)).ToList());
        }

        public List<Order> GetOrdersAssignedToTechnician(string technicianId)
        {
            return FillOrdersNavProps(GetAll().Where(ord => ord.FK_Technician_Id == technicianId).ToList());
        }

        public List<Order> GetDispatcherOrders(string dispatcherId)
        {
            return FillOrdersNavProps(
                GetAll().
                Where(ord =>
                (
                ord.FK_Dispatcher_Id == dispatcherId || ord.FK_Dispatcher_Id == null)
                &&
                ord.FK_Technician_Id == null
                )
                .ToList());
        }

        public List<Order> GetAllDispatcherOrders(string dispatcherId)
        {
            return FillOrdersNavProps(GetAll().Where(ord => ord.FK_Dispatcher_Id == dispatcherId).ToList());
        }

        public bool AssignOrderToTechnician(int orderId, string technicianId)
        {
            bool result = false;
            Order entity = Get(ord => ord.Id == orderId);
            if (entity != null)
            {
                entity.FK_Technician_Id = technicianId;
                OrderProgress orderProgress = orderProgressManager.AddAssignedProgress(entity);
                if (orderProgress != null)
                {
                    ProgressStatus progressStatus = progressStatusManager.Get(orderProgress.FK_ProgressStatus_Id);
                    if (progressStatus != null)
                        entity.FK_OrderStatus_Id = progressStatus.FK_OrderStatus_Id;
                }
                result = Update(entity);
            }
            return result;
        }

        public bool UnAssignOrderToTechnician(int orderId)
        {
            bool result = false;
            Order entity = Get(ord => ord.Id == orderId);
            if (entity != null)
            {
                entity.FK_Technician_Id = null;
                OrderProgress orderProgress = orderProgressManager.AddOpenProgress(entity);
                if (orderProgress != null)
                {
                    ProgressStatus progressStatus = progressStatusManager.Get(orderProgress.FK_ProgressStatus_Id);
                    if (progressStatus != null)
                        entity.FK_OrderStatus_Id = progressStatus.FK_OrderStatus_Id;
                }
                result = Update(entity);
            }
            return result;
        }

        public bool AssignOrderToDispatcher(int orderId, string dispatcherId)
        {
            bool result = false;
            Order entity = Get(ord => ord.Id == orderId);
            if (entity != null)
            {
                entity.FK_Dispatcher_Id = dispatcherId;
                result = Update(entity);
            }
            return result;
        }

        public bool SetPreferedVisitTime(int orderId, DateTime PreferedVisitTime)
        {
            bool result = false;
            Order entity = Get(ord => ord.Id == orderId);
            if (entity != null)
            {
                entity.PreferedVisitTime = PreferedVisitTime;
                result = Update(entity);
            }
            return result;
        }

        public bool TransferOrderToDispatcher(int orderId, string dispatcherId)
        {
            bool result = false;
            Order entity = Get(ord => ord.Id == orderId);
            if (entity != null)
            {
                entity.FK_Dispatcher_Id = dispatcherId;
                entity.FK_Technician_Id = null;
                result = Update(entity);
            }
            return result;
        }

        public bool AssignOrderToDispatcher_Technician(int orderId, string dispatcherId, string technicianId)
        {
            bool result = false;
            Order entity = Get(ord => ord.Id == orderId);
            if (entity != null)
            {
                entity.FK_Dispatcher_Id = dispatcherId;
                entity.FK_Technician_Id = technicianId;
                result = Update(entity);
            }
            return result;
        }

        public bool AssignOrdersToDispatcher(List<int> orderIds, string dispatcherId)
        {
            bool result = false;
            List<Order> list = GetAll().Where(ord => orderIds.Contains(ord.Id)).ToList();
            if (list != null)
            {
                foreach (Order entity in list)
                {
                    if (entity != null)
                    {
                        entity.FK_Dispatcher_Id = dispatcherId;
                        result = Update(entity);
                    }
                }
            }
            return result;
        }

        public bool AssignOrdersToTechnician(List<int> orderIds, string technicianId)
        {
            bool result = false;
            List<Order> list = GetAll().Where(ord => orderIds.Contains(ord.Id)).ToList();
            if (list != null)
            {
                foreach (Order entity in list)
                {
                    if (entity != null)
                    {
                        entity.FK_Technician_Id = technicianId;
                        OrderProgress orderProgress = orderProgressManager.AddAssignedProgress(entity);
                        if (orderProgress != null)
                        {
                            ProgressStatus progressStatus = progressStatusManager.Get(orderProgress.FK_ProgressStatus_Id);
                            if (progressStatus != null)
                                entity.FK_OrderStatus_Id = progressStatus.FK_OrderStatus_Id;
                        }
                        result = Update(entity);
                    }
                }
            }
            return result;
        }

        public bool AssignOrdersToDispatcher_Technician(List<int> orderIds, string dispatcherId, string technicianId)
        {
            bool result = false;
            IQueryable<Order> queryable = GetAll().Where(ord => orderIds.Contains(ord.Id));
            if (queryable != null)
            {
                foreach (Order entity in queryable)
                {
                    if (entity != null)
                    {
                        entity.FK_Dispatcher_Id = dispatcherId;
                        entity.FK_Technician_Id = technicianId;
                        result = Update(entity);
                    }
                }
            }
            return result;
        }

        public List<Order> GetOrdersAssignedToDispatchers(List<string> dispatcherIds)
        {
            return FillOrdersNavProps(
                GetAll().Where(ord => dispatcherIds.Contains(ord.FK_Dispatcher_Id)).ToList()
                );
        }

        public List<Order> GetOrdersAssignedToDispatcher(string dispatcherId)
        {
            return FillOrdersNavProps(
                GetAll().Where(ord => ord.FK_Dispatcher_Id == dispatcherId).ToList()
                );
        }

        public List<Order> SearchByCustomerId(int customerId)
        {
            return FillOrdersNavProps(GetAll().Where(ord => ord.FK_Customer_Id == customerId).ToList());
        }

        private int? GetMaxOrderToday(string date)
        {
            int? nullable = new int?();
            List<string> list = GetAll().Where(o => o.Code.StartsWith(ordersettings.OrderPrefix + date))
                .Select(e => e.Code.Replace(ordersettings.OrderPrefix + date, "")).ToList();
            if (list != null && list.Count > 0)
                nullable = new int?(list.Select(str => int.Parse(str)).Max());
            return nullable;
        }

        private string CreateRefNumber()
        {
            string orderDayStartNo = ordersettings.OrderDayStartNo;
            string infixDateCode = Helper.CreateInfixDateCode();
            int? maxOrderToday = GetMaxOrderToday(infixDateCode);
            if (maxOrderToday.HasValue)
                orderDayStartNo = (maxOrderToday.Value + 1).ToString();
            return ordersettings.OrderPrefix + infixDateCode + orderDayStartNo;
        }

        private string CreateRefNumber(Order order)
        {
            string randomnum;
            string ordercode;
            bool isexist = false;
            do
            {
                randomnum = GenerateRandom();
                order.OrderType = orderTypeMnger.Get(order.FK_OrderType_Id);
                ordercode = order.OrderType.Code + randomnum;
                isexist = _context.Order.Count(o => o.Code == ordercode) > 0;
            } while (isexist);
            return ordercode;
        }

        private string GenerateRandom()
        {
            Random random = new Random();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 5; i++)
            {
                sb.Append(random.Next(0, 9).ToString());
            }
            return sb.ToString();
        }

        private Order fillOrderNavProps(Order order)
        {
            order.OrderType = orderTypeMnger.Get(new object[1]
            {
         order.FK_OrderType_Id
            });
            order.OrderStatus = orderStatusManager.Get(new object[1]
            {
         order.FK_OrderStatus_Id
            });
            order.OrderPriority = orderPriorityManager.Get(new object[1]
            {
         order.FK_OrderPriority_Id
            });
            order.OrderProblem = orderProblemManager.Get(new object[1]
            {
         order.FK_OrderProblem_Id
            });
            order.OrderFiles = orderFilesManager.GetByOrderId(order.Id);
            return order;

        }

        private List<Order> FillOrdersNavProps(List<Order> orders)
        {
            foreach (Order order in orders)
                fillOrderNavProps(order);
            return orders;
        }

        private List<Order> FillOrderProgress(List<Order> orders)
        {
            foreach (Order order in orders)
                order.LstOrderProgress = orderProgressManager.GetOrderProgressByOrderId(order.Id);
            return orders;
        }

        private Order FillOrderProgress(Order order)
        {
            order.LstOrderProgress = orderProgressManager.GetOrderProgressByOrderId(order.Id);
            return order;
        }
    }
}
