﻿using DispatchProduct.Inventory.Context;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Ordering.Settings;
using DispatchProduct.Repoistry;
using Microsoft.Extensions.Options;
using System.Linq;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public class OrderStatusManager : Repositry<OrderStatus>, IOrderStatusManager, IRepositry<OrderStatus>
    {
        private OrderAppSettings settings;

        public OrderStatusManager(OrderDbContext context, IOptions<OrderAppSettings> _settings)
          : base(context)
        {
            settings = _settings.Value;
        }

        public OrderStatus GetIntialOrderStatus()
        {
            OrderStatus orderStatus = null;
            if (!string.IsNullOrEmpty(settings.IntialProgressStatusKey))
                orderStatus = GetAll().Where(st => st.Name == settings.IntialProgressStatusKey)
                    .FirstOrDefault();
            return orderStatus;
        }
    }
}
