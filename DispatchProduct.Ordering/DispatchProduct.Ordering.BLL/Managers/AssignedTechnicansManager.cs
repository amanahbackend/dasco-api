﻿using DispatchProduct.Inventory.Context;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;
using System.Linq;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public class AssignedTechniciansManager : Repositry<AssignedTechnicians>, IAssignedTechniciansManager
    {
        IOrderManager ordermanager;
        public AssignedTechniciansManager(OrderDbContext context, IOrderManager _Ordermanager)
            : base(context)
        {
            ordermanager = _Ordermanager;
        }
        public List<Technician> GetByDispatcherId(string fk_Dispatcher_Id)
        {
            List<Technician> result = new List<Technician>();
            var technicians = GetAll().Where(t => t.FK_Dispatcher_Id == fk_Dispatcher_Id).ToList();
            var technicianIds = technicians.Select(tk => tk.FK_Technician_Id).ToList();
            foreach (var technicianId in technicianIds)
            {
                List<Order> orders = ordermanager.GetOrdersAssignedToTechnician(technicianId);
                result.Add(new Technician()
                {
                    Orders = orders,
                    Id = technicianId
                });
            }
            return result;
        }

        public override AssignedTechnicians Add(AssignedTechnicians entity)
        {
           var technician= GetByTechnicianId(entity.FK_Technician_Id);
            if (technician != null)
            {
                technician.FK_Dispatcher_Id = entity.FK_Dispatcher_Id;
                Update(technician);
            }
            else
            {
                technician = base.Add(entity);
            }
            return technician;
        }
        public bool UpdateAssignedTechnician(AssignedTechnicians assignedTechnician)
        {
          var technician=  GetByTechnicianId(assignedTechnician.FK_Technician_Id);
            technician.FK_Dispatcher_Id = assignedTechnician.FK_Dispatcher_Id;
           return Update(technician);
        }
        public bool DeleteAssignedTechnician(string technicianId)
        {
           return Delete(GetAll().Where(t => t.FK_Technician_Id == technicianId).ToList());
        }
        public AssignedTechnicians GetByTechnicianId(string technicianId)
        {
            return GetAll().Where(t => t.FK_Technician_Id == technicianId).FirstOrDefault();
        }
    }

}
