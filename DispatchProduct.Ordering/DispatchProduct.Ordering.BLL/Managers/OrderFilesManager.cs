﻿using DispatchProduct.Inventory.Context;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;
using System.Linq;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public class OrderFilesManager: Repositry<OrderFiles>, IOrderFilesManager
    {
        public OrderFilesManager(OrderDbContext context)
            : base(context)
        {

        }
        public List<OrderFiles> GetByOrderId(int orderId)
        {
            return GetAll().Where(r => r.OrderId == orderId).ToList();
        }
    }
}
