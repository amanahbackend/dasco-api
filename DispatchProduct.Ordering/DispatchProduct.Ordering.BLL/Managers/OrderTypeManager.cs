﻿using DispatchProduct.Inventory.Context;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Repoistry;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public class OrderTypeManager: Repositry<OrderType>, IOrderTypeManager
    {
        public OrderTypeManager(OrderDbContext context)
            : base(context)
        {

        }
    }
}
