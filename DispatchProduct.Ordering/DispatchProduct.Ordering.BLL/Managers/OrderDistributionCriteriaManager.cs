﻿using DispatchProduct.Inventory.Context;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Repoistry;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DispatchProduct.Ordering.BLL.Managers
{
    public class OrderDistributionCriteriaManager : Repositry<OrderDistributionCriteria>, IOrderDistributionCriteriaManager
    {
        IOrderProblemManager orderManager;
        public OrderDistributionCriteriaManager(OrderDbContext context, IOrderProblemManager _orderManager)
            : base(context)
        {
            orderManager = _orderManager;
        }
        public List<OrderDistributionCriteria> AssignCriteriaToDispatcher(List<OrderDistributionCriteria> list)
        {
            foreach (var item in list)
            {
                Add(item);
                item.OrderProblem = orderManager.Get(item.FK_OrderProblem_Id);
            }
            return list;
        }
        public List<OrderDistributionCriteria> GetAllDistribution()
        {
            var lstOrderDistribution = base.GetAll().ToList();
            foreach (var item in lstOrderDistribution)
            {
                item.OrderProblem = orderManager.Get(item.FK_OrderProblem_Id);
            }
            return lstOrderDistribution;
        }
        public string GetDispatcherIdForOrder(int fk_Problem_Id, string area)
        {
            string result = null;
            var dispatchersCount = GetAll().Where(ordr => ordr.FK_OrderProblem_Id == fk_Problem_Id && ordr.Area == area).Count();
            if (dispatchersCount > 0)
            {
                Random rand = new Random();
                int toSkip = rand.Next(0, dispatchersCount-1);
                result = GetAll().Where(ordr => ordr.FK_OrderProblem_Id == fk_Problem_Id && ordr.Area == area).Skip(toSkip).Take(1).FirstOrDefault().FK_Dispatcher_Id;
            }
            return result;
        }
        public List<OrderDistributionCriteria> GetByDispatcherId(string dispatcherId)
        {
            var lstOrderDistribution = GetAll().Where(ordr => ordr.FK_Dispatcher_Id == dispatcherId).ToList();
            foreach (var item in lstOrderDistribution)
            {
                item.OrderProblem = orderManager.Get(item.FK_OrderProblem_Id);
            }
            return lstOrderDistribution;
        }
        public bool DeleteByDispatcherId(string dispatcherId)
        {
            bool result = false;
            var items = GetAll().Where(crit => crit.FK_Dispatcher_Id == dispatcherId).ToList();
            if (items != null && items.Count > 0)
            {
                result = base.Delete(items);
            }
            else
            {
                result = true;
            }
            return result;
        }
    }
}
