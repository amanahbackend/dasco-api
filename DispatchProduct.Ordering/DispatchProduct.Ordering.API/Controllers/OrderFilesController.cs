﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.Ordering.BLL.Managers;
using DispatchProduct.Ordering.Entities;

namespace DispatchProduct.CustomerModule.API.Controllers
{
    [Authorize]
    [Route("api/OrderFiles")]
    public class OrderFilesController : Controller
    {
        public IOrderFilesManager manger;
        public readonly IMapper mapper;
        public OrderFilesController(IOrderFilesManager _manger, IMapper _mapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;

        }



        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public IActionResult Get(int id)
        {
            OrderFiles entityResult = manger.Get(id);
            var result = mapper.Map<OrderFiles, OrderFilesViewModel>(entityResult);
            return Ok(result);
        }
        [Route("GetAll")]
        [HttpGet]
        public IActionResult Get()
        {
            List<OrderFiles> entityResult = manger.GetAll().ToList();
            List<OrderFilesViewModel>  result = mapper.Map<List<OrderFiles>, List<OrderFilesViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]OrderFilesViewModel model)
        {
            OrderFiles entityResult = mapper.Map<OrderFilesViewModel, OrderFiles>(model);
            entityResult = manger.Add(entityResult);
            OrderFilesViewModel result = mapper.Map<OrderFiles, OrderFilesViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]OrderFilesViewModel model)
        {
            OrderFiles entityResult = mapper.Map<OrderFilesViewModel, OrderFiles>(model);
            bool result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            OrderFiles entity = manger.Get(id);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion
       
    }
}