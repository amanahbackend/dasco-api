﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.Ordering.BLL.Managers;
using DispatchProduct.Ordering.API.ViewModel;
using DispatchProduct.Ordering.Entities;

namespace DispatchProduct.CustomerModule.API.Controllers
{
    [Authorize]
    [Route("api/OrderPriority")]
    public class OrderPriorityController : Controller
    {
        public IOrderPriorityManager manger;
        public readonly IMapper mapper;
        public OrderPriorityController(IOrderPriorityManager _manger, IMapper _mapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;

        }



        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public IActionResult Get(int id)
        {
            OrderPriority entityResult = manger.Get(id);
            var result = mapper.Map<OrderPriority, OrderPriorityViewModel>(entityResult);
            return Ok(result);
        }
        [Route("GetAll")]
        [HttpGet]
        public IActionResult Get()
        {
            List<OrderPriority> entityResult = manger.GetAll().ToList();
            List<OrderPriorityViewModel>  result = mapper.Map<List<OrderPriority>, List<OrderPriorityViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]OrderPriorityViewModel model)
        {
            OrderPriority entityResult = mapper.Map<OrderPriorityViewModel, OrderPriority>(model);
            entityResult = manger.Add(entityResult);
            OrderPriorityViewModel result = mapper.Map<OrderPriority, OrderPriorityViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]OrderPriorityViewModel model)
        {
            OrderPriority entityResult = mapper.Map<OrderPriorityViewModel, OrderPriority>(model);
            bool result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            OrderPriority entity = manger.Get(id);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion
       
    }
}