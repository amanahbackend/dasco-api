﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.Ordering.BLL.Managers;
using DispatchProduct.Ordering.API.ViewModel;
using DispatchProduct.Ordering.API.ServicesViewModels;
using Utilites;
using DispatchProduct.Ordering.API.ServicesCommunication;
using Microsoft.Extensions.Options;
using DispatchProduct.Ordering.API.ServicesCommunication.PreventiveMaintainence;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Ordering.Settings;
using DispatchProduct.Ordering.BLL.Filters;
using Utilites.UploadFile;
using Microsoft.AspNetCore.SignalR;
using DispatchProduct.Ordering.Hubs;
using Utilites.ProcessingResult;

namespace DispatchProduct.Ordering.API.Controllers
{
    [Authorize]
    [Route("api/Order")]
    public class OrderController : Controller
    {
        public IOrderManager manger;
        public IOrderFilesManager filesManger;
        public readonly IMapper mapper;
        private ICustomerService customerService;
        private ILocationService locService;
        private IOrderDistributionCriteriaManager ordDistribtion;
        private IUserService userService;
        private OrderAppSettings appSettings;
        private IPreventiveMaintainenceService preventiveService;
        private IContractService contractService;
        private IHubContext<OrderingHub> orderingHub;
        private IOrderingHub orderHub;
        private ITechnicianUsedItemService usedItmService;
        private IOrderPriorityManager orderPriorityManager;
        public OrderController(IContractService _contractService, IOrderManager _manger, 
            IOrderFilesManager _filesManger, IMapper _mapper, ICustomerService _customerService, 
            ILocationService _locService, IOrderDistributionCriteriaManager _ordDistribtion, 
            IUserService _userService, IOptions<OrderAppSettings> _appSettings, 
            IPreventiveMaintainenceService _preventiveService, IHubContext<OrderingHub> _orderingHub, 
            IOrderingHub _orderHub, ITechnicianUsedItemService _usedItmServ,
            IOrderPriorityManager _orderPriorityManager)
        {
            orderPriorityManager = _orderPriorityManager;
            manger = _manger;
            filesManger = _filesManger;
            mapper = _mapper;
            customerService = _customerService;
            locService = _locService;
            ordDistribtion = _ordDistribtion;
            userService = _userService;
            appSettings = _appSettings.Value;
            preventiveService = _preventiveService;
            contractService = _contractService;
            orderingHub = _orderingHub;
            orderHub = _orderHub;
            usedItmService = _usedItmServ;
        }



        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get/{id}")]
        [HttpGet]
        public async Task<IActionResult> Get([FromRoute]int id)
        {
            Order entityResult = manger.Get(id);
            var result = mapper.Map<Order, OrderViewModel>(entityResult);
            result.Customer = await GetCustomerById(result.FK_Customer_Id);
            result.Location = await GetLocationById(result.FK_Location_Id);
            result.Contract = await GetContractById(result.FK_Contract_Id);
            BindFilesURL(result);
            return Ok(result);
        }
        [Route("GetAll")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            List<Order> entityResult = manger.GetAllOrderFilledProps().ToList<Order>();
            List<OrderViewModel> result = mapper.Map<List<Order>, List<OrderViewModel>>(entityResult);
            result = await FillCustomerOrders(result);
            result = await FillLocationOrders(result);
            result = await FillContractOrders(result);
            BindFilesURL(result);
            BindFilesURL(result);
            return Ok(result);
        }
        [Route("GetFilteredOrderByDispatcherId")]
        [HttpPost]
        public async Task<IActionResult> GetFilteredOrderByDispatcherId([FromBody] FilterOrderViewModelByDispatcher filter)
        {
            FilterOrderByDispatcher filter1 = mapper.Map<FilterOrderViewModelByDispatcher, FilterOrderByDispatcher>(filter);
            List<Order> list = manger.GetFilteredOrderByDispatcherId(filter1).ToList();
            List<OrderViewModel> orders = mapper.Map<List<Order>, List<OrderViewModel>>(list);
            List<FilteredOrderViewModel> result = new List<FilteredOrderViewModel>();
            orders = await FillCustomerOrders(orders);
            orders = await FillLocationOrders(orders);
            var model = await Fill_Tech_Dispatcher_Orders(orders);
            BindFilesURL(model);
            foreach (OrderViewModel orderViewModel in model)
            {
                OrderViewModel item = orderViewModel;
                if (item.LstOrderProgress != null && item.LstOrderProgress.Count > 0)
                {
                    List<OrderProgressViewModel> progressViewModelList = await Fill_Tech_Dispatcher_OrderProgress(item.LstOrderProgress);
                    foreach (OrderProgressViewModel source in item.LstOrderProgress)
                    {
                        FilteredOrderViewModel destination = mapper.Map<OrderViewModel, FilteredOrderViewModel>(item);
                        result.Add(mapper.Map(source, destination));
                    }
                }
                else
                    result.Add(mapper.Map<OrderViewModel, FilteredOrderViewModel>(item));
            }
            return Ok(result);
        }

        [Route("GetMapFilteredOrderByDispatcherId")]
        [HttpPost]
        public async Task<IActionResult> GetMapFilteredOrderByDispatcherId([FromBody] FilterOrderViewModelByDispatcher model)
        {
            FilterOrderByDispatcher filter = mapper.Map<FilterOrderViewModelByDispatcher, FilterOrderByDispatcher>(model);
            List<Order> orders = manger.GetFilteredOrderByDispatcherId(filter);
            List<OrderViewModel> ordersViewModel = mapper.Map<List<Order>, List<OrderViewModel>>(orders);
            ordersViewModel = await FillCustomerOrders(ordersViewModel);
            ordersViewModel = await FillLocationOrders(ordersViewModel);
            ordersViewModel = await Fill_Tech_Dispatcher_Orders(ordersViewModel);
            BindFilesURL(ordersViewModel);
            return Ok(ordersViewModel);
        }

        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] OrderViewModel model)
        {
            ProcessResult<OrderViewModel> processResult = await AddOrder(model);
            return !processResult.IsSucceeded ? BadRequest(processResult.Message) : (IActionResult)Ok(processResult.returnData);
        }

        public async Task<ProcessResult<OrderViewModel>> AddOrder([FromBody] OrderViewModel model, string auth = null)
        {
            ProcessResult<OrderViewModel> result = new ProcessResult<OrderViewModel>();
            result.IsSucceeded = true;
            Order entityResult = mapper.Map<OrderViewModel, Order>(model);
            LocationViewModel locationById = await GetLocationById(entityResult.FK_Location_Id, auth);
            if (locationById != null)
            {
                entityResult.Area = locationById.Area;
                string str = ordDistribtion.GetDispatcherIdForOrder(entityResult.FK_OrderProblem_Id, locationById.Area);
                if (str == null)
                    str = (await GetDispatcherSupervisor(auth)).Id;
                if (str != null)
                {
                    entityResult.FK_Dispatcher_Id = str;
                    var customer = await GetCustomerById(entityResult.FK_Customer_Id);
                    var customerType = customer.CustomerType.Name;
                    switch (customerType)
                    {
                        case "VIP":
                            entityResult.FK_OrderPriority_Id = orderPriorityManager.Get(x => x.Name.Equals("High")).Id;
                            break;
                        case "Regular":
                            entityResult.FK_OrderPriority_Id = orderPriorityManager.Get(x => x.Name.Equals("Medium")).Id;
                            break;
                        case "Basic":
                            entityResult.FK_OrderPriority_Id = orderPriorityManager.Get(x => x.Name.Equals("Low")).Id;
                            break;
                        default:
                            break;
                    }
                    entityResult = manger.Add(entityResult);
                    result.returnData = mapper.Map<Order, OrderViewModel>(entityResult);
                    orderHub.AssignOrderToDispatcher(result.returnData, orderingHub, entityResult.FK_Dispatcher_Id);
                }
                else
                {
                    result.IsSucceeded = false;
                    result.Message = "Can't create order without assigning to dispatcher. set supervisor dispatcher or assign convenient criteria to dispatcher";
                }
            }
            else
            {
                result.IsSucceeded = false;
                result.Message = "Can't create order by this location";
            }
            return result;
        }
        [Route("AddOrderProgress")]
        [HttpPost]
        public async Task<IActionResult> AddOrderProgress([FromBody]OrderProgressViewModel model)
        {
            OrderProgress entityResult = mapper.Map<OrderProgressViewModel, OrderProgress>(model);
            entityResult = manger.AddOrderProgress(entityResult);
            OrderProgressViewModel result = mapper.Map<OrderProgress, OrderProgressViewModel>(entityResult);
            Order order = manger.Get(model.FK_Order_Id);
            OrderViewModel orderViewModel = mapper.Map<Order, OrderViewModel>(order);
            result.Order = orderViewModel;
            if (result.FK_CreatedBy_Id == result.FK_Technician_Id)
                orderHub.ChangeOrderProgress(result, orderingHub, order.FK_Dispatcher_Id);
            else
                orderHub.ChangeOrderProgress(result, orderingHub, order.FK_Technician_Id);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]OrderViewModel model, string authHeader = null)
        {
            Order entityResult = mapper.Map<OrderViewModel, Order>(model);
            bool result = manger.Update(entityResult);
            if (Request != null && authHeader == null)
                authHeader = Helper.GetValueFromRequestHeader(Request, "Authorization");
            orderHub.UpdateOrder(model, orderingHub, authHeader);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            Order entity = manger.Get(id);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion
        #region Assign
        [Route("AssignOrderToTechnician")]
        [HttpPost]
        public bool AssignOrderToTechnician([FromBody]AssignOrderToTechnicianViewModel model)
        {
            var result = manger.AssignOrderToTechnician(model.FK_Order_Id, model.FK_Technician_Id);
            var orderentity = manger.Get(model.FK_Order_Id);
            var order = mapper.Map<Order, OrderViewModel>(orderentity);
            orderHub.AssignOrderToTechnician(order, orderingHub, model.FK_Technician_Id);
            return result;

        }
        [Route("UnAssignOrderToTechnician/{orderId}")]
        [HttpGet]
        public bool UnAssignOrderToTechnician([FromRoute]int orderId)
        {
            return manger.UnAssignOrderToTechnician(orderId);
        }
        [Route("AssignOrdersToTechnician")]
        [HttpPost]
        public bool AssignOrdersToTechnician([FromBody]AssignMultiOrderToTechnicianViewModel model)
        {
            return manger.AssignOrdersToTechnician(model.FK_Order_Ids, model.FK_Technician_Id);
        }

        [Route("TransferOrderToDispatcher")]
        [HttpPost]
        public bool TransferOrderToDispatcher([FromBody]AssignOrderToDispatcherViewModel model)
        {

            var result = manger.TransferOrderToDispatcher(model.FK_Order_Id, model.FK_Dispatcher_Id);
            var orderentity = manger.Get(model.FK_Order_Id);
            var order = mapper.Map<Order, OrderViewModel>(orderentity);
            orderHub.AssignOrderToDispatcher(order, orderingHub, model.FK_Dispatcher_Id);
            return result;
        }
        [Route("SetPreferedVisitTime")]
        [HttpPost]
        public bool SetPreferedVisitTime([FromBody]OrderPreferedVisitTimeViewModel model)
        {
            return manger.SetPreferedVisitTime(model.FK_Order_Id, model.PreferedVisitTime);
        }
        [Route("AssignOrdersToDispatcher")]
        [HttpPost]
        public bool AssignOrdersToDispatcher([FromBody]AssignMultiOrderToDispatcherViewModel model)
        {
            return manger.AssignOrdersToDispatcher(model.FK_Order_Ids, model.FK_Dispatcher_Id);
        }
        [Route("AssignOrderToDispatcher_Technician")]
        [HttpPost]
        public bool AssignOrderToDispatcher_Technician([FromBody]AssignOrderToDispatcher_TechnicianViewModel model)
        {
            return manger.AssignOrderToDispatcher_Technician(model.FK_Order_Id, model.FK_Technician_Id, model.FK_Dispatcher_Id);
        }
        [Route("AssignOrdersToDispatcher_Technician")]
        [HttpPost]
        public bool AssignOrdersToDispatcher_Technician([FromBody]AssignMultiOrderToDispatcher_TechnicianViewModel model)
        {
            return manger.AssignOrdersToDispatcher_Technician(model.FK_Order_Ids, model.FK_Technician_Id, model.FK_Dispatcher_Id);
        }
        #endregion
        [HttpPost]
        [Route("CustomerSignature")]
        public async Task<IActionResult> CustomerSignature([FromBody]CustomerSignatureViewModel signature)
        {

            var issucceed = await AddUsedItems(signature.UsedItems, signature.OrderProgress.FK_Technician_Id, signature.OrderProgress.FK_Order_Id);
            if (issucceed)
            {
                await AddOrderProgress(signature.OrderProgress);
                Order entity = manger.Get(signature.OrderProgress.FK_Order_Id);
                if (entity != null)
                {
                    if (signature.File != null)
                    {
                        signature.File.FileName = signature.OrderProgress.FK_Order_Id.ToString() + ".jpeg";
                        if (appSettings.SignaturePath == null)
                            appSettings.SignaturePath = "OrderSignatures";
                        UploadImageFileManager imageFileManager = new UploadImageFileManager();
                        string path = $"{appSettings.SignaturePath}/{signature.OrderProgress.FK_Order_Id}";
                        ProcessResult<string> processResult = imageFileManager.AddFile(signature.File, path);
                        if (!processResult.IsSucceeded)
                            return BadRequest(processResult.Exception);
                        entity.SignaturePath = $"{path}/{signature.File.FileName}";
                    }
                    if (signature.ContractSignatureFile != null)
                    {
                        signature.ContractSignatureFile.FileName = signature.OrderProgress.FK_Order_Id.ToString() + ".jpeg";
                        if (appSettings.SignatureContractPath == null)
                            appSettings.SignatureContractPath = "OrderContractSignatures";
                        UploadImageFileManager imageFileManager = new UploadImageFileManager();
                        string path = $"{appSettings.SignatureContractPath}/{signature.OrderProgress.FK_Order_Id}";
                        ProcessResult<string> processResult = imageFileManager.AddFile(signature.ContractSignatureFile, path);
                        if (!processResult.IsSucceeded)
                            return BadRequest(processResult.Exception);
                        entity.SignatureContractPath = $"{path}/{signature.ContractSignatureFile.FileName}";
                    }
                    manger.Update(entity);
                }
                return Ok(true);
            }
            else
            {
                return BadRequest("Add Used Items Process Failed");
            }

        }

        [HttpPost]
        [Route("CustomerSignatures")]
        public async Task<IActionResult> CustomerSignatures([FromBody]CustomerSignaturesViewModel signature)
        {

            var issucceed = await AddUsedItems(signature.UsedItems, signature.OrderProgress.FK_Technician_Id, signature.OrderProgress.FK_Order_Id);
            if (issucceed)
            {
                await AddOrderProgress(signature.OrderProgress);
                Order entity = manger.Get(signature.OrderProgress.FK_Order_Id);
                if (entity != null)
                {
                    if (signature.OrderFiles != null && signature.OrderFiles.Count > 0)
                    {
                        if (appSettings.OrderFilesPath == null)
                            appSettings.OrderFilesPath = "OrderFiles";
                        UploadImageFileManager imageFileManager = new UploadImageFileManager();
                        string path = $"{appSettings.OrderFilesPath}/{signature.OrderProgress.FK_Order_Id}";

                        for (int i = 0; i < signature.OrderFiles.Count; i++)
                        {
                            var filename = signature.OrderFiles[i].FileName = (i + 1) + ".jpeg";
                            ProcessResult<string> processResult = imageFileManager.AddFile(signature.OrderFiles[i], path);
                            if (processResult.IsSucceeded)
                            {
                                var orderFile = new OrderFiles();
                                orderFile.FileName = filename;
                                orderFile.OrderId = signature.OrderProgress.FK_Order_Id;
                                orderFile.FileRelativePath = processResult.returnData + "/" + filename;
                                filesManger.Add(orderFile);
                            }
                            else
                            {
                                return BadRequest(processResult.Exception);
                            }
                        }
                    }

                }
                return Ok(true);
            }
            else
            {
                return BadRequest("Add Used Items Process Failed");
            }

        }
        [Route("Search/{key}")]
        [HttpGet]
        public async Task<IActionResult> Search(string key)
        {
            List<OrderViewModel> orderViewModelList = null;
            if (key != null)
            {
                List<Order> entityResult = manger.Search(key);
                if (entityResult == null || entityResult.Count == 0)
                {
                    CustomerViewModel customerViewModel = await SearchCustomer(key);
                    if (customerViewModel != null)
                    {
                        entityResult = manger.SearchByCustomerId(customerViewModel.Id);
                        foreach (OrderViewModel orderViewModel in mapper.Map<List<Order>, List<OrderViewModel>>(entityResult))
                            orderViewModel.Customer = customerViewModel;
                    }
                }
                List<OrderViewModel> orders = mapper.Map<List<Order>, List<OrderViewModel>>(entityResult);
                orderViewModelList = await FillCustomerOrders(orders);
            }
            return Ok(orderViewModelList);
        }
        [Route("SearchByCustomerId/{customerId}")]
        [HttpGet]
        public async Task<IActionResult> SearchByCustomerId(int customerId)
        {
            List<OrderViewModel> result = null;
            if (customerId > 0)
            {
                var entityResult = manger.SearchByCustomerId(customerId);
                result = mapper.Map<List<Order>, List<OrderViewModel>>(entityResult);
            }
            return Ok(result);
        }
        public async Task<List<OrderViewModel>> FillCustomerOrders(List<OrderViewModel> orders, string authHeader = null)
        {
            foreach (var item in orders)
            {
                item.Customer = await GetCustomerById(item.FK_Customer_Id, authHeader);
            }
            return orders;
        }
        public async Task<List<OrderProgressViewModel>> Fill_Tech_Dispatcher_OrderProgress(List<OrderProgressViewModel> orderProgress, string authHeader = null)
        {
            foreach (var item in orderProgress)
            {
                ApplicationUserViewModel user = await GetUser(item.FK_Technician_Id, authHeader);
                if (user != null)
                    item.TechnicianName = user.UserName;
                item.CreatedBy = await GetUser(item.FK_CreatedBy_Id, authHeader);
            }

            return orderProgress;
        }
        private async Task<CustomerViewModel> SearchCustomer(string key, string authHeader = null)
        {
            CustomerViewModel result = null;
            try
            {
                if (Request != null && authHeader == null)
                    authHeader = Helper.GetValueFromRequestHeader(Request, "Authorization");
                result = await customerService.SearchCustomer(key, authHeader);
            }
            catch (Exception ex)
            {
            }
            return result;
        }
        public async Task<List<OrderViewModel>> FillContractOrders(List<OrderViewModel> orders, string authHeader = null)
        {
            foreach (var item in orders)
            {
                item.Contract = await GetContractById(item.FK_Contract_Id, authHeader);
            }
            return orders;
        }
        public async Task<List<OrderViewModel>> FillLocationOrders(List<OrderViewModel> orders, string authHeader = null)
        {
            foreach (var item in orders)
            {
                item.Location = await GetLocationById(item.FK_Location_Id, authHeader);
            }
            return orders;
        }
        public async Task<List<OrderViewModel>> Fill_Tech_Dispatcher_Orders(List<OrderViewModel> orders, string authHeader = null)
        {
            foreach (var item in orders)
            {
                var tech = await GetUser(item.FK_Technician_Id, authHeader);
                if (tech != null)
                    item.TechnicianName = tech.UserName;
                var dispatcher = await GetUser(item.FK_Dispatcher_Id, authHeader);
                if (dispatcher != null)
                    item.DispatcherName = dispatcher.UserName;
            }

            return orders;
        }
        [Route("GetOrderByDispatcher/{dispatcherId}")]
        [HttpGet]
        private async Task<ApplicationUserViewModel> GetUser([FromRoute] string usreId, string authHeader = null)
        {
            if (Request != null && authHeader == null)
                authHeader = Helper.GetValueFromRequestHeader(Request, "Authorization");
            List<string> userIds = new List<string>();
            userIds.Add(usreId);
            string authHeader1 = authHeader;
            return (await userService.GetByUserIds(userIds, authHeader1)).FirstOrDefault();
        }
        public async Task<CustomerViewModel> GetCustomerById(int id, string authHeader = null)
        {

            CustomerViewModel result = null;
            try
            {
                if (Request != null && authHeader == null)
                {
                    authHeader = Helper.GetValueFromRequestHeader(Request);
                }
                result = await customerService.GetItem(id.ToString(), authHeader);
            }
            catch (Exception ex)
            {

            }
            return result;
        }
        public async Task<ContractViewModel> GetContractById(int id, string authHeader = null)
        {

            ContractViewModel result = null;
            try
            {
                if (Request != null && authHeader == null)
                {
                    authHeader = Helper.GetValueFromRequestHeader(Request);
                }
                result = await contractService.GetItem(id.ToString(), authHeader);
            }
            catch (Exception ex)
            {

            }
            return result;
        }
        public async Task<LocationViewModel> GetLocationById(int locId, string authHeader = null)
        {
            LocationViewModel result = null;
            try
            {
                if (Request != null && authHeader == null)
                {
                    authHeader = Helper.GetValueFromRequestHeader(Request);
                }
                result = await locService.GetItem(locId.ToString(), authHeader);
            }
            catch (Exception ex)
            {

            }
            return result;
        }
        public async Task<ApplicationUserViewModel> GetDispatcherSupervisor(string authHeader = null)
        {
            ApplicationUserViewModel result = null;
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            result = (await userService.GetDispatcherSupervisor(authHeader)).FirstOrDefault();
            return result;
        }
        private async Task<bool> AddUsedItems(List<int> usedItemIds, string FK_TechnicianId, int FK_Order_Id, string authHeader = null)
        {
            bool flag;
            try
            {
                if (usedItemIds != null && usedItemIds.Count > 0)
                {
                    List<TechnicianUsedItemsViewModel> usedItems = new List<TechnicianUsedItemsViewModel>();
                    var usdItmsgroped = usedItemIds.GroupBy((itm => itm)).Select(g => new
                    {
                        itemId = g.Key,
                        Count = g.Count()
                    });
                    if (Request != null && authHeader == null)
                        authHeader = Helper.GetValueFromRequestHeader(Request, "Authorization");
                    foreach (var data in usdItmsgroped)
                        usedItems.Add(new TechnicianUsedItemsViewModel()
                        {
                            ItemId = data.itemId,
                            Amount = data.Count,
                            FK_Technician_Id = FK_TechnicianId,
                            FK_Order_Id = FK_Order_Id
                        });
                    if (usedItems.Count > 0)
                    {
                        List<TechnicianUsedItemsViewModel> usedItemsViewModelList = await usedItmService.AddUsedItems(usedItems, authHeader);
                        flag = usedItemsViewModelList != null && usedItemsViewModelList.Count > 0;
                    }
                    else
                        flag = true;
                }
                else
                    flag = true;
            }
            catch (Exception ex)
            {
                flag = false;
            }
            return flag;
        }
        private List<OrderViewModel> BindFilesURL(List<OrderViewModel> model)
        {
            foreach (OrderViewModel model1 in model)
                BindFilesURL(model1);
            return model;
        }
        private OrderViewModel BindFilesURL(OrderViewModel model)
        {
            string str = $"{Request.Scheme}://{Request.Host}{Request.PathBase}";
            if (model.SignaturePath != null)
            {
                model.SignaturePath = model.SignaturePath.Replace('\\', '/');
                model.SignatureURL = $"{str}/{model.SignaturePath}";
            }
            if (model.SignatureContractPath != null)
            {
                model.SignatureContractPath = model.SignatureContractPath.Replace('\\', '/');
                model.SignatureContractURL = $"{str}/{model.SignatureContractPath}";
            }
            if (model.OrderFiles != null && model.OrderFiles.Count > 0)
            {
                BindFilesURL(model.OrderFiles);
            }

            return model;
        }

        private List<OrderFilesViewModel> BindFilesURL(List<OrderFilesViewModel> model)
        {
            foreach (OrderFilesViewModel model1 in model)
                BindFilesURL(model1);
            return model;
        }
        private OrderFilesViewModel BindFilesURL(OrderFilesViewModel model)
        {
            string str = $"{Request.Scheme}://{Request.Host}{Request.PathBase}";
            if (model.FileRelativePath != null)
            {
                model.FileURL = model.FileRelativePath.Replace('\\', '/');
                model.FileURL = $"{str}/{model.FileURL}";
            }
            return model;
        }

    }
}