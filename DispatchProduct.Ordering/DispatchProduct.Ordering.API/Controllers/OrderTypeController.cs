﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.Ordering.BLL.Managers;
using DispatchProduct.Ordering.API.ViewModel;
using DispatchProduct.Ordering.Entities;

namespace DispatchProduct.CustomerModule.API.Controllers
{
    [Authorize]
    [Route("api/OrderType")]
    public class OrderTypeController : Controller
    {
        public IOrderTypeManager manger;
        public readonly IMapper mapper;
        public OrderTypeController(IOrderTypeManager _manger, IMapper _mapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;

        }



        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public IActionResult Get(int id)
        {
            OrderType entityResult = manger.Get(id);
            var result = mapper.Map<OrderType, OrderTypeViewModel>(entityResult);
            return Ok(result);
        }
        [Route("GetAll")]
        [HttpGet]
        public IActionResult Get()
        {
            List<OrderType> entityResult = manger.GetAll().ToList();
            List<OrderTypeViewModel>  result = mapper.Map<List<OrderType>, List<OrderTypeViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]OrderTypeViewModel model)
        {
            OrderType entityResult = mapper.Map<OrderTypeViewModel, OrderType>(model);
            entityResult = manger.Add(entityResult);
            OrderTypeViewModel result = mapper.Map<OrderType, OrderTypeViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]OrderTypeViewModel model)
        {
            OrderType entityResult = mapper.Map<OrderTypeViewModel, OrderType>(model);
            bool result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            OrderType entity = manger.Get(id);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion
       
    }
}