﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.Ordering.BLL.Managers;
using DispatchProduct.Ordering.API.ViewModel;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Ordering.API;
using DispatchProduct.Ordering.API.ServicesCommunication;
using Utilites;
using DispatchProduct.Ordering.API.Controllers;

namespace DispatchProduct.CustomerModule.API.Controllers
{
    [Authorize]
    [Route("api/AssignedTechnicians")]
    public class AssignedTechniciansController : Controller
    {
        public IAssignedTechniciansManager manger;
        public readonly IMapper mapper;
        IUserService userService;
        IOrderManager ordermanager;
        OrderController ordController;
        public AssignedTechniciansController(IAssignedTechniciansManager _manger, IMapper _mapper, IUserService _userService,IOrderManager _ordermanager, OrderController _ordController)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            userService = _userService;
            ordermanager = _ordermanager;
            ordController= _ordController;
        }



        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get/{id}")]
        [HttpGet]
        public IActionResult Get(int id)
        {
            AssignedTechnicians entityResult = manger.Get(id);
            var result = mapper.Map<AssignedTechnicians, AssignedTechniciansViewModel>(entityResult);
            return Ok(result);
        }
        [Route("GetAll")]
        [HttpGet]
        public IActionResult Get()
        {
            List<AssignedTechnicians> entityResult = manger.GetAll().ToList();
            List<AssignedTechniciansViewModel>  result = mapper.Map<List<AssignedTechnicians>, List<AssignedTechniciansViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]AssignedTechniciansViewModel model)
        {
            AssignedTechnicians entityResult = mapper.Map<AssignedTechniciansViewModel, AssignedTechnicians>(model);
            entityResult = manger.Add(entityResult);
            AssignedTechniciansViewModel result = mapper.Map<AssignedTechnicians, AssignedTechniciansViewModel>(entityResult);
            return Ok(result);
        }

        [Route("Assign")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]AssignedMultiTechniciansViewModel model)
        {
            List<AssignedTechniciansViewModel> result =new List<AssignedTechniciansViewModel>();
            IList<AssignedTechnicians> entityResult = mapper.Map<AssignedMultiTechniciansViewModel, IList<AssignedTechnicians>>(model);
            foreach (var item in entityResult)
            {
               var res= manger.Add(item);
               result.Add(mapper.Map<AssignedTechnicians, AssignedTechniciansViewModel>(res));
            }
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]AssignedTechniciansViewModel model)
        {
            AssignedTechnicians entityResult = mapper.Map<AssignedTechniciansViewModel, AssignedTechnicians>(model);
            bool result = manger.Update(entityResult);
            return Ok(result);
        }

        [Route("UpdateAssignedTechnician")]
        [HttpPut]
        public async Task<IActionResult> UpdateAssignedTechnician([FromBody]AssignedTechniciansViewModel model)
        {
            AssignedTechnicians entityResult = mapper.Map<AssignedTechniciansViewModel, AssignedTechnicians>(model);
            bool result = manger.UpdateAssignedTechnician(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            AssignedTechnicians entity = manger.Get(id);
            result = manger.Delete(entity);
            return Ok(result);
        }

        [Route("DeleteAssignedTechnician/{technicianId}")]
        [HttpDelete]
        public async Task<IActionResult> DeleteAssignedTechnician([FromRoute]string technicianId)
        {
            bool result = false;
            result= manger.DeleteAssignedTechnician(technicianId);
            return Ok(result);
        }
        #endregion
        #endregion
        [Route("GetAssignedTechnicians")]
        [HttpGet]
        public async Task<List<ApplicationUserViewModel>> GetAssignedTechnicians(string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            List<string> fK_Technician_Ids = manger.GetAll().Select(t => t.FK_Technician_Id).ToList();
            return await userService.GetByUserIds(fK_Technician_Ids, authHeader);
        }

        [Route("GetUnAssignedTechnicians")]
        [HttpGet]
        public async Task<List<ApplicationUserViewModel>> GetUnAssignedTechnicians(string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            List<string> fK_Technician_Ids = manger.GetAll().Select(t => t.FK_Technician_Id).ToList();
            return await userService.GetUnAssignedTechnicians(fK_Technician_Ids, authHeader);
        }
        [Route("GetTechniciansByDispatcherId/{dispatcherId}")]
        [HttpGet]
        public async Task<List<TechnicianViewModel>> GetTechniciansByDispatcherId([FromRoute]string dispatcherId, string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            List<TechnicianViewModel> result =null ;
            var assignedTechnicians= manger.GetByDispatcherId(dispatcherId);
            var techniciansIds = assignedTechnicians.Select(t=>t.Id).ToList();
            var tecnicansInfo= await userService.GetByUserIds(techniciansIds, authHeader);
            foreach (var techn in assignedTechnicians)
            {
                var techInfo = tecnicansInfo.Find(t => t.Id == techn.Id);
                mapper.Map(techInfo, techn);
            }
            result=mapper.Map< List<Technician>, List<TechnicianViewModel>>(assignedTechnicians);
            foreach (var techViewMod in result)
            {
                var orders = techViewMod.Orders;
                orders=await ordController.FillCustomerOrders(orders, authHeader);
                orders=await ordController.FillLocationOrders(orders, authHeader);
            }
            return result;
        }
        [Route("GetDispatcher/{dispatcherId}")]
        [HttpGet]
        public async Task<DispatcherViewModel> GetDispatcher([FromRoute]string dispatcherId, string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            Dispatcher entityResult = new Dispatcher();
            entityResult.Orders = ordermanager.GetDispatcherOrders(dispatcherId);
            var dispatcherInfo= (await userService.GetByUserIds(new List<string> { dispatcherId }, authHeader)).FirstOrDefault();
            mapper.Map(dispatcherInfo, entityResult);
            var result = mapper.Map<Dispatcher, DispatcherViewModel>(entityResult);
            result.Orders = await ordController.FillCustomerOrders(result.Orders, authHeader);
            result.Orders = await ordController.FillLocationOrders(result.Orders, authHeader);
            return result;
        }
        [Route("GetOrderByDispatcher/{dispatcherId}")]
        [HttpGet]
        public async Task<DispatcherViewModel> GetOrderByDispatcher([FromRoute]string dispatcherId, string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            Dispatcher entityResult = new Dispatcher();
            entityResult.Orders = ordermanager.GetAllDispatcherOrders(dispatcherId);
            var dispatcherInfo = (await userService.GetByUserIds(new List<string> { dispatcherId }, authHeader)).FirstOrDefault();
            mapper.Map(dispatcherInfo, entityResult);
            var result = mapper.Map<Dispatcher, DispatcherViewModel>(entityResult);
            result.Orders = await ordController.FillCustomerOrders(result.Orders, authHeader);
            result.Orders = await ordController.FillLocationOrders(result.Orders, authHeader);
            return result;
        }
        [Route("GetDispatcherSupervisor/{dispatcherId}")]
        [HttpGet]
        public async Task<DispatcherViewModel> GetDispatcherSupervisor([FromRoute]string dispatcherId, string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            var dispatcherInfo = (await userService.GetByUserIds(new List<string> { dispatcherId }, authHeader)).FirstOrDefault();
            Dispatcher entityResult = new Dispatcher();
            entityResult.Orders = ordermanager.GetOrdersAssignedToDispatcher(dispatcherId);
            mapper.Map(dispatcherInfo, entityResult);
            var result = mapper.Map<Dispatcher, DispatcherViewModel>(entityResult);
            return result;
        }
        [Route("GetTechnician/{technicianId}")]
        [HttpGet]
        public async Task<DispatcherViewModel> GetTechnician([FromRoute]string technicianId, string authHeader = null)
        {
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            var dispatcherInfo = (await userService.GetByUserIds(new List<string> { technicianId }, authHeader)).FirstOrDefault();
            Dispatcher entityResult = new Dispatcher();
            entityResult.Orders = ordermanager.GetOrdersAssignedToTechnician(technicianId);
            mapper.Map(dispatcherInfo, entityResult);
            var result = mapper.Map<Dispatcher, DispatcherViewModel>(entityResult);
            await FillOrderInfo(result);
            return result;
        }
        public async Task FillOrderInfo(DispatcherViewModel result,string authHeader=null)
        {
            if (result != null && result.Orders != null)
            {
                if (Request != null && authHeader == null)
                {
                    authHeader = Helper.GetValueFromRequestHeader(Request);
                }
                foreach (var item in result.Orders)
                {
                    item.Customer = await ordController.GetCustomerById(item.FK_Customer_Id, authHeader);
                    item.Location = await ordController.GetLocationById(item.FK_Location_Id, authHeader);
                    item.Contract = await ordController.GetContractById(item.FK_Contract_Id, authHeader);
                }
            }
        }
    }
}