﻿using System.Collections.Generic;

namespace DispatchProduct.Ordering.API.ViewModel
{
  public class AssignMultiOrderToTechnicianViewModel : BaseEntityViewModel
  {
    public string FK_Technician_Id { get; set; }

    public List<int> FK_Order_Ids { get; set; }
  }
}
