﻿using System.Collections.Generic;

namespace DispatchProduct.Ordering.API.ViewModel
{
  public class AssignedMultiTechniciansViewModel : BaseEntityViewModel
  {
    public int Id { get; set; }

    public List<string> FK_Technicians_Id { get; set; }

    public string FK_Dispatcher_Id { get; set; }
  }
}
