﻿using System.Collections.Generic;

namespace DispatchProduct.Ordering.API.ViewModel
{
  public class AssignMultiOrderToDispatcherViewModel : BaseEntityViewModel
  {
    public string FK_Dispatcher_Id { get; set; }

    public List<int> FK_Order_Ids { get; set; }
  }
}
