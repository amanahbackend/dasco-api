﻿namespace DispatchProduct.Ordering.API.ViewModel
{
  public class FilteredOrderViewModel : BaseEntityViewModel
  {
    public OrderViewModel Order { get; set; }

    public OrderProgressViewModel OrderProgress { get; set; }
  }
}
