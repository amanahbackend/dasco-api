﻿namespace DispatchProduct.Ordering.API.ViewModel
{
  public class OrderPriorityViewModel : BaseEntityViewModel
  {
    public int Id { get; set; }

    public string Name { get; set; }
  }
}
