﻿namespace DispatchProduct.Ordering.API.ViewModel
{
  public class OrderStatusViewModel : BaseEntityViewModel
  {
    public int Id { get; set; }

    public string Name { get; set; }
  }
}
