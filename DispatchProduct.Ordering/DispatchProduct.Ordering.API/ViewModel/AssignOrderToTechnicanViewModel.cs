﻿namespace DispatchProduct.Ordering.API.ViewModel
{
  public class AssignOrderToTechnicianViewModel : BaseEntityViewModel
  {
    public string FK_Technician_Id { get; set; }

    public int FK_Order_Id { get; set; }
  }
}
