﻿namespace DispatchProduct.Ordering.API.ViewModel
{
  public class OrderProblemViewModel : BaseEntityViewModel
  {
    public int Id { get; set; }

    public string Name { get; set; }
  }
}
