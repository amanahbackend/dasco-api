﻿using DispatchProduct.Ordering.API;
using DispatchProduct.Ordering.API.ViewModel;

namespace DispatchProduct.Ordering.Entities
{
  public class OrderDistributionCriteriaViewModel : BaseEntityViewModel
  {
    public int Id { get; set; }

    public int FK_OrderProblem_Id { get; set; }

    public OrderProblemViewModel OrderProblem { get; set; }

    public string Area { get; set; }

    public string Governorate { get; set; }

    public string FK_Dispatcher_Id { get; set; }

    public ApplicationUserViewModel Dispatcher { get; set; }
  }
}
