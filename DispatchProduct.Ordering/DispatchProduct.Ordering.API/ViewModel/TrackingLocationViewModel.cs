﻿using DispatchProduct.Ordering.API.ViewModel;

namespace DispatchProduct.Ordering.Entities
{
  public class TrackingLocationViewModel : BaseEntityViewModel
  {
    public int Id { get; set; }

    public double Lat { get; set; }

    public double Long { get; set; }
  }
}
