﻿using System;

namespace DispatchProduct.Ordering.API.ViewModel
{
  public class OrderPreferedVisitTimeViewModel : BaseEntityViewModel
  {
    public DateTime PreferedVisitTime { get; set; }

    public int FK_Order_Id { get; set; }
  }
}
