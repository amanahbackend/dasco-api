﻿namespace DispatchProduct.Ordering.API.ViewModel
{
  public class AssignOrderToDispatcher_TechnicianViewModel : BaseEntityViewModel
  {
    public string FK_Dispatcher_Id { get; set; }

    public string FK_Technician_Id { get; set; }

    public int FK_Order_Id { get; set; }
  }
}
