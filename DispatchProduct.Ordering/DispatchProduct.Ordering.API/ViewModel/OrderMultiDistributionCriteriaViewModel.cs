﻿using DispatchProduct.Ordering.API;
using DispatchProduct.Ordering.API.ViewModel;
using System.Collections.Generic;

namespace DispatchProduct.Ordering.Entities
{
  public class OrderMultiDistributionCriteriaViewModel : BaseEntityViewModel
  {
    public List<DispatchProduct.Ordering.Entities.AreaProblems> AreaProblems { get; set; }

    public string FK_Dispatcher_Id { get; set; }

    public ApplicationUserViewModel Dispatcher { get; set; }
  }
}
