﻿namespace DispatchProduct.Ordering.API.ViewModel
{
  public class ProgressStatusViewModel : BaseEntityViewModel
  {
    public int Id { get; set; }

    public string Name { get; set; }

    public OrderStatusViewModel OrderStatus { get; set; }

    public int FK_OrderStatus_Id { get; set; }
  }
}
