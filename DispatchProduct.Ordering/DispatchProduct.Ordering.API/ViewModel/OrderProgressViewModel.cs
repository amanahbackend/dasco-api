﻿namespace DispatchProduct.Ordering.API.ViewModel
{
  public class OrderProgressViewModel : BaseEntityViewModel
  {
    public int Id { get; set; }

    public ApplicationUserViewModel CreatedBy { get; set; }

    public string FK_Technician_Id { get; set; }

    public string TechnicianName { get; set; }

    public string Note { get; set; }

    public int FK_Order_Id { get; set; }

    public OrderViewModel Order { get; set; }

    public int FK_ProgressStatus_Id { get; set; }

    public ProgressStatusViewModel ProgressStatus { get; set; }

    public double? Latitude { get; set; }

    public double? Longitude { get; set; }
  }
}
