﻿namespace DispatchProduct.Ordering.API.ViewModel
{
  public class AssignOrderToDispatcherViewModel : BaseEntityViewModel
  {
    public string FK_Dispatcher_Id { get; set; }

    public int FK_Order_Id { get; set; }
  }
}
