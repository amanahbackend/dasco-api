﻿namespace DispatchProduct.Ordering.API.ViewModel
{
  public class OrderTypeViewModel : BaseEntityViewModel
  {
    public int Id { get; set; }

    public string Name { get; set; }

    public string Code { get; set; }

    }
}
