﻿using System.Collections.Generic;

namespace DispatchProduct.Ordering.API.ViewModel
{
  public class TechnicianViewModel : ApplicationUserViewModel
  {
    public List<OrderViewModel> Orders { get; set; }
  }
}
