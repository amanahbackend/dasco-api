﻿namespace DispatchProduct.Ordering.API.ViewModel
{
  public class AssignedTechniciansViewModel : BaseEntityViewModel
  {
    public int Id { get; set; }

    public string FK_Technician_Id { get; set; }

    public string FK_Dispatcher_Id { get; set; }
  }
}
