﻿using DispatchProduct.Ordering.API.ViewModel;
using System.Collections.Generic;

namespace DispatchProduct.Ordering.Entities
{
  public class AreaProblems : BaseEntityViewModel
  {
    public List<int> FK_OrderProblem_Ids { get; set; }

    public List<OrderProblemViewModel> OrderProblems { get; set; }

    public string Area { get; set; }

    public string Governorate { get; set; }
  }
}
