﻿using System.Collections.Generic;

namespace DispatchProduct.Ordering.API.ViewModel
{
  public class DispatcherViewModel : ApplicationUserViewModel
  {
    public List<OrderViewModel> Orders { get; set; }
  }
}
