﻿using DispatchProduct.Ordering.API.ViewModel;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.Hubs
{
    public interface IOrderingHub
    {
        Task JoinGroup(string groupName);
        Task LeaveGroup(string groupName);
        Task OnConnectedAsync();
        void AssignOrderToDispatcher(OrderViewModel order, IHubContext<OrderingHub> orderingHub, string dispatcherId);
        void AssignOrderToTechnician(OrderViewModel order, IHubContext<OrderingHub> orderingHub, string technicianId);
        void UpdateOrder(OrderViewModel order, IHubContext<OrderingHub> orderingHub, string token);
        void ChangeOrderProgress(OrderProgressViewModel orderProgress, IHubContext<OrderingHub> orderingHub, string userId);
    }
}
