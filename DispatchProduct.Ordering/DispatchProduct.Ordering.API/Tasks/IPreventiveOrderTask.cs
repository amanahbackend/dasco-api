﻿using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.Tasks
{
    public interface IPreventiveOrderTask
    {
        Task AddPreventiveOrder();
    }
}
