﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Ordering.API.Settings
{
  public class ContractServiceSetting : DefaultHttpClientSettings
  {
    public override string Uri { get; set; }
  }
}
