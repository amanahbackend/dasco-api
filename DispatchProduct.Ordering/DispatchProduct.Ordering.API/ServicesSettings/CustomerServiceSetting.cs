﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Ordering.API.Settings
{
  public class CustomerServiceSetting : DefaultHttpClientSettings
  {
    public override string Uri { get; set; }

    public string SearchCustomerVerb { get; set; }
  }
}
