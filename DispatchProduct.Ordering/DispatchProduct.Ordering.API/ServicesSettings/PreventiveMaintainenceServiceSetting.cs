﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Ordering.API.Settings
{
  public class PreventiveMaintainenceServiceSetting : DefaultHttpClientSettings
  {
    public override string Uri { get; set; }

    public string GetDailyPreventiveOrdersVerb { get; set; }

    public string UpdatePreventiveMaintainenceByOrderVerb { get; set; }
  }
}
