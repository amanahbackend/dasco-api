﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Ordering.API.Settings
{
  public class TokenServiceSetting : DefaultHttpClientSettings
  {
    public override string Uri { get; set; }

    public string TokenSysVerb { get; set; }

    public string SysUserNameSetting { get; set; }

    public string SysPasswordSetting { get; set; }
  }
}
