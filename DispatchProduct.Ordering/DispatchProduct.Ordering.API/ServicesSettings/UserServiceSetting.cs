﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Ordering.API.Settings
{
  public class UserServiceSetting : DefaultHttpClientSettings
  {
    public override string Uri { get; set; }

    public string GetByUserIdsVerb { get; set; }

    public string GetUnAssignedTechniciansVerb { get; set; }

    public string GetDispatcherSupervisorVerb { get; set; }
  }
}
