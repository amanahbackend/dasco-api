﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Ordering.API.Settings
{
  public class LocationServiceSetting : DefaultHttpClientSettings
  {
    public override string Uri { get; set; }
  }
}
