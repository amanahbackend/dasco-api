﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Ordering.API.Settings
{
  public class TechnicianUsedItemServiceSetting : DefaultHttpClientSettings
  {
    public override string Uri { get; set; }

    public string AddUsedItemsVerb { get; set; }
  }
}
