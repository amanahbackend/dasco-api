﻿using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.ServicesViewModels;
using DispatchProduct.Ordering.API.Settings;

namespace DispatchProduct.Ordering.API.ServicesCommunication
{
    public interface ILocationService : IDefaultHttpClientCrud<LocationServiceSetting, LocationViewModel, LocationViewModel>
    {
        
    }
}
