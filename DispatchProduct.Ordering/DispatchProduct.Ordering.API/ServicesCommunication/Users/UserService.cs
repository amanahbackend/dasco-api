﻿using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Threading.Tasks;
using DispatchProduct.Ordering.API.Settings;

namespace DispatchProduct.Ordering.API.ServicesCommunication
{
    public class UserService : DefaultHttpClientCrud<UserServiceSetting, List<string>, List<ApplicationUserViewModel>>, IUserService
    {
        UserServiceSetting settings;
        public UserService(IOptions<UserServiceSetting> _settings) :base(_settings.Value)
        {
            settings = _settings.Value;
        }
        public async Task<List<ApplicationUserViewModel>> GetByUserIds(List<string> userIds, string authHeader = "")
        {
            var requesturi = $"{settings.Uri}/{settings.GetByUserIdsVerb}";
            return await Post(requesturi, userIds, authHeader);
        }
        public async Task<List<ApplicationUserViewModel>> GetUnAssignedTechnicians(List<string> userIds, string authHeader = "")
        {
            var requesturi = $"{settings.Uri}/{settings.GetUnAssignedTechniciansVerb}";
            return await Post(requesturi, userIds, authHeader);
        }
        public async Task<List<ApplicationUserViewModel>> GetDispatcherSupervisor(string authHeader = "")
        {
            var requesturi = $"{settings.Uri}/{settings.GetDispatcherSupervisorVerb}";
            return await GetByUri(requesturi, authHeader);
        }
    }
}
