﻿using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.Settings;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.ServicesCommunication
{
    public interface IUserService : IDefaultHttpClientCrud<UserServiceSetting, List<string>, List<ApplicationUserViewModel>>
    {
        Task<List<ApplicationUserViewModel>> GetByUserIds(List<string> userIds, string authHeader = "");
        Task<List<ApplicationUserViewModel>> GetUnAssignedTechnicians(List<string> userIds, string authHeader = "");
        Task<List<ApplicationUserViewModel>> GetDispatcherSupervisor(string authHeader = "");
    }
}
