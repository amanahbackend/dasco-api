﻿using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.Settings;
using DispatchProduct.Ordering.API.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.ServicesCommunication
{
  public interface ITechnicianUsedItemService : IDefaultHttpClientCrud<TechnicianUsedItemServiceSetting, List<TechnicianUsedItemsViewModel>, List<TechnicianUsedItemsViewModel>>
  {
    Task<List<TechnicianUsedItemsViewModel>> AddUsedItems(List<TechnicianUsedItemsViewModel> usedItems, string authHeader = "");
  }
}
