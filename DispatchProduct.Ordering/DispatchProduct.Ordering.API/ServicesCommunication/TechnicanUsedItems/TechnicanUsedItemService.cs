﻿

using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.Settings;
using DispatchProduct.Ordering.API.ViewModel;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.ServicesCommunication
{
  public class TechnicianUsedItemService : DefaultHttpClientCrud<TechnicianUsedItemServiceSetting, List<TechnicianUsedItemsViewModel>, List<TechnicianUsedItemsViewModel>>, ITechnicianUsedItemService, IDefaultHttpClientCrud<TechnicianUsedItemServiceSetting, List<TechnicianUsedItemsViewModel>, List<TechnicianUsedItemsViewModel>>
  {
    private TechnicianUsedItemServiceSetting settings;

    public TechnicianUsedItemService(IOptions<TechnicianUsedItemServiceSetting> _settings)
      : base(_settings.Value)
    {
      settings = _settings.Value;
    }

    public async Task<List<TechnicianUsedItemsViewModel>> AddUsedItems(List<TechnicianUsedItemsViewModel> usedItems, string authHeader = "")
    {
      string requesturi = $"{settings.Uri}/{settings.AddUsedItemsVerb}";
      return await Post(requesturi, usedItems, authHeader);
    }
  }
}
