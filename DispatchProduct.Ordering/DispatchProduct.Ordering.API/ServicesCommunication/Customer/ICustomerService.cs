﻿using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.ServicesViewModels;
using DispatchProduct.Ordering.API.Settings;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.ServicesCommunication
{
    public interface ICustomerService : IDefaultHttpClientCrud<CustomerServiceSetting, CustomerViewModel, CustomerViewModel>
    {
        Task<CustomerViewModel> SearchCustomer(string key, string authHeader = "");
    }
}
