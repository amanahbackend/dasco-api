﻿using DispatchProduct.Ordering.API.ServicesViewModels;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using DispatchProduct.Ordering.API.Settings;

namespace DispatchProduct.Ordering.API.ServicesCommunication
{
    public class CustomerService : DefaultHttpClientCrud<CustomerServiceSetting, CustomerViewModel, CustomerViewModel>, ICustomerService
    {
        CustomerServiceSetting settings;
        public CustomerService(IOptions<CustomerServiceSetting> _settings) :base(_settings.Value)
        {
            settings = _settings.Value;
        }
        public async Task<CustomerViewModel> SearchCustomer(string key, string authHeader = "")
        {
            CustomerService customerService = this;
            string requesturi = $"{customerService.settings.Uri}/{customerService.settings.SearchCustomerVerb}/{key}";
            return await customerService.GetByUri(requesturi, authHeader);
        }
    }
}
