﻿using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.Settings;
using DispatchProduct.Ordering.API.ViewModel;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.ServicesCommunication
{
  public class TokenService : DefaultHttpClientCrud<TokenServiceSetting, TokenViewModel, string>, ITokenService, IDefaultHttpClientCrud<TokenServiceSetting, TokenViewModel, string>
  {
    private TokenServiceSetting settings;

    public TokenService(IOptions<TokenServiceSetting> _settings)
      : base(_settings.Value)
    {
      this.settings = _settings.Value;
    }

    public async Task<string> GetSysToken()
    {
      TokenViewModel model = new TokenViewModel();
      model.Username = settings.SysUserNameSetting;
      model.Password = settings.SysPasswordSetting;
      string requesturi =$"{settings.Uri}/{settings.TokenSysVerb}";
      return await Post(requesturi, model, "");
    }
  }
}
