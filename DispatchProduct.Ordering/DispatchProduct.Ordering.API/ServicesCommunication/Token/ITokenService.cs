﻿using DispatchProduct.HttpClient;
using DispatchProduct.Ordering.API.Settings;
using DispatchProduct.Ordering.API.ViewModel;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.ServicesCommunication
{
  public interface ITokenService : IDefaultHttpClientCrud<TokenServiceSetting, TokenViewModel, string>
  {
    Task<string> GetSysToken();
  }
}
