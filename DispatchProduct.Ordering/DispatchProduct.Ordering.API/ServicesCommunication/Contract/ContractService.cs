﻿using DispatchProduct.Ordering.API.ServicesViewModels;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using DispatchProduct.Ordering.API.Settings;

namespace DispatchProduct.Ordering.API.ServicesCommunication
{
    public class ContractService : DefaultHttpClientCrud<ContractServiceSetting, ContractViewModel, ContractViewModel>, IContractService
    {
        ContractServiceSetting settings;
        public ContractService(IOptions<ContractServiceSetting> _settings) :base(_settings.Value)
        {
            settings = _settings.Value;
        }
    }
}
