﻿using System;

namespace DispatchProduct.Ordering.API.ViewModel
{
  public class PreventiveMaintainenceScheduleViewModel : BaseEntityViewModel
  {
    public int Id { get; set; }

    public string ContractNumber { get; set; }

    public DateTime OrderDate { get; set; }

    public int FK_Customer_Id { get; set; }

    public int FK_Contract_Id { get; set; }

    public int FK_Location_Id { get; set; }

    public int FK_OrderPriority_Id { get; set; }

    public int FK_OrderType_Id { get; set; }

    public int FK_OrderProblem_Id { get; set; }

    public string QuotationRefNo { get; set; }

    public int FK_Order_Id { get; set; }
  }
}
