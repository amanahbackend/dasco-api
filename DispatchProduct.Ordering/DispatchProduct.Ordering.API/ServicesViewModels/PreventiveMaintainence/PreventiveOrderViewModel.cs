﻿namespace DispatchProduct.Ordering.API.ViewModel
{
  public class PreventiveOrderViewModel : BaseEntityViewModel
  {
    public int FK_PreventiveMaintainence_Id { get; set; }

    public int FK_Order_Id { get; set; }
  }
}
