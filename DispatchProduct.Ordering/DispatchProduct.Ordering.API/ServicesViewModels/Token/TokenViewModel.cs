﻿namespace DispatchProduct.Ordering.API.ViewModel
{
  public class TokenViewModel
  {
    public string Username { get; set; }

    public string Password { get; set; }
  }
}
