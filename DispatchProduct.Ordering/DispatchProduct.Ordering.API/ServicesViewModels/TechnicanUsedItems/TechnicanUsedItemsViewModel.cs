﻿namespace DispatchProduct.Ordering.API.ViewModel
{
  public class TechnicianUsedItemsViewModel : BaseEntityViewModel
  {
    public int Id { get; set; }

    public int ItemId { get; set; }

    public int Amount { get; set; }

    public string FK_Technician_Id { get; set; }

    public int FK_Order_Id { get; set; }

    public bool IsReleased { get; set; }
  }
}
