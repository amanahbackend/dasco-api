﻿using DispatchProduct.Ordering.API.ViewModel;
using System;

namespace DispatchProduct.Ordering.API.ServicesViewModels
{
  public class ContractViewModel : BaseEntityViewModel
  {
    public int Id { get; set; }

    public string ContractNumber { get; set; }

    public DateTime StartDate { get; set; }

    public DateTime EndDate { get; set; }

    public double Price { get; set; }

    public string Remarks { get; set; }

    public bool HasPreventiveMaintainence { get; set; }

    public int PreventivePeriod { get; set; }

    public int FK_Customer_Id { get; set; }

    public int FK_ContractType_Id { get; set; }
  }
}
