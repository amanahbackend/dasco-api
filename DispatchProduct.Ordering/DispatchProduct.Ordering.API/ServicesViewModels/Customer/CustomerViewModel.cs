﻿using DispatchProduct.Ordering.API.ViewModel;
using System.Collections.Generic;

namespace DispatchProduct.Ordering.API.ServicesViewModels
{
    public class CustomerViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string CivilId { get; set; }

        public string Remarks { get; set; }

        public int FK_CustomerType_Id { get; set; }

        public string PhoneNumber { get; set; }

        public int FK_CallType_Id { get; set; }

        public int FK_PhoneType_Id { get; set; }

        public ICollection<ComplainViewModel> Complains { get; set; }

        public ICollection<CustomerPhoneBookViewModel> CustomerPhoneBook { get; set; }

        public ICollection<LocationViewModel> Locations { get; set; }

        public CustomerTypeViewModel CustomerType { get; set; }

    }
}
