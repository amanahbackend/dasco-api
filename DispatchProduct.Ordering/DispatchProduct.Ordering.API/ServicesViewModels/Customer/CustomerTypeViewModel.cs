﻿using DispatchProduct.Ordering.API.ViewModel;

namespace DispatchProduct.Ordering.API.ServicesViewModels
{
    public class CustomerTypeViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
