﻿
using DispatchProduct.Inventory.Context;
using DispatchProduct.Ordering.Settings;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Repoistry;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchProduct.Ordering.API.Seed
{
    public class OrderDbContextSeed : ContextSeed
    {
        OrderAppSettings orderAppSettings=null;
        public async Task SeedAsync(OrderDbContext context, IHostingEnvironment env,
            ILogger<OrderDbContextSeed> logger, IOptions<OrderAppSettings> settings, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;

            try
            {
                orderAppSettings = settings.Value;
                var useCustomizationData = settings.Value.UseCustomizationData;
                var contentRootPath = env.ContentRootPath;
                var webroot = env.WebRootPath;
                List<Order> Orders = new List<Order>();
                List<OrderPriority> OrderPriority = new List<OrderPriority>();
                List<OrderStatus> OrderStatus = new List<OrderStatus>();
                List<OrderType> OrderType = new List<OrderType>();
                List<ProgressStatus> ProgressStatus = new List<ProgressStatus>();

                if (useCustomizationData)
                {
                    //from file e.g (look at ApplicationDbContextSeed)
                }
                else
                {
                    //default from here
                    OrderType = GetDefaultOrderTypes();
                    OrderStatus = GetDefaultOrderStatus();
                    OrderPriority = GetDefaultOrderPriority();
                    ProgressStatus = GetDefaultProgressStatus();
                }
                await SeedEntityAsync(context, OrderType);
            
                await SeedEntityAsync(context, OrderStatus);
           
                await SeedEntityAsync(context, OrderPriority);

                await SeedEntityAsync(context, ProgressStatus);
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 3)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for ApplicationDbContext");

                    await SeedAsync(context, env, logger, settings, retryForAvaiability);
                }
            }
        }
        private List<OrderType> GetDefaultOrderTypes()
        {
            List<OrderType> result = new List<OrderType>
            {
                new OrderType() { Name = "Normal" },
                new OrderType() { Name = "Special" }
            };
            return result;
        }

        private List<OrderStatus> GetDefaultOrderStatus()
        {
            List<OrderStatus> result = new List<OrderStatus>
            {
                new OrderStatus() { Name = "Started" },
                new OrderStatus() { Name = "Mounted" },
                new OrderStatus() { Name = "CustomerNotExist" },
            };
            if (orderAppSettings != null && orderAppSettings.OrderCancelStatus!=null&& orderAppSettings.OrderCompleteStatus!=null)
            {
                result.Add(new OrderStatus() { Name = orderAppSettings.OrderCancelStatus });
                result.Add(new OrderStatus() { Name = orderAppSettings.OrderCompleteStatus });
            }
            return result;
        }

        private List<OrderPriority> GetDefaultOrderPriority()
        {
            List<OrderPriority> result = new List<OrderPriority>
            {
                new OrderPriority() { Name = "High" },
                new OrderPriority() { Name = "Medium" },
                new OrderPriority() { Name = "Low" }
            };
            return result;
        }

        private List<ProgressStatus> GetDefaultProgressStatus()
        {
            List<ProgressStatus> result = new List<ProgressStatus>
            {
                new ProgressStatus() { Name =orderAppSettings.TransferProgressStatusKey},
                new ProgressStatus() { Name =orderAppSettings.IntialProgressStatusKey},
                new ProgressStatus() { Name = "Holded" },
                new ProgressStatus() { Name = "Completed" }
            };
            return result;
        }

    }
}
