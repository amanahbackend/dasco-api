﻿using AutoMapper;
using DispatchProduct.Ordering.API.ViewModel;
using DispatchProduct.Ordering.BLL.Filters;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Vehicles.Entities;
using System.Collections.Generic;
using System.Linq;

namespace DispatchProduct.Ordering.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<FilterOrderViewModelByDispatcher, FilterOrderByDispatcher>();
            CreateMap<FilterOrderByDispatcher, FilterOrderViewModelByDispatcher>();

            CreateMap<OrderPriority, OrderPriorityViewModel>();
            CreateMap<OrderPriorityViewModel, OrderPriority>();

            CreateMap<OrderFiles, OrderFilesViewModel>()
              .ForMember(dest => dest.FileURL, opt => opt.Ignore());
            CreateMap<OrderFilesViewModel, OrderFiles>();

            CreateMap<AreaProblems, OrderDistributionCriteriaViewModel>()
              .ForMember(dest => dest.Id, opt => opt.Ignore())
              .ForMember(dest => dest.OrderProblem, opt => opt.Ignore())
              .ForMember(dest => dest.Dispatcher, opt => opt.Ignore())
              .ForMember(dest => dest.FK_OrderProblem_Id, opt => opt.Ignore())
              .ForMember(dest => dest.FK_Dispatcher_Id, opt => opt.Ignore());
            CreateMap<AreaProblems, IList<OrderDistributionCriteriaViewModel>>()
                .ConstructUsing(areaProb => areaProb.FK_OrderProblem_Ids.Select(probId => CreateOrderDistributionCriteriaViewModel(areaProb, probId)).ToList());


            CreateMap<OrderMultiDistributionCriteriaViewModel, OrderDistributionCriteria>()
              .ForMember(dest => dest.OrderProblem, opt => opt.Ignore())
              .ForMember(dest => dest.Area, opt => opt.Ignore())
              .ForMember(dest => dest.Governorate, opt => opt.Ignore())
              .ForMember(dest => dest.FK_OrderProblem_Id, opt => opt.Ignore())
              .ForMember(dest => dest.Id, opt => opt.Ignore());
            CreateMap<OrderMultiDistributionCriteriaViewModel, IList<OrderDistributionCriteria>>()
                .ConstructUsing(ordMultiCrit => CreateOrderDistributionCriteriaViewModel(ordMultiCrit));


            CreateMap<AssignedMultiTechniciansViewModel, AssignedTechnicians>()
             .ForMember(dest => dest.FK_Technician_Id, opt => opt.Ignore());
            CreateMap<AssignedMultiTechniciansViewModel, IList<AssignedTechnicians>>()
                .ConstructUsing(x => x.FK_Technicians_Id.Select(y => CreateAssignedTechniciansViewModel(x, y)).ToList());



            CreateMap<Order, OrderViewModel>()
                .ForMember(dest => dest.OrderPriority, opt => opt.MapFrom(src => src.OrderPriority))
                .ForMember(dest => dest.OrderType, opt => opt.MapFrom(src => src.OrderType))
                .ForMember(dest => dest.OrderStatus, opt => opt.MapFrom(src => src.OrderStatus))
                .ForMember(dest => dest.LstOrderProgress, opt => opt.MapFrom(src => src.LstOrderProgress))
                .ForMember(dest => dest.OrderFiles, opt => opt.MapFrom(src => src.OrderFiles))
                .ForMember(dest => dest.Customer, opt => opt.Ignore())
                .ForMember(dest => dest.Location, opt => opt.Ignore())
                .ForMember(dest => dest.TechnicianName, opt => opt.Ignore())
                .ForMember(dest => dest.DispatcherName, opt => opt.Ignore())
                .ForMember(dest => dest.Contract, opt => opt.Ignore());
            CreateMap<OrderViewModel, Order>()
                .ForMember(dest => dest.OrderPriority, opt => opt.MapFrom(src => src.OrderPriority))
                .ForMember(dest => dest.LstOrderProgress, opt => opt.MapFrom(src => src.LstOrderProgress))
                .ForMember(dest => dest.OrderType, opt => opt.MapFrom(src => src.OrderType))
                .ForMember(dest => dest.OrderStatus, opt => opt.MapFrom(src => src.OrderStatus))
                .ForMember(dest => dest.OrderFiles, opt => opt.MapFrom(src => src.OrderFiles));

            CreateMap<OrderStatus, OrderStatusViewModel>();
            CreateMap<OrderStatusViewModel, OrderStatus>();

            CreateMap<OrderProblem, OrderProblemViewModel>();
            CreateMap<OrderProblemViewModel, OrderProblem>();

            CreateMap<MobileLocation, MobileLocationViewModel>();
            CreateMap<MobileLocationViewModel, MobileLocation>();

            CreateMap<Vehicle, VehicleViewModel>();
            CreateMap<VehicleViewModel, Vehicle>();

            CreateMap<OrderDistributionCriteria, OrderDistributionCriteriaViewModel>()
                 .ForMember(dest => dest.OrderProblem, opt => opt.MapFrom(src => src.OrderProblem))
                 .ForMember(dest => dest.Dispatcher, opt => opt.MapFrom(src => src.Dispatcher));
            CreateMap<OrderDistributionCriteriaViewModel, OrderDistributionCriteria>()
                .ForMember(dest => dest.OrderProblem, opt => opt.MapFrom(src => src.OrderProblem))
                .ForMember(dest => dest.Dispatcher, opt => opt.MapFrom(src => src.Dispatcher));

            CreateMap<OrderProgress, OrderProgressViewModel>()
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy))
                .ForMember(dest => dest.TechnicianName, opt => opt.Ignore())
                .ForMember(dest => dest.Order, opt => opt.MapFrom(src => src.Order))
                .ForMember(dest => dest.ProgressStatus, opt => opt.MapFrom(src => src.ProgressStatus));
            CreateMap<OrderProgressViewModel, OrderProgress>()
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy))
                .ForMember(dest => dest.Order, opt => opt.MapFrom(src => src.Order))
                .ForMember(dest => dest.ProgressStatus, opt => opt.MapFrom(src => src.ProgressStatus));

            CreateMap<ProgressStatus, ProgressStatusViewModel>()
                .ForMember(dest => dest.OrderStatus, opt => opt.MapFrom(src => src.OrderStatus));
            CreateMap<ProgressStatusViewModel, ProgressStatus>()
                .ForMember(dest => dest.OrderStatus, opt => opt.MapFrom(src => src.OrderStatus));

            CreateMap<OrderType, OrderTypeViewModel>();
            CreateMap<OrderTypeViewModel, OrderType>();

            CreateMap<ApplicationUser, ApplicationUserViewModel>()
                .ForMember(dest => dest.RoleNames, opt => opt.MapFrom(src => src.RoleNames));
            CreateMap<ApplicationUserViewModel, ApplicationUser>()
                .ForMember(dest => dest.RoleNames, opt => opt.MapFrom(src => src.RoleNames));

            CreateMap<Technician, TechnicianViewModel>();
            CreateMap<TechnicianViewModel, Technician>();

            CreateMap<ApplicationUserViewModel, Technician>()
                .ForMember(dest => dest.Orders, opt => opt.Ignore());
            CreateMap<Technician, ApplicationUserViewModel>();

            CreateMap<ApplicationUserViewModel, Dispatcher>()
               .ForMember(dest => dest.Orders, opt => opt.Ignore());
            CreateMap<Dispatcher, ApplicationUserViewModel>();

            CreateMap<DispatcherViewModel, Dispatcher>();
            CreateMap<Dispatcher, DispatcherViewModel>();

            CreateMap<PreventiveMaintainenceScheduleViewModel, Order>()
                .ForMember(dest => dest.FK_Contract_Id, opt => opt.MapFrom(src => src.FK_Contract_Id))
                .ForMember(dest => dest.FK_Customer_Id, opt => opt.MapFrom(src => src.FK_Customer_Id))
                .ForMember(dest => dest.FK_Location_Id, opt => opt.MapFrom(src => src.FK_Location_Id))
                .ForMember(dest => dest.FK_OrderPriority_Id, opt => opt.MapFrom(src => src.FK_OrderPriority_Id))
                .ForMember(dest => dest.FK_OrderType_Id, opt => opt.MapFrom(src => src.FK_OrderType_Id))
                .ForMember(dest => dest.FK_OrderProblem_Id, opt => opt.MapFrom(src => src.FK_OrderProblem_Id))
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.OrderDate))

                .ForMember(dest => dest.QuotationRefNo, opt => opt.Ignore())
                .ForMember(dest => dest.OrderFiles, opt => opt.Ignore())
                .ForMember(dest => dest.Area, opt => opt.Ignore())
                 .ForMember(dest => dest.SignatureURL, opt => opt.Ignore())
                .ForMember(dest => dest.SignaturePath, opt => opt.Ignore())
                .ForMember(dest => dest.SignatureContractURL, opt => opt.Ignore())
                .ForMember(dest => dest.SignatureContractPath, opt => opt.Ignore())
                .ForMember(dest => dest.SignatureContractPath, opt => opt.Ignore())
                .ForMember(dest => dest.EndDate, opt => opt.Ignore())
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Price, opt => opt.Ignore())
                .ForMember(dest => dest.Code, opt => opt.Ignore())
                .ForMember(dest => dest.OrderPriority, opt => opt.Ignore())
                .ForMember(dest => dest.OrderType, opt => opt.Ignore())
                .ForMember(dest => dest.OrderProblem, opt => opt.Ignore())
                .ForMember(dest => dest.Note, opt => opt.Ignore())
                .ForMember(dest => dest.FK_OrderStatus_Id, opt => opt.Ignore())
                .ForMember(dest => dest.OrderStatus, opt => opt.Ignore())
                .ForMember(dest => dest.PreferedVisitTime, opt => opt.Ignore())
                .ForMember(dest => dest.LstOrderProgress, opt => opt.Ignore())
                .ForMember(dest => dest.FK_Technician_Id, opt => opt.Ignore())
                .ForMember(dest => dest.FK_Dispatcher_Id, opt => opt.Ignore());


            this.CreateMap<FilteredOrderViewModel, OrderViewModel>()
                .AfterMap(((src, dst) => Mapper.Map(src.Order, dst)))
                .ForAllOtherMembers((dest => dest.Ignore()));
            this.CreateMap<OrderViewModel, FilteredOrderViewModel>()
                .ForMember(dest => dest.Order,opt => opt.MapFrom(src => src))
                .ForAllOtherMembers((dest => dest.Ignore()));
            this.CreateMap<OrderProgressViewModel, FilteredOrderViewModel>()
                .ForMember(dest => dest.OrderProgress,opt => opt.MapFrom(src => src))
                .ForAllOtherMembers((dest => dest.Ignore()));
            this.CreateMap<FilteredOrderViewModel, OrderProgressViewModel>()
                .AfterMap((src, dst) => Mapper.Map(src.OrderProgress, dst))
                .ForAllOtherMembers((dest => dest.Ignore()));
        }

        private AssignedTechnicians CreateAssignedTechniciansViewModel(AssignedMultiTechniciansViewModel multitechnicians, string technician_Id)
        {
            var technician = Mapper.Map<AssignedMultiTechniciansViewModel, AssignedTechnicians>(multitechnicians);
            technician.FK_Technician_Id = technician_Id;
            return technician;
        }
        private List<OrderDistributionCriteria> CreateOrderDistributionCriteriaViewModel(OrderMultiDistributionCriteriaViewModel multiCriteria)
        {
            List<OrderDistributionCriteriaViewModel> resultVM = new List<OrderDistributionCriteriaViewModel>();
            List<OrderDistributionCriteria> result = null;
            foreach (var problem in multiCriteria.AreaProblems)
            {
                var criteria = Mapper.Map<AreaProblems, IList<OrderDistributionCriteriaViewModel>>(problem);

                foreach (var item in criteria)
                {
                    item.FK_Dispatcher_Id = multiCriteria.FK_Dispatcher_Id;
                    item.Dispatcher = multiCriteria.Dispatcher;
                }
                resultVM.AddRange(criteria.ToList());
            }
            result = Mapper.Map<List<OrderDistributionCriteriaViewModel>, List<OrderDistributionCriteria>>(resultVM);

            return result;
        }
        private OrderDistributionCriteriaViewModel CreateOrderDistributionCriteriaViewModel(AreaProblems problems, int probId)
        {
            var criteria = Mapper.Map<AreaProblems, OrderDistributionCriteriaViewModel>(problems);
            criteria.FK_OrderProblem_Id = probId;
            return criteria;
        }
    }
}
