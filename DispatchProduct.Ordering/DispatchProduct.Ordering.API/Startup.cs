﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using DispatchProduct.Inventory.Context;
using Microsoft.EntityFrameworkCore;
using DispatchProduct.Ordering.Settings;
using Swashbuckle.AspNetCore.Swagger;
using AutoMapper;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using DispatchProduct.Repoistry;
using System.IdentityModel.Tokens.Jwt;
using IdentityServer4.AccessTokenValidation;
using DispatchProduct.Ordering.Entities;
using DispatchProduct.Ordering.IEntities;
using DispatchProduct.Ordering.BLL.Managers;
using DispatchProduct.Ordering.API.Settings;
using DispatchProduct.Ordering.API.ServicesCommunication;
using DispatchProduct.Ordering.API.ServicesCommunication.PreventiveMaintainence;
using DispatchProduct.Ordering.API.Tasks;
using DispatchProduct.Vehicles.Entities;
using DispatchProduct.Vehicles.BLL.Managers;
using DispatchProduct.Ordering.API.Controllers;
using DispatchProduct.Ordering.Hubs;
using Microsoft.Extensions.FileProviders;
using System.IO;

namespace DispatchProduct.Ordering.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });
            services.AddDbContext<OrderDbContext>(options =>
            options.UseSqlServer(Configuration["ConnectionString"],
            sqlOptions => sqlOptions.MigrationsAssembly("DispatchProduct.Ordering.EFCore.MSSQL")));

            services.AddOptions();
            services.Configure<OrderAppSettings>(Configuration);
            services.Configure<OrderingHubSettings>(Configuration.GetSection("OrderingHubSettings"));
            services.Configure<CustomerServiceSetting>(Configuration.GetSection("CustomerServiceSetting"));
            services.Configure<ContractServiceSetting>(Configuration.GetSection("ContractServiceSetting"));
            services.Configure<TechnicianUsedItemServiceSetting>(Configuration.GetSection("TechnicianUsedItemServiceSetting"));
            services.Configure<LocationServiceSetting>(Configuration.GetSection("LocationServiceSetting"));
            services.Configure<UserServiceSetting>(Configuration.GetSection("UserServiceSetting"));
            services.Configure<PreventiveMaintainenceServiceSetting>(Configuration.GetSection("PreventiveMaintainenceServiceSetting"));
            services.Configure<TokenServiceSetting>(Configuration.GetSection("TokenServiceSetting"));


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new Info()
                    {
                        Title = "Calling API",
                        Description = "Calling  API"
                    });
                c.AddSecurityDefinition("oauth2", new OAuth2Scheme
                {
                    Type = "oauth2",
                    Flow = "implicit",
                    AuthorizationUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/authorize",
                    TokenUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/token",
                    Scopes = new Dictionary<string, string>()
                    {
                        { "calling", "calling API" }
                    }
                });
            });

            ConfigureAuthService(services);

            services.AddMvc();
            services.AddSignalR();
            services.AddAutoMapper(typeof(Startup));
            Mapper.AssertConfigurationIsValid();

            services.AddScoped<DbContext, OrderDbContext>();
            services.AddScoped(typeof(IOrderProblem), typeof(OrderProblem));
            services.AddScoped(typeof(IOrderDistributionCriteria), typeof(OrderDistributionCriteria));
            services.AddScoped(typeof(IMobileLocation), typeof(MobileLocation));
            services.AddScoped(typeof(IVehicle), typeof(Vehicle));
            services.AddScoped(typeof(IRepositry<>), typeof(Repositry<>));
            services.AddScoped(typeof(IOrder), typeof(Order));
            services.AddScoped(typeof(IOrderPriority), typeof(OrderPriority));
            services.AddScoped(typeof(IOrderFiles), typeof(OrderFiles));
            services.AddScoped(typeof(IOrderStatus), typeof(OrderStatus));
            services.AddScoped(typeof(IOrderType), typeof(OrderType));
            services.AddTransient(typeof(IOrderProgress), typeof(OrderProgress));
            services.AddScoped(typeof(IAssignedTechnicians), typeof(AssignedTechnicians));
            services.AddScoped(typeof(IProgressStatus), typeof(ProgressStatus));
            services.AddScoped(typeof(IOrderManager), typeof(OrderManager));
            services.AddScoped(typeof(IOrderPriorityManager), typeof(OrderPriorityManager));
            services.AddScoped(typeof(IOrderFilesManager), typeof(OrderFilesManager));
            services.AddScoped(typeof(IOrderStatusManager), typeof(OrderStatusManager));
            services.AddScoped(typeof(IOrderTypeManager), typeof(OrderTypeManager));
            services.AddScoped(typeof(ICustomerService), typeof(CustomerService));
            services.AddScoped(typeof(IUserService), typeof(UserService));
            services.AddScoped(typeof(ILocationService), typeof(LocationService));
            services.AddScoped(typeof(IPreventiveMaintainenceService), typeof(PreventiveMaintainenceService));
            services.AddScoped(typeof(ITokenService), typeof(TokenService));
            services.AddScoped(typeof(IOrderProgressManager), typeof(OrderProgressManager));
            services.AddScoped(typeof(IAssignedTechniciansManager), typeof(AssignedTechniciansManager));
            services.AddScoped(typeof(IProgressStatusManager), typeof(ProgressStatusManager));
            services.AddScoped(typeof(IOrderProblemManager), typeof(OrderProblemManager));
            services.AddScoped(typeof(IOrderDistributionCriteriaManager), typeof(OrderDistributionCriteriaManager));
            services.AddScoped(typeof(IMobileLocationManager), typeof(MobileLocationManager));
            services.AddScoped(typeof(IVehicleManager), typeof(VehicleManager));
            services.AddScoped(typeof(OrderController), typeof(OrderController));
            services.AddTransient(typeof(IPreventiveOrderTask), typeof(PreventiveOrderTask));
            services.AddTransient(typeof(IContractService), typeof(ContractService));
            services.AddTransient(typeof(ITechnicianUsedItemService), typeof(TechnicianUsedItemService));
            services.AddTransient(typeof(ILocationService), typeof(LocationService));
            services.AddTransient(typeof(IOrderingHub), typeof(OrderingHub));
            //services.AddHangfire(x => x.UseSqlServerStorage(Configuration["ConnectionString"]));
            var container = new ContainerBuilder();
            container.Populate(services);
            var serviceProvider = new AutofacServiceProvider(container.Build());
           // GlobalConfiguration.Configuration.UseActivator(new HangfireActivator(serviceProvider));
            return serviceProvider;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCors("AllowAll");
            //app.UseHangfireDashboard();
            //app.UseHangfireServer();
            //RecurringJob.AddOrUpdate<IPreventiveOrderTask>(pre => pre.AddPreventiveOrder(), Cron.Daily);
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            var pathBase = Configuration["PATH_BASE"];
            if (!string.IsNullOrEmpty(pathBase))
            {
                loggerFactory.CreateLogger("init").LogDebug($"Using PATH BASE '{pathBase}'");
                app.UsePathBase(pathBase);
            }

            app.UseSignalR(routes =>  // <-- SignalR
            {
                routes.MapHub<OrderingHub>("orderingHub");
            });

            ConfigureAuth(app);
            app.UseStaticFiles();
            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(
                 Path.Combine(Directory.GetCurrentDirectory(), "OrderSignatures")),
                RequestPath = "/OrderSignatures",
                EnableDirectoryBrowsing = true
            });
            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(
               Path.Combine(Directory.GetCurrentDirectory(), "OrderContractSignatures")),
                RequestPath = "/OrderContractSignatures",
                EnableDirectoryBrowsing = true
            });
            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(
              Path.Combine(Directory.GetCurrentDirectory(), "OrderFiles")),
                RequestPath = "/OrderFiles",
                EnableDirectoryBrowsing = true
            });
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Calling API");
            });


            // Make work identity server redirections in Edge and lastest versions of browers. WARN: Not valid in a production environment.
            //app.Use(async (context, next) =>
            //{
            //    context.Response.Headers.Add("Content-Security-Policy", "script-src 'unsafe-inline'");
            //    await next();
            //});

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private void ConfigureAuthService(IServiceCollection services)
        {
            // prevent from mapping "sub" claim to nameidentifier.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            var identityUrl = Configuration.GetValue<string>("IdentityUrl");
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
            .AddIdentityServerAuthentication(options =>
            {
                // base-address of your identityserver
                options.Authority = identityUrl;

                // name of the API resource
                options.ApiName = Configuration["ClientId"];
                options.ApiSecret = Configuration["Secret"];
                options.RequireHttpsMetadata = false;
                options.EnableCaching = true;
                options.CacheDuration = TimeSpan.FromMinutes(10);
                options.SaveToken = true;
            });

        }

        protected virtual void ConfigureAuth(IApplicationBuilder app)
        {
            app.UseAuthentication();
        }
    }
}
