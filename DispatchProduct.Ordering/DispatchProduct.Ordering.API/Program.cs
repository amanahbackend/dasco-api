﻿using System;
using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using DispatchProduct.Inventory.Context;
using DispatchProduct.Ordering.API.Seed;
using DispatchProduct.Ordering.Settings;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.DependencyInjection;

namespace DispatchProduct.Ordering.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).MigrateDbContext<OrderDbContext>((context, services) =>
            {
                Seed(context, services);

                //StartPreventiveProcess(context, services);
            }).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
         WebHost.CreateDefaultBuilder(args)
             .UseKestrel()
             .UseContentRoot(Directory.GetCurrentDirectory())
             .UseIISIntegration()
             .UseStartup<Startup>()
              .UseSetting("detailedErrors", "true")
              .CaptureStartupErrors(true)
             .ConfigureLogging((hostingContext, builder) =>
             {
                 builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                 builder.AddConsole();
                 builder.AddDebug();
             })
             .Build();
        public static void Seed(OrderDbContext context, IServiceProvider services)
        {
            var env = services.GetService<IHostingEnvironment>();
            var logger = services.GetService<ILogger<OrderDbContextSeed>>();
            var settings = services.GetService<IOptions<OrderAppSettings>>();
            new OrderDbContextSeed()
                .SeedAsync(context, env, logger, settings)
                .Wait();

        }
    }
}
