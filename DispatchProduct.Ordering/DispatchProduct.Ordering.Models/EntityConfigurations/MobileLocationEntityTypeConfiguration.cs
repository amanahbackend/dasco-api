﻿using DispatchProduct.Ordering.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Ordering.EntityConfigurations
{
    public class MobileLocationEntityTypeConfiguration
        : IEntityTypeConfiguration<MobileLocation>
    {
        public void Configure(EntityTypeBuilder<MobileLocation> MobileLocationConfiguration)
        {
            MobileLocationConfiguration.ToTable("MobileLocation");

            MobileLocationConfiguration.HasKey(o => o.ID);

            MobileLocationConfiguration.Property(o => o.ID)
                .ForSqlServerUseSequenceHiLo("MobileLocationseq");


            MobileLocationConfiguration.Property(o => o.Lat)
                .IsRequired();
            MobileLocationConfiguration.Property(o => o.Long)
                .IsRequired();
            MobileLocationConfiguration.Property(o => o.FK_Technician_Id)
              .IsRequired();
        }
    }
}
