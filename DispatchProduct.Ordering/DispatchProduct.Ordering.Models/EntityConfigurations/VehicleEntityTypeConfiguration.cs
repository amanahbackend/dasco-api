﻿using DispatchProduct.Vehicles.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Ordering.EntityConfigurations
{
    public class VehicleEntityTypeConfiguration
        : IEntityTypeConfiguration<Vehicle>
    {
        public void Configure(EntityTypeBuilder<Vehicle> VehicleConfiguration)
        {
            VehicleConfiguration.ToTable("Vehicle");

            VehicleConfiguration.HasKey(o => o.ID);

            VehicleConfiguration.Property(o => o.ID)
                .ForSqlServerUseSequenceHiLo("Vehicleseq");


            VehicleConfiguration.Property(o => o.Lat)
                .IsRequired();
            VehicleConfiguration.Property(o => o.Long)
                .IsRequired();
            VehicleConfiguration.Property(o => o.VID)
                .IsRequired();
            VehicleConfiguration.Property(o => o.PanicButton)
                .IsRequired();
        }
    }
}
