﻿using DispatchProduct.Ordering.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Ordering.EntityConfigurations
{
    public class OrderTypeEntityTypeConfiguration
        : IEntityTypeConfiguration<OrderType>
    {
        public void Configure(EntityTypeBuilder<OrderType> OrderTypeConfiguration)
        {
            OrderTypeConfiguration.ToTable("OrderType");

            OrderTypeConfiguration.HasKey(o => o.Id);

            OrderTypeConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("OrderTypeseq");


            OrderTypeConfiguration.Property(o => o.Name)
                .HasMaxLength(500)
                .IsRequired();
        }
    }
}
