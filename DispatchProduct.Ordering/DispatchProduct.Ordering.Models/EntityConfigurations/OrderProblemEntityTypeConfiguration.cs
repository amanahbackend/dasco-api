﻿using DispatchProduct.Ordering.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Ordering.EntityConfigurations
{
    public class OrderProblemEntityTypeConfiguration
        : IEntityTypeConfiguration<OrderProblem>
    {
        public void Configure(EntityTypeBuilder<OrderProblem> OrderProblemConfiguration)
        {
            OrderProblemConfiguration.ToTable("OrderProblem");

            OrderProblemConfiguration.HasKey(o => o.Id);

            OrderProblemConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("OrderProblemseq");
            OrderProblemConfiguration.Property(o => o.Name)
                .IsRequired();
        }
    }
}
