﻿using DispatchProduct.Ordering.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Ordering.EntityConfigurations
{
    public class AssignedTechniciansEntityTypeConfiguration
        : IEntityTypeConfiguration<AssignedTechnicians>
    {
        public void Configure(EntityTypeBuilder<AssignedTechnicians> OrderStatusConfiguration)
        {
            OrderStatusConfiguration.ToTable("AssignedTechnicians");

            OrderStatusConfiguration.HasKey(o => o.Id);

            OrderStatusConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("OrderStatusseq");

            OrderStatusConfiguration.Property(o => o.FK_Technician_Id)
                .IsRequired();
            OrderStatusConfiguration.Property(o => o.FK_Dispatcher_Id)
               .IsRequired();
        }
    }
}
