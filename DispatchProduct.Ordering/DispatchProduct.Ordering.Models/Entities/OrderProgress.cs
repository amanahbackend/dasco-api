﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.Ordering.Entities
{
    public class OrderProgress : BaseEntity, IOrderProgress, IBaseEntity
    {
        public int Id { get; set; }

        public ApplicationUser CreatedBy { get; set; }

        public string FK_Technician_Id { get; set; }

        public string Note { get; set; }

        public int FK_Order_Id { get; set; }

        public Order Order { get; set; }

        public int FK_ProgressStatus_Id { get; set; }

        public ProgressStatus ProgressStatus { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }
    }
}
