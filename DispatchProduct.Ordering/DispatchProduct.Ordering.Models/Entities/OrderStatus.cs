﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.Ordering.IEntities;

namespace DispatchProduct.Ordering.Entities
{
    public class OrderStatus : BaseEntity, IOrderStatus
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
