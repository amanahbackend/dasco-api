﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.Ordering.IEntities;

namespace DispatchProduct.Ordering.Entities
{
    public class AssignedTechnicians : BaseEntity, IAssignedTechnicians
    {
        public int Id { get; set; }

        public string FK_Technician_Id { get; set; }

        public string FK_Dispatcher_Id { get; set; }
    }
}
