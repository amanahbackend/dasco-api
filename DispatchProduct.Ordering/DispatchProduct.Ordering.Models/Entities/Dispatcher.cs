﻿using System.Collections.Generic;

namespace DispatchProduct.Ordering.Entities
{
    public class Dispatcher : ApplicationUser
    {
        public List<Order> Orders { get; set; }
    }
}
