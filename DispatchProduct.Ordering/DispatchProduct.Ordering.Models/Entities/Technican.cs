﻿
using System.Collections.Generic;

namespace DispatchProduct.Ordering.Entities
{
    public class Technician : ApplicationUser
    {
        public List<Order> Orders { get; set; }
    }
}
