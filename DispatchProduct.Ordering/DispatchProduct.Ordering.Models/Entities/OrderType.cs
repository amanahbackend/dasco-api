﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;

namespace DispatchProduct.Ordering.Entities
{
    public class OrderType : BaseEntity, IOrderType
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

    }
}
