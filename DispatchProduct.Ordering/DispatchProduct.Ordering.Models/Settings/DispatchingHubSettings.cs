﻿namespace DispatchProduct.Ordering.Settings
{
    public class OrderingHubSettings
    {
        public string token { get; set; }

        public string userId { get; set; }

        public string roles { get; set; }

        public string AssignOrderToDispatcher { get; set; }

        public string UpdateOrder { get; set; }

        public string AssignOrderToTechnician { get; set; }

        public string ChangeOrderProgress { get; set; }
    }
}

