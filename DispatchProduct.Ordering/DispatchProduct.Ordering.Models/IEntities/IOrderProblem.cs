﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.Ordering.IEntities
{
    public interface IOrderProblem : IBaseEntity
    {
        int Id { get; set; }

        string Name { get; set; }
    }
}
