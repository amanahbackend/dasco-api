﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.Ordering.Entities
{
    public interface IOrderDistributionCriteria : IBaseEntity
    {
        int Id { get; set; }

        int FK_OrderProblem_Id { get; set; }

        OrderProblem OrderProblem { get; set; }

        string Area { get; set; }

        string FK_Dispatcher_Id { get; set; }

        ApplicationUser Dispatcher { get; set; }
    }
}

