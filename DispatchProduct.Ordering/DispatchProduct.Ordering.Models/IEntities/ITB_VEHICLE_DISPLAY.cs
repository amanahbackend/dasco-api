﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.Ordering.Entities
{
    public interface ITB_VEHICLE_DISPLAY : ITrackingLocation, IBaseEntity
    {
    }
}
