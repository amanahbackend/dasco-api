﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.Ordering.Entities
{
    public interface IProgressStatus : IBaseEntity
    {
        int Id { get; set; }

        string Name { get; set; }
    }
}