﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.Ordering.IEntities
{
    public interface IOrderFiles : IBaseEntity
    {
        int Id { get; set; }
        string FileName { get; set; }
        string FileRelativePath { get; set; }
        int OrderId { get; set; }
    }
}
