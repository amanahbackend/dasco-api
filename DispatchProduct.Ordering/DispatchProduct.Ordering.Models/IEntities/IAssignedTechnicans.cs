﻿namespace DispatchProduct.Ordering.IEntities
{
    public interface IAssignedTechnicians
    {
        int Id { get; set; }

        string FK_Technician_Id { get; set; }

        string FK_Dispatcher_Id { get; set; }
    }
}
