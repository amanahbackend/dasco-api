﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Ordering.Entities;
using System;
using System.Collections.Generic;

namespace DispatchProduct.Ordering.IEntities
{
    public interface IOrder : IBaseEntity
    {
        int Id { get; set; }

        string Code { get; set; }

        int FK_Customer_Id { get; set; }

        int FK_Contract_Id { get; set; }

        int FK_Location_Id { get; set; }

        string QuotationRefNo { get; set; }

        double Price { get; set; }

        int FK_OrderPriority_Id { get; set; }

        OrderPriority OrderPriority { get; set; }

        int FK_OrderType_Id { get; set; }

        OrderType OrderType { get; set; }

        int FK_OrderStatus_Id { get; set; }

        OrderStatus OrderStatus { get; set; }

        int FK_OrderProblem_Id { get; set; }

        OrderProblem OrderProblem { get; set; }

        DateTime PreferedVisitTime { get; set; }

        string Note { get; set; }

        DateTime StartDate { get; set; }

        DateTime EndDate { get; set; }

        string FK_Technician_Id { get; set; }

        string FK_Dispatcher_Id { get; set; }

        List<OrderProgress> LstOrderProgress { get; set; }

        string SignatureURL { get; set; }

        string SignaturePath { get; set; }

        string SignatureContractURL { get; set; }

        string SignatureContractPath { get; set; }
        List<OrderFiles> OrderFiles { get; set; }

    }
}
