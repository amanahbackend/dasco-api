﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.Ordering.Entities
{
    public interface IMobileLocation : ITrackingLocation, IBaseEntity
    {
        string FK_Technician_Id { get; set; }
    }
}
