﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.CustomerModule.IEntities;
using System.Collections.Generic;

namespace DispatchProduct.CustomerModule.Entities
{
    public class CustomerHistory : BaseEntity, ICustomerHistory, IBaseEntity
    {
        public Customer Customer { get; set; }

        public IEnumerable<Complain> Complains { get; set; }
    }
}
