﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.CustomerModule.IEntities;

namespace DispatchProduct.CustomerModule.Entities
{
    public class Location : BaseEntity, ILocation, IBaseEntity
    {
        public int Id { get; set; }

        public string PACINumber { get; set; }

        public string Title { get; set; }

        public string Governorate { get; set; }

        public string Area { get; set; }

        public string Block { get; set; }

        public string Street { get; set; }

        public string AddressNote { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public int Fk_Customer_Id { get; set; }

        public Customer Customer { get; set; }
    }
}
