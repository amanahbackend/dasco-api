﻿using DispatchProduct.CustomerModule.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DispatchProduct.CustomerModule.EntityConfigurations
{
    public class ComplainEntityTypeConfiguration : IEntityTypeConfiguration<Complain>
    {
        public void Configure(EntityTypeBuilder<Complain> ComplainConfiguration)
        {
            ComplainConfiguration.ToTable("Complain");
            ComplainConfiguration.HasKey(o => (object)o.Id);
            ComplainConfiguration.Property(o => o.Id).ForSqlServerUseSequenceHiLo<int>("Complainseq", (string)null);
            ComplainConfiguration.Ignore(o => o.Customer);
            ComplainConfiguration.Property(o => o.Note).IsRequired(true);
            ComplainConfiguration.Property(o => o.FK_Customer_Id).IsRequired(true);
        }
    }
}

