﻿using DispatchProduct.CustomerModule.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.CustomerModule.EntityConfigurations
{
    public class CustomerTypeEntityTypeConfiguration
        : IEntityTypeConfiguration<CustomerType>
    {
        public void Configure(EntityTypeBuilder<CustomerType> CustomerTypeConfiguration)
        {
            CustomerTypeConfiguration.ToTable("CustomerType");

            CustomerTypeConfiguration.HasKey(o => o.Id);

            CustomerTypeConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("CustomerTypeseq");


            CustomerTypeConfiguration.Property(o => o.Name)
                .HasMaxLength(500)
                .IsRequired();
        }
    }
}
