﻿using DispatchProduct.CustomerModule.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.CustomerModule.EntityConfigurations
{
    public class LocationEntityTypeConfiguration
        : IEntityTypeConfiguration<Location>
    {
        public void Configure(EntityTypeBuilder<Location> LocationConfiguration)
        {
            LocationConfiguration.ToTable("Location");
            LocationConfiguration.Ignore(o => o.Customer);
            LocationConfiguration.HasKey(o => o.Id);

            LocationConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("Locationseq");

            LocationConfiguration.Property(o => o.AddressNote).IsRequired();
            LocationConfiguration.Property(o => o.Area).IsRequired();

            LocationConfiguration.Property(o => o.Block).IsRequired();
            LocationConfiguration.Property(o => o.Fk_Customer_Id).IsRequired();
            LocationConfiguration.Property(o => o.Governorate).IsRequired();
            LocationConfiguration.Property(o => o.Latitude).IsRequired();
            LocationConfiguration.Property(o => o.Longitude).IsRequired();
            LocationConfiguration.Property(o => o.PACINumber).IsRequired(false);
            LocationConfiguration.Property(o => o.Street).IsRequired();
            LocationConfiguration.Property(o => o.Title).IsRequired();

        }
    }
}
