﻿using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.CustomerModule.EntityConfigurations;
using Microsoft.EntityFrameworkCore;

namespace DispatchProduct.CustomerModule.Models
{
    public class CustomerDbContext : DbContext
    {
        public DbSet<Location> Location { get; set; }
        public DbSet<Complain> Complain { get; set; }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<CustomerPhoneBook> CustomerPhoneBook { get; set; }
        public DbSet<CustomerType> CustomerType { get; set; }
        public DbSet<PhoneType> PhoneType { get; set; }

        public CustomerDbContext(DbContextOptions<CustomerDbContext> options)
            : base(options)
        {
            


        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            modelBuilder.ApplyConfiguration(new LocationEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ComplainEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerPhoneBookEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerTypeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new PhoneTypeEntityTypeConfiguration());
        }
    }
}
