﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.CustomerModule.Entities;
using System.Collections.Generic;

namespace DispatchProduct.CustomerModule.IEntities
{
    public interface ICustomerHistory : IBaseEntity
    {
        Customer Customer { get; set; }

        IEnumerable<Complain> Complains { get; set; }
    }
}
