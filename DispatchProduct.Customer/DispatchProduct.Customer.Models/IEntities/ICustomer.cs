﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.CustomerModule.Entities;
using System.Collections.Generic;

namespace DispatchProduct.CustomerModule.IEntities
{
    public interface ICustomer : IBaseEntity
    {
        int Id { get; set; }

        string Name { get; set; }

        string CivilId { get; set; }

        string Remarks { get; set; }

        int FK_CustomerType_Id { get; set; }

        CustomerType CustomerType { get; set; }

        ICollection<Complain> Complains { get; set; }

        ICollection<DispatchProduct.CustomerModule.Entities.CustomerPhoneBook> CustomerPhoneBook { get; set; }

        ICollection<Location> Locations { get; set; }
    }
}
