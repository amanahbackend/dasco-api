﻿using AutoMapper;
using DispatchProduct.CustomerModule.API.ViewModel;
using DispatchProduct.CustomerModule.Entities;

namespace DispatchProduct.CustomerModule.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects

            CreateMap<Complain, ComplainViewModel>()
                .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => src.Customer));
            CreateMap<ComplainViewModel, Complain>()
               .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => src.Customer));

            CreateMap<CustomerPhoneBook, CustomerPhoneBookViewModel>()
             .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => src.Customer))
             .ForMember(dest => dest.PhoneType, opt => opt.MapFrom(src => src.PhoneType));

            CreateMap<CustomerPhoneBookViewModel, CustomerPhoneBook>()
             .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => src.Customer))
             .ForMember(dest => dest.PhoneType, opt => opt.MapFrom(src => src.PhoneType));

            CreateMap<CustomerHistory, CustomerHistoryViewModel>()
            .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => src.Customer))
            .ForMember(dest => dest.Complains, opt => opt.MapFrom(src => src.Complains));

            CreateMap<CustomerHistoryViewModel, CustomerHistory>()
            .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => src.Customer))
            .ForMember(dest => dest.Complains, opt => opt.MapFrom(src => src.Complains));

            CreateMap<CustomerType, CustomerTypeViewModel>();
            CreateMap<CustomerTypeViewModel, CustomerType>();

            CreateMap<Customer, CustomerViewModel>()
             .ForMember(dest => dest.CustomerType, opt => opt.MapFrom(src => src.CustomerType));


            CreateMap<CustomerViewModel, Customer>()
            .ForMember(dest => dest.CustomerType, opt => opt.MapFrom(src => src.CustomerType));
            CreateMap<Location, LocationViewModel>()
           .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => src.Customer));

            CreateMap<LocationViewModel, Location>()
            .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => src.Customer));


            CreateMap<PhoneType, PhoneTypeViewModel>();
            CreateMap<PhoneTypeViewModel, PhoneType>();


            CreateMap<DetailedCustomerViewModel, Customer>()
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
            .ForMember(dest => dest.CivilId, opt => opt.MapFrom(src => src.CivilId))
            .ForMember(dest => dest.Remarks, opt => opt.MapFrom(src => src.Remarks))
            .ForMember(dest => dest.FK_CustomerType_Id, opt => opt.MapFrom(src => src.FK_CustomerType_Id))
            .ForAllOtherMembers(dest => dest.Ignore());

            CreateMap<Customer, DetailedCustomerViewModel>()
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
            .ForMember(dest => dest.CivilId, opt => opt.MapFrom(src => src.CivilId))
            .ForMember(dest => dest.Remarks, opt => opt.MapFrom(src => src.Remarks))
            .ForMember(dest => dest.FK_CustomerType_Id, opt => opt.MapFrom(src => src.FK_CustomerType_Id))
            .ForAllOtherMembers(dest => dest.Ignore());

            CreateMap<DetailedCustomerViewModel, Location>()
            .ForMember(dest => dest.Fk_Customer_Id, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.PACINumber, opt => opt.MapFrom(src => src.PACINumber))
            .ForMember(dest => dest.Governorate, opt => opt.MapFrom(src => src.Governorate))
            .ForMember(dest => dest.Area, opt => opt.MapFrom(src => src.Area))
            .ForMember(dest => dest.Block, opt => opt.MapFrom(src => src.Block))
            .ForMember(dest => dest.Street, opt => opt.MapFrom(src => src.Street))
            .ForMember(dest => dest.AddressNote, opt => opt.MapFrom(src => src.AddressNote))
            .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.Latitude))
            .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.Longitude))
            .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title))
            .ForAllOtherMembers(dest => dest.Ignore());

            CreateMap<Location, DetailedCustomerViewModel>()
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Fk_Customer_Id))
            .ForMember(dest => dest.PACINumber, opt => opt.MapFrom(src => src.PACINumber))
            .ForMember(dest => dest.Governorate, opt => opt.MapFrom(src => src.Governorate))
            .ForMember(dest => dest.Area, opt => opt.MapFrom(src => src.Area))
            .ForMember(dest => dest.Block, opt => opt.MapFrom(src => src.Block))
            .ForMember(dest => dest.Street, opt => opt.MapFrom(src => src.Street))
            .ForMember(dest => dest.AddressNote, opt => opt.MapFrom(src => src.AddressNote))
            .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.Latitude))
            .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.Longitude))
            .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title))
            .ForAllOtherMembers(dest => dest.Ignore());

            CreateMap<CustomerPhoneBook, DetailedCustomerViewModel>()
             .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.FK_Customer_Id))
             .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.Phone))
             .ForMember(dest => dest.FK_PhoneType_Id, opt => opt.MapFrom(src => src.FK_PhoneType_Id))
             .ForAllOtherMembers(dest => dest.Ignore());

            CreateMap<DetailedCustomerViewModel, CustomerPhoneBook>()
             .ForMember(dest => dest.FK_Customer_Id, opt => opt.MapFrom(src => src.Id))
             .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.PhoneNumber))
             .ForMember(dest => dest.FK_PhoneType_Id, opt => opt.MapFrom(src => src.FK_PhoneType_Id))
             .ForAllOtherMembers(dest => dest.Ignore());


        }
    }
}
