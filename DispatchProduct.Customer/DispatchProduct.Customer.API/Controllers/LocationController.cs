﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using DispatchProduct.CustomerModule.API.ViewModel;
using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.CustomerModule.BLL.IManagers;
using Utilities.Utilites.PACI;
namespace DispatchProduct.CustomerModule.API.Controllers
{
    [Route("api/Location")]
    public class LocationController : Controller
    {
        private ILocationManager _manger;
        private readonly IMapper _mapper;

        public LocationController(ILocationManager manger, IMapper mapper)
        {
            _manger = manger;
            _mapper = mapper;
        }


        [Route("GetAll")]
        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get/{id}")]
        [HttpGet]
        public LocationViewModel Get([FromRoute]int id)
        {
            LocationViewModel result = new LocationViewModel();
            Location entityResult = new Location();
            entityResult = _manger.Get(id);
            result = _mapper.Map<Location, LocationViewModel>(entityResult);
            return result;
        }


        [Route("GetByCustomerId/{id}")]
        [HttpGet]
        public LocationViewModel GetByCustomerId([FromRoute] int id)
        {
            LocationViewModel result = new LocationViewModel();
            Location entityResult = new Location();
            entityResult = _manger.Get(id);
            result = _mapper.Map<Location, LocationViewModel>(entityResult);
            return result;
        }
        [Route("GetAll")]
        [HttpGet]
        public List<LocationViewModel> Get()
        {
            List<LocationViewModel> result = new List<LocationViewModel>();
            List<Location> entityResult = new List<Location>();
            entityResult = _manger.GetAll().ToList();
            result = _mapper.Map<List<Location>, List<LocationViewModel>>(entityResult);
            return result;
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]LocationViewModel model)
        {
            LocationViewModel result = new LocationViewModel();
            Location entityResult = new Location();
            entityResult = _mapper.Map<LocationViewModel, Location>(model);
            entityResult = _manger.Add(entityResult);

            result = _mapper.Map<Location, LocationViewModel>(entityResult);
            return Ok(result);

        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPost]
        public async Task<IActionResult> Put([FromBody]LocationViewModel model)
        {
            bool result = false;
            Location entityResult = new Location();
            entityResult = _mapper.Map<LocationViewModel, Location>(model);

            result = _manger.Update(entityResult);
            return Ok(result);

        }
        #endregion
        #region DeleteApi

        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]int id)
        {
            bool result = false;
            Location entityResult = new Location();
            LocationViewModel model = new LocationViewModel();
            model.Id = id;
            entityResult = _mapper.Map<LocationViewModel, Location>(model);
            result = _manger.Delete(entityResult);
            return Ok(result);

        }
        #endregion
        #endregion
     
        [HttpGet]
        [Route("GetLocationByPaci")]
        public IActionResult GetLocationByPaci(string paciNumber)
        {
            var result=_manger.GetLocationByPACI(Convert.ToInt32(paciNumber.Replace("\"", "")));
            if (result == null)
            {
                return Ok(GetNotFoundLocationByPaci());
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("GetCoordinatesByPaci/{paciNumber}")]
        public Point GetCoordinatesByPaci([FromRoute] string paciNumber)
        {
            return _manger.GetCoordinatesByPaci(Convert.ToInt32(paciNumber.Replace("\"", "")));
        }

        [HttpPost]
        [Route("GetCoordinatesByStreet_Block")]
        public Point GetCoordinatesByStreet_Block([FromBody] PointInputViewModel model)
        {
            return _manger.GetCoordinatesByStreet_Block(model.Street, model.Block);
        }

        [HttpGet]
        [Route("GetAllGovernorates")]
        public IActionResult GetAllGovernorates()
        {
            var result = _manger.GetAllGovernorates();
            if (result == null)
            {
                return Ok(GetNotFoundInPaci());
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAreas")]
        public IActionResult GetAreas(int? govId)
        {
            var result = _manger.GetAreas(govId);
            if (result == null)
            {
                return Ok(GetNotFoundInPaci());
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("GetBlocks")]
        public IActionResult GetBlocks(int? areaId)
        {
            var result= _manger.GetBlocks(areaId);
            if (result == null)
            {
                return Ok(GetNotFoundInPaci());
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("GetStreets")]
        public IActionResult GetStreets(int? govId, int? areaId, string blockName)
        {
            var result=_manger.GetStreets(govId, areaId,blockName);
            if (result == null)
            {
                return Ok(GetNotFoundInPaci());
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("GetLocationByCustomerId")]
        public IActionResult GetLocationByCustomerId(int customerId)
        {
            var result = _manger.GetByCustomerId(customerId);
            return Ok(result);
        }
        public object GetNotFoundInPaci()
        {
            var result = new[] { new { Id = 1, Name = "Not Found In Paci" } };
            return result;
        }
        public object GetNotFoundLocationByPaci()
        {
            var Governorate = new { Id = 1, Name = "Not Found In Paci" };
            var Area = new { Id = 1, Name = "Not Found In Paci" };
            var Block = new { Id = 1, Name = "Not Found In Paci" };
            var Street = new { Id = 1, Name = "Not Found In Paci" };

            var result =  new {
                Governorate = Governorate,
                Area = Area,
                Block = Block,
                Street = Street
            };
            return result;
        }

    }
}