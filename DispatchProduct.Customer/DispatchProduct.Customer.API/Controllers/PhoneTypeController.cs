﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.CustomerModule.API.ViewModel;
using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.CustomerModule.BLL.IManagers;

namespace DispatchProduct.CustomerModule.API.Controllers
{
    [Authorize]
    [Route("api/PhoneType")]
    public class PhoneTypeController : Controller
    {
        public IPhoneTypeManager manger;
        public readonly IMapper mapper;
        public PhoneTypeController(IPhoneTypeManager _manger, IMapper _mapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;

        }



        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public IActionResult Get(int id)
        {
            PhoneTypeViewModel result = new PhoneTypeViewModel();
            PhoneType entityResult = new PhoneType();
            entityResult = manger.Get(id);
            result = mapper.Map<PhoneType, PhoneTypeViewModel>(entityResult);
            return Ok(result);
        }
        [Route("GetAll")]
        [HttpGet]
        public IActionResult Get()
        {
            List<PhoneTypeViewModel> result = new List<PhoneTypeViewModel>();
            List<PhoneType> entityResult = new List<PhoneType>();
            entityResult = manger.GetAll().ToList();
            result = mapper.Map<List<PhoneType>, List<PhoneTypeViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]PhoneTypeViewModel model)
        {
            PhoneTypeViewModel result = new PhoneTypeViewModel();
            PhoneType entityResult = new PhoneType();
            entityResult = mapper.Map<PhoneTypeViewModel, PhoneType>(model);
            entityResult = manger.Add(entityResult);
            result = mapper.Map<PhoneType, PhoneTypeViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]PhoneTypeViewModel model)
        {
            bool result = false;
            PhoneType entityResult = new PhoneType();
            entityResult = mapper.Map<PhoneTypeViewModel, PhoneType>(model);
            result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            PhoneType entity = manger.Get(id);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion
       
    }
}