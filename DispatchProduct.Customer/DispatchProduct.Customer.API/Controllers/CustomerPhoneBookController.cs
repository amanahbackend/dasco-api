﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.CustomerModule.API.ViewModel;
using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.CustomerModule.BLL.IManagers;

namespace DispatchProduct.CustomerModule.API.Controllers
{
    [Authorize]
    [Route("api/CustomerPhoneBook")]
    public class CustomerPhoneBookController : Controller
    {
        public ICustomerPhoneBookManager manger;
        public readonly IMapper mapper;
        public CustomerPhoneBookController(ICustomerPhoneBookManager _manger, IMapper _mapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;

        }



        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public IActionResult Get(int id)
        {
            CustomerPhoneBookViewModel result = new CustomerPhoneBookViewModel();
            CustomerPhoneBook entityResult = new CustomerPhoneBook();
            entityResult = manger.Get(id);
            result = mapper.Map<CustomerPhoneBook, CustomerPhoneBookViewModel>(entityResult);
            return Ok(result);
        }
        [Route("GetAll")]
        [HttpGet]
        public IActionResult Get()
        {
            List<CustomerPhoneBookViewModel> result = new List<CustomerPhoneBookViewModel>();
            List<CustomerPhoneBook> entityResult = new List<CustomerPhoneBook>();
            entityResult = manger.GetAll().ToList();
            result = mapper.Map<List<CustomerPhoneBook>, List<CustomerPhoneBookViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CustomerPhoneBookViewModel model)
        {
            CustomerPhoneBookViewModel result = new CustomerPhoneBookViewModel();
            CustomerPhoneBook entityResult = new CustomerPhoneBook();
            entityResult = mapper.Map<CustomerPhoneBookViewModel, CustomerPhoneBook>(model);
            entityResult = manger.Add(entityResult);
            result = mapper.Map<CustomerPhoneBook, CustomerPhoneBookViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]CustomerPhoneBookViewModel model)
        {
            bool result = false;
            CustomerPhoneBook entityResult = new CustomerPhoneBook();
            entityResult = mapper.Map<CustomerPhoneBookViewModel, CustomerPhoneBook>(model);
            result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            CustomerPhoneBook entity = manger.Get(id);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion
       
    }
}