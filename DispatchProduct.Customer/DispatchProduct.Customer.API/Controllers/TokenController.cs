﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IdentityModel.Client;
using Microsoft.Extensions.Options;
using DispatchProduct.CustomerModule.API.Settings;
using DispatchProduct.Employees.Salary.API.ViewModel;
using DispatchProduct.CustomerModule.API.ServicesCommunication.Identity;
using DispatchProduct.CustomerModule.API.ServicesViewModels;

namespace DispatchProduct.CustomerModule.API.Controllers
{
    [Route("api/[controller]")]
    public class TokenController : Controller
    {
        private CustomerAppSettings _appSettings;
        IIdentityRoleService _identityRoleService;
        public TokenController(IOptions<CustomerAppSettings> appSettings, IIdentityRoleService identityRoleService)
        {
            _appSettings = appSettings.Value;
            _identityRoleService = identityRoleService;
        }


        [HttpPost]
        public async Task<IActionResult> Get([FromBody]TokenViewModel model)
        {
            TokenResponse tokenResponse = null;
            ApplicationUserViewModel user = null;
            
            if (ModelState.IsValid)
            {
                if (_appSettings.IdentityUrl != null && _appSettings.ClientId != null && _appSettings.Secret != null)
                {

                    var discoveryClient2 = new DiscoveryClient(_appSettings.IdentityUrl);
                    discoveryClient2.Policy.RequireHttps = false;
                    discoveryClient2.Policy.ValidateIssuerName = false;
                    var disco = await discoveryClient2.GetAsync();
                    if (disco.TokenEndpoint != null)
                    {
                        var tokenClient = new TokenClient(disco.TokenEndpoint, _appSettings.ClientId, _appSettings.Secret);
                        tokenResponse = await tokenClient.RequestResourceOwnerPasswordAsync(model.Username, model.Password, "calling");
                        if (tokenResponse.AccessToken != null)
                        {
                              string token = "bearer " + tokenResponse.AccessToken;
                               user= await _identityRoleService.GetUserRoles(model.Username, token);
                        }
                    }

                }
            }
            if (user != null && tokenResponse!=null)
            {
                return Ok(new
                {
                    Roles = user.RoleNames,
                    Id = user.Id,
                    UserName = user.UserName,
                    FullName = user.FirstName +" "+ user.MiddleName + " " + user.LastName,
                    token = tokenResponse
                });
            }
            return BadRequest();
        }
    }
}