﻿using AutoMapper;
using DispatchProduct.CustomerModule.API.ViewModel;
using DispatchProduct.CustomerModule.BLL.IManagers;
using DispatchProduct.CustomerModule.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ExcelToGenericList;
using Utilites.UploadFile;
using Utilities.Utilites;

namespace DispatchProduct.CustomerModule.API.Controllers
{
    [Route("api/Customer")]
    public class CustomerController : Controller
    {
        private ICustomerManager manger;
        private readonly IMapper mapper;
        ICustomerPhoneBookManager customerPhoneManger;
        ILocationManager locationManger;
        ICustomerTypeManager customerTypeManager;
        private readonly IHostingEnvironment _hostingEnv;
        public CustomerController(ICustomerManager _manger, IMapper _mapper,
            ICustomerPhoneBookManager _customerPhoneManger, ILocationManager _locationManger,
            IHostingEnvironment hostingEnv, ICustomerTypeManager customerTypeManager)
        {
            this.customerTypeManager = customerTypeManager;
            this.manger = _manger;
            this.mapper = _mapper;
            customerPhoneManger = _customerPhoneManger;
            locationManger = _locationManger;
            _hostingEnv = hostingEnv;
        }



        #region DefaultCrudOperation

        #region GetApi
        [Route("Get/{id}")]
        [HttpGet]
        public CustomerViewModel Get([FromRoute]int id)
        {
            CustomerViewModel result = new CustomerViewModel();
            Customer entityResult = new Customer();
            entityResult = manger.Get(id);
            result = mapper.Map<Customer, CustomerViewModel>(entityResult);
            return result;
        }
        [Route("GetAll")]
        [HttpGet]
        public List<CustomerViewModel> Get()
        {
            List<CustomerViewModel> result = new List<CustomerViewModel>();
            List<Customer> entityResult = new List<Customer>();
            entityResult = manger.GetAllDetailedCustomer().ToList();
            result = mapper.Map<List<Customer>, List<CustomerViewModel>>(entityResult);
            return result;
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CustomerViewModel model)
        {
            CustomerViewModel result = new CustomerViewModel();
            Customer entityResult = new Customer();
            entityResult = mapper.Map<CustomerViewModel, Customer>(model);
            entityResult = manger.Add(entityResult);
            result = mapper.Map<Customer, CustomerViewModel>(entityResult);
            return Ok(result);

        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]CustomerViewModel model)
        {
            bool result = false;
            Customer entityResult = new Customer();
            entityResult = mapper.Map<CustomerViewModel, Customer>(model);
            result = manger.Update(entityResult);
            return Ok(result);

        }
        #endregion
        #region DeleteApi
        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            Customer entity = manger.Get(id);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion


        #region Custom Buisness
        [Route("CustomerHistory/{id}")]
        [HttpGet]
        public IActionResult CustomerHistory([FromRoute]int id)
        {
            CustomerHistoryViewModel result = new CustomerHistoryViewModel();
            CustomerHistory entityResult = new CustomerHistory();
            entityResult = manger.GetCustomerHistory(id);
            result = mapper.Map<CustomerHistory, CustomerHistoryViewModel>(entityResult);
            return Ok(result);
        }
        [Route("SearchCustomer/{key}")]
        [HttpGet]
        public IActionResult SearchCustomer([FromRoute]string key)
        {
            var entityResult = manger.SearchForCustomer(key);
            var result = mapper.Map<Customer, CustomerViewModel>(entityResult);
            return Ok(result);
        }
        [Route("CreateDetailedCustomer")]
        [HttpPost]
        public Customer CreateDetailedCustomer([FromBody] DetailedCustomerViewModel model)
        {
            var customer = mapper.Map<DetailedCustomerViewModel, Customer>(model);
            customer = manger.Add(customer);
            if (customer != null)
            {
                customer.Locations = new List<Location>();
                model.Id = customer.Id;
                var call = mapper.Map<DetailedCustomerViewModel, CustomerPhoneBook>(model);
                customerPhoneManger.Add(call);
                var location = mapper.Map<DetailedCustomerViewModel, Location>(model);
                customer.Locations.Add(locationManger.Add(location));
            }
            return customer;
        }

        [Route("GetBy")]
        [HttpGet]
        public IActionResult GetBy([FromQuery]string name, [FromQuery]string phone)
        {
            var entityResult = manger.Get(x => x.Name.ToLower().Equals(name.ToLower()));
            var result = mapper.Map<Customer, CustomerViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        [HttpPost, Route("ImportFromFile")]
        public IActionResult ImportFromFile([FromBody]UploadFile file)
        {
            var fileExt = StringUtilities.GetFileExtension(file.FileContent);
            file.FileName = $"Customers_{DateTime.Now.Ticks}.{fileExt}";
            UploadExcelFileManager fileManager = new UploadExcelFileManager();
            var path = $@"{_hostingEnv.WebRootPath}\Import\";
            var addFileResult = fileManager.AddFile(file, path);
            var result = AddFromFile($@"{addFileResult.returnData}\{file.FileName}");
            return Ok(result);
        }

        private List<Customer> AddFromFile(string filePath)
        {
            var customers = new List<Customer>();
            var models = ExcelReader.GetDataToList(filePath, GetItems);
            string fileName = Path.GetFileName(filePath);
            int i = 0;
            foreach (var item in models)
            {
                i++;
                var customerData = new DetailedCustomerViewModel
                {
                    Name = item.Name,
                    Remarks = item.Remarks,
                    CivilId = item.CivilId,
                    AddressNote = item.AddressNote,
                    Area = item.Area,
                    Block = item.Block,
                    Governorate = item.Governorate,
                    Latitude = item.Latitude,
                    Longitude = item.Longitude,
                    PACINumber = item.PACINumber,
                    PhoneNumber = item.Phone,
                    Street = item.Street,
                    Title = item.Title,
                    FK_CustomerType_Id = customerTypeManager.Get(x => x.Name.Equals(item.CustomerType)).Id
                };
                var customer = CreateDetailedCustomer(customerData);
                customers.Add(customer);
            }
            return customers;
        }

        private CustomerFromFileViewModel GetItems(IList<string> rowData, IList<string> columnNames)
        {
            var model = new CustomerFromFileViewModel();

            var customer = new CustomerFromFileViewModel()
            {
                AddressNote = rowData[columnNames.IndexFor(nameof(model.AddressNote))],
                Area = rowData[columnNames.IndexFor(nameof(model.Area))],
                Block = rowData[columnNames.IndexFor(nameof(model.Block))],
                CivilId = rowData[columnNames.IndexFor(nameof(model.CivilId))],
                CustomerType = rowData[columnNames.IndexFor(nameof(model.CustomerType))],
                Governorate = rowData[columnNames.IndexFor(nameof(model.Governorate))],
                Latitude = Convert.ToDouble(rowData[columnNames.IndexFor(nameof(model.Latitude))]),
                Longitude = Convert.ToDouble(rowData[columnNames.IndexFor(nameof(model.Longitude))]),
                Name = rowData[columnNames.IndexFor(nameof(model.Name))],
                PACINumber = rowData[columnNames.IndexFor(nameof(model.PACINumber))],
                Phone = rowData[columnNames.IndexFor(nameof(model.Phone))],
                Remarks = rowData[columnNames.IndexFor(nameof(model.Remarks))],
                Street = rowData[columnNames.IndexFor(nameof(model.Street))],
                Title = rowData[columnNames.IndexFor(nameof(model.Title))]
            };
            return customer;
        }

        public void LogResult(string resultMessage, string excelFileName)
        {
            string date = DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year;
            //string dir = @"D:\Amanah\Dev\Migration"; 
            string dir = $@"{_hostingEnv.WebRootPath}\Logs";

            DirectoryInfo dirInfo = new DirectoryInfo(dir);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }
            string path = dir + @"\" + "Request_Log_" + excelFileName + ".txt";
            if (!System.IO.File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = System.IO.File.CreateText(path))
                {
                    sw.WriteLine(resultMessage);
                }
            }
            else
            {
                using (StreamWriter sw = System.IO.File.AppendText(path))
                {
                    sw.WriteLine(resultMessage);
                }
            }
        }
    }
}