﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using DispatchProduct.CustomerModule.API.ViewModel;
using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.CustomerModule.BLL.IManagers;
using Microsoft.AspNetCore.Authorization;

namespace DispatchProduct.CustomerModule.API.Controllers
{
    [Authorize]
    [Route("api/CustomerType")]
    public class CustomerTypeController : Controller
    {
        public readonly ICustomerTypeManager manger;
        public readonly IMapper mapper;

        public CustomerTypeController(ICustomerTypeManager _manger, IMapper _mapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;

        }


        public IActionResult Index()
        {
          
            return Ok();
        }


        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public CustomerTypeViewModel Get(int id)
        {
            CustomerTypeViewModel result = new CustomerTypeViewModel();
            CustomerType entityResult = new CustomerType();
            entityResult=manger.Get(id);
            result = mapper.Map<CustomerType,CustomerTypeViewModel> (entityResult);
            return result;
        }
        [Route("GetAll")]
        [HttpGet]
        public List<CustomerTypeViewModel> Get()
        {
            List<CustomerTypeViewModel> result = new List<CustomerTypeViewModel>();
            List<CustomerType> entityResult = new List<CustomerType>();
            entityResult = manger.GetAll().ToList();
            result= mapper.Map < List <CustomerType> ,List<CustomerTypeViewModel>>(entityResult);
            return result;
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CustomerTypeViewModel model)
        {
            CustomerTypeViewModel result = new CustomerTypeViewModel();
            CustomerType entityResult = new CustomerType();
            entityResult = mapper.Map<CustomerTypeViewModel, CustomerType> (model);


            entityResult = manger.Add(entityResult);
            result = mapper.Map<CustomerType, CustomerTypeViewModel>(entityResult);
            return Ok(result);

        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]CustomerTypeViewModel model)
        {
            bool result = false;
            CustomerType entityResult = new CustomerType();
            entityResult = mapper.Map<CustomerTypeViewModel, CustomerType>(model);
            result = manger.Update(entityResult);
            return Ok(result);

        }
        #endregion
        #region DeleteApi

        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            CustomerType entityResult = new CustomerType();
            CustomerTypeViewModel model = new CustomerTypeViewModel();
            model.Id = id;
            entityResult = mapper.Map<CustomerTypeViewModel, CustomerType>(model);
            result = manger.Delete(entityResult);
            return Ok(result);
        }
        #endregion
        #endregion
      
    }
}