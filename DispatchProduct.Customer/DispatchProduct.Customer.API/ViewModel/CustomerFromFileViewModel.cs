﻿namespace DispatchProduct.CustomerModule.API.ViewModel
{
    public class CustomerFromFileViewModel
    {
        public string Name { get; set; }
        public string CivilId { get; set; }
        public string Phone { get; set; }
        public string Remarks { get; set; }
        public string CustomerType { get; set; }
        public string PACINumber { get; set; }
        public string Title { get; set; }
        public string Governorate { get; set; }
        public string Area { get; set; }
        public string Block { get; set; }
        public string Street { get; set; }
        public string AddressNote { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
