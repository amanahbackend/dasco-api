﻿namespace DispatchProduct.CustomerModule.API.ViewModel
{
    public class DetailedCustomerViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string CivilId { get; set; }

        public string Remarks { get; set; }

        public int FK_CustomerType_Id { get; set; }

        public string PhoneNumber { get; set; }

        public CustomerTypeViewModel CustomerType { get; set; }

        public int FK_CallType_Id { get; set; }

        public int FK_PhoneType_Id { get; set; }

        public string PACINumber { get; set; }

        public string Governorate { get; set; }

        public string Area { get; set; }

        public string Block { get; set; }

        public string Street { get; set; }

        public string AddressNote { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string Title { get; set; }
    }
}
