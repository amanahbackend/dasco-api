﻿namespace DispatchProduct.CustomerModule.API.Settings
{
    public class CustomerAppSettings
    {
        public bool UseCustomizationData { get; set; }

        public string IdentityUrl { get; set; }

        public string ClientId { get; set; }

        public string Secret { get; set; }
    }
}
