﻿using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using DispatchProduct.CustomerModule.API.Seed;
using DispatchProduct.CustomerModule.Models;
using Microsoft.Extensions.Options;
using DispatchProduct.CustomerModule.API.Settings;

namespace DispatchProduct.CustomerModule.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).MigrateDbContext<CustomerDbContext>((context, services) =>
            {
                var env = services.GetService<IHostingEnvironment>();
                var logger = services.GetService<ILogger<CustomerDbContextSeed>>();
                var settings = services.GetService<IOptions<CustomerAppSettings>>();
                new CustomerDbContextSeed()
                    .SeedAsync(context, env, logger, settings)
                    .Wait();
            }).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
                   WebHost.CreateDefaultBuilder(args)
                       .UseKestrel()
                       .UseContentRoot(Directory.GetCurrentDirectory())
                       .UseIISIntegration()
                       .UseStartup<Startup>()
                       .ConfigureLogging((hostingContext, builder) =>
                       {
                           builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                           builder.AddConsole();
                           builder.AddDebug();
                       })
                       .Build();
    }
}
