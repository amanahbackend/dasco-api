﻿using DispatchProduct.CustomerModule.API.ServicesViewModels;
using DispatchProduct.CustomerModule.API.Settings;
using DispatchProduct.HttpClient;
using System.Threading.Tasks;

namespace DispatchProduct.CustomerModule.API.ServicesCommunication.Identity
{
    public interface IIdentityRoleService : IDefaultHttpClientCrud<IdentityServiceSetting, ApplicationUserViewModel, ApplicationUserViewModel>
    {
        Task<ApplicationUserViewModel> GetUserRoles(string userName, string authHeader = "");
    }
}
