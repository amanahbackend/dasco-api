﻿using DispatchProduct.CustomerModule.BLL.IManagers;
using DispatchProduct.CustomerModule.Models;
using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;
using System.Linq;

namespace DispatchProduct.CustomerModule.BLL.Managers
{
    public class CustomerManager : Repositry<Customer>, ICustomerManager
    {
        private readonly IComplainManager complainManger;
        private readonly ILocationManager locationManger;
        private readonly ICustomerPhoneBookManager phoneBookManager;
        private readonly ICustomerTypeManager customerTypeManager;

        public CustomerManager(CustomerDbContext context,
             IComplainManager _complainManger,
            ILocationManager _locationManger, ICustomerPhoneBookManager _phoneBookManager,
            ICustomerTypeManager _customerTypeManager)
            : base(context)
        {
            customerTypeManager = _customerTypeManager;
            complainManger = _complainManger;
            locationManger = _locationManger;
            phoneBookManager = _phoneBookManager;
        }

        public List<Customer> GetAllDetailedCustomer()
        {
            var customers= base.GetAll().ToList();
            foreach (var customer in customers)
            {
                BindNavProps(customer);
            }
            return customers;
        } 
        public Customer SearchForCustomer(string key)
        {
            Customer model = GetAll().Where(cus => cus.Name == key).FirstOrDefault<Customer>();
            if (model == null)
            {
                List<int> lstCustomerIds = phoneBookManager.Search(key).Select(phone => phone.FK_Customer_Id).ToList();
                model = GetAll().Where(cus => lstCustomerIds.Contains(cus.Id)).FirstOrDefault();
            }
            BindNavProps(model);
            return model;
        }
        public CustomerHistory GetCustomerHistory(int customerId)
        {
            var result = new CustomerHistory();
            result.Customer = Get(customerId);
            result.Complains = complainManger.GetComplainsByCustomerId(customerId);
            return result;
        }

        public override Customer Add(Customer entity)
        {
            var customer = base.Add(entity);
            if (entity.Locations != null)
            {
                foreach (Location location in entity.Locations)
                    location.Fk_Customer_Id = customer.Id;
                entity.Locations = locationManger.Add(entity.Locations.ToList());
            }
            if (entity.CustomerPhoneBook != null)
            {
                foreach (CustomerPhoneBook customerPhoneBook in entity.CustomerPhoneBook)
                    customerPhoneBook.FK_Customer_Id = customer.Id;
                entity.CustomerPhoneBook = phoneBookManager.Add(entity.CustomerPhoneBook.ToList());
            }
            return customer;
        }

        public override bool Update(Customer entity)
        {
            bool result;
            if (entity.CustomerPhoneBook != null)
            {
                entity.CustomerPhoneBook = phoneBookManager.UpdateCustomerPhones(entity.Id, entity.CustomerPhoneBook.ToList());
                result = base.Update(entity);
            }
            else
                result = base.Update(entity);
            return result;
        }

        public override Customer Get(params object[] id)
        {
            Customer result = null;
            result= base.Get(id);
            if (result != null)
            {
              
                BindNavProps(result);
            }
            return result;
        }
        public override bool Delete(Customer entity)
        {
                bool result = false;
                if (locationManger.DeleteByCustomerId(entity.Id))
                {
                    if (complainManger.DeleteByCustomerId(entity.Id))
                    {
                        result = base.Delete(entity);
                    }
                }
            return result;
        }

        public Customer BindNavProps(Customer model)
        {
            if (model != null)
            {
                model.Locations = locationManger.GetByCustomerId(model.Id);
                model.Complains = complainManger.GetComplainsByCustomerId(model.Id);
                model.CustomerPhoneBook = phoneBookManager.GetByCustomerId(model.Id);
                model.CustomerType = customerTypeManager.Get(model.FK_CustomerType_Id);
            }
            return model;
        }
    }
}
