﻿using DispatchProduct.CustomerModule.BLL.IManagers;
using DispatchProduct.Repoistry;
using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.CustomerModule.Models;

namespace DispatchProduct.CustomerModule.BLL.Managers
{
    public class PhoneTypeManager : Repositry<PhoneType>, IPhoneTypeManager
    {
        public PhoneTypeManager(CustomerDbContext context)
            : base(context)
        {

        }

    }
}
