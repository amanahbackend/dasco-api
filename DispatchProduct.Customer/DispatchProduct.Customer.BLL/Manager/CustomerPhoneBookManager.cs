﻿
using DispatchProduct.CustomerModule.BLL.IManagers;
using DispatchProduct.CustomerModule.Models;
using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;
using System.Linq;

namespace DispatchProduct.CustomerModule.Managers
{
    public class CustomerPhoneBookManager : Repositry<CustomerPhoneBook>, ICustomerPhoneBookManager
    {
        public CustomerPhoneBookManager(CustomerDbContext context)
            : base(context)
        {

        }
        public List<CustomerPhoneBook> GetByCustomerId(int customerId)
        {
            return GetAll().Where(phonebook => phonebook.FK_Customer_Id == customerId).ToList();
        }
        public bool DeleteByCustomerId(int customerId)
        {
            var lst = GetAll().Where(phonebook => phonebook.FK_Customer_Id == customerId).ToList();
            return Delete(lst);
        }
        public List<CustomerPhoneBook> AddByCustomerId(int customerId, List<CustomerPhoneBook> lstPhones)
        {
           
            for (int index = 0; index < lstPhones.Count(); ++index)
            {
                lstPhones[index].Id = 0;
                lstPhones[index].FK_Customer_Id = customerId;
                lstPhones[index] = this.Add(lstPhones[index]);
            }
            return lstPhones;
        }
        public List<CustomerPhoneBook> Search(string key)
        {
            return GetAll().Where(phonebook => phonebook.Phone == key).ToList();
        }
        public List<CustomerPhoneBook> UpdateCustomerPhones(int customerId, List<CustomerPhoneBook> lstPhones)
        {
            DeleteByCustomerId(customerId);
            return AddByCustomerId(customerId, lstPhones);
        }
    }
}
