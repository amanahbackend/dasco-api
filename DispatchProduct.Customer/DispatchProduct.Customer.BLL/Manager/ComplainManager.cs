﻿
using DispatchProduct.CustomerModule.BLL.IManagers;
using DispatchProduct.CustomerModule.Models;
using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;
using System.Linq;

namespace DispatchProduct.CustomerModule.Managers
{
    public class ComplainManager: Repositry<Complain>, IComplainManager
    {
        public ComplainManager(CustomerDbContext context)
            : base(context)
        {

        }

        public List<Complain> GetComplainsByCustomerId(int customerId)
        {
            List<Complain> result = null;
            result=GetAll().Where(complain => complain.FK_Customer_Id == customerId && complain.IsDeleted==false).ToList();
            return result;
        }

        public bool DeleteByCustomerId(int customerId)
        {

            bool result = false;
            var complains = GetComplainsByCustomerId(customerId);
            if (complains != null && complains.Count > 0)
            {
                result = base.Delete(complains);
            }
            else
            {
                result = true;
            }
            return result;
        }
    }
}
