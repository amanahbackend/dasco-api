﻿
using DispatchProduct.CustomerModule.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;

namespace DispatchProduct.CustomerModule.BLL.IManagers
{
    public interface ICustomerManager : IRepositry<Customer>
    {
    
        Customer SearchForCustomer(string key);

        CustomerHistory GetCustomerHistory(int customerId);

        List<Customer> GetAllDetailedCustomer();
    }
}
