﻿using AutoMapper;
using DispatchProduct.Vehicles.API.ViewModel;
using DispatchProduct.Vehicles.Entities;

namespace DispatchProduct.Ordering.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<VehicleViewModel, Vehicle>();
            CreateMap<Vehicle, VehicleViewModel>();
        }
    }
}
