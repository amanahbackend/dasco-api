﻿using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using System.Threading.Tasks;

namespace DispatchProduct.Vehicles.Middleware
{
    public class SignalRAuthenticationMiddleware
    {
        public readonly RequestDelegate _next;

        public SignalRAuthenticationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            if (string.IsNullOrEmpty(httpContext.Request.Headers[HeaderNames.Authorization]))
            {
                if (httpContext.Request.QueryString.HasValue)
                {
                    var token = httpContext.Request.Query["token"].ToString();
                    if (!string.IsNullOrEmpty(token))
                    {
                        httpContext.Request.Headers.Add(HeaderNames.Authorization, new[] { $"{token}" });
                    }
                }
            }
            await _next(httpContext);
        }
    }
}
