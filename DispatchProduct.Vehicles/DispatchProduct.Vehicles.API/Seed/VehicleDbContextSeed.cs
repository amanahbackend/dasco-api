﻿using DispatchProduct.Repoistry;
using DispatchProduct.Vehicles.Entities;
using DispatchProduct.Vehicles.Models.Context;
using DispatchProduct.Vehicles.Settings;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchProduct.Vehicles.API.Seed
{
    public class VehicleDbContextSeed : ContextSeed
    {
        VehicleAppSettings vehicleAppSettings =null;
        public async Task SeedAsync(VehicleDbContext context, IHostingEnvironment env,
            ILogger<VehicleDbContextSeed> logger, IOptions<VehicleAppSettings> settings, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;

            try
            {
                vehicleAppSettings = settings.Value;
                var useCustomizationData = settings.Value.UseCustomizationData;
                var contentRootPath = env.ContentRootPath;
                var webroot = env.WebRootPath;
                List<Vehicle> vehicles = new List<Vehicle>();
                if (useCustomizationData)
                {
                    //from file e.g (look at ApplicationDbContextSeed)
                }
                else
                {
                    //default from here
                    vehicles = GetDefaultVehicle();
                }
                await SeedEntityAsync(context, vehicles);
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 3)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for ApplicationDbContext");

                    await SeedAsync(context, env, logger, settings, retryForAvaiability);
                }
            }
        }
        private List<Vehicle> GetDefaultVehicle()
        {
            List<Vehicle> result = new List<Vehicle>
            {
            };
            return result;
        }
    }
}
