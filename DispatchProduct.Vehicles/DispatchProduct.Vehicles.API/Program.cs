﻿using System;
using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using DispatchProduct.Vehicles.Models.Context;
using DispatchProduct.Vehicles.API.Seed;
using Microsoft.Extensions.Options;
using DispatchProduct.Vehicles.Settings;

namespace DispatchProduct.Vehicles.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).MigrateDbContext<VehicleDbContext>((context, services) =>
            {
                Seed(context, services);

            }).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
         WebHost.CreateDefaultBuilder(args)
             .UseKestrel()
             .UseContentRoot(Directory.GetCurrentDirectory())
             .UseIISIntegration()
             .UseStartup<Startup>()
             .ConfigureLogging((hostingContext, builder) =>
             {
                 builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                 builder.AddConsole();
                 builder.AddDebug();
             })
             .Build();
        public static void Seed(VehicleDbContext context, IServiceProvider services)
        {
            var env = services.GetService<IHostingEnvironment>();
            var logger = services.GetService<ILogger<VehicleDbContextSeed>>();
            var settings = services.GetService<IOptions<VehicleAppSettings>>();
            new VehicleDbContextSeed()
                .SeedAsync(context, env, logger, settings)
                .Wait();
        }
    }
}
