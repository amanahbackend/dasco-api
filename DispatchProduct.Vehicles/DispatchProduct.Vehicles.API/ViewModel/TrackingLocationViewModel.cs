﻿namespace DispatchProduct.Vehicles.API.ViewModel
{
    public class TrackingLocationViewModel:BaseEntityViewModel
    {
        public int Id { get; set; }
        public double Lat {get; set;}
        public double Long {get; set;}
    }
}
