﻿using DispatchProduct.Vehicles.API.ViewModel;
using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchProduct.Vehicles.Hubs
{
    public interface IDispatchingHub
    {
        Task SendLiveVehicleLocation(List<VehicleViewModel> vehicles, IHubContext<DispatchingHub> dispatchingHub);
        Task JoinGroup(string groupName);
        Task LeaveGroup(string groupName);
        Task OnConnectedAsync();
    }
}
