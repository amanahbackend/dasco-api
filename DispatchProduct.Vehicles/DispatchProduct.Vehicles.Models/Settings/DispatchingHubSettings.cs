﻿using System.Collections.Generic;

namespace DispatchProduct.Vehicles.Settings
{
    public class DispatchingHubSettings
    {
        public string token { get; set; }

        public string userId { get; set; }

        public string roles { get; set; }

        public string SendLiveVehicleLocation { get; set; }

        public string SendLiveVehicleLocationGroupName { get; set; }

        public List<string> SendLiveVehicleLocationRoles { get; set; }
    }
}
