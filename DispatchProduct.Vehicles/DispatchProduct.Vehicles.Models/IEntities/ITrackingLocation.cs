﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.Vehicles.Entities
{
    public interface ITrackingLocation:IBaseEntity
    {
         double Lat {get; set;}
         double Long {get; set;}
    }
}
