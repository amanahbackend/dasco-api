﻿using DispatchProduct.Repoistry;
using DispatchProduct.Vehicles.Entities;
using System.Collections.Generic;
using Utilites.ProcessingResult;

namespace DispatchProduct.Vehicles.BLL.Managers
{
    public interface IVehicleManager : IRepositry<Vehicle>
    {
        ProcessResult<List<Vehicle>> AddOrUpdateVehicles(List<Vehicle> Vehicles);
        ProcessResult<List<Vehicle>> GetVehicles();
        ProcessResult<List<Vehicle>> GetVehiclesReg();
        ProcessResult<List<Vehicle>> GetVehiclesByVIDS(List<string> VIDS);
    }
}
