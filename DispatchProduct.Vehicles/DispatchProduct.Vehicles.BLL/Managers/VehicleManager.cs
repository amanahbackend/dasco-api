﻿using DispatchProduct.Repoistry;
using DispatchProduct.Vehicles.Entities;
using DispatchProduct.Vehicles.Models.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using Utilites;
using Utilites.ProcessingResult;

namespace DispatchProduct.Vehicles.BLL.Managers
{
    public class VehicleManager : Repositry<Vehicle>, IVehicleManager
    {

        public VehicleManager(VehicleDbContext context)
            : base(context)
        {


        }
        public ProcessResult<List<Vehicle>> AddOrUpdateVehicles(List<Vehicle> Vehicles)
        {
            ProcessResult<List<Vehicle>> result = new ProcessResult<List<Vehicle>>(MethodBase.GetCurrentMethod().Name);
            result.IsSucceeded = true;
            result.Message = "Succeeded";
            try
            {
                foreach (var item in Vehicles)
                {
                    var vehicle = GetAll().Where(x => x.VID == item.VID).FirstOrDefault();
                    if (vehicle == null)
                    {
                        Add(item);
                    }
                    else
                    {
                        item.ID = vehicle.ID;
                        Reflection.CopyProperties(item, vehicle);
                        Update(vehicle);
                    }
                    result.returnData = new List<Vehicle>();
                    result.returnData.Add(vehicle);
                    result.Status = HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.Status = HttpStatusCode.ExpectationFailed;
                result.IsSucceeded = true;
                result.Message = "Succeeded";
            }
            return result;
        }
        public ProcessResult<List<Vehicle>> GetVehicles()
        {
            ProcessResult<List<Vehicle>> result = new ProcessResult<List<Vehicle>>(MethodBase.GetCurrentMethod().Name);
            result.IsSucceeded = true;
            result.Message = "Succeeded";
            try
            {
                result.returnData = GetAll().OrderBy(x => x.ID).ToList();
                result.Status = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.Status = HttpStatusCode.ExpectationFailed;
                result.IsSucceeded = true;
                result.Message = "Succeeded";
            }
            return result;
        }
        public ProcessResult<List<Vehicle>> GetVehiclesReg()
        {
            ProcessResult<List<Vehicle>> result = new ProcessResult<List<Vehicle>>(MethodBase.GetCurrentMethod().Name);
            result.IsSucceeded = true;
            result.Message = "Succeeded";

            try
            {
                result.returnData = GetAll().OrderByDescending(x => x.ID).ToList();
                result.Status = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.Status = HttpStatusCode.ExpectationFailed;
                result.IsSucceeded = true;
                result.Message = "Succeeded";
            }
            return result;
        }
        public ProcessResult<List<Vehicle>> GetVehiclesByVIDS(List<string> VIDS)
        {
            ProcessResult<List<Vehicle>> result = new ProcessResult<List<Vehicle>>(MethodBase.GetCurrentMethod().Name);
            result.IsSucceeded = true;
            result.Message = "Succeeded";
            try
            {
                result.returnData = GetAll().Where(x=>VIDS.Contains(x.VID)).ToList();
                result.Status = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.Status = HttpStatusCode.ExpectationFailed;
                result.IsSucceeded = true;
                result.Message = "Succeeded";
            }
            return result;
        }
    }
}
