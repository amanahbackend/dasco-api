﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Contracting.IEntities;
using System;

namespace DispatchProduct.Contracting.Entities
{
    public class PreventiveMaintainenceSchedule : BaseEntity, IPreventiveMaintainenceSchedule, IBaseEntity
    {
        public int Id { get; set; }

        public string ContractNumber { get; set; }

        public DateTime OrderDate { get; set; }

        public int FK_Customer_Id { get; set; }

        public int FK_Contract_Id { get; set; }

        public int FK_Location_Id { get; set; }

        public int FK_OrderPriority_Id { get; set; }

        public int FK_OrderType_Id { get; set; }

        public int FK_OrderProblem_Id { get; set; }

        public string QuotationRefNo { get; set; }

        public Contract Contract { get; set; }

        public int FK_Order_Id { get; set; }
    }
}