﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Contracting.IEntities;

namespace DispatchProduct.Contracting.Entities
{
    public class ContractType : BaseEntity, IContractType, IBaseEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

    }
}
