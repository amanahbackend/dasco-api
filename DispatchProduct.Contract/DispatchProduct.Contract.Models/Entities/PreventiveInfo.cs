﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
namespace DispatchProduct.Contracting.Entities
{
    public class PreventiveInfo : BaseEntity
    {
        public int FK_Location_Id { get; set; }

        public int FK_OrderPriority_Id { get; set; }

        public int FK_OrderType_Id { get; set; }

        public int FK_OrderProblem_Id { get; set; }
    }
}
