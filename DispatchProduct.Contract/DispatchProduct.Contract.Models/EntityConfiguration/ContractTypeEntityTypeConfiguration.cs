﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using DispatchProduct.Contracting.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DispatchProduct.Contracting.EntityConfiguration
{
    class ContractTypeEntityTypeConfiguration
        : BaseEntityTypeConfiguration<ContractType>, IEntityTypeConfiguration<ContractType>
    {
        public override void Configure(EntityTypeBuilder<ContractType> ContractTypeConfiguration)
        {
            base.Configure(ContractTypeConfiguration);
            ContractTypeConfiguration.ToTable("ContractType");

            ContractTypeConfiguration.HasKey(o => o.Id);

            ContractTypeConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("ContractTypeseq");

            ContractTypeConfiguration.Property(c => c.Name).IsRequired().HasMaxLength(500);
        }
    }
}
