﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using DispatchProduct.Contracting.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DispatchProduct.Contracting.EntityConfiguration
{
    class PreventiveMaintainenceScheduleEntityTypeConfiguration
        : BaseEntityTypeConfiguration<PreventiveMaintainenceSchedule>, IEntityTypeConfiguration<PreventiveMaintainenceSchedule>
    {
        public override void Configure(EntityTypeBuilder<PreventiveMaintainenceSchedule> PreventiveMaintainenceScheduleConfiguration)
        {
            base.Configure(PreventiveMaintainenceScheduleConfiguration);
            PreventiveMaintainenceScheduleConfiguration.ToTable("PreventiveMaintainenceSchedule");

            PreventiveMaintainenceScheduleConfiguration.HasKey(o => o.Id);

            PreventiveMaintainenceScheduleConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("PreventiveMaintainenceScheduleseq");

            PreventiveMaintainenceScheduleConfiguration.Property(c => c.ContractNumber).IsRequired();
            PreventiveMaintainenceScheduleConfiguration.Property(c => c.OrderDate).IsRequired();
            PreventiveMaintainenceScheduleConfiguration.Property(c => c.FK_Customer_Id).IsRequired();
            PreventiveMaintainenceScheduleConfiguration.Property(c => c.FK_Contract_Id).IsRequired();
            PreventiveMaintainenceScheduleConfiguration.Property(c => c.FK_Location_Id).IsRequired();
            PreventiveMaintainenceScheduleConfiguration.Property(c => c.FK_OrderPriority_Id).IsRequired();
            PreventiveMaintainenceScheduleConfiguration.Property(c => c.FK_OrderType_Id).IsRequired();
            PreventiveMaintainenceScheduleConfiguration.Property(c => c.FK_OrderProblem_Id).IsRequired();
            PreventiveMaintainenceScheduleConfiguration.Property(c => c.QuotationRefNo).IsRequired(false);
        }
    }
}
