﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Utilites.UploadFile;

namespace DispatchProduct.Contracting.Entities
{
    public interface IContractFiles : IUploadFile, IBaseEntity
    {
        int Id { get; set; }

        int FK_Contract_Id { get; set; }

        Contract Contract { get; set; }
    }
}
