﻿using DispatchProduct.Contracting.Entities;
using DispatchProduct.Contracting.EntityConfiguration;
using Microsoft.EntityFrameworkCore;

namespace DispatchProduct.Contracting.Context
{
    public class ContractDbContext : DbContext
    {
        public ContractDbContext(DbContextOptions<ContractDbContext> options)
        : base(options)
        {
        }
        public DbSet<ContractFiles> ContractFiles { get; set; }
        public DbSet<Contract> Contract { get; set; }
        public DbSet<ContractType> ContractType { get; set; }
        public DbSet<ContractQuotation> ContractQuotation { get; set; }
        public DbSet<PreventiveMaintainenceSchedule> PreventiveMaintainenceSchedule { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ContractFilesEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ContractTypeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ContractEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ContractQuotationEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new PreventiveMaintainenceScheduleEntityTypeConfiguration());

        }
    }
}
