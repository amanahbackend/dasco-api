﻿using DispatchProduct.HttpClient;

namespace DispatchProduct.Contracting.API.Settings
{
    public class QuotationServiceSetting : DefaultHttpClientSettings
    {
        public override string Uri
        {
            get; set;
        }
        public string GetByRefVerb { get; set; }
    }
}
