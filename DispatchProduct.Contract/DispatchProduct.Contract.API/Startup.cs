﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using DispatchProduct.Contracting.Context;
using Microsoft.EntityFrameworkCore;
using DispatchProduct.Contracting.API.Settings;
using System.IdentityModel.Tokens.Jwt;
using IdentityServer4.AccessTokenValidation;
using Swashbuckle.AspNetCore.Swagger;
using AutoMapper;
using DispatchProduct.Repoistry;
using DispatchProduct.Contracting.IEntities;
using DispatchProduct.Contracting.Entities;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using DispatchProduct.Contracting.BLL.IManagers;
using DispatchProduct.Contracting.BLL.Managers;
using DispatchProduct.Contracting.API.ServicesCommunication.Quotation;
using DispatchProduct.Contracting.API.ServicesCommunication.Call;
using DispatchProduct.Contracting.Settings;
using DispatchProduct.Contracting.API.Controllers;
using Utilites.UploadFile;
using Microsoft.Extensions.FileProviders;
using System.IO;
using DispatchProduct.Inventory.ExcelSettings;

namespace DispatchProduct.Contracting.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });

            services.AddDbContext<ContractDbContext>(options =>
            options.UseSqlServer(Configuration["ConnectionString"],
            sqlOptions => sqlOptions.MigrationsAssembly("DispatchProduct.Contract.EFCore.MSSQL")));

            services.AddOptions();
            services.Configure<ContractAppSettings>(Configuration);
            services.Configure<QuotationServiceSetting>(Configuration.GetSection("QuotationServiceSetting"));
            services.Configure<CustomerServiceSetting>(Configuration.GetSection("CustomerServiceSetting"));
            services.Configure<CallServiceSetting>(Configuration.GetSection("CallServiceSetting"));
            services.Configure<ExcelSheetProperties>(Configuration.GetSection("ExcelSheetProperties"));
            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1",
                    new Info()
                    {
                        Title = "Contract API",
                        Description = "Contract API"
                    });
                c.AddSecurityDefinition("oauth2", new OAuth2Scheme
                {
                    Type = "oauth2",
                    Flow = "implicit",
                    AuthorizationUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/authorize",
                    TokenUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/token",
                    Scopes = new Dictionary<string, string>()
                    {
                        { "contract", "contract API" }
                    }
                });
            });

            ConfigureAuthService(services);
            services.AddMvc();
            services.AddDirectoryBrowser();
            services.AddAutoMapper(typeof(Startup));
            Mapper.AssertConfigurationIsValid();
            services.AddScoped<DbContext, ContractDbContext>();
            services.AddScoped(typeof(IRepositry<>), typeof(Repositry<>));
            services.AddTransient(typeof(IPreventiveMaintainenceSchedule), typeof(PreventiveMaintainenceSchedule));
            services.AddScoped(typeof(IContract), typeof(Contract));
            services.AddScoped(typeof(IContractFiles), typeof(ContractFiles));
            services.AddScoped(typeof(IContractFilesManager), typeof(ContractFilesManager));
            services.AddScoped(typeof(IUploadFileManager), typeof(UploadFileManager));
            services.AddScoped(typeof(IUploadFile), typeof(UploadFile));
            services.AddScoped(typeof(IContractType), typeof(ContractType));
            services.AddScoped(typeof(IContractQuotation), typeof(ContractQuotation));
            services.AddScoped(typeof(IContractQuotationManager), typeof(ContractQuotationManager));
            services.AddScoped(typeof(IContractManager), typeof(ContractManager));
            services.AddScoped(typeof(IContractTypeManager), typeof(ContractTypeManager));
            services.AddScoped(typeof(IQuotationService), typeof(QuotationService));
            services.AddScoped(typeof(ContractController), typeof(ContractController));
            services.AddScoped(typeof(ICustomerService), typeof(CustomerService));
            services.AddScoped(typeof(ICallService), typeof(CallService));
            services.AddScoped(typeof(IPreventiveMaintainenceScheduleManager), typeof(PreventiveMaintainenceScheduleManager));
            var container = new ContainerBuilder();
            container.Populate(services);

            return new AutofacServiceProvider(container.Build());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {

            app.UseCors("AllowAll");
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            var pathBase = Configuration["PATH_BASE"];
            if (!string.IsNullOrEmpty(pathBase))
            {
                loggerFactory.CreateLogger("init").LogDebug($"Using PATH BASE '{pathBase}'");
                app.UsePathBase(pathBase);
            }
            ConfigureAuth(app);
            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
            Path.Combine(Directory.GetCurrentDirectory(), "ContractFiles")),
                RequestPath = "/ContractFiles"
            });

            app.UseDirectoryBrowser(new DirectoryBrowserOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "ContractFiles")),
                RequestPath = "/ContractFiles"
            });
            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory(), "ContractFiles")),
                RequestPath = "/ContractFiles",
                EnableDirectoryBrowsing = true
            });
            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory(), "ExcelContracts")),
                RequestPath = "/ExcelContracts",
                EnableDirectoryBrowsing = true
            });

            //app.UseStaticFiles(new StaticFileOptions
            //{
            //    FileProvider = new PhysicalFileProvider(
            //        Path.Combine(Directory.GetCurrentDirectory(), "ContractFiles")),
            //    RequestPath = "/ContractFiles"
            //});
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Calling API");
            });


            // Make work identity server redirections in Edge and lastest versions of browers. WARN: Not valid in a production environment.
            //app.Use(async (context, next) =>
            //{
            //    context.Response.Headers.Add("Content-Security-Policy", "script-src 'unsafe-inline'");
            //    await next();
            //});

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private void ConfigureAuthService(IServiceCollection services)
        {
            // prevent from mapping "sub" claim to nameidentifier.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            var identityUrl = Configuration.GetValue<string>("IdentityUrl");
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
            .AddIdentityServerAuthentication(options =>
            {
                // base-address of your identityserver
                options.Authority = identityUrl;

                // name of the API resource
                options.ApiName = Configuration["ClientId"];
                options.ApiSecret = Configuration["Secret"];
                options.RequireHttpsMetadata = false;
                options.EnableCaching = true;
                options.CacheDuration = TimeSpan.FromMinutes(10);
                options.SaveToken = true;
            });

        }

        protected virtual void ConfigureAuth(IApplicationBuilder app)
        {
            app.UseAuthentication();
        }
    }
}
