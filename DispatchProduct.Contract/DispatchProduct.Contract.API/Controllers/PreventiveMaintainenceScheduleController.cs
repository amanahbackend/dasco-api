﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchProduct.Contracting.BLL.IManagers;
using DispatchProduct.Contracting.Entities;
using AutoMapper;
using DispatchProduct.Contracting.API.ViewModel;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Contracting.API.Controllers
{
    [Route("api/[controller]")]
    public class PreventiveMaintainenceScheduleController : Controller
    {
        public IPreventiveMaintainenceScheduleManager manger;
        public readonly IMapper mapper;
        public PreventiveMaintainenceScheduleController(IPreventiveMaintainenceScheduleManager _manger, IMapper _mapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;
        }
        // GET: api/values
        #region DefaultCrudOperation

        #region GetApi
        [HttpGet]
        [Route("Get/{id}")]
        public IActionResult Get(int id)
        {
            PreventiveMaintainenceScheduleViewModel result = new PreventiveMaintainenceScheduleViewModel();
            PreventiveMaintainenceSchedule entityResult = new PreventiveMaintainenceSchedule();
            entityResult = manger.Get(id);
            result = mapper.Map<PreventiveMaintainenceSchedule, PreventiveMaintainenceScheduleViewModel>(entityResult);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetDailyPreventiveOrders")]
        public IActionResult GetDailyPreventiveOrders()
        {
            var entityResult = manger.GetDailyPreventiveOrders();
            var result = mapper.Map<List<PreventiveMaintainenceSchedule>, List<PreventiveMaintainenceScheduleViewModel>>(entityResult);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetAll")]
        public IActionResult Get()
        {
            var entityResult = manger.GetAll().ToList();
            var result = mapper.Map<List<PreventiveMaintainenceSchedule>, List<PreventiveMaintainenceScheduleViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion
        [HttpPost]
        [Route("Filter")]
        public async Task<IActionResult> Filter([FromBody] FilterPreventiveViewModel model)
        {
            FilterPreventive filter = mapper.Map<FilterPreventiveViewModel, FilterPreventive>(model);
            List<PreventiveMaintainenceSchedule> list = manger.Filter(filter).ToList();
            List<PreventiveMaintainenceScheduleViewModel> scheduleViewModelList = mapper.Map<List<PreventiveMaintainenceSchedule>, List<PreventiveMaintainenceScheduleViewModel>>(list);
            return Ok(scheduleViewModelList);
        }
        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Route("Add")]
        public IActionResult Post([FromBody]PreventiveMaintainenceScheduleViewModel model)
        {
            PreventiveMaintainenceScheduleViewModel result = new PreventiveMaintainenceScheduleViewModel();
            PreventiveMaintainenceSchedule entityResult = new PreventiveMaintainenceSchedule();
            entityResult = mapper.Map<PreventiveMaintainenceScheduleViewModel, PreventiveMaintainenceSchedule>(model);
            // await fillEntityIdentity(entityResult);
            entityResult = manger.Add(entityResult);
            result = mapper.Map<PreventiveMaintainenceSchedule, PreventiveMaintainenceScheduleViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Route("Update")]
        public IActionResult Put([FromBody]PreventiveMaintainenceScheduleViewModel model)
        {
            bool result = false;
            PreventiveMaintainenceSchedule entityResult = new PreventiveMaintainenceSchedule();
            entityResult = mapper.Map<PreventiveMaintainenceScheduleViewModel, PreventiveMaintainenceSchedule>(model);
            // await fillEntityIdentity(entityResult);
            result = manger.Update(entityResult);
            return Ok(result);
        }

        [HttpPost]
        [Route("UpdatePreventiveMaintainenceByOrder")]
        public IActionResult UpdatePreventiveMaintainenceByOrder([FromBody] List<PreventiveOrderViewModel> model)
        {
            var entityModel = mapper.Map<List<PreventiveOrderViewModel>, List<PreventiveOrderModel>>(model);
            var entityResult = manger.UpdateByOrder(entityModel);
            return Ok(entityResult);
        }

        #endregion
        #region DeleteApi
        [HttpDelete]
        [Route("Delete/{id}")]
        public IActionResult Delete([FromRoute]int id)
        {
            bool result = false;
            PreventiveMaintainenceSchedule entity = manger.Get(id);
            // await fillEntityIdentity(entity);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion
    }
}
