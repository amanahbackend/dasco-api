﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using DispatchProduct.Contracting.BLL.IManagers;
using DispatchProduct.Contracting.Entities;
using AutoMapper;
using DispatchProduct.Contracting.API.ViewModel;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Contracting.API.Controllers
{
    [Route("api/[controller]")]
    //[Authorize]
    public class ContractQuotationController : Controller
    {
        public IContractQuotationManager manger;
        public readonly IMapper mapper;
        public ContractQuotationController(IContractQuotationManager _manger, IMapper _mapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;
        }
        // GET: api/values
        #region DefaultCrudOperation

        #region GetApi
        [HttpGet]
        [Route("Get/{id}")]
        public IActionResult Get(int id)
        {
            ContractQuotationViewModel result = new ContractQuotationViewModel();
            ContractQuotation entityResult = new ContractQuotation();
            entityResult = manger.Get(id);
            result = mapper.Map<ContractQuotation, ContractQuotationViewModel>(entityResult);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetAll")]
        public IActionResult Get()
        {
            List<ContractQuotationViewModel> result = new List<ContractQuotationViewModel>();
            List<ContractQuotation> entityResult = new List<ContractQuotation>();
            entityResult = manger.GetAll().ToList();
            result = mapper.Map<List<ContractQuotation>, List<ContractQuotationViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Route("Add")]
        public IActionResult Post([FromBody]ContractQuotationViewModel model)
        {
            ContractQuotationViewModel result = new ContractQuotationViewModel();
            ContractQuotation entityResult = new ContractQuotation();
            entityResult = mapper.Map<ContractQuotationViewModel, ContractQuotation>(model);
            // await fillEntityIdentity(entityResult);
            entityResult = manger.Add(entityResult);
            result = mapper.Map<ContractQuotation, ContractQuotationViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Route("Update")]
        public IActionResult Put([FromBody]ContractQuotationViewModel model)
        {
            bool result = false;
            ContractQuotation entityResult = new ContractQuotation();
            entityResult = mapper.Map<ContractQuotationViewModel, ContractQuotation>(model);
            // await fillEntityIdentity(entityResult);
            result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [HttpDelete]
        [Route("Delete/{id}")]

        public IActionResult Delete([FromRoute]int id)
        {
            bool result = false;
            ContractQuotation entity = manger.Get(id);
            // await fillEntityIdentity(entity);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion
    }
}
