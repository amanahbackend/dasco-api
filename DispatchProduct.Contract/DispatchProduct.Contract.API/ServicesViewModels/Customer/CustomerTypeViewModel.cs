﻿using DispatchProduct.Contracting.API.ViewModel;

namespace DispatchProduct.Contracting.API.ServicesViewModels
{
    public class CustomerTypeViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
