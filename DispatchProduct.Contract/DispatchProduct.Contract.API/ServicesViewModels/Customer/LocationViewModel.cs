﻿using DispatchProduct.Contracting.API.ViewModel;

namespace DispatchProduct.Contracting.API.ServicesViewModels
{
    public class LocationViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public string PACINumber { get; set; }
        public string Title { get; set; }
        public string Governorate { get; set; }
        public string Area { get; set; }
        public string Block { get; set; }
        public string Street { get; set; }
        public string AddressNote { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int Fk_Customer_Id { get; set; }
        public CustomerViewModel Customer { get; set; }
    }


}
