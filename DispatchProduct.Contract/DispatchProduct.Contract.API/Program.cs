﻿using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.DependencyInjection;
using DispatchProduct.Contracting.API.Seed;
using DispatchProduct.Contracting.Context;
using DispatchProduct.Contracting.Settings;

namespace DispatchProduct.Contracting.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).MigrateDbContext<ContractDbContext>((context, services) =>
            {
                var env = services.GetService<IHostingEnvironment>();
                var logger = services.GetService<ILogger<ContractDbContextSeed>>();
                var settings = services.GetService<IOptions<ContractAppSettings>>();
                new ContractDbContextSeed()
                    .SeedAsync(context, env, logger, settings)
                    .Wait();
            }).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
           WebHost.CreateDefaultBuilder(args)
               .UseKestrel()
               .UseContentRoot(Directory.GetCurrentDirectory())
               .UseIISIntegration()
               .UseStartup<Startup>()
               .ConfigureLogging((hostingContext, builder) =>
               {
                   builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                   builder.AddConsole();
                   builder.AddDebug();
               })
               .Build();
    }
}
