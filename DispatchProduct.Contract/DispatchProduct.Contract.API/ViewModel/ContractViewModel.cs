﻿using DispatchProduct.Contracting.API.ServicesViewModels;
using DispatchProduct.Contracting.Entities;
using System;
using System.Collections.Generic;

namespace DispatchProduct.Contracting.API.ViewModel
{
    public class ContractViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string ContractNumber { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public double Price { get; set; }

        public string Remarks { get; set; }

        public bool HasPreventiveMaintainence { get; set; }

        public PreventiveInfoViewModel PreventiveInfo { get; set; }

        public int PreventivePeriod { get; set; }

        public int FK_Customer_Id { get; set; }

        public string Area { get; set; }

        public CustomerViewModel Customer { get; set; }

        public int FK_ContractType_Id { get; set; }

        public ContractTypeViewModel ContractType { get; set; }

        public List<ContractQuotationViewModel> ContractQuotations { get; set; }

        public List<PreventiveMaintainenceSchedule> PreventiveMaintainence { get; set; }

        public List<ContractFilesViewModel> ContractFiles { get; set; }
    }
}
