﻿using DispatchProduct.Contracting.API.ServicesViewModels;

namespace DispatchProduct.Contracting.API.ViewModel
{
    public class ContractQuotationViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string QuotationRefNumber { get; set; }

        public int Fk_Contract_Id { get; set; }

        public ContractViewModel Contract { get; set; }

        public QuotationViewModel Quotation { get; set; }
    }
}