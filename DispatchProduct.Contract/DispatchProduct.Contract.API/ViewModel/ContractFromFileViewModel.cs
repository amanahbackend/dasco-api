﻿namespace DispatchProduct.Contracting.API.ViewModel
{
    public class ContractFromFileViewModel
    {
        public string ContractNumber { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Price { get; set; }
        public string Remarks { get; set; }
        public string HasPreventiveMaintainence { get; set; }
        public string PreventivePeriod { get; set; }
        public string Area { get; set; }
        public string ContractType { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerName { get; set; }
    }
}
