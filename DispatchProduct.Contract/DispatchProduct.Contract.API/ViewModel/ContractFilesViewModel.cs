﻿namespace DispatchProduct.Contracting.API.ViewModel
{
    public class ContractFilesViewModel : Utilites.UploadFile.UploadFile
    {
        public int Id { get; set; }

        public int FK_Contract_Id { get; set; }

        public ContractViewModel Contract { get; set; }

        public string URL { get; set; }
    }
}
