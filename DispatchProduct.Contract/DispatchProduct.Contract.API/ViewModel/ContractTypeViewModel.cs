﻿namespace DispatchProduct.Contracting.API.ViewModel
{
    public class ContractTypeViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }
    }
}
