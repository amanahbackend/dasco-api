﻿using DispatchProduct.Contracting.Context;
using DispatchProduct.Contracting.Entities;
using DispatchProduct.Repoistry;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DispatchProduct.Contracting.Settings;

namespace DispatchProduct.Contracting.API.Seed
{
    public class ContractDbContextSeed : ContextSeed
    {
        public ContractDbContextSeed()
        {
        }

        public async Task SeedAsync(ContractDbContext context, IHostingEnvironment env,
            ILogger<ContractDbContextSeed> logger, IOptions<ContractAppSettings> settings, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;

            try
            {
                var useCustomizationData = settings.Value.UseCustomizationData;
                var contentRootPath = env.ContentRootPath;
                var webroot = env.WebRootPath;
                List<ContractType> contractTypes = new List<ContractType>();

                if (useCustomizationData)
                {
                    //from file e.g (look at ApplicationDbContextSeed)
                }
                else
                {
                    //default from here
                    contractTypes = GetDefaultCallTypes();
                }
                await SeedEntityAsync(context, contractTypes);
            
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 3)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for ContractDbContext");

                    await SeedAsync(context, env, logger, settings, retryForAvaiability);
                }
            }
        }
        private List<ContractType> GetDefaultCallTypes()
        {
            List<ContractType> result = new List<ContractType>
            {
                new ContractType() { Name = "Individual" },
                new ContractType() { Name = "Assurance" }
            };
            return result;
        }
        
    }
}
