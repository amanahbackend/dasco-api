﻿using DispatchProduct.Contracting.API.ServicesViewModels;
using DispatchProduct.Contracting.API.Settings;
using DispatchProduct.HttpClient;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchProduct.Contracting.API.ServicesCommunication.Call
{
    public interface ICallService : IDefaultHttpClientCrud<CallServiceSetting, CallViewModel, CallViewModel>
    {
        Task<List<CallViewModel>> SearchCall(string key, string authHeader = "");
        Task<bool> UpdateCustomerById(CallViewModel model, string auth = "");
    }
}
