﻿using DispatchProduct.Contracting.API.ServicesViewModels;
using DispatchProduct.Contracting.API.Settings;
using DispatchProduct.HttpClient;
using System.Threading.Tasks;

namespace DispatchProduct.Contracting.API.ServicesCommunication.Quotation
{
    public interface ICustomerService : IDefaultHttpClientCrud<CustomerServiceSetting, CustomerViewModel, CustomerViewModel>
    {
        Task<CustomerViewModel> CreateCustomerFromCall(CustomerViewModel model, string authHeader = "");
        Task<CustomerViewModel> SearchCustomer(string key, string authHeader = "");
        Task<CustomerViewModel> GetBy(string name, string phone, string authHeader = "");
    }
}
