﻿using DispatchProduct.Contracting.API.ServicesViewModels;
using DispatchProduct.Contracting.API.Settings;
using DispatchProduct.HttpClient;
using System.Threading.Tasks;

namespace DispatchProduct.Contracting.API.ServicesCommunication.Quotation
{
    public interface IQuotationService : IDefaultHttpClientCrud<QuotationServiceSetting, QuotationViewModel, QuotationViewModel>
    {
        Task<QuotationViewModel> GetQuotationByRefNumber(string key, string authHeader = "");
    }
}
