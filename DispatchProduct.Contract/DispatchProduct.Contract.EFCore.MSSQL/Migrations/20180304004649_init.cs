﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DispatchProduct.Contracting.EFCore.MSSQL.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "ContractFileseq",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "ContractQuotationseq",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "Contractseq",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "ContractTypeseq",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "PreventiveMaintainenceScheduleseq",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "Contract",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    ContractNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EndDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FK_ContractType_Id = table.Column<int>(type: "int", nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_Customer_Id = table.Column<int>(type: "int", nullable: false),
                    FK_DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    HasPreventiveMaintainence = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    PreventivePeriod = table.Column<int>(type: "int", nullable: false),
                    Price = table.Column<double>(type: "float", nullable: false),
                    Remarks = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contract", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ContractFiles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FK_Contract_Id = table.Column<int>(type: "int", nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FileName = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    FileRelativePath = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractFiles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ContractQuotation",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Fk_Contract_Id = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    QuotationRefNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractQuotation", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ContractType",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PreventiveMaintainenceSchedule",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    ContractId = table.Column<int>(type: "int", nullable: true),
                    ContractNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FK_Contract_Id = table.Column<int>(type: "int", nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_Customer_Id = table.Column<int>(type: "int", nullable: false),
                    FK_DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_Location_Id = table.Column<int>(type: "int", nullable: false),
                    FK_OrderPriority_Id = table.Column<int>(type: "int", nullable: false),
                    FK_OrderProblem_Id = table.Column<int>(type: "int", nullable: false),
                    FK_OrderType_Id = table.Column<int>(type: "int", nullable: false),
                    FK_Order_Id = table.Column<int>(type: "int", nullable: false),
                    FK_UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    OrderDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    QuotationRefNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PreventiveMaintainenceSchedule", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PreventiveMaintainenceSchedule_Contract_ContractId",
                        column: x => x.ContractId,
                        principalTable: "Contract",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PreventiveMaintainenceSchedule_ContractId",
                table: "PreventiveMaintainenceSchedule",
                column: "ContractId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ContractFiles");

            migrationBuilder.DropTable(
                name: "ContractQuotation");

            migrationBuilder.DropTable(
                name: "ContractType");

            migrationBuilder.DropTable(
                name: "PreventiveMaintainenceSchedule");

            migrationBuilder.DropTable(
                name: "Contract");

            migrationBuilder.DropSequence(
                name: "ContractFileseq");

            migrationBuilder.DropSequence(
                name: "ContractQuotationseq");

            migrationBuilder.DropSequence(
                name: "Contractseq");

            migrationBuilder.DropSequence(
                name: "ContractTypeseq");

            migrationBuilder.DropSequence(
                name: "PreventiveMaintainenceScheduleseq");
        }
    }
}
