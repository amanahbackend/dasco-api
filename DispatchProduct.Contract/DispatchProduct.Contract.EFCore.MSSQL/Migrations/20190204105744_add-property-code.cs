﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DispatchProduct.Contracting.EFCore.MSSQL.Migrations
{
    public partial class addpropertycode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "ContractType",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Code",
                table: "ContractType");
        }
    }
}
