﻿using DispatchProduct.Contracting.BLL.IManagers;
using DispatchProduct.Contracting.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;
using DispatchProduct.Contracting.Context;
using System.Linq;

namespace DispatchProduct.Contracting.BLL.Managers
{
    public class ContractQuotationManager : Repositry<ContractQuotation>, IContractQuotationManager
    {
        public ContractQuotationManager(ContractDbContext context) : base(context)
        {
        }
        public List<ContractQuotation> GetByContractId(int contractId)
        {
            var result = GetAll().Where(c => c.Fk_Contract_Id == contractId).ToList();
            return result;
        }
    }
}
