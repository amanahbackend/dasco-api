﻿using DispatchProduct.Contracting.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;

namespace DispatchProduct.Contracting.BLL.IManagers
{
    public interface IPreventiveMaintainenceScheduleManager : IRepositry<PreventiveMaintainenceSchedule>
    {
        List<PreventiveMaintainenceSchedule> GetDailyPreventiveOrders();

        bool UpdateByOrder(List<PreventiveOrderModel> preventiveOrders);

        List<PreventiveMaintainenceSchedule> Filter(FilterPreventive filter);
    }
}
