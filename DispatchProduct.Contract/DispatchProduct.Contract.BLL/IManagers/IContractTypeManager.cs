﻿using DispatchProduct.Contracting.Entities;
using DispatchProduct.Repoistry;

namespace DispatchProduct.Contracting.BLL.IManagers
{
    public interface IContractTypeManager : IRepositry<ContractType>
    {
    }
}
