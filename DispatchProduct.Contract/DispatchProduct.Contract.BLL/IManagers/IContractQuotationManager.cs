﻿using DispatchProduct.Contracting.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;

namespace DispatchProduct.Contracting.BLL.IManagers
{
    public interface IContractQuotationManager : IRepositry<ContractQuotation>
    {
        List<ContractQuotation> GetByContractId(int contractId);
    }
}
