﻿
using DispatchProduct.Employees.Salary.API.Settings;
using DispatchProduct.Employees.Salary.Context;
using DispatchProduct.Employees.Salary.Entities;
using DispatchProduct.Repoistry;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchProduct.Employees.Salary.API.Seed
{
    public class EmployeeSalaryDbContextSeed : ContextSeed
    {
        public async Task SeedAsync(EmployeeSalaryDbContext context, IHostingEnvironment env,
            ILogger<EmployeeSalaryDbContextSeed> logger, IOptions<EmployeesSalaryAppSettings> settings, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;

            try
            {
                var useCustomizationData = settings.Value.UseCustomizationData;
                var contentRootPath = env.ContentRootPath;
                var webroot = env.WebRootPath;
                List<Currency> lstCurrencies = new List<Currency>();
                List<Period> lstPeriods = new List<Period>();
                List<EmployeeSalary> lstemployeeSalary = new List<EmployeeSalary>();

                if (useCustomizationData)
                {
                    //from file e.g (look at ApplicationDbContextSeed)
                }
                else
                {
                    //default from here
                    lstCurrencies = GetDefaultCurrencies();
                    lstPeriods = GetDefaultPeriod();
                    lstemployeeSalary = GetDefaultEmployeeSalary();
                }
                await SeedEntityAsync(context, lstCurrencies);
            
                await SeedEntityAsync(context, lstPeriods);
           
                await SeedEntityAsync(context, lstemployeeSalary);
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 3)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for ApplicationDbContext");

                    await SeedAsync(context, env, logger, settings, retryForAvaiability);
                }
            }
        }
        private List<Currency> GetDefaultCurrencies()
        {
            List<Currency> result = new List<Currency>
            {
                new Currency() { Name = "USD" },
                new Currency() { Name = "EGP" }
            };
            return result;
        }

        private List<Period> GetDefaultPeriod()
        {
            List<Period> result = new List<Period>
            {
                new Period() { Name = "Per Hour" },
                new Period() { Name = "Per Month" },
                new Period() { Name = "Per Week" },
                new Period() { Name = "Per Year" }
            };

            return result;
        }

        private List<EmployeeSalary> GetDefaultEmployeeSalary()
        {
            List<EmployeeSalary> result = new List<EmployeeSalary>
            {
                
            };
            return result;
        }

    }
}
