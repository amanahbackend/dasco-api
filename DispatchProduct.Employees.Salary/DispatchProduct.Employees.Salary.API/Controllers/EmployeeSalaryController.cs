﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using DispatchProduct.Employees.Salary.API.ViewModel;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.Employees.Salary.BLL.IManagers;
using DispatchProduct.Employees.Salary.Entities;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Employees.Salary.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]

    public class EmployeeSalaryController : Controller
    {
        public IEmployeeSalaryManager manger;
        public readonly IMapper mapper;
        public EmployeeSalaryController(IEmployeeSalaryManager _manger, IMapper _mapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;
        }
        // GET: api/values
        #region DefaultCrudOperation

        #region GetApi
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
           EmployeeSalaryViewModel result = new EmployeeSalaryViewModel();
           EmployeeSalary entityResult = new EmployeeSalary();
            entityResult = manger.Get(id);
            result = mapper.Map<EmployeeSalary, EmployeeSalaryViewModel>(entityResult);
            return Ok(result);
        }

        [HttpGet]
        public IActionResult Get()
        {
            List<EmployeeSalaryViewModel> result = new List<EmployeeSalaryViewModel>();
            List<EmployeeSalary> entityResult = new List<EmployeeSalary>();
            entityResult = manger.GetAll().ToList();
            result = mapper.Map<List<EmployeeSalary>, List<EmployeeSalaryViewModel>>(entityResult);
            return Ok(result);
        }
        [HttpGet("{empId}")]
        [Route("GetByEmpId/{empId}")]
        public IActionResult GetByEmpId(string empId)
        {
            EmployeeSalaryViewModel result = new EmployeeSalaryViewModel();
            EmployeeSalary entityResult = new EmployeeSalary();
            entityResult = manger.GetByEmployeeId(empId);
            result = mapper.Map<EmployeeSalary, EmployeeSalaryViewModel>(entityResult);
            return Ok(result);
        }
        #endregion
        [Route("Add")]
        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]EmployeeSalaryViewModel model)
        {
            EmployeeSalaryViewModel result = new EmployeeSalaryViewModel();
            EmployeeSalary entityResult = new EmployeeSalary();
            entityResult = mapper.Map<EmployeeSalaryViewModel, EmployeeSalary>(model);
           // await fillEntityIdentity(entityResult);
            entityResult = manger.Add(entityResult);
            result = mapper.Map<EmployeeSalary, EmployeeSalaryViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Put([FromBody]EmployeeSalaryViewModel model)
        {
            bool result = false;
            EmployeeSalary entityResult = new EmployeeSalary();
            entityResult = mapper.Map<EmployeeSalaryViewModel, EmployeeSalary>(model);
           // await fillEntityIdentity(entityResult);
            result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [HttpDelete]
        
        [Route("Delete/{id}")]

        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            EmployeeSalary entity = manger.Get(id);
           // await fillEntityIdentity(entity);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion
    }
}
