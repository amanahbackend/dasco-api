﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using DispatchProduct.Employees.Salary.API.ViewModel;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.Employees.Salary.BLL.IManagers;
using DispatchProduct.Employees.Salary.Entities;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Employees.Salary.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]

    public class PeriodController : Controller
    {
        public IPeriodManager manger;
        public readonly IMapper mapper;
        public PeriodController(IPeriodManager _manger, IMapper _mapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;
        }
        // GET: api/values
        #region DefaultCrudOperation

        #region GetApi
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
           PeriodViewModel result = new PeriodViewModel();
           Period entityResult = new Period();
            entityResult = manger.Get(id);
            result = mapper.Map<Period, PeriodViewModel>(entityResult);
            return Ok(result);
        }

        [HttpGet]
        public IActionResult Get()
        {
            List<PeriodViewModel> result = new List<PeriodViewModel>();
            List<Period> entityResult = new List<Period>();
            entityResult = manger.GetAll().ToList();
            result = mapper.Map<List<Period>, List<PeriodViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion
        [Route("Add")]
        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]PeriodViewModel model)
        {
            PeriodViewModel result = new PeriodViewModel();
            Period entityResult = new Period();
            entityResult = mapper.Map<PeriodViewModel, Period>(model);
           // await fillEntityIdentity(entityResult);
            entityResult = manger.Add(entityResult);
            result = mapper.Map<Period, PeriodViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Put([FromBody]PeriodViewModel model)
        {
            bool result = false;
            Period entityResult = new Period();
            entityResult = mapper.Map<PeriodViewModel, Period>(model);
           // await fillEntityIdentity(entityResult);
            result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [HttpDelete]
        
        [Route("Delete/{id}")]

        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            Period entity = manger.Get(id);
           // await fillEntityIdentity(entity);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion
    }
}
