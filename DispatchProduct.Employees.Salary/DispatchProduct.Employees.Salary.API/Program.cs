﻿using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;

using DispatchProduct.Employees.Salary.Context;
using DispatchProduct.Employees.Salary.API.Settings;
using Microsoft.Extensions.Options;
using DispatchProduct.Employees.Salary.API.Seed;

namespace DispatchProduct.Employees.Salary.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).MigrateDbContext<EmployeeSalaryDbContext>((context, services) =>
            {
                var env = services.GetService<IHostingEnvironment>();
                var logger = services.GetService<ILogger<EmployeeSalaryDbContextSeed>>();
                var settings = services.GetService<IOptions<EmployeesSalaryAppSettings>>();
                new EmployeeSalaryDbContextSeed()
                    .SeedAsync(context, env, logger, settings)
                    .Wait();
            }).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
           WebHost.CreateDefaultBuilder(args)
               .UseKestrel()
               .UseContentRoot(Directory.GetCurrentDirectory())
               .UseIISIntegration()
               .UseStartup<Startup>()
               .ConfigureLogging((hostingContext, builder) =>
               {
                   builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                   builder.AddConsole();
                   builder.AddDebug();
               })
               .Build();
    }
}
