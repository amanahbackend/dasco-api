﻿using AutoMapper;
using DispatchProduct.Employees.Salary.API.ViewModel;
using DispatchProduct.Employees.Salary.Entities;

namespace DispatchProduct.Customer.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<CurrencyViewModel, Currency>();
            CreateMap<Currency, CurrencyViewModel>();

            CreateMap<PeriodViewModel, Period>();
            CreateMap<Period, PeriodViewModel>();
            
            CreateMap<EmployeeSalaryViewModel, EmployeeSalary>()
             .ForMember(dest => dest.Period, opt => opt.MapFrom(src => src.Period))
           .ForMember(dest => dest.Currency, opt => opt.MapFrom(src => src.Currency));
            CreateMap<EmployeeSalary, EmployeeSalaryViewModel> ()
             .ForMember(dest => dest.Period, opt => opt.MapFrom(src => src.Period))
            .ForMember(dest => dest.Currency, opt => opt.MapFrom(src => src.Currency));
            
        }
    }
}
