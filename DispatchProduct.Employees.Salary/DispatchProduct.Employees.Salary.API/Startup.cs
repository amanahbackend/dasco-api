﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.IdentityModel.Tokens.Jwt;
using IdentityServer4.AccessTokenValidation;
using Swashbuckle.AspNetCore.Swagger;
using DispatchProduct.Employees.Salary.Context;
using Microsoft.EntityFrameworkCore;
using DispatchProduct.Employees.Salary.API.Settings;
using AutoMapper;
using DispatchProduct.Repoistry;
using DispatchProduct.Employees.Salary.IEntities;
using DispatchProduct.Employees.Salary.Entities;
using DispatchProduct.Employees.Salary.BLL.IManagers;
using DispatchProduct.Employees.Salary.BLL.Managers;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using DispatchProduct.Employees.Salary.ExcelSettings;

namespace DispatchProduct.Employees.Salary.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });
            services.AddDbContext<EmployeeSalaryDbContext>(options =>
            options.UseSqlServer(Configuration["ConnectionString"],
            sqlOptions => sqlOptions.MigrationsAssembly("DispatchProduct.Employees.Salary.EFCore.MSSQL")));

            services.AddOptions();
            services.Configure<EmployeesSalaryAppSettings>(Configuration);
            services.Configure<ExcelSheetProperties>(Configuration.GetSection("ExcelSheetProperties"));


            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1",
                    new Info()
                    {
                        Title = "Salary API",
                        Description = "Salary  API"
                    });
                c.AddSecurityDefinition("oauth2", new OAuth2Scheme
                {
                    Type = "oauth2",
                    Flow = "implicit",
                    AuthorizationUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/authorize",
                    TokenUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/token",
                    Scopes = new Dictionary<string, string>()
                    {
                        { "Salary", "Salary API" }
                    }
                });
            });

            ConfigureAuthService(services);

            services.AddMvc();

            services.AddAutoMapper(typeof(Startup));
            Mapper.AssertConfigurationIsValid();

            services.AddScoped<DbContext, EmployeeSalaryDbContext>();
            services.AddScoped(typeof(IRepositry<>), typeof(Repositry<>));
            services.AddScoped(typeof(ICurrency), typeof(Currency));
            services.AddScoped(typeof(IEmployeeSalary), typeof(EmployeeSalary));
            services.AddScoped(typeof(IPeriod), typeof(Period));
            services.AddScoped(typeof(ICurrencyManager), typeof(CurrencyManager));
            services.AddScoped(typeof(IEmployeeSalaryManager), typeof(EmployeeSalaryManager));
            services.AddScoped(typeof(IPeriodManager), typeof(PeriodManager));
            services.AddScoped(typeof(IExcelSalaryManager), typeof(ExcelSalaryManager));
            services.AddScoped(typeof(IRepositry<>), typeof(Repositry<>));

            var container = new ContainerBuilder();
            container.Populate(services);

            return new AutofacServiceProvider(container.Build());
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCors("AllowAll");

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            var pathBase = Configuration["PATH_BASE"];
            if (!string.IsNullOrEmpty(pathBase))
            {
                loggerFactory.CreateLogger("init").LogDebug($"Using PATH BASE '{pathBase}'");
                app.UsePathBase(pathBase);
            }
            ConfigureAuth(app);
            app.UseStaticFiles();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Calling API");
            });


            // Make work identity server redirections in Edge and lastest versions of browers. WARN: Not valid in a production environment.
            //app.Use(async (context, next) =>
            //{
            //    context.Response.Headers.Add("Content-Security-Policy", "script-src 'unsafe-inline'");
            //    await next();
            //});

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
       
        private void ConfigureAuthService(IServiceCollection services)
        {
            // prevent from mapping "sub" claim to nameidentifier.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            var identityUrl = Configuration.GetValue<string>("IdentityUrl");
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
            .AddIdentityServerAuthentication(options =>
            {
                // base-address of your identityserver
                options.Authority = identityUrl;

                // name of the API resource
                options.ApiName = Configuration["ClientId"];
                options.ApiSecret = Configuration["Secret"];
                options.RequireHttpsMetadata = false;
                options.EnableCaching = true;
                options.CacheDuration = TimeSpan.FromMinutes(10);
                options.SaveToken = true;
            });

        }

        protected virtual void ConfigureAuth(IApplicationBuilder app)
        {
            app.UseAuthentication();
        }
    }
}
