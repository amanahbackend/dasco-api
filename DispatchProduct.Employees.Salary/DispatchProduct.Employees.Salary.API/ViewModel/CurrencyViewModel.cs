﻿using System;
using System.Linq.Expressions;
using DispatchProduct.Employees.Salary.API.ViewModel.Infrastructure;
using DispatchProduct.Employees.Salary.Entities;

namespace DispatchProduct.Employees.Salary.API.ViewModel
{
    public class CurrencyViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class CurrencyViewModelMapper : MapperBase<Currency, CurrencyViewModel>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        private BaseEntityViewModelMapper _baseEntityMapper = new BaseEntityViewModelMapper();
        public override Expression<Func<Currency, CurrencyViewModel>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<Currency, CurrencyViewModel>>)(p => new CurrencyViewModel()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    Id = p.Id,
                    Name = p.Name
                })).MergeWith(this._baseEntityMapper.SelectorExpression);
            }
        }

        public override void MapToModel(CurrencyViewModel dto, Currency model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.Id = dto.Id;
            model.Name = dto.Name;
            this._baseEntityMapper.MapToModel(dto, model);
        }
    }
}
