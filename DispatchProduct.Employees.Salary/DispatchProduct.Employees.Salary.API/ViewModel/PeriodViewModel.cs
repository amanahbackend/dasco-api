﻿using System;
using System.Linq.Expressions;
using DispatchProduct.Employees.Salary.API.ViewModel.Infrastructure;
using DispatchProduct.Employees.Salary.Entities;

namespace DispatchProduct.Employees.Salary.API.ViewModel
{
    public class PeriodViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class PeriodViewModelMapper : MapperBase<Period, PeriodViewModel>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        private BaseEntityViewModelMapper _baseEntityMapper = new BaseEntityViewModelMapper();
        public override Expression<Func<Period, PeriodViewModel>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<Period, PeriodViewModel>>)(p => new PeriodViewModel()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    Id = p.Id,
                    Name = p.Name
                })).MergeWith(this._baseEntityMapper.SelectorExpression);
            }
        }

        public override void MapToModel(PeriodViewModel dto, Period model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.Id = dto.Id;
            model.Name = dto.Name;
            this._baseEntityMapper.MapToModel(dto, model);
        }
    }
}
