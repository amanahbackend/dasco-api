﻿using DispatchProduct.Employees.Salary.Entities;
using DispatchProduct.Employees.Salary.ExcelSettings;
using System.Collections.Generic;
using Utilites.ProcessingResult;
using Utilites.UploadFile;

namespace DispatchProduct.Employees.Salary.BLL.IManagers
{
    public interface IExcelSalaryManager
    {
        ProcessResult<List<EmployeeSalary>> Process(string path, UploadFile Uploadfile, ExcelSheetProperties excelSheetProperties);
    }
}
