﻿using DispatchProduct.Repoistry;
using DispatchProduct.Employees.Salary.Entities;

namespace DispatchProduct.Employees.Salary.BLL.IManagers
{
    public interface IEmployeeSalaryManager : IRepositry<EmployeeSalary>
    {
        EmployeeSalary GetByEmployeeId(string empId);
    }
}
