﻿using DispatchProduct.Repoistry;
using DispatchProduct.Employees.Salary.BLL.IManagers;
using DispatchProduct.Employees.Salary.Entities;
using DispatchProduct.Employees.Salary.Context;

namespace DispatchProduct.Employees.Salary.BLL.Managers
{
    public class EmployeeSalaryManager : Repositry<EmployeeSalary>, IEmployeeSalaryManager
    {
        public EmployeeSalaryManager(EmployeeSalaryDbContext context )
            : base(context)
        {

        }
        public EmployeeSalary GetByEmployeeId(string empId)
        {
            EmployeeSalary result = null;
            if (empId != null)
            {
                result = Get(emp => emp.EmployeeId == empId);
            }
            return result;
        }
    }
}
