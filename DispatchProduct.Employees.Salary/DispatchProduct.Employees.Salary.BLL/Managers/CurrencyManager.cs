﻿using DispatchProduct.Employees.Salary.BLL.IManagers;
using DispatchProduct.Employees.Salary.Context;
using DispatchProduct.Employees.Salary.Entities;
using DispatchProduct.Repoistry;

namespace DispatchProduct.Employees.Salary.BLL.Managers
{
    public class CurrencyManager : Repositry<Currency>, ICurrencyManager
    {
        public CurrencyManager(EmployeeSalaryDbContext context)
            : base(context)
        {

        }
    }
}
