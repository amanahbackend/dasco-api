﻿namespace DispatchProduct.Employees.Salary.ExcelSettings
{
    public class ExcelSheetProperties
    {
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string Currency { get; set; }
        public string Period { get; set; }
        public string Salary { get; set; }
    }
}
