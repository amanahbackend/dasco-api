﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using DispatchProduct.Employees.Salary.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DispatchProduct.Employees.Salary.EntityConfigurations
{
    public class EmployeeSalaryEntityTypeConfiguration : BaseEntityTypeConfiguration<EmployeeSalary>, IEntityTypeConfiguration<EmployeeSalary>
    {
        public void Configure(EntityTypeBuilder<EmployeeSalary> EmployeeSalaryConfiguration)
        {
            base.Configure(EmployeeSalaryConfiguration);

            EmployeeSalaryConfiguration.ToTable("EmployeeSalary");

            EmployeeSalaryConfiguration.HasKey(o => o.Id);

            EmployeeSalaryConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("EmployeeSalaryseq");
            EmployeeSalaryConfiguration.Property(o => o.Salary).IsRequired();
            EmployeeSalaryConfiguration.Property(o => o.EmployeeId).IsRequired();
            EmployeeSalaryConfiguration.Property(o => o.FK_SalaryPeriod_Id).IsRequired();
            EmployeeSalaryConfiguration.Property(o => o.FK_Currency_Id).IsRequired();
        }
    }
}