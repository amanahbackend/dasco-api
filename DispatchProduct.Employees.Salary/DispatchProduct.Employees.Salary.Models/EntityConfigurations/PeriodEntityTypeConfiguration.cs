﻿using DispatchProduct.Employees.Salary.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Employees.Salary.EntityConfigurations
{
    public class PeriodEntityTypeConfiguration
        : IEntityTypeConfiguration<Period>
    {
        public void Configure(EntityTypeBuilder<Period> PeriodConfiguration)
        {
            PeriodConfiguration.ToTable("Period");

            PeriodConfiguration.HasKey(o => o.Id);

            PeriodConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("Periodseq");


            PeriodConfiguration.Property(o => o.Name)
                .HasMaxLength(500)
                .IsRequired();
        }
    }
}
