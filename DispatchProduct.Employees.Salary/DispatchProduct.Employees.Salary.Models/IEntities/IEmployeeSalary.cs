﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.Employees.Salary.IEntities
{
    public interface IEmployeeSalary:IBaseEntity
    {
        int Id { get; set; }
        string EmployeeId { get; set; }
        double Salary { get; set; }
        
    }
}
