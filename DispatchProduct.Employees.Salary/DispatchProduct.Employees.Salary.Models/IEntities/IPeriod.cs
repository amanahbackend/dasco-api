﻿namespace DispatchProduct.Employees.Salary.IEntities
{
    public interface IPeriod
    {
         int Id { get; set; }
         string Name { get; set; }
    }
}
