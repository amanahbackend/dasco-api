﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.Employees.Salary.IEntities
{
    public interface ICurrency : IBaseEntity
    {
        int Id { get; set; }
        string Name { get; set; }
    }
}
