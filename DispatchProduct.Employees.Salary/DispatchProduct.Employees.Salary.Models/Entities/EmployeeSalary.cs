﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using DispatchProduct.Employees.Salary.IEntities;
namespace DispatchProduct.Employees.Salary.Entities
{
    public class EmployeeSalary:BaseEntity, IEmployeeSalary
    {
       public int Id { get; set; }
       public string EmployeeId { get; set; }
       public string EmployeeName { get; set; }
       public double Salary { get; set; }
       public int FK_Currency_Id { get; set; }
       [NotMapped]
       public Currency Currency { get; set; }
       public int FK_SalaryPeriod_Id { get; set; }
       [NotMapped]
       public Period Period { get; set; }

    }
}
