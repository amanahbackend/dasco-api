﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;

namespace DispatchProduct.Employees.Salary.Entities
{
    public class ExcelEmployeeSalary : BaseEntity
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string Currency { get; set; }
        public string Period { get; set; }
        public double Salary { get; set; }
    }

}
