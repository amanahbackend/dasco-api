﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.Employees.Salary.IEntities;

namespace DispatchProduct.Employees.Salary.Entities
{
    public class Currency : BaseEntity, ICurrency
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
