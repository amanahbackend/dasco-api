﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.Calling.Entities
{
    public interface ICallPriority : IBaseEntity
    {
        int Id { get; set; }

        string Name { get; set; }
    }
}
