﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.Calling.Entities
{
    public interface ICallType :IBaseEntity
    {
        int Id { get; set; }

        string Name { get; set; }
    }
}
