﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System.Collections.Generic;

namespace DispatchProduct.Calling.Entities
{
    public interface ICall : IBaseEntity
    {
        int Id { get; set; }

        string CallerNumber { get; set; }

        string CallerName { get; set; }

        string CustomerServiceName { get; set; }
        string CustomerServiceUserName { get; set; }

        string CustomerDescription { get; set; }

        int FK_CallPriority_Id { get; set; }
        CallPriority CallPriority { get; set; }

         int FK_CustomerType_Id { get; set; }

         int FK_PhoneType_Id { get; set; }
        string CallStatus { get; set; }

        int FK_CallType_Id { get; set; }
        CallType CallType { get; set; }
        bool HasEstimation { get; set; }

        #region LocationAttributes
        string PACINumber { get; set; }
        string Governorate { get; set; }
        string Area { get; set; }
        string Block { get; set; }
        string Street { get; set; }
        string AddressNote { get; set; }
        string Title { get; set; }
        double Latitude { get; set; }
        double Longitude { get; set; }
        #endregion

        ICollection<CallLog> Logs { get; set; }
    }
}

