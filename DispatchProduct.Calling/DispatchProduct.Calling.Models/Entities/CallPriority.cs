﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.Calling.Entities
{
    public class CallPriority : BaseEntity, ICallPriority, IBaseEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
