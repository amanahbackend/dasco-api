﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace DispatchProduct.Calling.Entities
{
    public class CallLog : BaseEntity, ICallLog, IBaseEntity
    {
        public int FK_CallStatus_Id { get; set; }

        public string CustomerServiceComment { get; set; }

        public string CustomerServiceName { get; set; }

        public string CustomerServiceUserName { get; set; }

        public CallStatus CallStatus { get; set; }

        public int Id { get; set; }

        public int FK_Call_Id { get; set; }
    }
}
