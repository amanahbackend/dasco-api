﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System.Collections.Generic;

namespace DispatchProduct.Calling.Entities
{
    public class Call : BaseEntity, ICall, IBaseEntity
    {
        public int Id { get; set; }

        public string CallerNumber { get; set; }

        public string CallerName { get; set; }

        public string CustomerServiceName { get; set; }

        public string CustomerServiceUserName { get; set; }

        public bool NeedAction { get; set; }

        public int FK_Customer_Id { get; set; }

        public string CustomerDescription { get; set; }

        public int FK_CustomerType_Id { get; set; }

        public int FK_PhoneType_Id { get; set; }

        public int FK_CallPriority_Id { get; set; }

        public CallPriority CallPriority { get; set; }

        public string CallStatus { get; set; }

        public int FK_CallType_Id { get; set; }

        public CallType CallType { get; set; }

        public string Title { get; set; }


        #region Area

        public string PACINumber { get; set; }

        public string Governorate { get; set; }

        public string Area { get; set; }

        public string Block { get; set; }

        public string Street { get; set; }

        public string AddressNote { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }
        #endregion

        public ICollection<CallLog> Logs { get; set; }

        public bool HasEstimation { get; set; }

        public string EstimationRefNo { get; set; }
    }
}
