﻿using AutoMapper;
using DispatchProduct.Calling.API.ViewModel;
using DispatchProduct.Calling.Entities;

namespace DispatchProduct.Calling.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<CallPriority, CallPriorityViewModel>();
            CreateMap<CallPriorityViewModel, CallPriority>();

            CreateMap<CallType, CallTypeViewModel>();
            CreateMap<CallTypeViewModel, CallType>();

            CreateMap<CallStatusViewModel, CallStatus>();
            CreateMap<CallStatus, CallStatusViewModel>();

            CreateMap<CallLog, CallLogViewModel>()
            .ForMember(dest => dest.CallStatus, opt => opt.MapFrom(src => src.CallStatus));
            CreateMap<CallLogViewModel, CallLog>()
            .ForMember(dest => dest.CallStatus, opt => opt.MapFrom(src => src.CallStatus));


           CreateMap<CallViewModel, PointInputViewModel>()
                .ForMember(dest => dest.Street,opt => opt.MapFrom(src => src.Street))
                .ForMember(dest => dest.Block,opt => opt.MapFrom(src => src.Block))
                .ForAllOtherMembers(dest => dest.Ignore());
            CreateMap<PointInputViewModel, CallViewModel>()
                .ForMember(dest => dest.Street,opt => opt.MapFrom(src => src.Street))
                .ForMember(dest => dest.Block,opt => opt.MapFrom(src => src.Block))
                .ForAllOtherMembers(dest => dest.Ignore());

            CreateMap<CallViewModel, Call>()
            .ForMember(dest => dest.CallPriority, opt => opt.MapFrom(src => src.CallPriority))
            .ForMember(dest => dest.CallStatus, opt => opt.MapFrom(src => src.CallStatus))
            .ForMember(dest => dest.CallType, opt => opt.MapFrom(src => src.CallType))
            .ForMember(dest => dest.Logs, opt => opt.MapFrom(src => src.Logs));

            CreateMap<Call, CallViewModel>()
           .ForMember(dest => dest.CallPriority, opt => opt.MapFrom(src => src.CallPriority))
           .ForMember(dest => dest.CallStatus, opt => opt.MapFrom(src => src.CallStatus))
           .ForMember(dest => dest.CallType, opt => opt.MapFrom(src => src.CallType))
           .ForMember(dest => dest.Logs, opt => opt.MapFrom(src => src.Logs));


        }
    }
}
