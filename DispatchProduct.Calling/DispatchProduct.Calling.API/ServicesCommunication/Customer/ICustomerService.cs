﻿using DispatchProduct.Calling.API.ServicesViewCustomer;
using DispatchProduct.Calling.API.Settings;
using DispatchProduct.HttpClient;

namespace DispatchProduct.Calling.API.ServicesCommunication.Customer
{
    public interface ICustomerService: IDefaultHttpClientCrud<CustomerServiceSettings, CustomerViewModel, CustomerViewModel>
    {
        
    }
}
