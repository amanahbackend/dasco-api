﻿using DispatchProduct.Calling.API.ServicesViewCustomer;
using DispatchProduct.Calling.API.Settings;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;

namespace DispatchProduct.Calling.API.ServicesCommunication.Customer
{
    public class CustomerService: DefaultHttpClientCrud<CustomerServiceSettings,CustomerViewModel, CustomerViewModel>, ICustomerService
    {
        public CustomerService(IOptions<CustomerServiceSettings> settings) :base(settings.Value)
        {

        }
    }
}
