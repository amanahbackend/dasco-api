﻿using DispatchProduct.Calling.API.ViewModel;
using DispatchProduct.Calling.Settings;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites;

namespace DispatchProduct.Calling.Hubs
{
    public class CallingHub : Hub, ICallingHub
    {
        private static List<SignalRConnection> connections = new List<SignalRConnection>();
        private CallingHubSettings setting;

        public CallingHub(IOptions<CallingHubSettings> _setting)
        {
            setting = _setting.Value;
        }

        public async Task AddCall(CallViewModel order, IHubContext<CallingHub> callingHub)
        {
            if (callingHub.Clients != null)
            {
               await callingHub.Clients.Group(setting.AddCallGroupName).InvokeAsync(setting.AddCall, order);
            }
        }

        public async Task JoinGroup(string groupName)
        {
            await Groups.AddAsync(Context.ConnectionId, groupName);
        }

        public async Task LeaveGroup(string groupName)
        {
            if (connections.Where(r=>r.ConnectionId==Context.ConnectionId).FirstOrDefault() != null)
            {
                await Groups.RemoveAsync(Context.ConnectionId, groupName);
            }
        }

        public override Task OnConnectedAsync()
        {
            SignalRConnection signalRconnection = new SignalRConnection();
            HttpContext httpContext = Context.Connection.GetHttpContext();
            string token = httpContext.Request.Query[setting.token].ToString();
            string userId = httpContext.Request.Query[setting.userId].ToString();
            List<string> roles = ((IEnumerable<string>)httpContext.Request.Query[setting.roles].ToString().Split(',', StringSplitOptions.None)).ToList<string>();
            if (token != null && userId != null && roles != null)
            {
                signalRconnection.Token = token;
                signalRconnection.Roles = roles;
                signalRconnection.UserId = userId;
                signalRconnection.ConnectionId = Context.ConnectionId;
                connections.Add(signalRconnection);
                if (setting.AddCallRoles.Contains("All"))
                {
                    JoinGroup(setting.AddCallGroupName).Wait();
                }
                else
                {
                    foreach (string itm in roles)
                    {
                        List<string> addCallRoles = new List<string>();
                        foreach (var item in setting.AddCallRoles)
                        {
                            addCallRoles.Add(item.ToLower());
                        }
                        if (addCallRoles.Contains(itm.ToLower()))
                        {
                            JoinGroup(setting.AddCallGroupName).Wait();
                            break;
                        }
                    }
                }
            }
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception ex)
        {
            LeaveGroup(setting.AddCallGroupName);
            return base.OnDisconnectedAsync(ex);
        }
    }
}
