﻿

using DispatchProduct.Calling.API.ViewModel;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace DispatchProduct.Calling.Hubs
{
    public interface ICallingHub
    {
        Task JoinGroup(string groupName);

        Task LeaveGroup(string groupName);

        Task OnConnectedAsync();

        Task AddCall(CallViewModel order, IHubContext<CallingHub> orderingHub);
    }
}
