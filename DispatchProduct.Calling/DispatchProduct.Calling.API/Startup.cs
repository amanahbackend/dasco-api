﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using DispatchProduct.Identity.Context;
using Microsoft.EntityFrameworkCore;
using DispatchProduct.Calling.API.Settings;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using AutoMapper;
using DispatchProduct.Repoistry;
using DispatchProduct.Calling.Entities;
using DispatchingProduct.Calling.BLL.IManagers;
using DispatchingProduct.Calling.BLL.Managers;
using System.IdentityModel.Tokens.Jwt;
using IdentityServer4.AccessTokenValidation;
using DispatchProduct.HttpClient;
using DispatchProduct.Calling.API.ServicesCommunication.Customer;
using DispatchProduct.Calling.API.ServicesCommunication;
using DispatchProduct.Calling.Hubs;
using DispatchProduct.Calling.Settings;

namespace DispatchProduct.Calling.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });

            services.AddDbContext<CallDbContext>(options =>
            options.UseSqlServer(Configuration["ConnectionString"],
            sqlOptions => sqlOptions.MigrationsAssembly("DispatchProduct.Calling.EFCore.MSSQL")));

            services.AddOptions();
            services.Configure<CallAppSettings>(Configuration);
            services.Configure<CustomerServiceSettings>(Configuration.GetSection("CustomerServiceSettings"));
            services.Configure<LocationServiceSetting>(Configuration.GetSection("LocationServiceSetting"));
            services.Configure<CallingHubSettings>(Configuration.GetSection("CallingHubSettings"));


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new Info()
                    {
                        Title = "Calling API",
                        Description = "Calling  API"
                    });
                c.AddSecurityDefinition("oauth2", new OAuth2Scheme
                {
                    Type = "oauth2",
                    Flow = "implicit",
                    AuthorizationUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/authorize",
                    TokenUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/token",
                    Scopes = new Dictionary<string, string>()
                    {
                        { "calling", "calling API" }
                    }
                });
            });

            ConfigureAuthService(services);

            services.AddMvc();
            services.AddSignalR();
            services.AddAutoMapper(typeof(Startup));
            Mapper.AssertConfigurationIsValid();

            services.AddScoped<DbContext, CallDbContext>();
            services.AddScoped(typeof(IRepositry<>), typeof(Repositry<>));
            services.AddScoped(typeof(ICallPriority), typeof(CallPriority));
            services.AddScoped(typeof(ICallStatus), typeof(CallStatus));
            services.AddScoped(typeof(ICallType), typeof(CallType));
            services.AddScoped(typeof(ICall), typeof(Call));
            services.AddScoped(typeof(ICallLog), typeof(CallLog));
            services.AddScoped(typeof(ICallManager), typeof(CallManager));
            services.AddScoped(typeof(ICallLogManager), typeof(CallLogManager));
            services.AddScoped(typeof(ICallTypeManager), typeof(CallTypeManager));
            services.AddScoped(typeof(ICallPriorityManager), typeof(CallPriorityManager));
            services.AddScoped(typeof(ICallStatusManager), typeof(CallStatusManager));
            services.AddScoped(typeof(IDefaultHttpClientCrud<,,>), typeof(DefaultHttpClientCrud<,,>));
            services.AddScoped(typeof(IHttpClientSettings), typeof(DefaultHttpClientSettings));
            services.AddScoped(typeof(IHttpClientSettings), typeof(DefaultHttpClientSettings));
            services.AddScoped(typeof(ICustomerService), typeof(CustomerService));
            services.AddScoped(typeof(ILocationService), typeof(LocationService));
            services.AddScoped(typeof(ICallingHub), typeof(CallingHub));
            var container = new ContainerBuilder();
            container.Populate(services);

            return new AutofacServiceProvider(container.Build());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCors("AllowAll");
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            var pathBase = Configuration["PATH_BASE"];
            if (!string.IsNullOrEmpty(pathBase))
            {
                loggerFactory.CreateLogger("init").LogDebug($"Using PATH BASE '{pathBase}'");
                app.UsePathBase(pathBase);
            }
            ConfigureAuth(app);
            app.UseStaticFiles();
            app.UseSignalR(routes => routes.MapHub<CallingHub>("callingHub"));
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Calling API");
            });


            // Make work identity server redirections in Edge and lastest versions of browers. WARN: Not valid in a production environment.
            //app.Use(async (context, next) =>
            //{
            //    context.Response.Headers.Add("Content-Security-Policy", "script-src 'unsafe-inline'");
            //    await next();
            //});

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private void ConfigureAuthService(IServiceCollection services)
        {
            // prevent from mapping "sub" claim to nameidentifier.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            var identityUrl = Configuration.GetValue<string>("IdentityUrl");
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
            .AddIdentityServerAuthentication(options =>
            {
                // base-address of your identityserver
                options.Authority = identityUrl;

                // name of the API resource
                options.ApiName = Configuration["ClientId"];
                options.ApiSecret = Configuration["Secret"];
                options.RequireHttpsMetadata = false;
                options.EnableCaching = true;
                options.CacheDuration = TimeSpan.FromMinutes(10);
                options.SaveToken = true;
            });

            //services.AddAuthorization(options =>
            //{
            //    // Policy for dashboard: only administrator role.
            //    options.AddPolicy("AdminRole", policy => policy.RequireClaim("role", "Admin"));
            //    // Policy for resources: user or administrator role. 
            //    options.AddPolicy("GlobalRole", policyBuilder => policyBuilder.RequireAssertion(
            //            context => context.User.HasClaim(claim => (claim.Type == "role" && claim.Value == "user")
            //               || (claim.Type == "role" && claim.Value == "Admin"))
            //        )
            //    );
            //});
        }

        protected virtual void ConfigureAuth(IApplicationBuilder app)
        {
            app.UseAuthentication();
        }
    }
}
