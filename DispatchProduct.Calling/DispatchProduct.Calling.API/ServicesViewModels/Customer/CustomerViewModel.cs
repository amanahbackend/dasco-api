﻿using DispatchProduct.Calling.API.ViewModel;
using System.Collections.Generic;

namespace DispatchProduct.Calling.API.ServicesViewCustomer
{
    public class CustomerViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string CivilId { get; set; }

        public string Remarks { get; set; }

        public string CompanyName { get; set; }

        public string Division { get; set; }

        public int FK_CustomerType_Id { get; set; }

        public CustomerTypeViewModel CustomerType { get; set; }

        public ICollection<ComplainViewModel> Complains { get; set; }

        public ICollection<CustomerPhoneBookViewModel> CustomerPhoneBook { get; set; }

        public ICollection<LocationViewModel> Locations { get; set; }
    }
}
