﻿namespace DispatchProduct.Calling.API.ViewModel
{
    public class CallEstimationViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int CallId { get; set; }
        public string EstimationRefNo { get; set; }
    }
}
