﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchingProduct.Calling.BLL.IManagers;
using DispatchProduct.Calling.Entities;
using AutoMapper;
using DispatchProduct.Calling.API.ViewModel;
using Microsoft.AspNetCore.Authorization;
using DispatchProduct.Calling.API.ServicesCommunication.Customer;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Calling.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]

    public class CallLogController : Controller
    {
        public ICallLogManager manger;
        public readonly IMapper mapper;
        public readonly ICustomerService customerService;
        public CallLogController(ICallLogManager _manger, IMapper _mapper,ICustomerService _customerService)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            customerService = _customerService;
        }
        // GET: api/values
        #region DefaultCrudOperation

        #region GetApi
        [HttpGet("{id}")]
        [Route("Get/{id}")]
        public IActionResult Get([FromRoute]int id)
        {
           CallLogViewModel result = new CallLogViewModel();
            CallLog entityResult = new CallLog();
            entityResult = manger.Get(id);
            result = mapper.Map<CallLog, CallLogViewModel>(entityResult);
            return Ok(result);
        }

        [HttpGet("{callId}")]
        [Route("GetByCall/{callId}")]
        public IActionResult GetByCall([FromRoute]int callId)
        {
            List<CallLogViewModel> result = null;
            var entityResult = manger.GetByCallId(callId);
            if (entityResult != null)
            {
               result = mapper.Map<List<CallLog>, List<CallLogViewModel>>(entityResult);
            }
           return Ok(result);
        }

        [HttpGet]
        [Route("GetAll")]
        public IActionResult Get()
        {
            List<CallLogViewModel> result = new List<CallLogViewModel>();
            List<CallLog> entityResult = new List<CallLog>();
            entityResult = manger.GetAll().ToList();
            result = mapper.Map<List<CallLog>, List<CallLogViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post([FromBody]CallLogViewModel model)
        {
            CallLogViewModel result = new CallLogViewModel();
            CallLog entityResult = new CallLog();
            entityResult = mapper.Map<CallLogViewModel, CallLog>(model);
           // await fillEntityIdentity(entityResult);
            entityResult = manger.Add(entityResult);
            result = mapper.Map<CallLog, CallLogViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Put([FromBody]CallLogViewModel model)
        {
            bool result = false;
            CallLog entityResult = new CallLog();
            entityResult = mapper.Map<CallLogViewModel, CallLog>(model);
           // await fillEntityIdentity(entityResult);
            result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [HttpDelete]
        [Route("Delete/{id}")]

        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            CallLog entity = manger.Get(id);
           // await fillEntityIdentity(entity);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion


        [HttpGet("{phone}")]
        [Route("GetByCustomerPhone/{phone}")]
        public List<CallLogViewModel> GetByCustomerPhone(string phone)
        {
            List<CallLogViewModel> result = null;
            if (phone != null)
            {
               var entityResult=  manger.GetByCustomerPhoneNo(phone);
               result = mapper.Map<List<CallLog>, List<CallLogViewModel>>(entityResult);
            }
            return result;
        }
    }
}
