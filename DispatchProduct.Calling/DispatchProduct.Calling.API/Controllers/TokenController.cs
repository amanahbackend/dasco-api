﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IdentityModel.Client;
using DispatchProduct.Calling.API.Settings;
using Microsoft.Extensions.Options;
using DispatchProduct.Calling.API.ViewModel;

namespace DispatchProduct.Calling.API.Controllers
{
    [Route("api/[controller]")]
    public class TokenController : Controller
    {
        private CallAppSettings _appSettings;

        public TokenController(IOptions<CallAppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }


        [HttpPost]
        public async Task<string> Get([FromBody]TokenViewModel model)
        {
            string result = null;
            if (ModelState.IsValid)
            {
                if (_appSettings.IdentityUrl != null && _appSettings.ClientId != null && _appSettings.Secret != null)
                {

                    var discoveryClient2 = new DiscoveryClient(_appSettings.IdentityUrl);
                    discoveryClient2.Policy.RequireHttps = false;
                    discoveryClient2.Policy.ValidateIssuerName = false;
                    var disco = await discoveryClient2.GetAsync();
                    if (disco.TokenEndpoint != null)
                    {
                        var tokenClient = new TokenClient(disco.TokenEndpoint, _appSettings.ClientId, _appSettings.Secret);
                        var tokenResponse = await tokenClient.RequestResourceOwnerPasswordAsync(model.Username, model.Password, "calling");
                        var token = tokenResponse.AccessToken;
                        result = tokenResponse.AccessToken;
                    }

                }
            }
            return result;
        }
    }
}