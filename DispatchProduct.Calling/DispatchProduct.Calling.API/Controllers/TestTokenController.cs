﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IdentityModel.Client;
using Microsoft.Extensions.Options;
using DispatchProduct.Calling.API.Settings;

namespace DispatchProduct.Calling.API.Controllers
{
    [Route("api/[controller]")]
    public class TestTokenController : Controller
    {
        private CallAppSettings _appSettings;
        public TestTokenController(IOptions<CallAppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }
        [HttpGet]
        public async Task<string> Get()
        {
            string result = null;

            if (_appSettings.IdentityUrl != null && _appSettings.ClientId != null && _appSettings.Secret != null)
            {

                var discoveryClient2 = new DiscoveryClient(_appSettings.IdentityUrl);
                discoveryClient2.Policy.RequireHttps = false;
                discoveryClient2.Policy.ValidateIssuerName = false;
                var disco = await discoveryClient2.GetAsync();
                var tokenClient = new TokenClient(disco.TokenEndpoint, _appSettings.ClientId, _appSettings.Secret);
                var tokenResponse = await tokenClient.RequestResourceOwnerPasswordAsync("demouser@microsoft.com", "Pass@word1", "calling");
                var token = tokenResponse.AccessToken;
                result = tokenResponse.AccessToken;
            }
            return result;
        }
    }
}