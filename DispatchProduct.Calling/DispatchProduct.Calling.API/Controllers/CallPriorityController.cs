﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchingProduct.Calling.BLL.IManagers;
using DispatchProduct.Calling.Entities;
using AutoMapper;
using DispatchProduct.Calling.API.ViewModel;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Calling.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]

    public class CallPriorityController : Controller
    {
        public ICallPriorityManager manger;
        public readonly IMapper mapper;
        public CallPriorityController(ICallPriorityManager _manger, IMapper _mapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;
        }
        // GET: api/values
        #region DefaultCrudOperation

        #region GetApi
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
           CallPriorityViewModel result = new CallPriorityViewModel();
            CallPriority entityResult = new CallPriority();
            entityResult = manger.Get(id);
            result = mapper.Map<CallPriority, CallPriorityViewModel>(entityResult);
            return Ok(result);
        }

        [HttpGet]
        public IActionResult Get()
        {
            List<CallPriorityViewModel> result = new List<CallPriorityViewModel>();
            List<CallPriority> entityResult = new List<CallPriority>();
            entityResult = manger.GetAll().ToList();
            result = mapper.Map<List<CallPriority>, List<CallPriorityViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion
        [Route("Add")]
        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CallPriorityViewModel model)
        {
            CallPriorityViewModel result = new CallPriorityViewModel();
            CallPriority entityResult = new CallPriority();
            entityResult = mapper.Map<CallPriorityViewModel, CallPriority>(model);
           // await fillEntityIdentity(entityResult);
            entityResult = manger.Add(entityResult);
            result = mapper.Map<CallPriority, CallPriorityViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Put([FromBody]CallPriorityViewModel model)
        {
            bool result = false;
            CallPriority entityResult = new CallPriority();
            entityResult = mapper.Map<CallPriorityViewModel, CallPriority>(model);
           // await fillEntityIdentity(entityResult);
            result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [HttpDelete]
        
        [Route("Delete/{id}")]

        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            CallPriority entity = manger.Get(id);
           // await fillEntityIdentity(entity);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion
    }
}
