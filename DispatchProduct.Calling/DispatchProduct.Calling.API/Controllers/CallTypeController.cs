﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchingProduct.Calling.BLL.IManagers;
using DispatchProduct.Calling.Entities;
using AutoMapper;
using DispatchProduct.Calling.API.ViewModel;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Calling.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]

    public class CallTypeController : Controller
    {
        public ICallTypeManager manger;
        public readonly IMapper mapper;
        public CallTypeController(ICallTypeManager _manger, IMapper _mapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;
        }
        // GET: api/values
        #region DefaultCrudOperation

        #region GetApi
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
           CallTypeViewModel result = new CallTypeViewModel();
            CallType entityResult = new CallType();
            entityResult = manger.Get(id);
            result = mapper.Map<CallType, CallTypeViewModel>(entityResult);
            return Ok(result);
        }
        [HttpGet]
        public IActionResult Get()
        {
            List<CallTypeViewModel> result = new List<CallTypeViewModel>();
            List<CallType> entityResult = new List<CallType>();
            entityResult = manger.GetAll().ToList();
            result = mapper.Map<List<CallType>, List<CallTypeViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post([FromBody]CallTypeViewModel model)
        {
            CallTypeViewModel result = new CallTypeViewModel();
            CallType entityResult = new CallType();
            entityResult = mapper.Map<CallTypeViewModel, CallType>(model);
           // await fillEntityIdentity(entityResult);
            entityResult = manger.Add(entityResult);
            result = mapper.Map<CallType, CallTypeViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Put([FromBody]CallTypeViewModel model)
        {
            bool result = false;
            CallType entityResult = new CallType();
            entityResult = mapper.Map<CallTypeViewModel, CallType>(model);
           // await fillEntityIdentity(entityResult);
            result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            CallType entity = manger.Get(id);
           // await fillEntityIdentity(entity);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion
    }
}
