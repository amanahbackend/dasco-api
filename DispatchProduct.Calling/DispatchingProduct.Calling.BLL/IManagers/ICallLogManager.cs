﻿using DispatchProduct.Repoistry;
using DispatchProduct.Calling.Entities;
using System.Collections.Generic;

namespace DispatchingProduct.Calling.BLL.IManagers
{
    public interface ICallLogManager:IRepositry<CallLog>
    {
        List<CallLog> GetByCallId(int callId);
        List<CallLog> GetByCustomerPhoneNo(string customerPhone);
    }
}
