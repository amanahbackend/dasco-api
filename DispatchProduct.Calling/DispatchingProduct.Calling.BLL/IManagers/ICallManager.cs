﻿using DispatchProduct.Repoistry;
using DispatchProduct.Calling.Entities;
using System.Collections.Generic;

namespace DispatchingProduct.Calling.BLL.IManagers
{
    public interface ICallManager:IRepositry<Call>
    {
        List<Call> Search(string key);
        List<Call> UpdateCallByCustomerId(int customerId, string phoneNumber);
        bool UpdateCallEstimation(int callId, string estimationRefNo);
    }
}
