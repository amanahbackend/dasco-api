﻿using DispatchingProduct.Calling.BLL.IManagers;
using DispatchProduct.Calling.Entities;
using DispatchProduct.Identity.Context;
using DispatchProduct.Repoistry;
using System.Collections.Generic;
using System.Linq;

namespace DispatchingProduct.Calling.BLL.Managers
{
    public class CallManager : Repositry<Call>, ICallManager
    {
        public CallManager(CallDbContext context)
            : base(context)
        {

        }
        public List<Call> Search(string key)
        {
            List<Call> result = null;
            result = GetAll().Where(
                cal =>
                cal.CallerName.ToLower().Contains(key.ToLower()) ||
                cal.CallerNumber.ToLower().Contains(key.ToLower()) ||
                cal.Area.ToLower().Contains(key.ToLower()) ||
                cal.Block.ToLower().Contains(key.ToLower()) ||
                cal.Street.ToLower().Contains(key.ToLower()) ||
                cal.Governorate.ToLower().Contains(key.ToLower()) ||
                cal.CallStatus.ToLower().Contains(key.ToLower()) ||
                cal.PACINumber.ToLower().Contains(key.ToLower())
                ).ToList();
            return result;
        }
        public List<Call> UpdateCallByCustomerId(int customerId, string phoneNumber)
        {
            List<Call> result = null;
            result = GetAll().Where(cal => cal.CallerNumber == phoneNumber).ToList();
            foreach (var call in result)
            {
                call.FK_Customer_Id = customerId;
                call.NeedAction = false;
                Update(call);
            }
            return result;
        }
        public bool UpdateCallEstimation(int callId, string estimationRefNo)
        {
            bool result = false;
            var call = Get(callId);
            if (call != null)
            {
                call.HasEstimation = true;
                call.EstimationRefNo = estimationRefNo;
            }
            result = base.Update(call);
            return result;
        }
    }
}
