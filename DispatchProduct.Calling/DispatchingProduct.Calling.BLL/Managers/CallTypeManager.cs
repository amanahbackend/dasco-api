﻿using DispatchingProduct.Calling.BLL.IManagers;
using DispatchProduct.Calling.Entities;
using DispatchProduct.Identity.Context;
using DispatchProduct.Repoistry;

namespace DispatchingProduct.Calling.BLL.Managers
{
    public class CallTypeManager: Repositry<CallType>, ICallTypeManager
    {
        public CallTypeManager(CallDbContext context)
            : base(context)
        {

        }
    }
}
