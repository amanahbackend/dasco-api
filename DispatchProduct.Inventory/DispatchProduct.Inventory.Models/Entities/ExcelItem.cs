﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;

namespace DispatchProduct.Inventory.Entities
{
    public class ExcelItem : BaseEntity
    {
        public string CategoryName { get; set; }

        public int Quantity { get; set; }

        public double Price { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime ExpiryDate { get; set; }
    }
}

