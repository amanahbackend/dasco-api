﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Inventory.IEntities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchProduct.Inventory.Entities
{
    public class Item : BaseEntity, IItem, IBaseEntity
    {
        [Key]
        public int Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Quantity { get; set; }

        public double Price { get; set; }

        public DateTime ExpiryDate { get; set; }

        public int FK_Category_Id { get; set; }

        [NotMapped]
        public Category Category { get; set; }
    }
}