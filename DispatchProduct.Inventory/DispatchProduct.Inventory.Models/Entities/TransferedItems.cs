﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Inventory.IEntities;

namespace DispatchProduct.Inventory.Entities
{
    public class TransferedItems : BaseEntity, ITransferedItems, IBaseEntity
    {
        public int Id { get; set; }

        public int ItemId { get; set; }

        public Item Item { get; set; }

        public int Amount { get; set; }

        public string FK_FromTechnician_Id { get; set; }

        public string FK_ToTechnician_Id { get; set; }
    }
}
