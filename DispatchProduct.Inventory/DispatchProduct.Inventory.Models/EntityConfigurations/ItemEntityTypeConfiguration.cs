﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using DispatchProduct.Inventory.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DispatchProduct.Inventory.EntityConfigurations
{
    public class ItemEntityTypeConfiguration : BaseEntityTypeConfiguration<Item>, IEntityTypeConfiguration<Item>
    {
        public void Configure(EntityTypeBuilder<Item> ItemConfiguration)
        {
            base.Configure(ItemConfiguration);

            ItemConfiguration.ToTable("Item");

            ItemConfiguration.HasKey(o => o.Id);

            ItemConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("Itemseq");
            ItemConfiguration.Property(o => o.Price).IsRequired();
            ItemConfiguration.Property(o => o.Quantity).IsRequired();
            ItemConfiguration.Property(o => o.Name).IsRequired();
            ItemConfiguration.Property(o => o.Code).IsRequired();
            ItemConfiguration.Property(o => o.FK_Category_Id).IsRequired();

        }
    }
}