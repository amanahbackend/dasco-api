﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using DispatchProduct.Inventory.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DispatchProduct.Inventory.EntityConfigurations
{
    public class TransferedItemsEntityTypeConfiguration : BaseEntityTypeConfiguration<TransferedItems>, IEntityTypeConfiguration<TransferedItems>
    {
        public void Configure(EntityTypeBuilder<TransferedItems> TransferedItemsConfiguration)
        {
            base.Configure(TransferedItemsConfiguration);

            TransferedItemsConfiguration.ToTable("TransferedItems");

            TransferedItemsConfiguration.HasKey(o => o.Id);

            TransferedItemsConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("TransferedItemsseq");
            TransferedItemsConfiguration.Property(o => o.ItemId).IsRequired();
            TransferedItemsConfiguration.Property(o => o.Amount).IsRequired();
            TransferedItemsConfiguration.Property(o => o.FK_FromTechnician_Id).IsRequired();
            TransferedItemsConfiguration.Property(o => o.FK_FromTechnician_Id).IsRequired();
            TransferedItemsConfiguration.Ignore(o => o.Item);

        }
    }
}