﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using DispatchProduct.Inventory.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DispatchProduct.Inventory.EntityConfigurations
{
    public class TechnicianUsedItemsEntityTypeConfiguration : BaseEntityTypeConfiguration<TechnicianUsedItems>, IEntityTypeConfiguration<TechnicianUsedItems>
    {
        public void Configure(EntityTypeBuilder<TechnicianUsedItems> technicianUsedItemsConfiguration)
        {
            base.Configure(technicianUsedItemsConfiguration);

            technicianUsedItemsConfiguration.ToTable("TechnicianUsedItems");

            technicianUsedItemsConfiguration.HasKey(o => o.Id);

            technicianUsedItemsConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("TechnicianUsedItemsseq");
            technicianUsedItemsConfiguration.Property(o => o.ItemId).IsRequired();
            technicianUsedItemsConfiguration.Property(o => o.Amount).IsRequired();
            technicianUsedItemsConfiguration.Property(o => o.FK_Technician_Id).IsRequired();
            technicianUsedItemsConfiguration.Property(o => o.FK_Order_Id).IsRequired();
            technicianUsedItemsConfiguration.Ignore(o => o.Item);

        }
    }
}