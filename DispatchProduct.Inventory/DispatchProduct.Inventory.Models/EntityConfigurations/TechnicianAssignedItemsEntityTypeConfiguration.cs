﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using DispatchProduct.Inventory.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DispatchProduct.Inventory.EntityConfigurations
{
    public class TechnicianAssignedItemsEntityTypeConfiguration : BaseEntityTypeConfiguration<TechnicianAssignedItems>, IEntityTypeConfiguration<TechnicianAssignedItems>
    {
        public void Configure(EntityTypeBuilder<TechnicianAssignedItems> technicianAssignedItemsConfiguration)
        {
            base.Configure(technicianAssignedItemsConfiguration);

            technicianAssignedItemsConfiguration.ToTable("TechnicianAssignedItems");

            technicianAssignedItemsConfiguration.HasKey(o => o.Id);

            technicianAssignedItemsConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("TechnicianAssignedItemsseq");
            technicianAssignedItemsConfiguration.Property(o => o.ItemId).IsRequired();
            technicianAssignedItemsConfiguration.Property(o => o.CurrentAmount).IsRequired();
            technicianAssignedItemsConfiguration.Property(o => o.TotalAmount).IsRequired();
            technicianAssignedItemsConfiguration.Property(o => o.TotalUsed).IsRequired();
            technicianAssignedItemsConfiguration.Property(o => o.FK_Technician_Id).IsRequired();
            technicianAssignedItemsConfiguration.Ignore(o => o.Item);

        }
    }
}