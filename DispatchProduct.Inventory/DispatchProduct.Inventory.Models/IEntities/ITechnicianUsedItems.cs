﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Inventory.Entities;

namespace DispatchProduct.Inventory.IEntities
{
    public interface ITechnicianUsedItems : IBaseEntity
    {
        int Id { get; set; }

        int ItemId { get; set; }

        Item Item { get; set; }

        int Amount { get; set; }

        string FK_Technician_Id { get; set; }

        int FK_Order_Id { get; set; }

        bool IsReleased { get; set; }
    }
}
