﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Inventory.Entities;

namespace DispatchProduct.Inventory.IEntities
{
    public interface ITechnicianAssignedItems : IBaseEntity
    {
        int Id { get; set; }

        int ItemId { get; set; }

        Item Item { get; set; }

        int CurrentAmount { get; set; }

        int TotalAmount { get; set; }

        int TotalUsed { get; set; }

        string FK_Technician_Id { get; set; }
    }
}
