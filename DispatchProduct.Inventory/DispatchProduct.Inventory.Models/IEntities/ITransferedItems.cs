﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Inventory.Entities;

namespace DispatchProduct.Inventory.IEntities
{
    public interface ITransferedItems : IBaseEntity
    {
        int Id { get; set; }

        int ItemId { get; set; }

        Item Item { get; set; }

        int Amount { get; set; }

        string FK_FromTechnician_Id { get; set; }

        string FK_ToTechnician_Id { get; set; }
    }
}
