﻿using DispatchProduct.Inventory.Entities;
using DispatchProduct.Inventory.EntityConfigurations;
using DispatchProduct.Ordering.EntityConfigurations;
using Microsoft.EntityFrameworkCore;

namespace DispatchProduct.Inventory.Context
{
    public class InventoryDbContext : DbContext
    {

        public DbSet<Item> Item { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<TechnicianAssignedItems> TechnicianAssignedItems { get; set; }
        public DbSet<TechnicianUsedItems> TechnicianUsedItems { get; set; }
        public DbSet<TransferedItems> TransferedItems { get; set; }

        public InventoryDbContext(DbContextOptions<InventoryDbContext> options)
            : base(options)
        {
            


        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CategoryEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ItemEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TechnicianAssignedItemsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TechnicianUsedItemsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TransferedItemsEntityTypeConfiguration());
        }
    }
}
