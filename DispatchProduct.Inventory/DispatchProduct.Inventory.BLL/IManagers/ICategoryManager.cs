﻿using DispatchProduct.Repoistry;
using DispatchProduct.Inventory.Entities;

namespace DispatchingProduct.Inventory.BLL.IManagers
{
    public interface ICategoryManager : IRepositry<Category>
    {
    }
}
