﻿using DispatchProduct.Inventory.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;
using Utilites.ProcessingResult;

namespace DispatchingProduct.Inventory.BLL.Managers
{
    public interface ITechnicianAssignedItemsManager : IRepositry<TechnicianAssignedItems>
    {
        ProcessResult<List<TechnicianAssignedItems>> AssignItems(List<TechnicianAssignedItems> list);

        ProcessResult<TechnicianAssignedItems> AssignItem(TechnicianAssignedItems item);

        ProcessResult<TechnicianAssignedItems> ReleaseItems(int itemId, int amount, string technicianId);

        ProcessResult<List<TechnicianAssignedItems>> UsedItems(List<TechnicianAssignedItems> list);

        ProcessResult<TechnicianAssignedItems> UsedItem(TechnicianAssignedItems item);

        ProcessResult<TechnicianAssignedItems> UsedItem(int itemId, int noItems, string technicianId);

        ProcessResult<bool> AssignTransferedItem(int itemId, int noItems, string fromTechnicianId, string toTechnicianId);

        List<TechnicianAssignedItems> GetAssignedItemsByTechnician(string technicianId);

        bool IsItemExistForTechnician(string technicianId, int itemId);
    }
}
