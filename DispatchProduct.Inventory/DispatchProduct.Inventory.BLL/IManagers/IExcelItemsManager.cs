﻿using DispatchProduct.Inventory.ExcelSettings;
using DispatchProduct.Inventory.Entities;
using System.Collections.Generic;
using Utilites.ProcessingResult;
using Utilites.UploadFile;

namespace DispatchProduct.Inventory.BLL.IManagers
{
    public interface IExcelItemsManager
    {
        ProcessResult<List<Item>> Process(string path, UploadFile Uploadfile, ExcelSheetProperties excelSheetProperties);
        ExcelItem GetItems(IList<string> rowData, IList<string> columnNames);
        List<Item> SaveItems(IList<ExcelItem> dataList);
    }
}
