﻿using DispatchProduct.Inventory.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;
using Utilites.ProcessingResult;

namespace DispatchingProduct.Inventory.BLL.Managers
{
    public interface ITechnicianUsedItemsManager : IRepositry<TechnicianUsedItems>
    {
        ProcessResult<List<TechnicianUsedItems>> AddUsedItems(List<TechnicianUsedItems> items);

        ProcessResult<TechnicianUsedItems> AddUsedItem(TechnicianUsedItems item);

        List<TechnicianUsedItems> GetUsedItemsByTechnician(string technicianId);

        ProcessResult<bool> ReleaseUsedItem(int usedItemId);

        List<TechnicianUsedItems> Filter(FilterTechnician filter);
    }
}
