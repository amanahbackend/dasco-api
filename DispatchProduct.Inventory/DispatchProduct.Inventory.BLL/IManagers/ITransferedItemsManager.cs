﻿using DispatchProduct.Inventory.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;
using Utilites.ProcessingResult;

namespace DispatchingProduct.Inventory.BLL.Managers
{
    public interface ITransferedItemsManager : IRepositry<TransferedItems>
    {
        ProcessResult<TransferedItems> TransferItem(TransferedItems item);

        ProcessResult<List<TransferedItems>> TransferItems(List<TransferedItems> items);

        List<TransferedItems> GetTransferedItemsFromTechnician(string technicianId);

        List<TransferedItems> GetTransferedItemsToTechnician(string technicianId);
    }
}
