﻿using DispatchingProduct.Inventory.BLL.IManagers;
using DispatchProduct.Inventory.Context;
using DispatchProduct.Inventory.Entities;
using DispatchProduct.Repoistry;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Utilites.ProcessingResult;

namespace DispatchingProduct.Inventory.BLL.Managers
{
    public class TechnicianUsedItemsManager : Repositry<TechnicianUsedItems>, ITechnicianUsedItemsManager, IRepositry<TechnicianUsedItems>
    {
        private ITechnicianAssignedItemsManager manager;
        private IItemManager itemManger;

        public TechnicianUsedItemsManager(InventoryDbContext context, ITechnicianAssignedItemsManager _manager, IItemManager _itemManger)
          : base((DbContext)context)
        {
            manager = _manager;
            itemManger = _itemManger;
        }

        public ProcessResult<List<TechnicianUsedItems>> AddUsedItems(List<TechnicianUsedItems> items)
        {
            ProcessResult<List<TechnicianUsedItems>> result = new ProcessResult<List<TechnicianUsedItems>>();
            result.returnData = new List<TechnicianUsedItems>();
            result.MethodName = MethodBase.GetCurrentMethod().Name;
            foreach (TechnicianUsedItems technicianUsedItems in items)
            {
                ProcessResult<TechnicianUsedItems> input = AddUsedItem(technicianUsedItems);
                if (input.IsSucceeded)
                {
                    result.returnData.Add(input.returnData);
                }
                else
                {
                    result = ProcessResultMapping.Map(input, result);
                    return result;
                }
            }
            result.IsSucceeded = true;
            return result;
        }

        public ProcessResult<TechnicianUsedItems> AddUsedItem(TechnicianUsedItems item)
        {
            ProcessResult<TechnicianUsedItems> result = new ProcessResult<TechnicianUsedItems>();
            result.MethodName = MethodBase.GetCurrentMethod().Name;
            ProcessResult<TechnicianAssignedItems> input = manager.UsedItem(item.ItemId, item.Amount, item.FK_Technician_Id);
            if (input.IsSucceeded)
            {
                result.returnData = Add(item);
                result.IsSucceeded = true;
            }
            else
                ProcessResultMapping.Map(input, result);
            return result;
        }

        public ProcessResult<bool> ReleaseUsedItem(int usedItemId)
        {
            ProcessResult<bool> result = new ProcessResult<bool>();
            result.MethodName = MethodBase.GetCurrentMethod().Name;
            TechnicianUsedItems entity = Get(usedItemId);
            if (entity != null)
            {
                ProcessResult<TechnicianAssignedItems> input = manager.ReleaseItems(entity.ItemId, entity.Amount, entity.FK_Technician_Id);
                if (input.IsSucceeded)
                {
                    entity.IsReleased = true;
                    Update(entity);
                    result.returnData = true;
                    result.IsSucceeded = true;
                }
                else
                    ProcessResultMapping.Map(input, result);
            }
            else
            {
                result.returnData = false;
                result.IsSucceeded = false;
                result.Message = "item not found to release";
            }
            return result;
        }

        public List<TechnicianUsedItems> Filter(FilterTechnician filter)
        {
            List<TechnicianUsedItems> list = GetAll().Where<TechnicianUsedItems>((Expression<Func<TechnicianUsedItems, bool>>)(itm => (filter.ItemIds != default(object) && filter.ItemIds.Count > 0 ? filter.ItemIds.Contains(itm.ItemId) : true) && (filter.FK_Technician_Ids != default(object) && filter.FK_Technician_Ids.Count > 0 ? filter.FK_Technician_Ids.Contains(itm.FK_Technician_Id) : true) && (filter.FK_Order_Ids != default(object) && filter.FK_Order_Ids.Count > 0 ? filter.FK_Order_Ids.Contains(itm.FK_Order_Id) : true) && (filter.AmountFrom != new int?() ? (int?)itm.Amount >= filter.AmountFrom : true) && (filter.AmountTo != new int?() ? (int?)itm.Amount <= filter.AmountTo : true) && (filter.DateFrom != new DateTime?() ? (DateTime?)itm.CreatedDate >= filter.DateFrom : true) && (filter.DateTo != new DateTime?() ? (DateTime?)itm.CreatedDate <= filter.DateTo : true))).ToList<TechnicianUsedItems>();
            foreach (TechnicianUsedItems technicianUsedItems in list)
                technicianUsedItems.Item = itemManger.Get(technicianUsedItems.ItemId);
            return list;
        }

        public List<TechnicianUsedItems> GetUsedItemsByTechnician(string technicianId)
        {
            List<TechnicianUsedItems> list = GetAll().Where(itm => itm.FK_Technician_Id == technicianId).ToList();
            foreach (TechnicianUsedItems technicianUsedItems in list)
                technicianUsedItems.Item = itemManger.Get(technicianUsedItems.ItemId);
            return list;
        }
    }
}
