﻿using DispatchingProduct.Inventory.BLL.IManagers;
using DispatchProduct.Inventory.Context;
using DispatchProduct.Inventory.Entities;
using DispatchProduct.Repoistry;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Reflection;
using Utilites.ProcessingResult;

namespace DispatchingProduct.Inventory.BLL.Managers
{
    public class TechnicianAssignedItemsManager : Repositry<TechnicianAssignedItems>, ITechnicianAssignedItemsManager, IRepositry<TechnicianAssignedItems>
    {
        private IItemManager itemManger;

        public TechnicianAssignedItemsManager(InventoryDbContext context, IItemManager _itemManger)
          : base((DbContext)context)
        {
            itemManger = _itemManger;
        }

        public ProcessResult<List<TechnicianAssignedItems>> AssignItems(List<TechnicianAssignedItems> list)
        {
            ProcessResult<List<TechnicianAssignedItems>> result = new ProcessResult<List<TechnicianAssignedItems>>();
            result.returnData = new List<TechnicianAssignedItems>();
            result.MethodName = MethodBase.GetCurrentMethod().Name;
            foreach (TechnicianAssignedItems technicianAssignedItems in list)
            {
                ProcessResult<TechnicianAssignedItems> input = AssignItem(technicianAssignedItems);
                if (input.IsSucceeded)
                {
                    result.returnData.Add(input.returnData);
                }
                else
                {
                    result = ProcessResultMapping.Map(input, result);
                    return result;
                }
            }
            result.IsSucceeded = true;
            return result;
        }

        public ProcessResult<TechnicianAssignedItems> AssignItem(TechnicianAssignedItems item)
        {
            ProcessResult<TechnicianAssignedItems> result = new ProcessResult<TechnicianAssignedItems>();
            result.MethodName = MethodBase.GetCurrentMethod().Name;
            ProcessResult<bool> input = itemManger.DecreaseAssignedAmount(item.CurrentAmount, item.ItemId);
            if (input.IsSucceeded)
            {
                result.returnData = GetAll().Where(itm => itm.ItemId == item.ItemId && itm.FK_Technician_Id == item.FK_Technician_Id).FirstOrDefault();
                if (result.returnData == null)
                {
                    item.TotalUsed = 0;
                    item.TotalAmount = item.CurrentAmount;
                    result.returnData = Add(item);
                    result.IsSucceeded = true;
                }
                else
                {
                    if (result.returnData.TotalUsed > item.CurrentAmount)
                        result.returnData.TotalUsed -= item.CurrentAmount;
                    else
                        result.returnData.TotalUsed = 0;
                    result.returnData.CurrentAmount += item.CurrentAmount;
                    result.returnData.TotalAmount = result.returnData.CurrentAmount;
                    Update(result.returnData);
                    result.IsSucceeded = true;
                }
            }
            else
                result = ProcessResultMapping.Map(input, result);
            return result;
        }

        public ProcessResult<TechnicianAssignedItems> ReleaseItems(int itemId, int amount, string technicianId)
        {
            ProcessResult<TechnicianAssignedItems> result = new ProcessResult<TechnicianAssignedItems>();
            result.MethodName = MethodBase.GetCurrentMethod().Name;
            ProcessResult<bool> input = itemManger.DecreaseAssignedAmount(amount, itemId);
            if (input.IsSucceeded)
            {
                result.returnData = GetAll().Where(itm => itm.ItemId == itemId && itm.FK_Technician_Id == technicianId).FirstOrDefault();
                result.Message = MethodBase.GetCurrentMethod().Name;
                if (result != null)
                {
                    result.returnData.TotalUsed = 0;
                    result.returnData.CurrentAmount += amount;
                    result.returnData.TotalAmount = result.returnData.CurrentAmount;
                    if (Update(result.returnData))
                    {
                        result.IsSucceeded = true;
                    }
                    else
                    {
                        result.IsSucceeded = false;
                        result.Message = "Update Operation Failed";
                    }
                }
            }
            else
                result = ProcessResultMapping.Map(input, result);
            return result;
        }

        public ProcessResult<List<TechnicianAssignedItems>> UsedItems(List<TechnicianAssignedItems> list)
        {
            ProcessResult<List<TechnicianAssignedItems>> result = new ProcessResult<List<TechnicianAssignedItems>>();
            result.returnData = new List<TechnicianAssignedItems>();
            foreach (TechnicianAssignedItems technicianAssignedItems in list)
            {
                ProcessResult<TechnicianAssignedItems> input = UsedItem(technicianAssignedItems);
                if (input.IsSucceeded)
                {
                    result.returnData.Add(input.returnData);
                }
                else
                {
                    result = ProcessResultMapping.Map(input, result);
                    return result;
                }
            }
            result.IsSucceeded = true;
            return result;
        }

        public ProcessResult<TechnicianAssignedItems> UsedItem(TechnicianAssignedItems item)
        {
            ProcessResult<TechnicianAssignedItems> processResult = new ProcessResult<TechnicianAssignedItems>();
            processResult.MethodName = MethodBase.GetCurrentMethod().Name;
            processResult.returnData = GetAll().Where(itm => itm.ItemId == item.ItemId && itm.FK_Technician_Id == item.FK_Technician_Id).FirstOrDefault();
            if (processResult.returnData != null)
            {
                if (processResult.returnData.CurrentAmount >= item.TotalUsed)
                {
                    processResult.returnData.TotalUsed += item.TotalUsed;
                    processResult.returnData.CurrentAmount -= item.TotalUsed;
                    if (Update(processResult.returnData))
                    {
                        processResult.IsSucceeded = true;
                        processResult.Message = "Succeded";
                    }
                    else
                    {
                        processResult.IsSucceeded = false;
                        processResult.Message = "Operation Update Error";
                    }
                }
                else
                {
                    processResult.IsSucceeded = false;
                    processResult.Message = "amount marked to decrease more than assigned amount";
                }
            }
            else
            {
                processResult.IsSucceeded = false;
                processResult.Message = "This technician hasn't been assigned to this item before";
            }
            return processResult;
        }

        public ProcessResult<TechnicianAssignedItems> UsedItem(int itemId, int noItems, string technicianId)
        {
            return UsedItem(new TechnicianAssignedItems()
            {
                ItemId = itemId,
                TotalUsed = noItems,
                FK_Technician_Id = technicianId
            });
        }

        public ProcessResult<bool> AssignTransferedItem(int itemId, int noItems, string fromTechnicianId, string toTechnicianId)
        {
            ProcessResult<bool> processResult = new ProcessResult<bool>();
            processResult.returnData = false;
            processResult.MethodName = MethodBase.GetCurrentMethod().Name;
            TechnicianAssignedItems entity = GetAll().Where(itm => itm.ItemId == itemId && itm.FK_Technician_Id == fromTechnicianId).FirstOrDefault();
            if (entity != null)
            {
                if (entity.CurrentAmount >= noItems)
                {
                    entity.CurrentAmount -= noItems;
                    entity.TotalAmount -= noItems;
                    processResult.returnData = Update(entity);
                    TechnicianAssignedItems entity2 = GetAll().Where<TechnicianAssignedItems>((Expression<Func<TechnicianAssignedItems, bool>>)(itm => itm.ItemId == itemId && itm.FK_Technician_Id == toTechnicianId)).FirstOrDefault<TechnicianAssignedItems>();
                    if (entity2 != null)
                    {
                        entity2.CurrentAmount += noItems;
                        entity2.TotalAmount += noItems;
                        Update(entity2);
                        processResult.returnData = true;
                        processResult.IsSucceeded = true;
                    }
                    else
                    {
                        Add(new TechnicianAssignedItems()
                        {
                            TotalAmount = noItems,
                            CurrentAmount = noItems,
                            TotalUsed = 0,
                            ItemId = itemId,
                            FK_Technician_Id = toTechnicianId
                        });
                        processResult.returnData = true;
                        processResult.IsSucceeded = true;
                    }
                }
                else
                {
                    processResult.IsSucceeded = false;
                    processResult.Message = "technician current amount from this item less than transfered";
                    processResult.Status = HttpStatusCode.BadRequest;
                    processResult.MethodName = MethodBase.GetCurrentMethod().Name;
                }
            }
            else
            {
                processResult.IsSucceeded = false;
                processResult.Message = "technician transfered from doesn't have this item";
                processResult.Status = HttpStatusCode.BadRequest;
                processResult.MethodName = MethodBase.GetCurrentMethod().Name;
            }
            return processResult;
        }

        public List<TechnicianAssignedItems> GetAssignedItemsByTechnician(string technicianId)
        {
            List<TechnicianAssignedItems> list = GetAll().Where(itm => itm.FK_Technician_Id == technicianId).ToList();
            foreach (TechnicianAssignedItems technicianAssignedItems in list)
                technicianAssignedItems.Item = itemManger.Get(technicianAssignedItems.ItemId);
            return list;
        }

        public bool IsItemExistForTechnician(string technicianId, int itemId)
        {
            bool flag = false;
            if (GetAll().Where(itm => itm.FK_Technician_Id == technicianId && itm.ItemId == itemId).FirstOrDefault() != null)
                flag = true;
            return flag;
        }
    }
}
