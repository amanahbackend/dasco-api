﻿using DispatchingProduct.Inventory.BLL.IManagers;
using DispatchProduct.Inventory.Context;
using DispatchProduct.Inventory.Entities;
using DispatchProduct.Repoistry;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Utilites.ProcessingResult;

namespace DispatchingProduct.Inventory.BLL.Managers
{
    public class TransferedItemsManager : Repositry<TransferedItems>, ITransferedItemsManager, IRepositry<TransferedItems>
    {
        private ITechnicianAssignedItemsManager manager;
        private IItemManager itemManger;

        public TransferedItemsManager(InventoryDbContext context, ITechnicianAssignedItemsManager _manager, IItemManager _itemManger)
          : base(context)
        {
            manager = _manager;
            itemManger = _itemManger;
        }

        public ProcessResult<TransferedItems> TransferItem(TransferedItems item)
        {
            ProcessResult<TransferedItems> output = new ProcessResult<TransferedItems>();
            ProcessResult<bool> input = manager.AssignTransferedItem(item.ItemId, item.Amount, item.FK_FromTechnician_Id, item.FK_ToTechnician_Id);
            output.MethodName = MethodBase.GetCurrentMethod().Name;
            if (input.IsSucceeded)
            {
                output.returnData = Add(item);
                output.IsSucceeded = true;
            }
            else
                output = ProcessResultMapping.Map(input, output);
            return output;
        }

        public ProcessResult<List<TransferedItems>> TransferItems(List<TransferedItems> items)
        {
            ProcessResult<List<TransferedItems>> output = new ProcessResult<List<TransferedItems>>();
            output.returnData = new List<TransferedItems>();
            output.MethodName = MethodBase.GetCurrentMethod().Name;
            foreach (TransferedItems transferedItems in items)
            {
                ProcessResult<TransferedItems> input = TransferItem(transferedItems);
                if (input.IsSucceeded)
                    output.returnData.Add(input.returnData);
                else
                    output = ProcessResultMapping.Map(input, output);
            }
            output.IsSucceeded = true;
            return output;
        }

        public List<TransferedItems> GetTransferedItemsFromTechnician(string technicianId)
        {
            List<TransferedItems> list = GetAll().Where(itm => itm.FK_FromTechnician_Id == technicianId).ToList();
            foreach (TransferedItems transferedItems in list)
                transferedItems.Item = itemManger.Get(transferedItems.ItemId);
            return list;
        }

        public List<TransferedItems> GetTransferedItemsToTechnician(string technicianId)
        {
            List<TransferedItems> list = GetAll().Where(itm => itm.FK_ToTechnician_Id == technicianId).ToList();
            foreach (TransferedItems transferedItems in list)
                transferedItems.Item = itemManger.Get(transferedItems.ItemId);
            return list;
        }
    }
}
