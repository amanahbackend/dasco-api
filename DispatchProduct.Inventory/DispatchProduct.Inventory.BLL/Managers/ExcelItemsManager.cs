﻿using DispatchingProduct.Inventory.BLL.IManagers;
using DispatchProduct.Inventory.BLL.IManagers;
using DispatchProduct.Inventory.Entities;
using DispatchProduct.Inventory.ExcelSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using Utilites.ExcelToGenericList;
using Utilites.ProcessingResult;
using Utilites.UploadFile;

namespace DispatchProduct.Inventory.BLL.Managers
{
    public class ExcelItemsManager: IExcelItemsManager
    {
        ICategoryManager categoryManager;
        IItemManager itemManager;
        public ExcelItemsManager(ICategoryManager _categoryManager,IItemManager _itemManager)
        {
            categoryManager = _categoryManager;
            itemManager = _itemManager;
        }
        public ExcelSheetProperties ExcelSheetProperties{ get; set; }
        public ExcelItem GetItems(IList<string> rowData, IList<string> columnNames)
        {
            var EmployeeSalary = new ExcelItem()
            {
                Code = rowData[columnNames.IndexFor(nameof(ExcelSheetProperties.Code))].ToString(),
                Name = rowData[columnNames.IndexFor(nameof(ExcelSheetProperties.Name))].ToString(),
                Description = rowData[columnNames.IndexFor(nameof(ExcelSheetProperties.Description))].ToString(),
                Quantity = rowData[columnNames.IndexFor(nameof(ExcelSheetProperties.Quantity))].ToInt32(),
                Price = rowData[columnNames.IndexFor(nameof(ExcelSheetProperties.Price))].ToDouble(),
                ExpiryDate = DateTime.FromOADate(double.Parse(rowData[columnNames.IndexFor(nameof(ExcelSheetProperties.ExpiryDate))])), 
                CategoryName = rowData[columnNames.IndexFor(nameof(ExcelSheetProperties.CategoryName))].ToString(),
            };
            return EmployeeSalary;
        }
        public ProcessResult<List<Item>> Process(string path, UploadFile Uploadfile, ExcelSheetProperties excelSheetProperties)
        {
            ProcessResult<List<Item>> result = new ProcessResult<List<Item>>();
            try {
                if (excelSheetProperties != null && path != null)
                    ExcelSheetProperties = excelSheetProperties;
                UploadExcelFileManager uploadExcelFileManager = new UploadExcelFileManager();
                //Uploadfile = mapper.Map<UploadFileViewModel, UploadFile>(file);
                ProcessResult<string> uploadedFile = uploadExcelFileManager.AddFile(Uploadfile, path);
                if (uploadedFile.IsSucceeded)
                {
                    string excelPath = ExcelReader.CheckPath(Uploadfile.FileName, path);
                    IList<ExcelItem> dataList = ExcelReader.GetDataToList(excelPath, GetItems);
                    result.returnData = SaveItems(dataList);
                    result.IsSucceeded = true;
                }
            }
            catch (Exception ex)
            {
                result.IsSucceeded = false;
                result.Exception = ex;
            }
            return result;
        }
        public List<Item> SaveItems(IList<ExcelItem> dataList)
        {
            List<Item> result = new List<Item>();
            foreach (var item in dataList)
            {
                if (item.CategoryName!=null && item.Code!=null && item.Name!=null && item.Price>0)
                {

                    var itemDB = GetItemIfExist(item);
                    bool isItemExist=true;
                    if (itemDB == null)
                    {
                        isItemExist = false;
                        itemDB = new Item();
                    }
                    var Category = GetOrAddCategory(item.CategoryName);
                    
                    itemDB.Price = item.Price;
                    itemDB.FK_Category_Id = Category.Id;
                    itemDB.Quantity = item.Quantity;
                    itemDB.Description = item.Description;
                    itemDB.ExpiryDate = item.ExpiryDate;
                    if (isItemExist)
                    {
                        itemManager.Update(itemDB);
                    }
                    else
                    {
                        itemDB.Code = item.Code;
                        itemDB.Name = item.Name;
                        itemDB = itemManager.Add(itemDB);
                    }
                    result.Add(itemDB);
                }
            }
            return result;
        }
        private Category GetOrAddCategory(string categoryName)
        {
            var Category = categoryManager.GetAll().Where(p => p.Name == categoryName).FirstOrDefault();
            if (Category == null)
            {
                Category = new Category();
                Category.Name = categoryName;
                Category = categoryManager.Add(Category);
            }
            return Category;
        }
        private Item GetItemIfExist(ExcelItem item)
        {
            return itemManager.GetAll().Where(e => e.Code == item.Code && e.Name == e.Name).FirstOrDefault();
        }

    }
}
