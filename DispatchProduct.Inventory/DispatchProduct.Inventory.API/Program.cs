﻿using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore;
using Microsoft.Extensions.Logging;
using DispatchProduct.Inventory.Context;
using Microsoft.Extensions.DependencyInjection;
using DispatchProduct.Inventory.API.Settings;
using DispatchProduct.Inventory.API.Seed;
using Microsoft.Extensions.Options;

namespace DispatchProduct.Inventory.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).MigrateDbContext<InventoryDbContext>((context, services) =>
            {
                var env = services.GetService<IHostingEnvironment>();
                var logger = services.GetService<ILogger<InventoryDbContextSeed>>();
                var settings = services.GetService<IOptions<InventoryAppSettings>>();
                new InventoryDbContextSeed()
                    .SeedAsync(context, env, logger, settings)
                    .Wait();
            }).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .ConfigureLogging((hostingContext, builder) =>
                {
                    builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                    builder.AddConsole();
                    builder.AddDebug();
                })
                .Build();
    }
}
