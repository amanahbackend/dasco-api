﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using DispatchingProduct.Inventory.BLL.IManagers;
using DispatchProduct.Inventory.API.ViewModel;
using DispatchProduct.Inventory.Entities;
using Utilities.Utilites;
using Utilites.UploadFile;
using Microsoft.AspNetCore.Hosting;
using Utilites.ExcelToGenericList;
using DispatchProduct.Inventory.BLL.IManagers;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Calling.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]

    public class ItemController : Controller
    {
        private readonly IHostingEnvironment _hostingEnv;
        public IItemManager manger;
        public readonly IMapper mapper;
        IExcelItemsManager excelItemsManager;
        public ItemController(IItemManager _manger, IMapper _mapper,IHostingEnvironment hostingEnv , IExcelItemsManager _excelItemsManager)
        {
            _hostingEnv = hostingEnv;
            excelItemsManager = _excelItemsManager;
            this.manger = _manger;
            this.mapper = _mapper;
        }
        // GET: api/values
        #region DefaultCrudOperation

        #region GetApi
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
           ItemViewModel result = new ItemViewModel();
            Item entityResult = new Item();
            entityResult = manger.Get(id);
            result = mapper.Map<Item, ItemViewModel>(entityResult);
            return Ok(result);
        }
        [HttpGet]
        public IActionResult Get()
        {
            List<ItemViewModel> result = new List<ItemViewModel>();
            List<Item> entityResult = new List<Item>();
            entityResult = manger.GetAll().ToList();
            result = mapper.Map<List<Item>, List<ItemViewModel>>(entityResult);
            return Ok(result);
        }
        [HttpGet("{code}")]
        [Route("GetByCode/{code}")]
        public IActionResult GetByCode(string code)
        {
            ItemViewModel result = new ItemViewModel();
            Item entityResult = new Item();
            entityResult = manger.GetByCode(code);
            result = mapper.Map<Item, ItemViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post([FromBody]ItemViewModel model)
        {
            ItemViewModel result = new ItemViewModel();
            Item entityResult = new Item();
            entityResult = mapper.Map<ItemViewModel, Item>(model);
           // await fillEntityIdentity(entityResult);
            entityResult = manger.Add(entityResult);
            result = mapper.Map<Item, ItemViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Put([FromBody]ItemViewModel model)
        {
            bool result = false;
            Item entityResult = new Item();
            entityResult = mapper.Map<ItemViewModel, Item>(model);
           // await fillEntityIdentity(entityResult);
            result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [HttpDelete]
        [Route("Delete/{id}")]

        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            Item entity = manger.Get(id);
           // await fillEntityIdentity(entity);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion

        [HttpPost]
        [Route("ImportItemFromFile"), AllowAnonymous]
        public async Task<IActionResult> ImportTechFromFile([FromBody]UploadFile file)
        {
            var fileExt = StringUtilities.GetFileExtension(file.FileContent);
            file.FileName = $"Tech_{DateTime.Now.Ticks}.{fileExt}";
            UploadExcelFileManager fileManager = new UploadExcelFileManager();
            var path = $@"{_hostingEnv.WebRootPath}\Import\";
            var addFileResult = fileManager.AddFile(file, path);
            IList<ExcelItem> models = ExcelReader.GetDataToList($@"{addFileResult.returnData}\{file.FileName}", excelItemsManager.GetItems);
            var result = excelItemsManager.SaveItems(models);

            return Ok(result);
        }

    }
}
