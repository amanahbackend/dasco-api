﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using DispatchingProduct.Inventory.BLL.IManagers;
using DispatchProduct.Inventory.API.ViewModel;
using DispatchProduct.Inventory.Entities;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Calling.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]

    public class CategoryController : Controller
    {
        public ICategoryManager manger;
        public readonly IMapper mapper;
        public CategoryController(ICategoryManager _manger, IMapper _mapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;
        }
        // GET: api/values
        #region DefaultCrudOperation

        #region GetApi
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
           CategoryViewModel result = new CategoryViewModel();
            Category entityResult = new Category();
            entityResult = manger.Get(id);
            result = mapper.Map<Category, CategoryViewModel>(entityResult);
            return Ok(result);
        }
        [HttpGet]
        public IActionResult Get()
        {
            List<CategoryViewModel> result = new List<CategoryViewModel>();
            List<Category> entityResult = new List<Category>();
            entityResult = manger.GetAll().ToList();
            result = mapper.Map<List<Category>, List<CategoryViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post([FromBody]CategoryViewModel model)
        {
            CategoryViewModel result = new CategoryViewModel();
            Category entityResult = new Category();
            entityResult = mapper.Map<CategoryViewModel, Category>(model);
           // await fillEntityIdentity(entityResult);
            entityResult = manger.Add(entityResult);
            result = mapper.Map<Category, CategoryViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Put([FromBody]CategoryViewModel model)
        {
            bool result = false;
            Category entityResult = new Category();
            entityResult = mapper.Map<CategoryViewModel, Category>(model);
           // await fillEntityIdentity(entityResult);
            result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [HttpDelete]
        [Route("Delete/{id}")]

        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            Category entity = manger.Get(id);
           // await fillEntityIdentity(entity);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion
    }
}
