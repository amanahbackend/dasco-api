﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using DispatchingProduct.Inventory.BLL.IManagers;
using DispatchingProduct.Inventory.BLL.Managers;
using DispatchProduct.Inventory.API.ViewModel;
using DispatchProduct.Inventory.Entities;
using Utilites.ProcessingResult;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Calling.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]

    public class TechnicianAssignedItemsController : Controller
    {
        public ITechnicianAssignedItemsManager manger;
        public readonly IMapper mapper;
        IItemManager itemManger;
        public TechnicianAssignedItemsController(ITechnicianAssignedItemsManager _manger, IMapper _mapper, IItemManager _itemManger)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            itemManger = _itemManger;
        }
        // GET: api/values
        #region DefaultCrudOperation

        #region GetApi
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
           TechnicianAssignedItemsViewModel result = new TechnicianAssignedItemsViewModel();
            TechnicianAssignedItems entityResult = new TechnicianAssignedItems();
            entityResult = manger.Get(id);
            result = mapper.Map<TechnicianAssignedItems, TechnicianAssignedItemsViewModel>(entityResult);
            return Ok(result);
        }
        [HttpGet]
        public IActionResult Get()
        {
            List<TechnicianAssignedItemsViewModel> result = new List<TechnicianAssignedItemsViewModel>();
            List<TechnicianAssignedItems> entityResult = new List<TechnicianAssignedItems>();
            entityResult = manger.GetAll().ToList();
            result = mapper.Map<List<TechnicianAssignedItems>, List<TechnicianAssignedItemsViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post([FromBody]TechnicianAssignedItemsViewModel model)
        {
            var entityResult = mapper.Map<TechnicianAssignedItemsViewModel, TechnicianAssignedItems>(model);
           // await fillEntityIdentity(entityResult);
            entityResult = manger.Add(entityResult);
            var result = mapper.Map<TechnicianAssignedItems, TechnicianAssignedItemsViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Put([FromBody]TechnicianAssignedItemsViewModel model)
        {
            bool result = false;
            TechnicianAssignedItems entityResult = new TechnicianAssignedItems();
            entityResult = mapper.Map<TechnicianAssignedItemsViewModel, TechnicianAssignedItems>(model);
           // await fillEntityIdentity(entityResult);
            result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [HttpDelete]
        [Route("Delete/{id}")]

        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            TechnicianAssignedItems entity = manger.Get(id);
           // await fillEntityIdentity(entity);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Route("AssignItem")]
        public async Task<IActionResult> AssignItem([FromBody] TechnicianAssignedItemsViewModel model)
        {
            TechnicianAssignedItemsController assignedItemsController = this;
            TechnicianAssignedItems technicianAssignedItems = mapper.Map<TechnicianAssignedItemsViewModel, TechnicianAssignedItems>(model);
            ProcessResult<TechnicianAssignedItems> processResult = manger.AssignItem(technicianAssignedItems);
            if (!processResult.IsSucceeded)
                return BadRequest(processResult);
            TechnicianAssignedItemsViewModel assignedItemsViewModel = mapper.Map<TechnicianAssignedItems, TechnicianAssignedItemsViewModel>(processResult.returnData);
            return Ok(assignedItemsViewModel);
        }

        [HttpPost]
        [Route("AssignItems")]
        public async Task<IActionResult> AssignItems([FromBody] List<TechnicianAssignedItemsViewModel> model)
        {
            TechnicianAssignedItemsController assignedItemsController = this;
            List<TechnicianAssignedItems> list = mapper.Map<List<TechnicianAssignedItemsViewModel>, List<TechnicianAssignedItems>>(model);
            ProcessResult<List<TechnicianAssignedItems>> processResult = manger.AssignItems(list);
            if (!processResult.IsSucceeded)
                return BadRequest(processResult);
            List<TechnicianAssignedItemsViewModel> assignedItemsViewModelList = mapper.Map<List<TechnicianAssignedItems>, List<TechnicianAssignedItemsViewModel>>(processResult.returnData);
            return Ok(assignedItemsViewModelList);
        }

        [HttpGet]
        [Route("GetAssignedItemsByTechnician/{technicianId}")]
        public async Task<IActionResult> GetAssignedItemsByTechnician([FromRoute] string technicianId)
        {
            List<TechnicianAssignedItems> itemsByTechnician = manger.GetAssignedItemsByTechnician(technicianId);
            List<TechnicianAssignedItemsViewModel> assignedItemsViewModelList = mapper.Map<List<TechnicianAssignedItems>, List<TechnicianAssignedItemsViewModel>>(itemsByTechnician);
            return Ok(assignedItemsViewModelList);
        }

        [HttpPost]
        [Route("ChekItemExistForTechnician")]
        public async Task<IActionResult> ChekItemExistForTechnician([FromBody] TechnicianAssignedItemsViewModel model)
        {
            bool result = manger.IsItemExistForTechnician(model.FK_Technician_Id, model.ItemId);
            Item source = null;
            if (!result)
            {
                Item obj = itemManger.Get(model.ItemId);
                return Ok(obj);
            }
            mapper.Map<Item, ItemViewModel>(source);
            return Ok(result);
        }
    }

}

