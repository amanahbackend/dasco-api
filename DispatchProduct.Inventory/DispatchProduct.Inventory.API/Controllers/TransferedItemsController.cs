﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using DispatchingProduct.Inventory.BLL.Managers;
using DispatchProduct.Inventory.API.ViewModel;
using DispatchProduct.Inventory.Entities;
using Utilites.ProcessingResult;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Calling.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]

    public class TransferedItemsController : Controller
    {
        public ITransferedItemsManager manger;
        public readonly IMapper mapper;
        public TransferedItemsController(ITransferedItemsManager _manger, IMapper _mapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;
        }
        // GET: api/values
        #region DefaultCrudOperation

        #region GetApi
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
           TransferedItemsViewModel result = new TransferedItemsViewModel();
            TransferedItems entityResult = new TransferedItems();
            entityResult = manger.Get(id);
            result = mapper.Map<TransferedItems, TransferedItemsViewModel>(entityResult);
            return Ok(result);
        }
        [HttpGet]
        public IActionResult Get()
        {
            List<TransferedItemsViewModel> result = new List<TransferedItemsViewModel>();
            List<TransferedItems> entityResult = new List<TransferedItems>();
            entityResult = manger.GetAll().ToList();
            result = mapper.Map<List<TransferedItems>, List<TransferedItemsViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post([FromBody]TransferedItemsViewModel model)
        {
            TransferedItemsViewModel result = new TransferedItemsViewModel();
            TransferedItems entityResult = new TransferedItems();
            entityResult = mapper.Map<TransferedItemsViewModel, TransferedItems>(model);
           // await fillEntityIdentity(entityResult);
            entityResult = manger.Add(entityResult);
            result = mapper.Map<TransferedItems, TransferedItemsViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Put([FromBody]TransferedItemsViewModel model)
        {
            bool result = false;
            TransferedItems entityResult = new TransferedItems();
            entityResult = mapper.Map<TransferedItemsViewModel, TransferedItems>(model);
           // await fillEntityIdentity(entityResult);
            result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [HttpDelete]
        [Route("Delete/{id}")]

        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            TransferedItems entity = manger.Get(id);
           // await fillEntityIdentity(entity);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion

        [HttpPost]
        [Route("TransferItem")]
        public async Task<IActionResult> TransferItem([FromBody] TransferedItemsViewModel model)
        {
            TransferedItems transferedItems = mapper.Map<TransferedItemsViewModel, TransferedItems>(model);
            ProcessResult<TransferedItems> processResult = manger.TransferItem(transferedItems);
            if (!processResult.IsSucceeded)
                return BadRequest(processResult);
            TransferedItemsViewModel transferedItemsViewModel = mapper.Map<TransferedItems, TransferedItemsViewModel>(processResult.returnData);
            return Ok(transferedItemsViewModel);
        }

        [HttpPost]
        [Route("TransferItems")]
        public async Task<IActionResult> TransferItems([FromBody] List<TransferedItemsViewModel> model)
        {
            List<TransferedItems> items = mapper.Map<List<TransferedItemsViewModel>, List<TransferedItems>>(model);
            ProcessResult<List<TransferedItems>> processResult = manger.TransferItems(items);
            if (!processResult.IsSucceeded)
                return BadRequest(processResult);
            List<TransferedItemsViewModel> transferedItemsViewModelList = mapper.Map<List<TransferedItems>, List<TransferedItemsViewModel>>(processResult.returnData);
            return Ok(transferedItemsViewModelList);
        }

        [HttpGet]
        [Route("GetTransferedItemsFromTechnician/{technicianId}")]
        public async Task<IActionResult> GetTransferedItemsFromTechnician([FromRoute] string technicianId)
        {
            List<TransferedItems> itemsFromTechnician = manger.GetTransferedItemsFromTechnician(technicianId);
            List<TransferedItemsViewModel> transferedItemsViewModelList = mapper.Map<List<TransferedItems>, List<TransferedItemsViewModel>>(itemsFromTechnician);
            return Ok(transferedItemsViewModelList);
        }

        [HttpGet]
        [Route("GetTransferedItemsToTechnician/{technicianId}")]
        public async Task<IActionResult> GetTransferedItemsToTechnician([FromRoute] string technicianId)
        {
            List<TransferedItems> itemsToTechnician = manger.GetTransferedItemsToTechnician(technicianId);
            List<TransferedItemsViewModel> transferedItemsViewModelList = mapper.Map<List<TransferedItems>, List<TransferedItemsViewModel>>(itemsToTechnician);
            return Ok(transferedItemsViewModelList);
        }
    }
}

