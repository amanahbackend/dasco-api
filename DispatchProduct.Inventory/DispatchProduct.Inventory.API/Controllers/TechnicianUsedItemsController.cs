﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using DispatchingProduct.Inventory.BLL.Managers;
using DispatchProduct.Inventory.API.ViewModel;
using DispatchProduct.Inventory.Entities;
using DispatchProduct.Inventory.API.ServicesViewModels;
using Utilites;
using DispatchProduct.Inventory.API.ServicesCommunication;
using Utilites.ProcessingResult;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DispatchProduct.Calling.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]

    public class TechnicianUsedItemsController : Controller
    {
        public ITechnicianUsedItemsManager manger;
        public readonly IMapper mapper;
        IOrderService orderService;
        public TechnicianUsedItemsController(ITechnicianUsedItemsManager _manger, IMapper _mapper,IOrderService _orderService)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            orderService = _orderService;
        }
        // GET: api/values
        #region DefaultCrudOperation

        #region GetApi
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
           TechnicianUsedItemsViewModel result = new TechnicianUsedItemsViewModel();
            TechnicianUsedItems entityResult = new TechnicianUsedItems();
            entityResult = manger.Get(id);
            result = mapper.Map<TechnicianUsedItems, TechnicianUsedItemsViewModel>(entityResult);
            return Ok(result);
        }
        [HttpGet]
        public IActionResult Get()
        {
            List<TechnicianUsedItemsViewModel> result = new List<TechnicianUsedItemsViewModel>();
            List<TechnicianUsedItems> entityResult = new List<TechnicianUsedItems>();
            entityResult = manger.GetAll().ToList();
            result = mapper.Map<List<TechnicianUsedItems>, List<TechnicianUsedItemsViewModel>>(entityResult);
            return Ok(result);
        }
       
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post([FromBody]TechnicianUsedItemsViewModel model)
        {
            TechnicianUsedItemsViewModel result = new TechnicianUsedItemsViewModel();
            TechnicianUsedItems entityResult = new TechnicianUsedItems();
            entityResult = mapper.Map<TechnicianUsedItemsViewModel, TechnicianUsedItems>(model);
           // await fillEntityIdentity(entityResult);
            entityResult = manger.Add(entityResult);
            result = mapper.Map<TechnicianUsedItems, TechnicianUsedItemsViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Put([FromBody]TechnicianUsedItemsViewModel model)
        {
            bool result = false;
            TechnicianUsedItems entityResult = new TechnicianUsedItems();
            entityResult = mapper.Map<TechnicianUsedItemsViewModel, TechnicianUsedItems>(model);
           // await fillEntityIdentity(entityResult);
            result = manger.Update(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [HttpDelete]
        [Route("Delete/{id}")]

        public async Task<IActionResult> Delete([FromRoute]int id)
        {
            bool result = false;
            TechnicianUsedItems entity = manger.Get(id);
           // await fillEntityIdentity(entity);
            result = manger.Delete(entity);
            return Ok(result);
        }
        #endregion
        #endregion

        [HttpPost]
        [Route("AddUsedItem")]
        public async Task<IActionResult> AddUsedItem([FromBody] TechnicianUsedItemsViewModel model)
        {
           
            TechnicianUsedItems technicianUsedItems = mapper.Map<TechnicianUsedItemsViewModel, TechnicianUsedItems>(model);
            ProcessResult<TechnicianUsedItems> processResult = manger.AddUsedItem(technicianUsedItems);
            TechnicianUsedItemsViewModel usedItemsViewModel = mapper.Map<TechnicianUsedItems, TechnicianUsedItemsViewModel>(processResult.returnData);
            return Ok(usedItemsViewModel);
        }

        [HttpPost]
        [Route("AddUsedItems")]
        public async Task<IActionResult> AddUsedItems([FromBody] List<TechnicianUsedItemsViewModel> model)
        {
           
            List<TechnicianUsedItems> items = mapper.Map<List<TechnicianUsedItemsViewModel>, List<TechnicianUsedItems>>(model);
            ProcessResult<List<TechnicianUsedItems>> processResult = manger.AddUsedItems(items);
            List<TechnicianUsedItemsViewModel> usedItemsViewModelList = mapper.Map<List<TechnicianUsedItems>, List<TechnicianUsedItemsViewModel>>(processResult.returnData);
            return Ok(usedItemsViewModelList);
        }

        [HttpGet]
        [Route("GetUsedItemsByTechnician/{technicianId}")]
        public async Task<IActionResult> GetUsedItemsByTechnician([FromRoute] string technicianId)
        {
           
            List<TechnicianUsedItems> itemsByTechnician = manger.GetUsedItemsByTechnician(technicianId);
            List<TechnicianUsedItemsViewModel> result = mapper.Map<List<TechnicianUsedItems>, List<TechnicianUsedItemsViewModel>>(itemsByTechnician);
            foreach (TechnicianUsedItemsViewModel usedItemsViewModel1 in result)
            {
                TechnicianUsedItemsViewModel usedItemsViewModel = usedItemsViewModel1;
                usedItemsViewModel.Order = await GetOrderById(usedItemsViewModel1.FK_Order_Id);
                usedItemsViewModel = null;
            }
            return Ok(result);
        }

        [HttpPost]
        [Route("Filter")]
        public async Task<IActionResult> Filter([FromBody] FilterTechnicianViewModel model)
        {
           
            FilterTechnician filter = mapper.Map<FilterTechnicianViewModel, FilterTechnician>(model);
            List<TechnicianUsedItems> source = manger.Filter(filter);
            List<TechnicianUsedItemsViewModel> result = mapper.Map<List<TechnicianUsedItems>, List<TechnicianUsedItemsViewModel>>(source);
            foreach (TechnicianUsedItemsViewModel itm in result)
            {
                itm.Order = await GetOrderById(itm.FK_Order_Id);
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("ReleaseUsedItem/{usedItemId}")]
        public async Task<IActionResult> ReleaseUsedItem([FromRoute] int usedItemId)
        {
           
            ProcessResult<bool> processResult = manger.ReleaseUsedItem(usedItemId);
            return Ok(processResult.returnData);
        }

        private async Task<OrderViewModel> GetOrderById(int id, string authHeader = null)
        {
           
            OrderViewModel result = null;
            try
            {
                if (Request != null && authHeader == null)
                    authHeader = Helper.GetValueFromRequestHeader(Request, "Authorization");
                result = await orderService.GetItem(id.ToString(), authHeader);
            }
            catch (Exception ex)
            {
            }
            return result;
        }
    }
}
