﻿using DispatchProduct.Inventory.API.ViewModel;

namespace DispatchProduct.Inventory.API.ServicesViewModels
{
    public class OrderProblemViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
