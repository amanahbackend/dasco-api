﻿using DispatchProduct.Inventory.API.ViewModel;

namespace DispatchProduct.Inventory.API.ServicesViewModels
{
    public class CustomerPhoneBookViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public int FK_Customer_Id { get; set; }

        public string Phone { get; set; }

        public int FK_PhoneType_Id { get; set; }
    }
}