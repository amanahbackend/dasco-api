﻿namespace DispatchProduct.Inventory.API.ViewModel
{
    public class TransferedItemsViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public int ItemId { get; set; }

        public ItemViewModel Item { get; set; }

        public int Amount { get; set; }

        public string FK_FromTechnician_Id { get; set; }

        public string FK_ToTechnician_Id { get; set; }
    }
}
