﻿using System;
using System.Linq.Expressions;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.Inventory.API.ViewModel.Infrastructure;

namespace DispatchProduct.Inventory.API.ViewModel
{
    public class BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public string FK_CreatedBy_Id { get; set; }
        public string FK_UpdatedBy_Id { get; set; }
        public string FK_DeletedBy_Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime DeletedDate { get; set; }
    }

    public class BaseEntityViewModelMapper : MapperBase<BaseEntity, BaseEntityViewModel>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public override Expression<Func<BaseEntity, BaseEntityViewModel>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<BaseEntity, BaseEntityViewModel>>)(p => new BaseEntityViewModel()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    FK_CreatedBy_Id = p.FK_CreatedBy_Id,
                    FK_UpdatedBy_Id = p.FK_UpdatedBy_Id,
                    FK_DeletedBy_Id = p.FK_DeletedBy_Id,
                    IsDeleted = p.IsDeleted,
                    CreatedDate = p.CreatedDate,
                    UpdatedDate = p.UpdatedDate,
                    DeletedDate = p.DeletedDate
                }));
            }
        }

        public override void MapToModel(BaseEntityViewModel dto, BaseEntity model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.FK_CreatedBy_Id = dto.FK_CreatedBy_Id;
            model.FK_UpdatedBy_Id = dto.FK_UpdatedBy_Id;
            model.FK_DeletedBy_Id = dto.FK_DeletedBy_Id;
            model.IsDeleted = dto.IsDeleted;
            model.CreatedDate = dto.CreatedDate;
            model.UpdatedDate = dto.UpdatedDate;
            model.DeletedDate = dto.DeletedDate;

        }
    }
}
