﻿using System;
using System.Collections.Generic;

namespace DispatchProduct.Inventory.API.ViewModel
{
    public class FilterTechnicianViewModel
    {
        public List<int> ItemIds { get; set; }

        public int? AmountFrom { get; set; }

        public int? AmountTo { get; set; }

        public List<string> FK_Technician_Ids { get; set; }

        public List<int> FK_Order_Ids { get; set; }

        public DateTime? DateFrom { get; set; }

        public DateTime? DateTo { get; set; }
    }
}