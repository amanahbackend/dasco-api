﻿namespace DispatchProduct.Inventory.API.ViewModel
{
    public class TechnicianAssignedItemsViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public int ItemId { get; set; }

        public ItemViewModel Item { get; set; }

        public int CurrentAmount { get; set; }

        public int TotalAmount { get; set; }

        public int TotalUsed { get; set; }

        public string FK_Technician_Id { get; set; }
    }
}
