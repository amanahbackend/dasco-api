﻿using System;

namespace DispatchProduct.Inventory.API.ViewModel
{
    public class ItemViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Quantity { get; set; }

        public double Price { get; set; }

        public DateTime ExpiryDate { get; set; }

        public int FK_Category_Id { get; set; }

        public CategoryViewModel Category { get; set; }
    }
}
