﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using DispatchProduct.Inventory.Context;
using Microsoft.EntityFrameworkCore;
using DispatchProduct.Inventory.API.Settings;
using Swashbuckle.AspNetCore.Swagger;
using System.IdentityModel.Tokens.Jwt;
using AutoMapper;
using DispatchProduct.Repoistry;
using DispatchProduct.Inventory.Entities;
using DispatchProduct.Inventory.IEntities;
using DispatchingProduct.Inventory.BLL.Managers;
using DispatchingProduct.Inventory.BLL.IManagers;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using IdentityServer4.AccessTokenValidation;
using DispatchProduct.Inventory.BLL.IManagers;
using DispatchProduct.Inventory.BLL.Managers;
using DispatchProduct.Inventory.ExcelSettings;
using DispatchProduct.Inventory.API.ServicesCommunication;
using DispatchProduct.Ordering.API.ServicesCommunication;
using DispatchProduct.Ordering.API.Settings;

namespace DispatchProduct.Inventory.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });
            services.AddDbContext<InventoryDbContext>(options =>
            options.UseSqlServer(Configuration["ConnectionString"],
            sqlOptions => sqlOptions.MigrationsAssembly("DispatchProduct.Inventory.EFCore.MSSQL")));

            services.AddOptions();
            services.Configure<InventoryAppSettings>(Configuration);
            services.Configure<ExcelSheetProperties>(Configuration.GetSection("ExcelSheetProperties"));
            services.Configure<OrderServiceSetting>(Configuration.GetSection("OrderServiceSetting"));


            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1",
                    new Info()
                    {
                        Title = "Inventory API",
                        Description = "Inventory  API"
                    });
                c.AddSecurityDefinition("oauth2", new OAuth2Scheme
                {
                    Type = "oauth2",
                    Flow = "implicit",
                    AuthorizationUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/authorize",
                    TokenUrl = $"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/token",
                    Scopes = new Dictionary<string, string>()
                    {
                        { "Inventory", "Inventory API" }
                    }
                });
            });

            ConfigureAuthService(services);

            services.AddMvc();

            services.AddAutoMapper(typeof(Startup));
            Mapper.AssertConfigurationIsValid();

            services.AddScoped<DbContext, InventoryDbContext>();
            services.AddScoped(typeof(IRepositry<>), typeof(Repositry<>));
            services.AddScoped(typeof(ICategory), typeof(Category));
            services.AddScoped(typeof(IItem), typeof(Item));
            services.AddScoped(typeof(ITransferedItems), typeof(TransferedItems));
            services.AddScoped(typeof(ITechnicianAssignedItems), typeof(TechnicianAssignedItems));
            services.AddScoped(typeof(ITechnicianUsedItems), typeof(TechnicianUsedItems));
            services.AddScoped(typeof(ICategoryManager), typeof(CategoryManager));
            services.AddScoped(typeof(IItemManager), typeof(ItemManager));
            services.AddScoped(typeof(IExcelItemsManager), typeof(ExcelItemsManager));
            services.AddScoped(typeof(ITransferedItemsManager), typeof(TransferedItemsManager));
            services.AddScoped(typeof(ITechnicianAssignedItemsManager), typeof(TechnicianAssignedItemsManager));
            services.AddScoped(typeof(ITechnicianUsedItemsManager), typeof(TechnicianUsedItemsManager));
            services.AddScoped(typeof(IOrderService), typeof(OrderService));
            services.AddScoped(typeof(IRepositry<>), typeof(Repositry<>));

            var container = new ContainerBuilder();
            container.Populate(services);

            return new AutofacServiceProvider(container.Build());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCors("AllowAll");


            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            var pathBase = Configuration["PATH_BASE"];
            if (!string.IsNullOrEmpty(pathBase))
            {
                loggerFactory.CreateLogger("init").LogDebug($"Using PATH BASE '{pathBase}'");
                app.UsePathBase(pathBase);
            }
            ConfigureAuth(app);
            app.UseStaticFiles();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Calling API");
            });


            // Make work identity server redirections in Edge and lastest versions of browers. WARN: Not valid in a production environment.
            //app.Use(async (context, next) =>
            //{
            //    context.Response.Headers.Add("Content-Security-Policy", "script-src 'unsafe-inline'");
            //    await next();
            //});

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
        private void ConfigureAuthService(IServiceCollection services)
        {
            // prevent from mapping "sub" claim to nameidentifier.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            var identityUrl = Configuration.GetValue<string>("IdentityUrl");
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
            .AddIdentityServerAuthentication(options =>
            {
                // base-address of your identityserver
                options.Authority = identityUrl;

                // name of the API resource
                options.ApiName = Configuration["ClientId"];
                options.ApiSecret = Configuration["Secret"];
                options.RequireHttpsMetadata = false;
                options.EnableCaching = true;
                options.CacheDuration = TimeSpan.FromMinutes(10);
                options.SaveToken = true;
            });

        }

        protected virtual void ConfigureAuth(IApplicationBuilder app)
        {
            app.UseAuthentication();
        }
    }
}
