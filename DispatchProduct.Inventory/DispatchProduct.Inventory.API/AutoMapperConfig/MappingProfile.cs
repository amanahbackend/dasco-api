﻿using AutoMapper;
using DispatchProduct.Inventory.API.ViewModel;
using DispatchProduct.Inventory.Entities;

namespace DispatchProduct.Inventory.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<Category, CategoryViewModel>();
            CreateMap<CategoryViewModel, Category>();

            CreateMap<FilterTechnicianViewModel, FilterTechnician>();
            CreateMap<FilterTechnician, FilterTechnicianViewModel>();

            CreateMap<TransferedItems, TransferedItemsViewModel>()
            .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item));

            CreateMap<TransferedItemsViewModel, TransferedItems>()
            .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item));


            CreateMap<TechnicianAssignedItems, TechnicianAssignedItemsViewModel>()
            .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item));

            CreateMap<TechnicianAssignedItemsViewModel, TechnicianAssignedItems>()
            .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item));


            CreateMap<TechnicianUsedItems, TechnicianUsedItemsViewModel>()
            .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item))
            .ForMember(dest => dest.Order, opt => opt.Ignore());

            CreateMap<TechnicianUsedItemsViewModel, TechnicianUsedItems>()
            .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item));


            CreateMap<Item, ItemViewModel>()
            .ForMember(dest => dest.Category, opt => opt.MapFrom(src => src.Category));
            CreateMap<ItemViewModel, Item>()
            .ForMember(dest => dest.Category, opt => opt.MapFrom(src => src.Category));
        }
    }
}
