﻿using DispatchProduct.HttpClient;
using DispatchProduct.Inventory.API.ServicesViewModels;
using DispatchProduct.Ordering.API.Settings;

namespace DispatchProduct.Inventory.API.ServicesCommunication
{
    public interface IOrderService : IDefaultHttpClientCrud<OrderServiceSetting, OrderViewModel, OrderViewModel>
    {
        
    }
}
