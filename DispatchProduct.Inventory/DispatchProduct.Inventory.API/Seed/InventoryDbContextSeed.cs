﻿
using DispatchProduct.Inventory.API.Settings;
using DispatchProduct.Inventory.Context;
using DispatchProduct.Inventory.Entities;
using DispatchProduct.Repoistry;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchProduct.Inventory.API.Seed
{
    public class InventoryDbContextSeed: ContextSeed
    {
        public async Task SeedAsync(InventoryDbContext context, IHostingEnvironment env,
            ILogger<InventoryDbContextSeed> logger, IOptions<InventoryAppSettings> settings, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;

            try
            {
                var useCustomizationData = settings.Value.UseCustomizationData;
                var contentRootPath = env.ContentRootPath;
                var webroot = env.WebRootPath;
                List<Category> categories = new List<Category>();
                if (useCustomizationData)
                {
                    //from file e.g (look at ApplicationDbContextSeed)
                }
                else
                {
                    //default from here
                    categories = GetDefaultCategorys();
                }
                await SeedEntityAsync(context, categories);
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 3)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for InventoryDbContext");

                    await SeedAsync(context, env, logger, settings, retryForAvaiability);
                }
            }
        }
        private List<Category> GetDefaultCategorys()
        {
            List<Category> result = new List<Category>
            {
                new Category() { Name = "General" },
                new Category() { Name = "Not Specific" }
            };
            return result;
        }

    }
}
