﻿using System;
using System.IO;
using Utilites.ProcessingResult;

namespace Utilites.UploadFile
{
    public class UploadExcelFileManager:UploadFileManager
    {
        public override ProcessResult<string> AddFile(UploadFile file,string filePath)
        {
            ProcessResult<string> result = new ProcessResult<string>();
            try
            {

                var isFileNotNull = ISFileNull(file);
                if (isFileNotNull.IsSucceeded)
                {

                    var extension = Path.GetExtension(file.FileName);
                    if (extension == ".xls" || extension == ".xlsx")
                    {
                        result = base.AddFile(file, filePath);
                    }
                    else
                    {
                        result.IsSucceeded = false;
                        result.Message = "File must be of excel extension";
                    }
                }
                else
                {
                    result = isFileNotNull;
                }

            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.IsSucceeded = false;
            }
            return result;
        }
    }
}
