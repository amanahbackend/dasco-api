﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Utilites.ProcessingResult;

namespace Utilites.UploadFile
{
    public class UploadFileManager : IUploadFileManager
    {
        public virtual ProcessResult<string> AddFile(Utilites.UploadFile.UploadFile file, string path)
        {
            ProcessResult<string> processResult1 = new ProcessResult<string>();
            processResult1.MethodName = MethodBase.GetCurrentMethod().Name;
            try
            {
                ProcessResult<string> processResult2 = this.ISFileNull(file);
                if (processResult2.IsSucceeded)
                {
                    string fileBase64 = file.FileContent.Clone().ToString();
                    if (fileBase64 != null)
                    {
                        FileUtilities.CreateIfMissing(path);
                        string filePath = Path.Combine(path, file.FileName);
                        FileUtilities.WriteFileToPath(fileBase64, filePath);
                        processResult1.IsSucceeded = true;
                        processResult1.returnData = path;
                        file.FileRelativePath = filePath;
                    }
                }
                else
                {
                    processResult1 = processResult2;
                    processResult1.IsSucceeded = false;
                }
            }
            catch (Exception ex)
            {
                processResult1.Exception = ex;
                processResult1.IsSucceeded = false;
            }
            return processResult1;
        }

        public virtual ProcessResult<List<string>> AddFiles(List<Utilites.UploadFile.UploadFile> files, string path)
        {
            ProcessResult<List<string>> output = new ProcessResult<List<string>>();
            output.returnData = new List<string>();
            foreach (Utilites.UploadFile.UploadFile file in files)
            {
                ProcessResult<string> input = this.AddFile(file, path);
                if (input.IsSucceeded)
                {
                    output.returnData.Add(input.returnData);
                    output = ProcessResultMapping.Map<string, List<string>>(input, output);
                }
                else
                {
                    output = ProcessResultMapping.Map<string, List<string>>(input, output);
                    break;
                }
            }
            return output;
        }

        public virtual ProcessResult<string> ISFileNull(Utilites.UploadFile.UploadFile file)
        {
            ProcessResult<string> processResult = new ProcessResult<string>();
            if (file != null && file.FileName != null && file.FileContent != null)
            {
                processResult.IsSucceeded = true;
            }
            else
            {
                processResult.IsSucceeded = false;
                processResult.Message = "File can't be null, Please Insert valid File";
                processResult.Exception = (Exception)new ArgumentNullException();
            }
            return processResult;
        }
    }
}
