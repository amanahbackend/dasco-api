﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;

namespace Utilites.UploadFile
{
    public interface IUploadFile : IBaseEntity
    {
        string FileContent { get; set; }

        string FileName { get; set; }

        string FileRelativePath { get; set; }
    }
}