﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace Utilites
{
    public static class Helper
    {
        public static string GenerateUniqueId()
        {
            return DateTime.Now.Ticks.ToString();
        }
        public static string CreateInfixDateCode()
        {
            string day = DateTime.Now.Day.ToString();
            string month = DateTime.Now.Month.ToString();
            string year = DateTime.Now.Year.ToString().Substring(2);
            string orderdate = day + month + year;
            return orderdate;
        }
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }
        public static string  GetValueFromRequestHeader(HttpRequest requst,string key="Authorization")
        {
             string authHeader = requst.Headers["Authorization"];
            authHeader=authHeader.Replace("bearer ", "");
            authHeader=authHeader.Replace("Bearer ", "");
            return authHeader;
        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}

