﻿namespace DispatchProduct.HttpClient
{
    public class DefaultHttpClientSettings : IHttpClientSettings
    {

        public virtual string Uri
        {
            get; set;
        }
        public virtual string GetVerb
        {
            get
            {
                return "Get";
            }
        }
        public virtual string GetAllVerb
        {
            get {
                return "GetAll";
            }
        }
        public virtual string PutVerb
        {
            get
            {
                return "Update";
            }
        }
        public virtual string PostVerb
        {
            get
            {
                return "Add";
            }
        }
        public virtual string DeleteVerb
        {
            get
            {
                return "Delete";
            }
        }
        public virtual string PatchVerb
        {
            get
            {
                return "";
            }
        }
    }
}
