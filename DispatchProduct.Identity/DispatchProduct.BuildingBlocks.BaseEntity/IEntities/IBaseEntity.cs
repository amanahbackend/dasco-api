﻿using System;

namespace DispatchProduct.BuildingBlocks.BaseEntities.IEntities
{
    public interface IBaseEntity
    {
        string FK_CreatedBy_Id { get; set; }

        string FK_UpdatedBy_Id { get; set; }

        string FK_DeletedBy_Id { get; set; }


        bool IsDeleted { get; set; }
        DateTime CreatedDate { get; set; }
        DateTime UpdatedDate { get; set; }
        DateTime DeletedDate { get; set; }
    }
}
