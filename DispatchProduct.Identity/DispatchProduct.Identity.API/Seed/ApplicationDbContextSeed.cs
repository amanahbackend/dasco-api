﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using DispatchProduct.Identity.BLL.IManagers;
using DispatchProduct.Identity.Context;
using DispatchProduct.Identity.Entities;
using DispatchProduct.Identity.Settings;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace DispatchProduct.Identity.API.Seed
{
    public class ApplicationDbContextSeed
    {
        IApplicationUserManager applicationUserManager;
        IApplicationRoleManager applicationRoleManager;
        public ApplicationDbContextSeed(IApplicationUserManager _applicationUserManager, IApplicationRoleManager _applicationRoleManager)
        {
            applicationUserManager = _applicationUserManager;
            applicationRoleManager = _applicationRoleManager;
        }

        private readonly IPasswordHasher<ApplicationUser> _passwordHasher = new PasswordHasher<ApplicationUser>();
        
        public async Task SeedAsync(ApplicationDbContext context, IHostingEnvironment env,
            ILogger<ApplicationDbContextSeed> logger, IOptions<AppSettings> settings, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;

            try
            {
                var useCustomizationData = settings.Value.UseCustomizationData;
                var contentRootPath = env.ContentRootPath;
                var webroot = env.WebRootPath;
                if (!context.Roles.Any())
                {
                    await applicationRoleManager.AddRolesAsync(GetDefaultRoles());
                }
                if (!context.Users.Any())
                {
                    var users = GetDefaultUsers();
                    await applicationUserManager.AddUsersAsync(users);
                }

                if (useCustomizationData)
                {
                    GetPreconfiguredImages(contentRootPath, webroot, logger);
                }
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 10)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for ApplicationDbContext");

                    await SeedAsync(context, env, logger, settings, retryForAvaiability);
                }
            }
        }

        //private IEnumerable<ApplicationUser> GetUsersFromFile(string contentRootPath, ILogger logger)
        //{
        //    string csvFileUsers = Path.Combine(contentRootPath, "Setup", "Users.csv");

        //    if (!File.Exists(csvFileUsers))
        //    {
        //        return GetDefaultUsers();
        //    }

        //    string[] csvheaders;
        //    try
        //    {
        //        string[] requiredHeaders = {
        //            "cardholdername", "cardnumber", "cardtype", "city", "country",
        //            "email", "expiration", "lastname", "name", "phonenumber",
        //            "username", "zipcode", "state", "street", "securitynumber",
        //            "normalizedemail", "normalizedusername", "password"
        //        };
        //        csvheaders = GetHeaders(requiredHeaders, csvFileUsers);
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.LogError(ex.Message);

        //        return GetDefaultUsers();
        //    }

        //    List<ApplicationUser> users = File.ReadAllLines(csvFileUsers)
        //                .Skip(1) // skip header column
        //                .Select(row => Regex.Split(row, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)") )
        //                .SelectTry(column => CreateApplicationUser(column, csvheaders))
        //                .OnCaughtException(ex => { logger.LogError(ex.Message); return null; })
        //                .Where(x => x != null)
        //                .ToList();

        //    return users;
        //}

        private ApplicationUser CreateApplicationUser(string[] column, string[] headers)
        {
            if (column.Length != headers.Length)
            {
                throw new Exception($"column count '{column.Length}' not the same as headers count'{headers.Length}'");
            }

            string cardtypeString = column[Array.IndexOf(headers, "cardtype")].Trim('"').Trim();
            if (!int.TryParse(cardtypeString, out int cardtype))
            {
                throw new Exception($"cardtype='{cardtypeString}' is not a number");
            }

            var user = new ApplicationUser
            {

                Id = Guid.NewGuid().ToString(),
                LastName = column[Array.IndexOf(headers, "lastname")].Trim('"').Trim(),
                PhoneNumber = column[Array.IndexOf(headers, "phonenumber")].Trim('"').Trim(),
                UserName = column[Array.IndexOf(headers, "username")].Trim('"').Trim(),
                NormalizedEmail = column[Array.IndexOf(headers, "normalizedemail")].Trim('"').Trim(),
                NormalizedUserName = column[Array.IndexOf(headers, "normalizedusername")].Trim('"').Trim(),
                SecurityStamp = Guid.NewGuid().ToString("D"),
                PasswordHash = column[Array.IndexOf(headers, "password")].Trim('"').Trim(), // Note: This is the password
            };

            user.PasswordHash = _passwordHasher.HashPassword(user, user.PasswordHash);

            return user;
        }

        private List<Tuple<ApplicationUser, string>> GetDefaultUsers()
        {
            var user1 =
            new ApplicationUser
            {

                FirstName = "admin",
                MiddleName = "admin",
                LastName = "admin",
                Id = Guid.NewGuid().ToString(),
                PhoneNumber = "12345678",
                UserName = "admin",
                Email = "admin@microsoft.com",
                NormalizedEmail = "admin@MICROSOFT.COM",
                NormalizedUserName = "admin@MICROSOFT.COM",
                SecurityStamp = Guid.NewGuid().ToString("D"),
                RoleNames = new List<string> { "Admin" }
            };
            var user2 =
            new ApplicationUser
            {

                FirstName = "callCenter",
                MiddleName = "callCenter",
                LastName = "callCenter",
                Id = Guid.NewGuid().ToString(),
                PhoneNumber = "12345678",
                UserName = "callCenter",
                Email = "callCenter@microsoft.com",
                NormalizedEmail = "callCenter@MICROSOFT.COM",
                NormalizedUserName = "callCenter@MICROSOFT.COM",
                SecurityStamp = Guid.NewGuid().ToString("D"),
                RoleNames = new List<string> { "CallCenter" }
            };
            var user3 =
            new ApplicationUser
            {

                FirstName = "Technician",
                MiddleName = "Technician",
                LastName = "Technician",
                Id = Guid.NewGuid().ToString(),
                PhoneNumber = "12345678",
                UserName = "Technician",
                Email = "Technician@microsoft.com",
                NormalizedEmail = "Technician@MICROSOFT.COM",
                NormalizedUserName = "Technician@MICROSOFT.COM",
                SecurityStamp = Guid.NewGuid().ToString("D"),
                RoleNames = new List<string> { "Technician" }
            };
            var user4 =
            new ApplicationUser
            {

                FirstName = "SupervisorDispatcher",
                MiddleName = "SupervisorDispatcher",
                LastName = "SupervisorDispatcher",
                Id = Guid.NewGuid().ToString(),
                PhoneNumber = "12345678",
                UserName = "SupervisorDispatcher",
                Email = "SupervisorDispatcher@microsoft.com",
                NormalizedEmail = "SupervisorDispatcher@MICROSOFT.COM",
                NormalizedUserName = "SupervisorDispatcher@MICROSOFT.COM",
                SecurityStamp = Guid.NewGuid().ToString("D"),
                RoleNames = new List<string> { "SupervisorDispatcher" }
            };

            return new List<Tuple<ApplicationUser, string>>
            {
                new Tuple<ApplicationUser, string>(user1,"P@ssw0rd"),
                new Tuple<ApplicationUser, string>(user2,"P@ssw0rd"),
                new Tuple<ApplicationUser, string>(user3,"P@ssw0rd"),
                new Tuple<ApplicationUser, string>(user4,"P@ssw0rd")
            };
        }

        private static List<ApplicationRole> GetDefaultRoles()
        {
            return new List<ApplicationRole>
            {
                new ApplicationRole {Name="Admin"},
                new ApplicationRole {Name="Technician"},
                new ApplicationRole {Name="CallCenter"},
                new ApplicationRole {Name="SupervisorDispatcher"}
            };
        }

        static string[] GetHeaders(string[] requiredHeaders, string csvfile)
        {
            string[] csvheaders = File.ReadLines(csvfile).First().ToLowerInvariant().Split(',');

            if (csvheaders.Length != requiredHeaders.Length)
            {
                throw new Exception($"requiredHeader count '{ requiredHeaders.Length}' is different then read header '{csvheaders.Length}'");
            }

            foreach (var requiredHeader in requiredHeaders)
            {
                if (!csvheaders.Contains(requiredHeader))
                {
                    throw new Exception($"does not contain required header '{requiredHeader}'");
                }
            }

            return csvheaders;
        }

        static void GetPreconfiguredImages(string contentRootPath, string webroot, ILogger logger)
        {
            try
            {
                string imagesZipFile = Path.Combine(contentRootPath, "Setup", "images.zip");
                if (!File.Exists(imagesZipFile))
                {
                    logger.LogError($" zip file '{imagesZipFile}' does not exists.");
                    return;
                }

                string imagePath = Path.Combine(webroot, "images");
                string[] imageFiles = Directory.GetFiles(imagePath).Select(file => Path.GetFileName(file)).ToArray();

                using (ZipArchive zip = ZipFile.Open(imagesZipFile, ZipArchiveMode.Read))
                {
                    foreach (ZipArchiveEntry entry in zip.Entries)
                    {
                        if (imageFiles.Contains(entry.Name))
                        {
                            string destinationFilename = Path.Combine(imagePath, entry.Name);
                            if (File.Exists(destinationFilename))
                            {
                                File.Delete(destinationFilename);
                            }
                            entry.ExtractToFile(destinationFilename);
                        }
                        else
                        {
                            logger.LogWarning($"Skip file '{entry.Name}' in zipfile '{imagesZipFile}'");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Exception in method GetPreconfiguredImages WebMVC. Exception Message={ex.Message}");
            }
        }
    }
}
