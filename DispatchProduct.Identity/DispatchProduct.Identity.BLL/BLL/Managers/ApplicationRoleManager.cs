﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.Identity.BLL.IManagers;
using DispatchProduct.Identity.Entities;
using DispatchProduct.Identity.Models.Entities;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchProduct.Identity.BLL.Managers
{
    public class ApplicationRoleManager: IApplicationRoleManager
    {
        RoleManager<ApplicationRole> identityRoleManager;
        IRolePrivilgeManager rolePrivilgeManager;
        IPrivilgeManager privilgeManager;
        public ApplicationRoleManager(RoleManager<ApplicationRole> _identityRoleManager, IRolePrivilgeManager _rolePrivilgeManager, IPrivilgeManager _privilgeManager)
        {
            identityRoleManager = _identityRoleManager;
            rolePrivilgeManager = _rolePrivilgeManager;
            privilgeManager = _privilgeManager;
        }
        public List<Privilge> GetPrivilgesByRoleId(string roleId)
        {
            List<Privilge> result = new List<Privilge>();
            var rolePrivilges= rolePrivilgeManager.GetAll().Where(r => r.FK_Role_Id == roleId).ToList();
            foreach (var rolePrivilge in rolePrivilges)
            {
                var privilge = privilgeManager.Get(rolePrivilge.FK_Privilge_Id);
                if (privilge != null)
                {
                    result.Add(privilge);
                }
            }
            return result;
        }
        public async Task<List<ApplicationRole>> GetRolesByPrivilgeId(int privilgeId)
        {
            List<ApplicationRole> result = new List<ApplicationRole>();
            var rolePrivilges = rolePrivilgeManager.GetAll().Where(r => r.FK_Privilge_Id == privilgeId).ToList();
            foreach (var rolePrivilge in rolePrivilges)
            {
                var role =await GetRoleAsyncById(rolePrivilge.FK_Role_Id);
                if (role != null)
                {
                    result.Add(role);
                }
            }
            return result;
        }
        public async Task<ApplicationRole> GetRoleAsyncByName(string roleName)
        {
            return await identityRoleManager.FindByNameAsync(roleName);
        }
        public async Task<ApplicationRole> GetRoleAsyncById(string Id)
        {
            return await identityRoleManager.FindByIdAsync(Id);
        }
        public async Task<ApplicationRole> GetRoleAsync(ApplicationRole role)
        {
            ApplicationRole result=null;
            if (!string.IsNullOrEmpty(role.Name))
                result = await GetRoleAsyncByName(role.Name);
            if (result==null)
            {
                if (!string.IsNullOrEmpty(role.Id))
                    result = await GetRoleAsyncById(role.Id);
            }
            return result;
        }
        public  List<ApplicationRole> GetAllRoles()
        {
            return  identityRoleManager.Roles.ToList();
        }

        public async Task<string> AddRoleAsync(ApplicationRole applicationRole)
        {
            IdentityResult result = await identityRoleManager.CreateAsync(applicationRole);
            if (result.Succeeded)
            {
                return applicationRole.Name;
            }
            else
            {
                return null;
            }
        }
        public async Task AddRolesAsync(List<ApplicationRole> applicationRoles)
        {
            foreach (var role in applicationRoles)
            {
               await AddRoleAsync(role);
            }
        }
        public async Task<ApplicationRole> AddRoleAsyncronous(ApplicationRole applicationRole)
        {
            IdentityResult result = await identityRoleManager.CreateAsync(applicationRole);
            if (result.Succeeded)
            {
                return applicationRole;
            }
            else
            {
                return null;
            }
        }

        public async Task<bool> UpdateRoleAsync(ApplicationRole applicationRole)
        {
            var result = false;
             var oldRole = await GetRoleAsync(applicationRole);
            if(applicationRole!=null)
            {
                oldRole.Name = applicationRole.Name;
                IdentityResult callBack = await identityRoleManager.UpdateAsync(oldRole);
                if (callBack.Succeeded)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }

        public async Task<bool> DeleteRoleAsync(ApplicationRole applicationRole)
        {
            var result = false;
            var role = await GetRoleAsync(applicationRole);
            IdentityResult callBack = await identityRoleManager.DeleteAsync(role);
            if (callBack.Succeeded)
            {
                result = true;
            }
            else
            {
                result = false;

            }
            return result;
        }

        public async Task<string> AddRoleAsync(string roleName)
        {
            ApplicationRole applicationRole = new ApplicationRole(new BaseEntity(), roleName);
            return await AddRoleAsync(applicationRole);
        }

        public async Task<bool> IsRoleExistAsync(string roleName)
        {
            return await identityRoleManager.RoleExistsAsync(roleName);
        }

        public async Task<List<Privilge>> GetPrivilgesByRoleName(string roleName)
        {
            List<Privilge> result = null;
            var role= await GetRoleAsyncByName(roleName);
            if (role != null)
            {
                result=GetPrivilgesByRoleId(role.Id);
            }
            return result;
        }
    }
}
