﻿using DispatchProduct.Repoistry;
using DispatchProduct.Identity.Models.Entities;
using DispatchProduct.Identity.Context;
using System.Collections.Generic;
using System.Linq;

namespace DispatchProduct.Identity.BLL.IManagers
{
    public class RolePrivilgeManager : Repositry<RolePrivilge>,IRolePrivilgeManager
    {
        public RolePrivilgeManager(ApplicationDbContext context)
            : base(context)
        {

        }
        public List<RolePrivilge> GetRolePrivilgeByRoleId(string roleId)
        {
            return GetAll().Where(role => role.FK_Role_Id == roleId).ToList();
        }
    }
}
