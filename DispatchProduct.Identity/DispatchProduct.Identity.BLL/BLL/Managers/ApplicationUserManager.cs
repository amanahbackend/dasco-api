﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.Identity.BLL.IManagers;
using DispatchProduct.Identity.Entities;
using Microsoft.AspNetCore.Identity;

namespace DispatchProduct.Identity.BLL.Managers
{
    public class ApplicationUserManager : IApplicationUserManager
    {
        //private readonly IPasswordHasher<ApplicationUser> _passwordHasher;//= new PasswordHasher<ApplicationUser>();
        public UserManager<ApplicationUser> _identityUserManager;
        RoleManager<ApplicationRole> _identityRoleManager;
        public ApplicationUserManager(UserManager<ApplicationUser> identityUserManager, RoleManager<ApplicationRole> identityRoleManager)//, IPasswordHasher<ApplicationUser> passwordHasher)
        {
            _identityUserManager = identityUserManager;
            // _passwordHasher = passwordHasher;
            _identityRoleManager = identityRoleManager;
        }

        /// <summary>
        /// Method to add user in the database using identity provider.
        /// </summary>
        /// <param name="user">user object to be added.</param>
        /// <returns>id of the added user in case of operation succeed. null in case of failure.</returns>

        public async Task AddUsersAsync(List<Tuple<ApplicationUser, string>> users)
        {
            foreach (var usr in users)
            {
                await AddUserAsync(usr.Item1, usr.Item2);
            }

        }
        public async Task<ApplicationUser> AddUserAsync(ApplicationUser user, string password)
        {
            ApplicationUser result = null;
            if (user.RoleNames != null && user.RoleNames.Count > 0)
            {
                user.SecurityStamp = Guid.NewGuid().ToString("D");
                var identityResult = await _identityUserManager.CreateAsync(user, password);
                if (identityResult.Succeeded)
                {
                    await AddUserToRolesAsync(user);
                    result = user;
                }
            }
            return result;
        }
        public async Task<bool> ChangePassword(ApplicationUser user, string oldPassword, string newPassword)
        {
            bool result = false;
            IdentityResult identityResult = await _identityUserManager.ChangePasswordAsync(user, oldPassword, newPassword);
            if (identityResult.Succeeded)
            {
                result = true;
            }
            return result;

        }

        public async Task<bool> UpdateUserAsync(ApplicationUser user)
        {
            bool result = false;
            var olduser = await _identityUserManager.FindByIdAsync(user.Id);
            var oldRoles = (await GetRolesAsync(olduser.UserName)).ToList();
            var identityRMVRolResult = await _identityUserManager.RemoveFromRolesAsync(olduser, oldRoles);
            if (identityRMVRolResult.Succeeded)
            {
                CopyProps(olduser, user);
                IdentityResult identityResult = await _identityUserManager.UpdateAsync(olduser);
                if (identityResult.Succeeded)
                {
                    result = true;

                    if (olduser.RoleNames != null)
                    {
                        result = true;
                        var identityAddRolResult = await _identityUserManager.AddToRolesAsync(olduser, olduser.RoleNames);
                        if (identityAddRolResult.Succeeded)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }

            }
            else
            {
                result = false;
            }
            return result;
        }

        public async Task<bool> ChangeUserAvailabiltyAsync(string userId, bool isAvailable)
        {
            bool result = false;
            var user = await _identityUserManager.FindByIdAsync(userId);
            user.IsAvailable = isAvailable;
            var identityResult = await _identityUserManager.UpdateAsync(user);
            if (identityResult.Succeeded)
            {
                result = true;
            }
            return result;
        }

        public async Task<ApplicationUser> AddUserAsync(string userName, string email, string password)
        {
            ApplicationUser applicationUser = new ApplicationUser(new BaseEntity())
            {
                Email = email,
                UserName = userName
            };
            return await AddUserAsync(applicationUser, password);
        }

        public async Task<ApplicationUser> GetByUserNameAsync(string userName)
        {
            ApplicationUser user = await _identityUserManager.FindByNameAsync(userName);
            return user;
        }

        public async Task<bool> DeleteAsync(ApplicationUser user)
        {
            var result = false;
            user = await GetAsync(user);
            var callBack = await _identityUserManager.DeleteAsync(user);
            if (callBack.Succeeded)
            {
                result = true;
            }
            return result;
        }
        public async Task<ApplicationUser> GetAsync(ApplicationUser user)
        {
            if (user.UserName != null)
                user = await _identityUserManager.FindByNameAsync(user.UserName);
            else if (user == null && user.Email != null)
                user = await _identityUserManager.FindByEmailAsync(user.Email);
            else if (user == null && user.Id != null)
                user = await _identityUserManager.FindByIdAsync(user.Id);
            if (user != null)
            {
                user.RoleNames = (await GetRolesAsync(user)).ToList();
            }
            return user;
        }

        public async Task<List<ApplicationUser>> GetAll()
        {
            var result = _identityUserManager.Users.ToList();
            foreach (var user in result)
            {
                if (user != null)
                {
                    var userRoles = (await GetRolesAsync(user)).ToList();
                    if (userRoles != null && userRoles.Count > 0)
                    {
                        user.RoleNames = userRoles;
                    }
                    // result.Add(user);
                }
            }
            return result;
        }

        public List<ApplicationUser> GetByUserIds(List<string> userIds)
        {
            return _identityUserManager.Users.Where(usr => userIds.Contains(usr.Id)).ToList();
        }
        public List<ApplicationUser> GetAllExcept(List<string> userIds)
        {
            return _identityUserManager.Users.Where(usr => !userIds.Contains(usr.Id)).ToList();
        }
        public async Task<List<ApplicationUser>> GetByRoleExcept(string roleName, List<string> userIds = null)
        {
            var result = (await _identityUserManager.GetUsersInRoleAsync(roleName)).ToList();
            if (userIds != null)
            {
                result = result.Where(usr => !userIds.Contains(usr.Id)).ToList();
            }
            foreach (var user in result)
            {
                if (user != null)
                {
                    var userRoles = (await GetRolesAsync(user)).ToList();
                    if (userRoles != null && userRoles.Count > 0)
                    {
                        user.RoleNames = userRoles;
                    }
                }
            }
            return result;
        }

        public async Task<IList<Claim>> GetClaimsAsync(ApplicationUser user)
        {
            return await _identityUserManager.GetClaimsAsync(user);
        }

        public async Task<IList<string>> GetRolesAsync(string userName)
        {

            var user = await GetByUserNameAsync(userName);
            IList<string> rolesNames = await _identityUserManager.GetRolesAsync(user);
            return rolesNames;
        }

        public async Task<IList<string>> GetRolesAsync(ApplicationUser user)
        {
            IList<string> rolesNames = await _identityUserManager.GetRolesAsync(user);
            return rolesNames;
        }


        public async Task<bool> AddUserToRoleAsync(string userName, string roleName)
        {
            bool result = false;
            var applicationUser = await GetByUserNameAsync(userName);
            if (applicationUser != null)
            {
                if (roleName != null)
                {
                    var role = await _identityRoleManager.FindByNameAsync(roleName);
                    IdentityResult roleResult = await _identityUserManager.AddToRoleAsync(applicationUser, role.Name);
                    if (roleResult.Succeeded)
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }

            }
            return result;
        }

        public async Task AddUsersToRolesAsync(List<ApplicationUser> users)
        {
            foreach (var user in users)
            {
                await AddUserToRolesAsync(user);
            }
        }
        public async Task AddUserToRolesAsync(ApplicationUser user)
        {
            if (user != null && user.RoleNames != null)
            {
                foreach (var role in user.RoleNames)
                {
                    if (await _identityRoleManager.FindByNameAsync(role) == null)
                    {
                        await _identityRoleManager.CreateAsync(new ApplicationRole() { Name = role });
                    }
                }
                await _identityUserManager.AddToRolesAsync(user, user.RoleNames);
            }
        }
        public async Task<bool> AddUserToRoleAsync(ApplicationUser applicationUser, ApplicationRole applicationRole)
        {
            bool result = false;
            if (applicationUser != null)
            {
                IdentityResult roleResult = await _identityUserManager.AddToRoleAsync(applicationUser, applicationRole.Name);
                if (roleResult.Succeeded)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }

        public async Task<bool> IsUserNameExistAsync(string userName)
        {
            var user = await _identityUserManager.FindByNameAsync(userName);
            if (user != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public async Task<bool> IsEmailExistAsync(string email)
        {
            var user = await _identityUserManager.FindByEmailAsync(email);
            if (user != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void CopyProps(ApplicationUser oldUser, ApplicationUser user)
        {
            oldUser.PhoneNumber = user.PhoneNumber;
            oldUser.RoleNames = user.RoleNames;
            oldUser.UserName = user.UserName;
            oldUser.Email = user.Email;
            oldUser.FirstName = user.FirstName;
            oldUser.MiddleName = user.MiddleName;
            oldUser.LastName = user.LastName;
            oldUser.RoleNames = user.RoleNames;
        }

        public async Task<ApplicationUser> GetByUserIdAsync(string userId)
        {
            ApplicationUser user = await _identityUserManager.FindByIdAsync(userId);
            return user;
        }

        public string HashPassword(ApplicationUser applicationUser, string newPassword)
        {
            return _identityUserManager.PasswordHasher.HashPassword(applicationUser, newPassword);
        }

        public Task<IdentityResult> UpdateAsync(ApplicationUser user)
        {
            return _identityUserManager.UpdateAsync(user);
        }
    }
}
