﻿using DispatchProduct.Identity.Entities;
using DispatchProduct.Identity.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchProduct.Identity.BLL.IManagers
{
    public interface IApplicationRoleManager
    {
        Task<ApplicationRole> GetRoleAsyncByName(string roleName);

        Task<ApplicationRole> GetRoleAsync(ApplicationRole role);

        Task<string> AddRoleAsync(ApplicationRole applicationRole);

        Task AddRolesAsync(List<ApplicationRole> applicationRoles);

        Task<ApplicationRole> AddRoleAsyncronous(ApplicationRole applicationRole);

        Task<string> AddRoleAsync(string roleName);

        Task<bool> IsRoleExistAsync(string roleName);

        Task<List<Privilge>> GetPrivilgesByRoleName(string roleName);

        List<ApplicationRole> GetAllRoles();

        Task<bool> DeleteRoleAsync(ApplicationRole applicationRole);

        Task<bool> UpdateRoleAsync(ApplicationRole applicationRole);
    }
}