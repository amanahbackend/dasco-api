﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using DispatchProduct.Identity.Entities;
using Microsoft.AspNetCore.Identity;

namespace DispatchProduct.Identity.BLL.IManagers
{
    public interface IApplicationUserManager
    {
        Task<ApplicationUser> AddUserAsync(ApplicationUser user, string password);

        Task<ApplicationUser> AddUserAsync(string userName, string email, string password);

        Task<bool> UpdateUserAsync(ApplicationUser user);

        Task<ApplicationUser> GetByUserNameAsync(string userName);

        Task<IList<Claim>> GetClaimsAsync(ApplicationUser user);

        Task<IList<string>> GetRolesAsync(string userName);

        Task<IList<string>> GetRolesAsync(ApplicationUser user);

        Task<bool> AddUserToRoleAsync(string userName, string roleName);

        Task<bool> AddUserToRoleAsync(ApplicationUser applicationUser, ApplicationRole applicationRole);

        Task<bool> IsUserNameExistAsync(string userName);

        Task<bool> IsEmailExistAsync(string email);

        Task<bool> DeleteAsync(ApplicationUser user);

        Task<ApplicationUser> GetAsync(ApplicationUser user);

        Task<List<ApplicationUser>> GetAll();

        List<ApplicationUser> GetByUserIds(List<string> userIds);

        List<ApplicationUser> GetAllExcept(List<string> userIds);

        Task<List<ApplicationUser>> GetByRoleExcept(string roleName, List<string> userIds = null);

        Task AddUsersAsync(List<Tuple<ApplicationUser, string>> users);

        Task<bool> ChangeUserAvailabiltyAsync(string userId, bool isAvailable);

        Task<ApplicationUser> GetByUserIdAsync(string userId);

        string HashPassword(ApplicationUser applicationUser, string newPassword);

        Task<IdentityResult> UpdateAsync(ApplicationUser applicationUser);
    }
}
