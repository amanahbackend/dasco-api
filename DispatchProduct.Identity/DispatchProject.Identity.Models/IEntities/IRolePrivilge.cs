﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Identity.Entities;

namespace DispatchProduct.Identity.Models.Entities
{
    public interface IRolePrivilge : IBaseEntity
    {
        int FK_Privilge_Id { get; set; }

        Privilge Privilge { get; set; }

        string FK_Role_Id { get; set; }

        ApplicationRole Role { get; set; }
    }
}
