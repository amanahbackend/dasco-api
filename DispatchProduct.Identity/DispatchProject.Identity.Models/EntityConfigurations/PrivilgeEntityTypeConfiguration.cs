﻿using DispatchProduct.Identity.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DispatchProduct.Identity.EntityConfigurations
{
    public class PrivilgeEntityTypeConfiguration
        : IEntityTypeConfiguration<Privilge>
    {
        public void Configure(EntityTypeBuilder<Privilge> PrivilgeConfiguration)
        {
            PrivilgeConfiguration.ToTable("Privilge");

            PrivilgeConfiguration.HasKey(o => o.Id);

            PrivilgeConfiguration.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("Privilgeseq");

            PrivilgeConfiguration.Ignore(o => o.Roles);

            PrivilgeConfiguration.Property(o => o.Name)
                .HasMaxLength(500)
                .IsRequired();
        }
    }
}
