﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchProduct.Identity.Entities
{
    public class ApplicationUser : IdentityUser
    {
        private IBaseEntity baseEntity;

        public ApplicationUser(IBaseEntity _baseEntity)
        {
            this.baseEntity = _baseEntity;
        }

        public ApplicationUser()
        {
        }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public bool IsAvailable { get; set; }

        public string FK_CreatedBy_Id { get; set; }

        public string FK_UpdatedBy_Id { get; set; }

        public string FK_DeletedBy_Id { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public DateTime DeletedDate { get; set; }

        [NotMapped]
        public List<string> RoleNames { get; set; }
    }
}
