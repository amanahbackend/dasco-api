﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Identity.Entities;
using System.Collections.Generic;

namespace DispatchProduct.Identity.Models.Entities
{
    public class Privilge : BaseEntity, IPrivilge, IBaseEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<ApplicationRole> Roles { get; set; }
    }
}
