﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using DispatchProduct.Identity.Models.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace DispatchProduct.Identity.Entities
{
    public class ApplicationRole : IdentityRole, IBaseEntity, IApplicationRole
    {
        private IBaseEntity baseEntity;

        public ApplicationRole()
        {
        }

        public ApplicationRole(IBaseEntity _baseEntity, string roleName)
          : base(roleName)
        {
            this.baseEntity = _baseEntity;
        }

        public string FK_CreatedBy_Id { get; set; }

        public string FK_UpdatedBy_Id { get; set; }

        public string FK_DeletedBy_Id { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public DateTime DeletedDate { get; set; }

        public List<Privilge> Privilges { get; set; }
    }
}
