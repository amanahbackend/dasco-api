﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IdentityModel.Client;
using Microsoft.Extensions.Options;
using DispatchProduct.Identity.Settings;
using DispatchProduct.Identity.BLL.IManagers;
using DispatchProduct.Identity.Models.Entities;

namespace DispatchProduct.UserManagment.API.Controllers
{
    [Route("api/[controller]")]
    public class TokenController : Controller
    {
        private AppSettings _appSettings;
        private UserController userController;
        private IApplicationRoleManager applicationRoleManager;

        public TokenController(IOptions<AppSettings> appSettings, UserController _userController, IApplicationRoleManager _applicationRoleManager)
        {
            _appSettings = appSettings.Value;
            userController = _userController;
            applicationRoleManager = _applicationRoleManager;
        }

        [HttpPost]
        [Route("Technician")]
        public async Task<IActionResult> Technician([FromBody]TokenViewModel model, string authHeader = null)
        {
            TokenResponse tokenResponse = null;
            ApplicationUserViewModel user = null;
            if (ModelState.IsValid)
            {
                var result = await GetResponse(model, authHeader);
                user = result.Item1;
                tokenResponse = result.Item2;
            }

            if (user != null && tokenResponse != null)
            {
                if (user.RoleNames.Contains("Technician"))
                {
                    return Ok(new
                    {
                        Roles = user.RoleNames,
                        Id = user.Id,
                        UserName = user.UserName,
                        FullName = user.FirstName + " " + user.MiddleName + " " + user.LastName,
                        token = tokenResponse
                    });
                }
            }
            return BadRequest();
        }

        /// <summary>
        /// Login Method
        /// </summary>
        /// <param name="model"></param>
        /// <param name="authHeader"></param>
        /// <returns></returns>
        [HttpPost/*, Route("Login")*/]
        public async Task<IActionResult> Login([FromBody]TokenViewModel model, string authHeader = null)
        {
            TokenResponse tokenResponse = null;
            ApplicationUserViewModel user = null;
            List<string> privilges = new List<string>();

            if (ModelState.IsValid)
            {
                var result = await GetResponse(model, authHeader);
                user = result.Item1;
                tokenResponse = result.Item2;
            }

            if (user != null && tokenResponse != null)
            {
                if (user.RoleNames.Contains(_appSettings.TechnicianRole))
                {
                    return BadRequest();
                }

                foreach (string roleName in user.RoleNames)
                {
                    List<Privilge> privilgesByRoleName = await applicationRoleManager.GetPrivilgesByRoleName(roleName);

                    if (privilgesByRoleName != null && privilgesByRoleName.Count > 0)
                    {
                        privilges.AddRange(privilgesByRoleName.Select(r => r.Name).ToList());
                    }
                }

                return Ok(new
                {
                    Roles = user.RoleNames,
                    Id = user.Id,
                    UserName = user.UserName,
                    Privilges = privilges,
                    FullName = $"{user.FirstName} {user.MiddleName} {user.LastName}",
                    token = tokenResponse
                });
            }

            return BadRequest();
        }

        [HttpPost, Route("GetSysToken")]
        public async Task<IActionResult> GetSysToken([FromBody] TokenViewModel model, string authHeader = null)
        {
            string result = null;
            if (ModelState.IsValid)
            {
                Tuple<ApplicationUserViewModel, TokenResponse> response = await GetResponse(model, authHeader);
                TokenResponse tokenResponse = response.Item2;
                if (tokenResponse.AccessToken != null)
                    result = tokenResponse.AccessToken;
            }
            return Ok(result);
        }

        private async Task<Tuple<ApplicationUserViewModel, TokenResponse>> GetResponse([FromBody]TokenViewModel model, string authHeader = null)
        {
            TokenResponse tokenResponse = null;
            ApplicationUserViewModel user = null;

            if (ModelState.IsValid)
            {
                if (_appSettings.IdentityUrl != null && _appSettings.ClientId != null && _appSettings.Secret != null)
                {
                    var discoveryClient = new DiscoveryClient(_appSettings.IdentityUrl);
                    discoveryClient.Policy.RequireHttps = false;
                    discoveryClient.Policy.ValidateIssuerName = false;
                    var disco = await discoveryClient.GetAsync();

                    if (disco.TokenEndpoint != null)
                    {
                        var tokenClient = new TokenClient(disco.TokenEndpoint, _appSettings.ClientId, _appSettings.Secret);
                        tokenResponse = await tokenClient.RequestResourceOwnerPasswordAsync(model.Username, model.Password, "calling");

                        if (tokenResponse.AccessToken != null)
                        {
                            string token = "bearer " + tokenResponse.AccessToken;
                            //var userInfoClient = new UserInfoClient(disco.UserInfoEndpoint);
                            //var response = await userInfoClient.GetAsync(token);
                            //var claims = response.Claims;
                            user = await userController.UserRoles(model.Username, token);
                        }
                    }

                }
            }

            return new Tuple<ApplicationUserViewModel, TokenResponse>(user, tokenResponse);
        }
    }
}