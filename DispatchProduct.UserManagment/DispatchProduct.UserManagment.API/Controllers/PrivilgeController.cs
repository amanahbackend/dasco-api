﻿using AutoMapper;
using DispatchProduct.Identity.BLL.IManagers;
using DispatchProduct.Identity.Models.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace DispatchProduct.UserManagment.API.Controllers
{
    [Route("api/Privilge")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class PrivilgeController : Controller
    {
        private readonly IPrivilgeManager manger;
        public readonly IMapper mapper;

        public PrivilgeController(IPrivilgeManager _manger, IMapper _mapper)
        {
            mapper = _mapper;
            manger = _manger;
        }
        
        [Route("Get")]
        [HttpGet]
        public IActionResult Get(int id)
        {
            return Ok(mapper.Map<Privilge, PrivilgeViewModel>(manger.Get(id)));
        }

        [Route("GetAll")]
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(mapper.Map<List<Privilge>, List<PrivilgeViewModel>>(manger.GetAll().ToList()));
        }

        [Route("Add")]
        [HttpPost]
        public IActionResult Post([FromBody] PrivilgeViewModel model)
        {
            return Ok(mapper.Map<Privilge, PrivilgeViewModel>(manger.Add(mapper.Map<PrivilgeViewModel, Privilge>(model))));
        }

        [Route("Update")]
        [HttpPost]
        public IActionResult Put([FromBody] PrivilgeViewModel model)
        {
            return Ok(manger.Update(mapper.Map<PrivilgeViewModel, Privilge>(model)));
        }

        [Route("Delete/{id}")]
        [HttpDelete]
        public IActionResult Delete([FromRoute] int id)
        {
            return Ok(manger.DeleteById(id));
        }
    }
}
