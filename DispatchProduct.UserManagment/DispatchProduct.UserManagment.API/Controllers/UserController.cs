﻿using AutoMapper;
using DispatchProduct.Identity.BLL.IManagers;
using DispatchProduct.Identity.Entities;
using DispatchProduct.Identity.Settings;
using DispatchProduct.UserManagment.API.ViewModels;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authentication.Twitter;
using Utilites;
using Utilites.ExcelToGenericList;
using Utilites.UploadFile;
using Utilities.Utilites;

namespace DispatchProduct.UserManagment.API.Controllers
{
    [Route("api/User")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class UserController : Controller
    {
        private readonly IHostingEnvironment _hostingEnv;
        public readonly IMapper mapper;
        public readonly UserManager<ApplicationUser> userManager;
        IApplicationUserManager appUserManager;
        AppSettings appSettings;
        private SignInManager<ApplicationUser> _signInManager;

        public UserController(IMapper _mapper, UserManager<ApplicationUser> _userManager,
            IApplicationUserManager _appuserManager, IOptions<AppSettings> _appSettings,
            IHostingEnvironment hostingEnv, SignInManager<ApplicationUser> signManager)
        {
            _hostingEnv = hostingEnv;
            this.mapper = _mapper;
            this.userManager = _userManager;
            appUserManager = _appuserManager;
            appSettings = _appSettings.Value;
            _signInManager = signManager;
        }

        #region DefaultCrudOperation

        #region GetApi

        [Route("Get")]
        [HttpGet]
        public async Task<IActionResult> Get(ApplicationUserViewModel model)
        {
            ApplicationUserViewModel result = new ApplicationUserViewModel();
            ApplicationUser entityResult = new ApplicationUser();
            entityResult = mapper.Map<ApplicationUserViewModel, ApplicationUser>(result);
            entityResult = await appUserManager.GetAsync(entityResult);
            result = mapper.Map<ApplicationUser, ApplicationUserViewModel>(entityResult);
            return Ok(result);
        }

        [Route("GetAll")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Get()
        {
            List<ApplicationUserViewModel> result = new List<ApplicationUserViewModel>();
            List<ApplicationUser> entityResult = new List<ApplicationUser>();
            entityResult = await appUserManager.GetAll();
            result = mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return Ok(result);
        }

        [Route("GetByUserIds")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public List<ApplicationUserViewModel> GetByUserIds([FromBody]List<string> userIds)
        {
            List<ApplicationUser> entityResult = appUserManager.GetByUserIds(userIds);
            List<ApplicationUserViewModel> result = mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return result;
        }

        [Route("GetTechniciansExceptUserIds")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<List<ApplicationUserViewModel>> GetTechniciansExceptUserIds([FromBody]List<string> userIds)
        {
            List<ApplicationUser> entityResult = await appUserManager.GetByRoleExcept(appSettings.TechnicianRole, userIds);
            List<ApplicationUserViewModel> result = mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return result;
        }

        [Route("GetAllExceptUserIds")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public List<ApplicationUserViewModel> GetByRoleExcept([FromBody]List<string> userIds)
        {
            List<ApplicationUser> entityResult = appUserManager.GetAllExcept(userIds);
            List<ApplicationUserViewModel> result = mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return result;
        }

        [Route("GetTechnicians")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetTechnicians()
        {
            List<ApplicationUserViewModel> result = new List<ApplicationUserViewModel>();
            List<ApplicationUser> entityResult = new List<ApplicationUser>();
            entityResult = await appUserManager.GetByRoleExcept(appSettings.TechnicianRole);
            result = mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return Ok(result);
        }

        [Route("GetDispatchers")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetDispatchers()
        {
            List<ApplicationUserViewModel> result = new List<ApplicationUserViewModel>();
            List<ApplicationUser> entityResult = new List<ApplicationUser>();
            entityResult = await appUserManager.GetByRoleExcept(appSettings.DispatcherRole);
            result = mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return Ok(result);
        }

        [Route("GetDispatcherSupervisor")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetDispatcherSupervisor()
        {
            List<ApplicationUserViewModel> result = new List<ApplicationUserViewModel>();
            List<ApplicationUser> entityResult = new List<ApplicationUser>();
            entityResult = await appUserManager.GetByRoleExcept(appSettings.SupervisorDispatcherRole);
            result = mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        [HttpGet, Route("UserRoles/{username}")]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<ApplicationUserViewModel> UserRoles([FromRoute]string username, string authHeader = null)
        {
            ApplicationUserViewModel result = null;

            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }

            if (!string.IsNullOrEmpty(username) && !string.IsNullOrWhiteSpace(username))
            {
                var user = await appUserManager.GetByUserNameAsync(username);

                if (user != null)
                {
                    user.RoleNames = (await appUserManager.GetRolesAsync(username)).ToList();
                    result = mapper.Map<ApplicationUser, ApplicationUserViewModel>(user);
                }

            }

            return result;
        }


        #region PostApi
        [HttpPost, Route("Add")]
        public async Task<IActionResult> Add([FromBody] ApplicationUserViewModel model)
        {
            IActionResult result = BadRequest();
            if (ModelState.IsValid)
            {
                var entityResult = new ApplicationUser();
                entityResult = mapper.Map<ApplicationUserViewModel, ApplicationUser>(model);
                entityResult = await appUserManager.AddUserAsync(entityResult, model.Password);
                model = mapper.Map<ApplicationUser, ApplicationUserViewModel>(entityResult);

                if (model != null)
                {
                    result = Ok(model);
                }
            }

            return result;
        }
        #endregion


        [AllowAnonymous]
        [HttpPost]
        [Route("Update")]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]

        public async Task<IActionResult> Update([FromBody] EditApplicationUserViewModel model)
        {
            bool result = false;
            if (ModelState.IsValid)
            {
                var entityResult = mapper.Map<EditApplicationUserViewModel, ApplicationUser>(model);
                result = await appUserManager.UpdateUserAsync(entityResult);
            }
            return Ok(result);
        }


        #region Update User Password

        [HttpPut, Route("UpdateUserPassword")]
        public async Task<ResponseResult<bool>> UpdateUserPassword([FromBody] UserChangePasswordViewModel userViewModel)
        {
            var responseResult = new ResponseResult<bool>();

            try
            {
                string userId = userViewModel.Id;
                string newPassword = userViewModel.Password;
                string modifiedByUserId = userViewModel.ModifiedByUserId;

                bool isUserPasswordUpdated = await UpdateUserPassword(userId, newPassword, modifiedByUserId);

                responseResult.Data = isUserPasswordUpdated;
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 0;
                responseResult.Message = ex.Message;
            }

            return responseResult;
        }

        private async Task<bool> UpdateUserPassword(string userId, string newPassword, string modifiedByUserId)
        {
            ApplicationUser applicationUser = await appUserManager.GetByUserIdAsync(userId);

            if (applicationUser == null)
            {
                return false;
            }

            applicationUser.PasswordHash = appUserManager.HashPassword(applicationUser, newPassword);
            applicationUser.FK_UpdatedBy_Id = modifiedByUserId;
            applicationUser.UpdatedDate = DateTime.Now;

            IdentityResult identityResult = await appUserManager.UpdateAsync(applicationUser);

            return identityResult.Succeeded;
        }

        #endregion Update User Password


        #region DeleteApi
        [Route("Delete/{username}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]string username)
        {
            bool result = false;
            var entity = await appUserManager.GetByUserNameAsync(username);
            result = await appUserManager.DeleteAsync(entity);
            return Ok(result);
        }
        #endregion

        #endregion

        [HttpGet]
        [Route("UsernameExists/{username}")]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]

        public async Task<IActionResult> UsernameExists([FromRoute]string username)
        {
            bool result = await appUserManager.IsUserNameExistAsync(username);
            return Ok(result);
        }

        [HttpGet]
        [Route("EmailExists/{email}")]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> EmailExists([FromRoute]string email)
        {
            bool result = await appUserManager.IsEmailExistAsync(email);
            return Ok(result);
        }

        [HttpPost]
        [Route("AssignUserToRole")]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> AssignUserToRole([FromBody]AssignRoleViewModel assignRoleViewModel)
        {

            bool result = await appUserManager.AddUserToRoleAsync(assignRoleViewModel.UserName, assignRoleViewModel.RoleName);
            return Ok(result);
        }

        [HttpPost]
        [Route("ChangeUserAvailability")]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<bool> ChangeUserAvailability([FromBody]UserAvailableViewModel userAvailableViewModel)
        {
            bool result = false;
            result = await appUserManager.ChangeUserAvailabiltyAsync(userAvailableViewModel.Id, userAvailableViewModel.IsAvailable);
            return result;
        }

        [HttpPost]
        [Route("ImportTechFromFile"), AllowAnonymous]
        public async Task<IActionResult> ImportTechFromFile([FromBody]UploadFile file)
        {
            var fileExt = StringUtilities.GetFileExtension(file.FileContent);
            file.FileName = $"Tech_{DateTime.Now.Ticks}.{fileExt}";
            UploadExcelFileManager fileManager = new UploadExcelFileManager();
            var path = $@"{_hostingEnv.WebRootPath}\Import\";
            var addFileResult = fileManager.AddFile(file, path);
            var result = await AddFromFile($@"{addFileResult.returnData}\{file.FileName}");
            return Ok(result);
        }

        private async Task<List<ApplicationUserViewModel>> AddFromFile(string filePath)
        {
            var users = new List<ApplicationUserViewModel>();
            var models = ExcelReader.GetDataToList(filePath, GetItems);

            foreach (var item in models)
            {
                var userData = new ApplicationUserViewModel
                {
                    UserName = item.UserName,
                    PhoneNumber = item.PhoneNumber,
                    LastName = item.LastName,
                    MiddleName = "",
                    Password = item.Password,
                    FirstName = item.FirstName,
                    Email = item.Email,
                    RoleNames = new List<string> { "Technician" }
                };
                var entityResult = new ApplicationUser();
                entityResult = mapper.Map<ApplicationUserViewModel, ApplicationUser>(userData);
                entityResult = await appUserManager.AddUserAsync(entityResult, userData.Password);
                userData = mapper.Map<ApplicationUser, ApplicationUserViewModel>(entityResult);
                users.Add(userData);
            }
            return users;
        }

        private TechFromFileViewModel GetItems(IList<string> rowData, IList<string> columnNames)
        {
            var model = new TechFromFileViewModel();

            var tech = new TechFromFileViewModel()
            {
                Email = rowData[columnNames.IndexFor(nameof(model.Email))],
                FirstName = rowData[columnNames.IndexFor(nameof(model.FirstName))],
                LastName = rowData[columnNames.IndexFor(nameof(model.LastName))],
                Password = rowData[columnNames.IndexFor(nameof(model.Password))],
                PhoneNumber = rowData[columnNames.IndexFor(nameof(model.PhoneNumber))],
                UserName = rowData[columnNames.IndexFor(nameof(model.UserName))]
            };
            return tech;
        }

        public void LogResult(string resultMessage, string excelFileName)
        {
            string date = DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year;
            string dir = $@"{_hostingEnv.WebRootPath}\Logs";

            DirectoryInfo dirInfo = new DirectoryInfo(dir);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }
            string path = dir + @"\" + "Request_Log_" + excelFileName + ".txt";
            if (!System.IO.File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = System.IO.File.CreateText(path))
                {
                    sw.WriteLine(resultMessage);
                }
            }
            else
            {
                using (StreamWriter sw = System.IO.File.AppendText(path))
                {
                    sw.WriteLine(resultMessage);
                }
            }
        }
    }
}