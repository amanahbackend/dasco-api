﻿using AutoMapper;
using DispatchProduct.Identity.Entities;
using DispatchProduct.Identity.Models.Entities;

namespace DispatchProduct.UserManagment.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<RolePrivilgeViewModel, RolePrivilge>()
              .ForMember(dest => dest.Privilge, opt => opt.MapFrom(src => src.Privilge))
              .ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.Role));
            CreateMap<RolePrivilge, RolePrivilgeViewModel>()
              .ForMember(dest => dest.Privilge, opt => opt.MapFrom(src => src.Privilge))
              .ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.Role));

            CreateMap<PrivilgeViewModel, Privilge>()
             .ForMember(dest => dest.Roles, opt => opt.MapFrom(src => src.Roles));
            CreateMap<Privilge, PrivilgeViewModel>()
              .ForMember(dest => dest.Roles, opt => opt.MapFrom(src => src.Roles));
            CreateMap<ApplicationRoleViewModel, ApplicationRole>()
              .ForMember(dest => dest.NormalizedName, opt => opt.Ignore())
              .ForMember(dest => dest.Privilges, opt => opt.MapFrom(src => src.Privilges))
              .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore());
            CreateMap<ApplicationRole, ApplicationRoleViewModel>()
              .ForMember(dest => dest.Privilges, opt => opt.MapFrom(src=>src.Privilges));

            CreateMap<ApplicationUserViewModel, ApplicationUser>()
                 .ForMember(dest => dest.NormalizedUserName, opt => opt.Ignore())
                 .ForMember(dest => dest.NormalizedEmail, opt => opt.Ignore())
                 .ForMember(dest => dest.PasswordHash, opt => opt.Ignore())
                 .ForMember(dest => dest.SecurityStamp, opt => opt.Ignore())
                 .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore())
                 .ForMember(dest => dest.TwoFactorEnabled, opt => opt.Ignore())
                 .ForMember(dest => dest.LockoutEnd, opt => opt.Ignore())
                 .ForMember(dest => dest.LockoutEnabled, opt => opt.Ignore())
                 .ForMember(dest => dest.AccessFailedCount, opt => opt.Ignore());
            CreateMap<ApplicationUser, ApplicationUserViewModel>()
            .ForMember(dest => dest.Password, opt => opt.Ignore());

            CreateMap<EditApplicationUserViewModel, ApplicationUser>()
                 .ForMember(dest => dest.NormalizedUserName, opt => opt.Ignore())
                 .ForMember(dest => dest.NormalizedEmail, opt => opt.Ignore())
                 .ForMember(dest => dest.PasswordHash, opt => opt.Ignore())
                 .ForMember(dest => dest.SecurityStamp, opt => opt.Ignore())
                 .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore())
                 .ForMember(dest => dest.TwoFactorEnabled, opt => opt.Ignore())
                 .ForMember(dest => dest.LockoutEnd, opt => opt.Ignore())
                 .ForMember(dest => dest.LockoutEnabled, opt => opt.Ignore())
                 .ForMember(dest => dest.AccessFailedCount, opt => opt.Ignore());

            CreateMap<ApplicationUser,EditApplicationUserViewModel> ();
        



        }
    }
}
