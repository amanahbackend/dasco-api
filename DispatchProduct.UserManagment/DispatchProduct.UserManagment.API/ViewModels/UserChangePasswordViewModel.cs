﻿namespace DispatchProduct.UserManagment.API.ViewModels
{
    public class UserChangePasswordViewModel
    {
        public string Id { get; set; }

        public string Password { get; set; }

        public string ModifiedByUserId { get; set; }
    }
}
