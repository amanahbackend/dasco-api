﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.UserManagment.API;

namespace DispatchProduct.Identity.Models.Entities
{
    public class RolePrivilgeViewModel : BaseEntity
    {
        public int Id { get; set; }
        public int FK_Privilge_Id { get; set; }
        public PrivilgeViewModel Privilge { get; set; }
        public string FK_Role_Id { get; set; }
        public ApplicationRoleViewModel Role { get; set; }
    }
}
