﻿namespace DispatchProduct.UserManagment.API
{
    public class AssignRoleViewModel
    {
        public string UserName { get; set; }
        public string RoleName { get; set; }
    }
}
