﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using DispatchProduct.UserManagment.API;
using System.Collections.Generic;

namespace DispatchProduct.Identity.Models.Entities
{
    public class PrivilgeViewModel : BaseEntity, IPrivilge
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<ApplicationRoleViewModel> Roles { get; set; }
    }
}
