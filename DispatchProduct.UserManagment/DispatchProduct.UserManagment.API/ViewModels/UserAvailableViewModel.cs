﻿namespace DispatchProduct.UserManagment.API
{
    public class UserAvailableViewModel
    {
        
        public string Id { get; set; }

        public bool IsAvailable { get; set; }
    }
}
