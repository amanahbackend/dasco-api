﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using DispatchProduct.Identity.BLL.IManagers;
using DispatchProduct.Identity.BLL.Managers;
using DispatchProduct.UserManagment.API.Controllers;
using System.IdentityModel.Tokens.Jwt;
using AutoMapper;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using IdentityServer4.AccessTokenValidation;
using DispatchProduct.Identity.Context;
using Microsoft.EntityFrameworkCore;
using DispatchProduct.Identity.Entities;
using DispatchProduct.Identity.Settings;
using DispatchProduct.Identity.Models.Entities;

namespace DispatchProduct.UserManagment.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });

            services.AddDbContext<ApplicationDbContext>(options =>
            options.UseSqlServer(Configuration["ConnectionString"]));
            services.AddOptions();
            services.AddMvc();


            services.Configure<AppSettings>(Configuration);
            services.AddAutoMapper(typeof(Startup));
            Mapper.AssertConfigurationIsValid();

            services.AddIdentity<ApplicationUser, ApplicationRole>()
                 .AddEntityFrameworkStores<ApplicationDbContext>();

            ConfigureAuthService(services);
            services.AddScoped<DbContext, ApplicationDbContext>();
            services.AddScoped(typeof(IApplicationRole), typeof(ApplicationRole));
            services.AddScoped(typeof(IRolePrivilge), typeof(RolePrivilge));
            services.AddScoped(typeof(IPrivilge), typeof(Privilge));
            services.AddScoped(typeof(IApplicationRoleManager), typeof(ApplicationRoleManager));
            services.AddScoped(typeof(IApplicationUserManager), typeof(ApplicationUserManager));
            services.AddScoped(typeof(IRolePrivilgeManager), typeof(RolePrivilgeManager));
            services.AddScoped(typeof(IPrivilgeManager), typeof(PrivilgeManager));
            services.AddScoped(typeof(UserController), typeof(UserController));
            var container = new ContainerBuilder();
            container.Populate(services);

            return new AutofacServiceProvider(container.Build());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCors("AllowAll");

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            var pathBase = Configuration["PATH_BASE"];
            if (!string.IsNullOrEmpty(pathBase))
            {
                loggerFactory.CreateLogger("init").LogDebug($"Using PATH BASE '{pathBase}'");
                app.UsePathBase(pathBase);
            }
            ConfigureAuth(app);
            app.UseStaticFiles();
            // Make work identity server redirections in Edge and lastest versions of browers. WARN: Not valid in a production environment.
            //app.Use(async (context, next) =>
            //{
            //    context.Response.Headers.Add("Content-Security-Policy", "script-src 'unsafe-inline'");
            //    await next();
            //});

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
        private void ConfigureAuthService(IServiceCollection services)
        {
            // prevent from mapping "sub" claim to nameidentifier.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            var identityUrl = Configuration.GetValue<string>("IdentityUrl");
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
            .AddIdentityServerAuthentication(options =>
            {
                // base-address of your identityserver
                options.Authority = identityUrl;

                // name of the API resource
                options.ApiName = Configuration["ClientId"];
                options.ApiSecret = Configuration["Secret"];
                options.RequireHttpsMetadata = false;
                options.EnableCaching = true;
                options.CacheDuration = TimeSpan.FromMinutes(10);
                options.SaveToken = true;
            });

            //services.AddAuthorization(options =>
            //{
            //    // Policy for dashboard: only administrator role.
            //    options.AddPolicy("AdminRole", policy => policy.RequireClaim("role", "Admin"));
            //    // Policy for resources: user or administrator role. 
            //    options.AddPolicy("GlobalRole", policyBuilder => policyBuilder.RequireAssertion(
            //            context => context.User.HasClaim(claim => (claim.Type == "role" && claim.Value == "user")
            //               || (claim.Type == "role" && claim.Value == "Admin"))
            //        )
            //    );
            //});
        }

        protected virtual void ConfigureAuth(IApplicationBuilder app)
        {
            app.UseAuthentication();
        }
    }
}
