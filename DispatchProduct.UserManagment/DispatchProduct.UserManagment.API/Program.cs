﻿using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;

namespace DispatchProduct.UserManagment.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
          WebHost.CreateDefaultBuilder(args)
              .UseKestrel()
              .UseContentRoot(Directory.GetCurrentDirectory())
              .UseIISIntegration()
              .UseStartup<Startup>()
              .ConfigureLogging((hostingContext, builder) =>
              {
                  builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                  builder.AddConsole();
                  builder.AddDebug();
              })
              .Build();
    }
}
